<?php
/**
 * Application main configuration file.
 * Contains parameters detection, some preparations
 * and application main configuration.
 */

/**
 * Some prepares
 */
/**
 * Singular flag for translation
 * @var int
 */
define("SINGULAR", 1);
/**
 * Plural flag for translation
 * @var int
 */
define("PLURAL", 1.1);
// Upload folder alias
Yii::setPathOfAlias('uploads', 'uploads');

// Yii app not started yet so start native php session for some parameters
if( ! isset($_SESSION))
{
	session_start();
}
/**
 * Available application languages
 * @var array
 */
$availableLanguages = array('ru', 'en');
/**
 * Current application language. Gets from server accept language variable
 * @var string
 */
$language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
// Check for stored language
if(isset($_COOKIE['language']) && ! empty($_COOKIE['language']))
{
	// Get stored language
	$language = $_COOKIE['language'];
}
else if(isset($_SESSION['language']) && ! empty($_SESSION['language']))
{
	// Get stored language
	$language = $_SESSION['language'];
}


/**
 * This is the main Web application configuration. Any writable
 * CWebApplication properties can be configured here.
 */
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Maritime CV',
	'defaultController'=>'site',
	'sourceLanguage'=>'',
    'timeZone' => 'Europe/Kiev',
	'language' => $language,
	// preloading 'log' component
	'preload'=>array('log'),
	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*'
	),
	'modules'=>array(		
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'@secret',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1')
		),
		'admin'
	),
	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true
		),		
		'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName'=>false,
			'caseSensitive'=>false,
			'rules'=>array(
				'gii/<controller:\w+>/<action:\w+>'=>'gii/<controller>/<action>',
				'admin/<controller:\w+>/<action:\w+>'=>'admin/<controller>/<action>',
				'admin/login'=>'admin/default/login',
				'admin/logout'=>'admin/default/logout',
                'contact'=>'site/contact',
				'admin/triggeremailing'=>'admin/default/triggeremailing',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>(/<value:\w+>)'=>'<controller>/<action>',
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>'
			),
		),
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=maritim8_cv',
			'emulatePrepare' => true,
			'username' => 'maritim8_dev',
			'password' => 'sHeehwe5ajwIxLagPymJ',
			'charset' => 'utf8',
			'autoConnect'=>false
		),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error'
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'logFile' => 'application.log',
					'levels'=>'error, warning, info',
					// Log additional data as session, server variables etc
					//'filter' => 'CLogFilter'
				),
				array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'trace',
                    'categories'=>'system.db.*',
                    'logFile'=>'sql.log',
                )
			)
		),
		'messages' => array(
			'class' => 'CDbMessageSource'
		),
		'Settings' => array(
			'class' => 'DbSettings'
		),
		'Str' => array(
			'class' => 'Str'
		),
		'Email' => array(
			'class' => 'Email'
		),
		'File' => array(
			'class' => 'File'
		)
	),
	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'demnarctis@gmail.com',
		'email'=>array(
			'SMTP'=>true,
			'SMTPAuth'=>true,			
			'SMTPSecure'=>'ssl',
			'host'=>'smtp.gmail.com',
			'port'=>465,
			'user'=>'laspiairlines@gmail.com',
			'password'=>'S1332jJK',
			'charset'=>'UTF-8'
		),
		'interkassa' => array(
			'shopId' => '538ec854bf4efc6719507933', 
			'secretKey' => 'IcqdLHLZtg8rR8Gf'// It is the test key. A key for the real pays is 2UAPIkRQJ62Fdmf7
		),
		'contactEmailSubject'=>'Contact Email',
		'availableLanguages' => $availableLanguages,
		// A price for the notification of companies (all from user's order)
		'companyNotificationPrice' => 4,
		// The sendings maximum per hour, excluding the important sendings.
		'sendingsPerHour' => 5,
		// The important sendings maximum per hour.
		'importantSendingsPerHour' => 2
	)
);