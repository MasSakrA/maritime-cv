<?php

return array(
    'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
    'name'=>'Maritime CV Console Application',
    'language' => 'ru',
    'timeZone' => 'Europe/Kiev',

    'components'=>array(
        'db'=>array(
            'connectionString' => 'mysql:host=localhost;dbname=maritim8_cv',
            'emulatePrepare' => true,
            'username' => 'maritim8_dev',
            'password' => 'sHeehwe5ajwIxLagPymJ',
            'charset' => 'utf8',
            'autoConnect'=>false
        ),
        'errorHandler'=>array(
            // use 'site/error' action to display errors
            'errorAction'=>'site/error'
        ),
        'messages' => array(
            'class' => 'CDbMessageSource'
        ),
        'Str' => array(
            'class' => 'Str'
        ),
        'File' => array(
            'class' => 'File'
        ),
        'Email' => array(
            'class' => 'Email'
        ),
        'Settings' => array(
            'class' => 'DbSettings'
        ),
    ),
    'import'=>array(
        'application.commands.*',
        'application.components.*',
        'application.models.*'
    ),
    'params'=>array(
        // this is used in contact page
        'adminEmail'=>'demnarctis@gmail.com',
        'email'=>array(
            'SMTP'=>true,
            'SMTPAuth'=>true,
            'SMTPSecure'=>'ssl',
            'host'=>'smtp.gmail.com',
            'port'=>465,
            'user'=>'laspiairlines@gmail.com',
            'password'=>'S1332jJK',
            'charset'=>'UTF-8'
        ),
        'contactEmailSubject'=>'Contact Email',
        'availableLanguages' => $availableLanguages,
        // A price for the notification of companies (all from user's order)
        'companyNotificationPrice' => 4,
        // The sendings maximum per hour, excluding the important sendings.
        'sendingsPerHour' => 5,
        // The important sendings maximum per hour.
        'importantSendingsPerHour' => 2
    )
);