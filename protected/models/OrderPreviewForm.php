<?php
/**
 * OrderPreviewForm class.
 */
class OrderPreviewForm extends CFormModel
{
	/**
	 * User email
	 * @var string 
	 */
	public $email = '';
	/**
	 * Resume file name
	 * @var string 
	 */
	public $CVFile = '';
	/**
	 * Attachment file name
	 * @var string 
	 */
	public $attachment1 = '';
	/**
	 * Attachment file name
	 * @var string 
	 */
	public $attachment2 = '';
	/**
	 * Resume file name as pseudonym
	 * @var string 
	 */
	public $CVFileName = 'Резюме';
	/**
	 * Attachment file name as pseudonym
	 * @var string 
	 */
	public $attachment1Name = 'Приложение 1';
	/**
	 * Attachment file name as pseudonym
	 * @var string 
	 */
	public $attachment2Name = 'Приложение 2';	
	/**
	 * Email subject
	 * @var string 
	 */
	public $subject = '';
	/**
	 * Email body
	 * @var string 
	 */
	public $body = '';
	/**
	 * Selected company ids
	 * @var string 
	 */
	public $selectedCompanyIds = '';
	/**
	 * All company list
	 * @var array 
	 */
	public $companyList = array();
	/**
	 * The selected color scheme
	 * @var string
	 */
	public $colorScheme = '';

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			// Required fields
			array('CVFileName, attachment1Name, attachment2Name', 'required')
		);
	}
	
	/**
	 * Method collects previous steps data
	 */
	public function init()
	{
		/*
		 * First get all stored data
		 */
		// Get files step form data
		$filesForm = Yii::app()->session->get('OrderFilesForm');
		// Get content step form data
		$contentForm = Yii::app()->session->get('OrderContentForm');
		// Get companies selecting step form data
		$companiesForm = Yii::app()->session->get('OrderCompaniesForm');
		
		/*
		 * Initialize self properties by stored data
		 */
		// User email
		$this->email = $filesForm['email'];
		// Files
		$this->CVFile = $filesForm['CVFile'];
		$attachment1Exists = false;
		if(isset($filesForm['attachment1']))
		{
			$this->attachment1 = $filesForm['attachment1'];
		}
		if(isset($filesForm['attachment2']))
		{
			$this->attachment2 = $filesForm['attachment2'];
		}			
		// Email data
		$this->subject = $contentForm['subject'];
		$this->body = $contentForm['body'];		
		$this->colorScheme = $contentForm['colorTemplate'];
		// Selected companies
		$this->selectedCompanyIds = $companiesForm['companies'];
		
		// Load all companies
		$orderCompaniesFormModel = new OrderCompaniesForm;
		$orderCompaniesFormModel->LoadCompanies();
		$this->companyList = $orderCompaniesFormModel->companiesList;
		
	}
	
	/**
	 * Method save stored session data to database
	 */
	public function SaveUserDataToDatabase()
	{
		/**
		 * User creation status
		 * @var bool
		 */
		$userCreated = false;
		// Get user
		$User = $this->GetUser();
		// If the user already exists
		if( ! $User->getIsNewRecord())
		{
			// Regenerate user id (session id)
			Yii::app()->session->regenerateID();	
			$User = new User;
		}
		// Set user data
		$User->email = $this->email;
		$User->identificator = Yii::app()->session->getSessionID();
		// Save new user
		if($User->save())
		{
			// Remember user in session
			Yii::app()->session->add('user', $User->attributes);
			$userCreated = true;
		}
		
		// Save user files
		$this->SaveUserFilesToDatabase($User);
		
		// Save user email content
		$this->SaveUserEmailContentToDatabase($User);
		
		// Save selected companies
		$this->SaveUserSeletedComaniesToDatabase($User);
		
		return $userCreated;
	}
	
	/**
	 * Method saves user's files data to database
	 * @param User $User User data object
	 */
	private function SaveUserFilesToDatabase($User)
	{
		// Create user active recordfile object
		$UserFile = new UserFile;
		// Set user if for all files
		$UserFile->user = $User->id;
		
		/*
		 * Save CV file
		 */
		
		// Set CV file data		
		$UserFile->category = 'CVFile';
		$UserFile->name = $this->CVFile;
		$UserFile->pseudonym = $this->CVFileName;
		// Save file data
		$UserFile->save();
		
		/*
		 * Save attachment 1
		 */
		
		// Set user file object as new record
		$UserFile->setIsNewRecord(true);
		// Reset id
		$UserFile->id = null;
		// Override file data by attachment1 file data		
		$UserFile->category = 'attachment1';
		$UserFile->name = $this->attachment1;
		$UserFile->pseudonym = $this->attachment1Name;
		// Save file data
		$UserFile->save();
		
		/*
		 * Save attachment 2
		 */
		
		// Set user file object as new record
		$UserFile->setIsNewRecord(true);
		// Reset id
		$UserFile->id = null;
		// Override file data by attachment2 file data
		$UserFile->category = 'attachment2';
		$UserFile->name = $this->attachment2;
		$UserFile->pseudonym = $this->attachment2Name;
		// Save file data
		$UserFile->save();		
	}
	
	/**
	 * Method saves user's email content to database
	 * @param User $User User data object
	 */
	private function SaveUserEmailContentToDatabase($User)
	{
		// Create user active record file object
		$UserEmailContent = new UserEmailContent;
		// Set ueser id
		$UserEmailContent->user = $User->id;
		// Set email data
		$UserEmailContent->subject = $this->subject;
		$UserEmailContent->body = $this->body;		
		$UserEmailContent->colorScheme = str_replace('cs-', '', $this->colorScheme);
		// Save user email
		$UserEmailContent->save();
	}
	
	/**
	 * Method saves user's selected to order companies to database
	 * @param User $User User data object
	 */
	private function SaveUserSeletedComaniesToDatabase($User)
	{
		// Create user active record file object
		$UserOrderedCompanies = new UserOrderedCompanies;
		// Set ueser id
		$UserOrderedCompanies->user = $User->id;
		// Set company ids as string separated by comma
		$UserOrderedCompanies->companies = implode(',', $this->selectedCompanyIds);
		// Save companies
		$UserOrderedCompanies->save();
	}

	/**
	 * Method check for user existing and return if exists
	 * or create new user if not
	 */
	public function GetUser()
	{
		// Create criteria condition
		$Criteria = new CDbCriteria;
		$Criteria->condition = 'identificator=:userIdentify';
		$Criteria->params = array(':userIdentify' => Yii::app()->session->getSessionID());
		// Try to get existing user
		$User = User::model()->find($Criteria);
		// If user not exists
		if(is_null($User))
		{
			// Create new user
			$User = new User;
		}
		return $User;
	}
}