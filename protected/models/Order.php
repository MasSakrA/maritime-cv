<?php

/**
 * This is the model class for table "orders".
 *
 * The followings are the available columns in table 'orders':
 * @property integer $id
 * @property integer $user
 * @property string $number
 * @property string $creationDate
 * @property float $amount
 * @property string $currency
 * @property integer $paid
 * @property integer $executed
 * @property string[] $OrderedSectors User ordered companies sectors codes. Used for cache.
 *
 * The followings are the available model relations:
 * @property User $User
 */
class Order extends CActiveRecord
{
	public $OrderedSectors;
	/**
	 * Ordered companies count. Store companies count after
	 * calling method to get it.
	 * @var int
	 */
	public $orderedCompaniesCount;
	/**
	 * Order description. Used for interkassa
	 * @var string
	 */
	public $description = 'Companies emailing order';
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orders';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user, number, creationDate, amount, paid, executed', 'required'),
			array('user, paid', 'numerical', 'integerOnly'=>true),
			array('number', 'length', 'max'=>100),
			array('amount', 'length', 'max'=>10),
			array('currency', 'length', 'max'=>3),
			array('paid, executed', 'length', 'max'=>1),
			array('orderedCompaniesCount', 'safe'),
			// The following rule is used by search().
			array('id, user, number, creationDate, amount, paid, executed', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'User' => array(self::BELONGS_TO, 'User', 'user')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('word', 'id'),
			'user' => Yii::t('word', 'User', SINGULAR),
			'number' => Yii::t('word', 'number'),
			'creationDate' => Yii::t('phrase', 'CreatonDate'),
			'amount' => Yii::t('word', 'amount'),
			'currency' => Yii::t('word', 'Currency'),
			'paid' => Yii::t('word', 'paid'),
			'executed' => Yii::t('word', 'Executed')
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{

		$criteria = new CDbCriteria;
		$criteria->with = array('User');

		$criteria->compare('t.id', $this->id);
		$criteria->compare('User.email', $this->user, true);
		$criteria->compare('number', $this->number, true);
		$criteria->compare('DATE_FORMAT(creationDate, "%d.%m.%Y")', $this->creationDate, true);
		
		$criteria->compare('amount', $this->amount, true);
		$criteria->compare('currency', $this->currency, true);
		$criteria->compare('paid', $this->paid,true);
		$criteria->compare('executed', $this->executed, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort' => array(
				'attributes' => array(
					'user' => array(
						'asc' => 'User.email',
						'desc' => 'User.email DESC'
					),
					'*'
				)
			),			
			'pagination'=>array(
					'pageSize' => Yii::app()->Settings->Get('rowsPerPage', 'adminPanel')
			)
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Order the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * Generates unique order number
	 * @return string 
	 */
	public function GenerateNumber()
	{
		/**
		 * Flag for order non unique
		 * @var bool
		 */
		$orderNotUnique = true;
		// Generate unique number
		while($orderNotUnique)
		{
			/**
			 * Random integer prefix the three symbols of length
			 * @var int
			 */
			$prefix = mt_rand(100, 999);
			/**
			 * Current timestamp
			 * @var string
			 */
			$curentTimeStamp = strtotime('now');
			$this->number = $prefix . $curentTimeStamp;			
			/**
			 * Condition
			 * @var CDbCriteria
			 */
			$Criteria = new CDbCriteria;
			$Criteria->condition = 'number=:number';
			$Criteria->params = array(':number' => $this->number);
			/**
			 * Order from database. Null if not exists.
			 * @var CActiveRecord
			 */
			$Order = Order::model()->find($Criteria);
			// Check for unique
			if(is_null($Order))
			{
				$orderNotUnique = false;
			}
		}		
		// Return number
		return $this->number;
	}
	
	/** 
	 * Calculates payment amount depending on ordered sectors and sector's prices
	 * @return float Amount of payment
	 */
	public function CalculateAmount()
	{
		// Get ordered sectors
		$orderedSectors = $this->GetOrderedSectors();
		// Reset amount
		$this->amount = 0.00;
		// Calculate new amount
		foreach($orderedSectors as $sectorCode)
		{
			$this->amount += floatval(Yii::app()->Settings->Get($sectorCode, 'price'));
		}
		// Check for minimum amount value.
		$this->amount = (floor($this->amount) < Yii::app()->Settings->Get('minimum', 'price')) ? 
			Yii::app()->Settings->Get('minimum', 'price') 
			: $this->amount;
		// Return amount
		return $this->amount;
	}
	
	/**
	 * @return string[] Ordered companies sectors.
	 */
	public function GetOrderedSectors()
	{
		// Check cache.
		if(empty($this->OrderedSectors))
		{
			/**
			 * Get ordered companies.
			 * 
			 * If the order is new then it is don't has user relation yet,
			 * then use session data. In other case the order already has 
			 * been created then use user relation object.
			 * 
			 * @var string[] $orderedCompanies User ordered companies ids.
			 * @var string[] $storedCompaniesForm The selected companies form stored in session.
			 */
			if($this->isNewRecord)
			{
				// Get the companies from stored order companies form.
				$storedCompaniesForm = Yii::app()->session->get('OrderCompaniesForm');				
				$orderedCompanies = $storedCompaniesForm['companies'];
			}
			else
			{
				// Get the companies from user and his ordered companies relations.
				$orderedCompanies = explode(',', $this->User->OrderedCompanies->companies);
			}
			
			/**
			 * Get ordered sectors.
			 */
			$this->OrderedSectors = Yii::app()->db->createCommand()
				->selectDistinct('workArea')
				->from('offices')
				->leftJoin('officeworkareas', 'officeworkareas.office = offices.id')
				->where(array('in', 'offices.id', $orderedCompanies))
				->queryColumn();
		}
		// Return data.
		return $this->OrderedSectors;
	}
	
	/**
	 * Gets and cache count of ordered companies
	 * @return ordered company count
	 */
	public function GetOrderedCompaniesCount()
	{
		// Try to return cached value.
		if( ! empty($this->orderedCompaniesCount))
		{
			return $this->orderedCompaniesCount;
		}
		/**
		 * Current user info
		 * @var array
		 */
		$user = Yii::app()->session->get('user');
		
		/*
		 *  Get ordered companies count
		 */
		// Query data from database
		$orderedCompanies = Yii::app()->db->createCommand()
			->select('companies')
			->from('userOrderedCompanies')
			->where('user = :user', array('user' => $user['id']))
			->queryRow();
		// Check database result
		if($orderedCompanies)
		{
			$this->orderedCompaniesCount = count(explode(',', $orderedCompanies['companies']));
		}
		else
		{
			$this->orderedCompaniesCount = 0;
		}
		// Return count
		return $this->orderedCompaniesCount;
	}

	/**
	 * @return int Mailed companies
	 */
	public function GetMailedCompanies()
	{
		return Yii::app()->db->createCommand()
			->select('companies.name, datetimeOut as mailedDatetime, emailQueue.id')
			->from('emailQueue')
			->join('emailQueueParams', 'emailQueueParams.emailsQueueId = emailQueue.queueId')
			->join('offices', 'offices.email = emailQueue.address')
			->join('companies', 'offices.company = companies.id')
			->where('
				-- Select rows for current user
				emailQueueParams.name = "user"
				AND emailQueueParams.value = :userId
				-- And which already not in queue
				AND inQueue = 0',
				array(':userId' => $this->User->id))
			->queryAll();
	}
	
	/**
	 * Method to restore order dataif it exists in session
	 * @return bool Existing order flag
	 */
	public function Restore()
	{
		/**
		 * Existing order data (active user order)
		 * @var array
		 */
		$storedOrder = Yii::app()->session->get('order');
		// If order data exists then get it
		if( ! is_null($storedOrder))
		{
			$this->attributes = $storedOrder;
			$this->orderedCompaniesCount = $storedOrder['orderedCompaniesCount'];
			// Stored order exists
			return true;
		}
		else
		{
			// Stored order not exists
			return false;
		}
	}
	
	/**
	 * Order executing.
	 * 
	 * Runs sending emails to the ordered companies
	 * @return void
	 */
	public function Execute()
	{
		/**
		 * Get companies emails.
		 * 
		 * @var Company[] $companies		A list of ordered companies.
		 * @var string[] $emails			A list of companies emails.	
		 */ 
		$companies = $this->User->OrderedCompanies->GetOffices();
		$emails = array();
		foreach($companies as $Company)
		{
			$emails[] = $Company->email;
		}
		
		/**
		 * Create template and its parameters.
		 * 
		 * @var Emailtemplait $Template			The template object.
		 * @var string[] $templateParameters	The parameters for template.
		 */
		$Template = Emailtemplate::model()->findByAttributes(array('name' => 'company-notify'));
		$templateParameters = array('user' => $this->User->id);
		
		/**
		 * Add companies emails to queue
		 */
		Yii::app()->Email->Queue(array(
			'emails' => $emails,
			'emailsTemplate' => $Template->id,
			'templateParameters' => $templateParameters
		));
		// Order is executed
		$this->executed = 1;
		$this->save();
	}
}
