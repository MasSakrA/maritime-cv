<?php

/**
 * This is the model class for table "emailQueue".
 *
 * The followings are the available columns in table 'emailQueue':
 * @property integer $id
 * @property integer $queueId
 * @property integer $emailTemplate
 * @property string $address
 * @property string $datetimeIn
 * @property string $datetimeOut
 * @property integer $inQueue
 *
 * The followings are the available model relations:
 * @property Emailtemplate $EmailTemplateDetails
 */
class EmailQueue extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'emailQueue';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, address, datetimeIn', 'required'),
			array('id, queueId, emailTemplate, inQueue', 'numerical', 'integerOnly'=>true),
			array('address', 'length', 'max'=>255),
			array('datetimeOut', 'safe'),
			// The following rule is used by search().
			array('id, queueId, emailTemplate, address, datetimeIn, datetimeOut, inQueue', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'EmailTemplateDetails' => array(self::BELONGS_TO, 'Emailtemplate', 'emailTemplate'),
			'EmailTemplateParams' => array(self::HAS_MANY, 'EmailQueueParam', 'emailsQueueId')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'queueId' => 'Queue',
			'emailTemplate' => 'Email Template',
			'address' => 'Address',
			'datetimeIn' => 'Datetime In',
			'datetimeOut' => 'Datetime Out',
			'inQueue' => 'In Queue',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('queueId',$this->queueId);
		$criteria->compare('emailTemplate',$this->emailTemplate);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('datetimeIn',$this->datetimeIn,true);
		$criteria->compare('datetimeOut',$this->datetimeOut,true);
		$criteria->compare('inQueue',$this->inQueue);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EmailQueue the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * Removes current email from queue.
	 * 
	 * Removing is logical.
	 * @return bool			Operation success.
	 */
	public function Remove()
	{
		$this->inQueue = 0;
		$this->datetimeOut = date('Y-m-d H:i:s');
		return $this->save();
	}

    /**
     * Removes current email from queue with error.
     *
     * Removing is logical.
     * @return bool			Operation success.
     */
    public function RemoveWithError()
    {
        $this->inQueue = 2;
        $this->datetimeOut = date('Y-m-d H:i:s');
        return $this->save();
    }
}
