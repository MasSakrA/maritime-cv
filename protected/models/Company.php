<?php

/**
 * This is the model class for table "companies".
 *
 * The followings are the available columns in table 'companies':
 * @property integer $id
 * @property string $name
 *
 * The followings are the available model relations:
 * @property Office[] $offices
 * @property OrderStatus[] $orderStatuses
 */
class Company extends CActiveRecord
	
{	/**
	 * Quantity of offices
	 * @var int
	 */
	 protected $officesQty;
	
	/**
	 * @return int quantity of offices
	 */
	public function loadOfficesQty()
	{
		// Get application database
		$db = Yii::app()->db;
		// Connect
		$db->active = true;
		// Select companies list
		$officesQty = $db->createCommand()
			->select('COUNT(`email`) as count')
			->from('offices')
			->where('offices.inactive=0')
			->queryRow();			
		
		$this->officesQty = $officesQty['count'];	
			
	}
	
	
	
	public function getOfficesQty() {
		return $this->officesQty;
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'companies';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('name', 'length', 'max'=>255),
			// The following rule is used by search().
			array('id, name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'offices' => array(self::HAS_MANY, 'Office', 'company'),
			'orderStatuses' => array(self::HAS_MANY, 'OrderStatus', 'company')
//            'country' => array(self::BELONGS_TO,'Office.country','id'),
//            'sectors' => array(self::HAS_MANY, 'OfficeWorkarea', 'office'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Company the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
