<?php
/**
 * OrderCompaniesForm class.
 */
class OrderCompaniesForm extends CFormModel
{
	/**
	 * Company attribute
	 * @var array
	 */
	public $companies;
	/**
	 * Company list
	 * @var array 
	 */
	public $companiesList;
	/**
	 * Company work area filters
	 * @var array
	 */
	public $workAreaFilters;
	/**
	 * Company region filters
	 * @var array
	 */
	public $regionFilters;
	/**
	 * Company region filters
	 * @var array
	 */
	public $regions;
	/**
	 *Company country filters
	 * @var array 
	 */
	public $countryFilters;
	
	/**
	 * Declare the validation rules.
	 */
	public function rules()
	{
		return array(
			array('companies', 'ValidateCompanies')
		);
	}

	/**
	 * Validator for companies attribute
	 * @param string $attribute attribute name
	 * @param array $params parameters array
	 */
	public function ValidateCompanies($attribute, $params)
	{
		if(empty($this->companies))
		{
			 $this->addError('companies', 'Должна быть выбрана компания');
		}
	}
	/**
	 * Method loads companies list from darabase
	 */
	public function LoadCompanies()
	{
		// Get application database
		$db = Yii::app()->db;
		// Connect
		$db->active = true;
		// Select companies list
		$this->companiesList = $db->createCommand()
			->select('offices.id, if(offices.name IS NULL OR offices.name = "", comp.name, offices.name) as name, offices.country, group_concat(offWork.workArea separator \',\') as sectors')
			->from('offices')
			->leftJoin('companies as comp', 'offices.company = comp.id')
			->leftJoin('officeworkareas as offWork', 'offWork.office = offices.id')
			->where('offices.inactive=0')
			->group('offWork.office')
			->order('name ASC')
			->queryAll();				
	}
	/**
	 * Method loads work areas list from darabase
	 */
	public function LoadWorkAreaFilters()
	{
		// Get application database
		$db = Yii::app()->db;
		// Connect
		$db->active = true;
		// Select companies list
		$this->workAreaFilters = $db->createCommand()
			->selectDistinct('offWork.workArea as name')
			->from('offices')
			->leftJoin('officeworkareas as offWork', 'offWork.office = offices.id')
			->where('offices.inactive=0 and offWork.workArea is not null')
			->queryAll();
	}
	/**
	 * Method loads available region list from database
	 */
	public function LoadRegionFilters()
	{
		// Get application database
		$db = Yii::app()->db;
		// Connect
		$db->active = true;
		// Select companies list
		$this->regionFilters = $db->createCommand()
			->selectDistinct('countries.region as name')
			->from('offices')
			->leftJoin('countries', 'countries.country = offices.country')
			->where('offices.inactive=0 and countries.region is not null')
			->queryAll();
    }
	/**
	 * Method loads a list of all existing regions from database
	 */
	public function LoadRegions()
	{
		// Get application database
		$db = Yii::app()->db;
		// Connect
		$db->active = true;
		// Select companies list
		$this->regions = $db->createCommand()
			->selectDistinct('a.region, concat("\'", group_concat(distinct b.country order by b.country separator "\',\'"), "\'") as countries')
			->from('countries as a')
			->leftJoin('countries as b', 'a.region = b.region')
			->where('b.country<>\'\' and b.country is not null')
			->group('a.region')
			->order('a.region')
			->queryAll();		
	}
	/**
	 * Method loads available country list from database
	 */
	public function LoadCountryFilters()
	{
		// Get application database
		$db = Yii::app()->db;
		// Connect
		$db->active = true;
		// Select companies list
		$this->countryFilters = $db->createCommand()
			->selectDistinct('country as name')
			->from('offices')
			->where(array('and', 'inactive=0', 'country<>\'\''))
			->order('name')
			->queryAll();
	}
	
}