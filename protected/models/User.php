<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property string $identificator
 * @property string $email
 *
 * The followings are the available model relations:
 * @property UserEmailContent		$EmailContent
 * @property UserFiles[]			$Files
 * @property UserOrderedCompanies	$OrderedCompanies
 * @property Order					$Order
 */
class User extends CActiveRecord
{
	
	/**
	 * Authorization status
	 * @var bool
	 */
	public $authorized;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('identificator, email', 'required'),
			array('identificator', 'length', 'max'=>40),
			array('email', 'length', 'max'=>255),
			array('authorized', 'safe'),
			// The following rule is used by search().
			array('id, identificator, email', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * Override parent method for adding additional safe attributes
	 * @return array Safe atributes names
	 */
	public function getAttributes($names=null)
	{
		/**
		 * Safe attributes names
		 * @var array
		 */
		$safeAttributes = parent::getAttributes();
		// Check for existing safe attribute
		if(!in_array('authorized', $safeAttributes))
		{
			// Add if not exists
			$safeAttributes['authorized'] = $this->authorized;
		}
		//print_r($safeAttributes);
		return $safeAttributes;		
	}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'EmailContent' => array(self::HAS_ONE, 'UserEmailContent', 'user'),
			'Files' => array(self::HAS_MANY, 'UserFile', 'user'),
			'OrderedCompanies' => array(self::HAS_ONE, 'UserOrderedCompanies', 'user'),
			'Order'=> array(self::HAS_ONE, 'Order', 'user'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('word', 'id'),
			'identificator' => Yii::t('word', 'identificator'),
			'email' => Yii::t('word', 'email'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('identificator',$this->identificator,true);
		$criteria->compare('email',$this->email,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,			
			'pagination'=>array(
					'pageSize' => Yii::app()->Settings->Get('rowsPerPage', 'adminPanel')
			)
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
