<?php

/**
 * This is the model class for table "offices".
 *
 * The followings are the available columns in table 'offices':
 * @property integer $id
 * @property integer $company
 * @property string $name
 * @property string $email
 * @property string $Regions
 * @property string $workarea
 * @property string $regionSearch Value for search region
 * @property string $workAreaSearch Value for search sector
 * @property string $country
 * @property integer $inactive
 *
 * The followings are the available model relations:
 * @property Company $parentCompany
 */
class Office extends CActiveRecord
{
	public $workAreaSearch = '';
	public $regionSearch = '';
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'offices';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('company, email', 'required'),
			array('company, inactive', 'numerical', 'integerOnly'=>true),
			array('name, email', 'length', 'max'=>255),
			array('country', 'length', 'max'=>2),
			// The following rule is used by search().
			array('id, company, name, email, country, regionSearch, workAreaSearch', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'parentCompany' => array(self::BELONGS_TO, 'Company', 'company'),
			'workarea' => array(self::HAS_MANY, 'OfficeWorkarea', 'office'),
			'Regions' => array(self::HAS_MANY, 'OfficeRegion', 'office'),
		);	
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'company' => 'Company',
			'name' => Yii::t('word','Name', SINGULAR),
			'email' => Yii::t('word','email'),
			'regionSearch' => Yii::t('word','Region', SINGULAR),
			'country' => Yii::t('word','Country', SINGULAR),
			'workAreaSearch' => Yii::t('phrase','workAreaSearch'),
			'inactive' => Yii::t('phrase','Inactive'),
            'comment' => Yii::t('word','comment')
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;
		$criteria->with = array('parentCompany', 'Regions', 'workarea');
		// Join regions and workarea with main table when searched region or sector
		if( ! empty($this->regionSearch) || ! empty($this->workAreaSearch))
		{
			$criteria->together = true;
		}		
		
		$criteria->compare('id',$this->id);
		//$criteria->compare('company',$this->company);
		$criteria->compare('parentCompany.name',$this->name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('Regions.region',$this->regionSearch,true);
		$criteria->compare('country',$this->country,true);		
		$criteria->compare('workarea.workArea',$this->workAreaSearch,true);
		$criteria->compare('inactive',$this->inactive);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,			
			'pagination'=>array(
					'pageSize' => Yii::app()->Settings->Get('rowsPerPage', 'adminPanel')
			)
		));
	}
	
	/**
	 * @param string $delimiter Delimiter charachter.
	 * @return string Delimited regions
	 */
	public function GetRegionsString($delimiter = ';')
	{
		/**
		 * @var string $regions Returned regions
		 * @var OfficeRegion $Region Office region object
		 */
		$regions = "";
		foreach($this->Regions as $Region)
		{
			// Add delimiter if regions already exists.
			if( ! empty($regions))
			{
				$regions .= $delimiter;
			}
			$regions .= Yii::t("region", $Region->region);
		}
		return $regions;
	}

	/**
	 * @param string $delimiter Delimiter charachter.
	 * @return string Delimited sectors
	 */
	public function GetSectorsString($delimiter = ';')
	{
		/**
		 * @var string $sectors Returned sectors
		 * @var OfficeWorkArea $Sectors Office sectors object
		 */
		$sectors = "";
		foreach($this->workarea as $Sector)
		{
			// Add delimiter if regions already exists.
			if( ! empty($sectors))
			{
				$sectors .= $delimiter;
			}
			$sectors .= Yii::t("workArea", $Sector->workArea);
		}
		return $sectors;
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Office the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
