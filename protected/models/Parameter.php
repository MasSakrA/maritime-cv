<?php

/**
 * This is the model class for table "parameters".
 *
 * The followings are the available columns in table 'parameters':
 * @property integer $id
 * @property string $category
 * @property string $name
 * @property string $value
 */
class Parameter extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'parameters';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('name, value', 'required', 'on' => 'create'),
			array('category', 'length', 'max'=>50),
			array('name', 'length', 'max'=>100),
			array('value', 'length', 'max'=>255),
			// The following rule is used by search().
			array('id, category, name, value', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('word', 'id'),
			'category' => Yii::t('word', 'Category', SINGULAR),
			'name' => Yii::t('word', 'Name', SINGULAR),
			'value' => Yii::t('word', 'Value', SINGULAR),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
        $criteria->compare('category','<>admin');
        $criteria->compare('category',$this->category);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('value',$this->value,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,			
			'sort' => array(
				'defaultOrder'=>array(
					'category' => CSort::SORT_ASC
				)
			),			
			'pagination'=>array(
					'pageSize' => Yii::app()->Settings->Get('rowsPerPage', 'adminPanel')
			)
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Parameter the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
