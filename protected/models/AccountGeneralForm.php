<?php
/**
 * AccountGeneralForm class.
 */
class AccountGeneralForm extends CFormModel
{
	/**
	 * User email
	 * @var string 
	 */
	public $email = '';
	/**
	 * Resume file name
	 * @var string 
	 */
	public $CVFile = '';
	/**
	 * Attachment file name
	 * @var string 
	 */
	public $attachment1 = '';
	/**
	 * Attachment file name
	 * @var string 
	 */
	public $attachment2 = '';
	/**
	 * Resume file name as pseudonym
	 * @var string 
	 */
	public $CVFileName = 'Резюме';
	/**
	 * Attachment file name as pseudonym
	 * @var string 
	 */
	public $attachment1Name = 'Приложение 1';
	/**
	 * Attachment file name as pseudonym
	 * @var string 
	 */
	public $attachment2Name = 'Приложение 2';	
	/**
	 * Email subject
	 * @var string 
	 */
	public $subject = '';
	/**
	 * Email body
	 * @var string 
	 */
	public $body = '';
	/**
	 * Selected company ids
	 * @var string 
	 */
	public $selectedCompanyIds = '';
	/**
	 * All company list
	 * @var array 
	 */
	public $companyList = array();
	/**
	 * The selected color scheme
	 * @var string
	 */
	public $colorScheme = '';

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
		);
	}
	
	/**
	 * Method display genral user order data
	 * @retutn bool Loading success
	 */
	public function LoadGeneralData()
	{
		/**
		 * Loading success flag
		 * @var bool
		 */
		$loadingSuccess = false;
		/**
		 * Authorized user data
		 * @var array
		 */
		$user = Yii::app()->session->get('user');
		
		/**
		 * Loading user's data success
		 * @var bool
		 */
		$userDataLoaded = $this->LoadUserData($user);
		
		/**
		 * Loading user's files success
		 * @var bool
		 */
		$userFilesLoaded = $this->LoadUserFiles($user);
		
		/**
		 * Loading user's email content success
		 * @var bool
		 */
		$userEmailContentLoaded = $this->LoadUserEmailContent($user);
		
		/**
		 * Loading all companies success
		 * @var bool
		 */
		$companiesLoaded = $this->LoadCompanies();
		
		/**
		 * Loading user's ordered companies success
		 * @var bool
		 */
		$userOrderedCompaniesLoaded = $this->LoadUserOrderedComanies($user);
		
		if(
			$userDataLoaded
			&& $userFilesLoaded
			&& $userEmailContentLoaded
			&& $userOrderedCompaniesLoaded
			&& $companiesLoaded
		)
		{
			$loadingSuccess = true;
		}
		return $loadingSuccess;
		
		
		/*
		 * Initialize self properties by stored data
		 */
		// User email
		/*$this->email = $filesForm['email'];
		// Files
		$this->CVFile = $filesForm['CVFile'];
		$attachment1Exists = false;
		if(isset($filesForm['attachment1']))
		{
			$this->attachment1 = $filesForm['attachment1'];
		}
		if(isset($filesForm['attachment2']))
		{
			$this->attachment2 = $filesForm['attachment2'];
		}			
		// Email data
		$this->subject = $contentForm['subject'];
		$this->body = $contentForm['body'];
		// Selected companies
		$this->selectedCompanyIds = $companiesForm['companies'];*/
		
		
		
	}

	
	/**
	 * Method loads user's files data
	 * @param array $user User data
	 * @return bool Data loading success
	 */
	private function LoadUserFiles($user)
	{
		/**
		 * Data loading success
		 * @var bool
		 */
		$dataLoaded = false;
		/**
		 * User files
		 * @var array
		 */
		$userFiles = UserFile::model()->findAll('user = :userId', array('userId' => $user['id']));
		
		// Load each file data
		foreach($userFiles as $File)
		{
			switch($File->category)
			{
				case 'CVFile':
					$this->CVFile = $File->name;
					$this->CVFileName = $File->pseudonym;
					break;
				
				case 'attachment1':
					$this->attachment1 = $File->name;
					$this->attachment1Name = $File->pseudonym;
					break;
				
				case 'attachment2':
					$this->attachment2 = $File->name;
					$this->attachment2Name = $File->pseudonym;
					break;
			}
		}
	}
	
	/**
	 * Method loads user's email content
	 * @param array $user User data
	 * @return bool Data loading success
	 */
	private function LoadUserEmailContent($user)
	{
		/**
		 * Data loading success
		 * @var bool
		 */
		$dataLoaded = false;
		/**
		 * User email content
		 * @var UserEmailContent
		 */
		$UserEmailContent = UserEmailContent::model()->find('user = :userId', array('userId' => $user['id']));
		// Check for data existing
		if( ! is_null($UserEmailContent))
		{
			// Set email data
			$this->subject = $UserEmailContent->subject ;
			$this->body = $UserEmailContent->body;
			$this->colorScheme = $UserEmailContent->colorScheme;
			// Set loading success
			$dataLoaded = true;
		}
		
		return $dataLoaded;
	}
	
	/**
	 * Method saves user's selected to order companies to database
	 * @param User $User User data object
	 * @return bool Data loading success
	 */
	private function LoadUserOrderedComanies($user)
	{
		/**
		 * Data loading success
		 * @var bool
		 */
		$dataLoaded = false;
		/**
		 * User ordered companies
		 * @var UserOrderedCompanies
		 */ 
		$UserOrderedCompanies = UserOrderedCompanies::model()->find('user = :userId', array('userId' => $user['id']));
		if( ! is_null($UserOrderedCompanies))
		{
			// Load company ids from string separated by comma
			$this->selectedCompanyIds = explode(',', $UserOrderedCompanies->companies);
			$dataLoaded = true;
		}
		
		return $dataLoaded;		
	}

	/**
	 * Loads user data from session authorized user
	 * @param array $user User data
	 * @return bool Data loading success
	 */
	public function LoadUserData($user)
	{		
		/**
		 * Data loading success
		 * @var bool
		 */
		$dataLoaded = false;
		if( ! empty($user))
		{
			// Get email
			$this->email = $user['email'];
			// Set loading success
			$dataLoaded = true;
		}
		return $dataLoaded;
	}
	
	/**
	 * Method loads all companies list
	 */
	public function LoadCompanies()
	{
		/**
		 * Data loading success
		 * @var bool
		 */
		$dataLoaded = false;
		/**
		 * All companies list
		 * @var array
		 */
		$companies = Company::model()->findAll();
		if( ! is_null($companies))
		{
			$this->companyList = $companies;
			$dataLoaded = true;
		}
		
		return $dataLoaded;
	}
}