<?php
/**
 * NOTE: not used in application yet.
 */

/**
 * This is the model class for table "orderStatus".
 *
 * The followings are the available columns in table 'orderStatus':
 * @property integer $id
 * @property integer $order
 * @property integer $company
 * @property string $datetime
 * @property integer $success
 *
 * The followings are the available model relations:
 * @property Order $orderDetails
 * @property Company $companyDetails
 */
class OrderStatus extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orderStatus';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('order, company', 'required'),
			array('order, company, success', 'numerical', 'integerOnly'=>true),
			array('datetime', 'safe'),
			// The following rule is used by search().
			array('id, order, company, datetime, success', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'orderDetails' => array(self::BELONGS_TO, 'Order', 'order'),
			'companyDetails' => array(self::BELONGS_TO, 'Company', 'company'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'order' => 'Order ID',
			'company' => 'Company ID',
			'datetime' => 'Date',
			'success' => 'Success',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('order',$this->order);
		$criteria->compare('company',$this->company);
		$criteria->compare('datetime',$this->datetime,true);
		$criteria->compare('success',$this->success);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrderStatus the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
