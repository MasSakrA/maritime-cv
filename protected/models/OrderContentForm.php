<?php
/**
 * OrderContentForm class.
 */
class OrderContentForm extends CFormModel
{
	public $subject;
	public $body;
	
	public $colorSchemes;
	public $emailTemplates;
	
	/**
	 * Method loads available color schemes from database
	 */
	public function LoadColorSchemes()
	{
		// Get application database
		$db = Yii::app()->db;
		// Connect
		$db->active = true;
		// Select companies list
		$this->colorSchemes = $db->createCommand()
			->select('id, name, scheme, additionalCSS')
			->from('colorschemes')
			->where('inactive=0')
			->order('order')
			->queryAll();
	}
	/**
	 * Method loads available email templates of given type from database
	 */
	public function LoadEmailTemplates( $type )
	{
		// Get application database
		$db = Yii::app()->db;
		// Connect
		$db->active = true;
		// Select companies list
		$this->emailTemplates = $db->createCommand()
			->select('name')
			->from('emailtemplates')
			->where('inactive=0 and type=:type', array(':type'=>$type))
			->order('order')
			->queryAll();
	}
	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			// Required fields
			array('subject, body', 'required')
		);
	}
}