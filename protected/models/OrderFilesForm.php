<?php
/**
 * orderFilesForm class.
 * orderFilesForm is the data structure for keeping
 * initial order data..
 */
class OrderFilesForm extends CFormModel
{
	public $email;
	public $CVFile;
	public $attachment1;
	public $attachment2;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		/**
		 * Maximum upload file size in bytes
		 * @var int
		 */
		$maxUploadSize = (int) ini_get('upload_max_filesize') * 1024 * 1024;
		return array(
			// Required fields
			array('email', 'required'),
			// Email has to be a valid email address
			array('email', 'email'),
			// CV file
			array('CVFile', 'file', 'types' => 'doc, docx, xls, xlsx, rtf, pdf, zip, rar, jpg', 'maxSize' => $maxUploadSize / 2, 'message' => Yii::t('phrase', 'not valid file empty')),
			// Attachments files
			array('attachment1, attachment2', 'file', 'types' => 'zip', 'maxSize' => $maxUploadSize / 4, 'allowEmpty' => true, 'tooLarge'=>'File has to be smaller than 0.5 MB')
		);
	}
	
	protected function beforeValidate()
	{
		/**
		 * Correct validation flag
		 * @var bool
		 */
		$valid = parent::beforeValidate();
		// If parent validation not valid return false
		if( ! $valid)
		{
			return false;
		}
		/**
		 * Post content max length
		 * @var int
		 */
        $maxPostSize = (int) ini_get('post_max_size') * 1024 * 1024;
        /**
		 * Request content size
		 * @var int
		 */
        $contentSize = (int) $_SERVER['CONTENT_LENGTH'];
        // Check content size
        if($contentSize < $maxPostSize)
        {
			$valid = true;
        }
        else
        {
			$valid = false;
			// Add error
			$this->addError('CVFile', 'Your booty is too large!'); 
        }
        
        return $valid;
	}
}