<?php
/**
 * This is extended model of source message used for email templates.
 * @property string $name
 * 
 * The followings are the available model relations:
 * @property Emailtemplate EmailTemplate
 */
class EmailtemplateMessage extends SourceMessage
{
	public $name;
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array_merge(parent::relations(), array(
			'EmailTemplate' => array(self::HAS_MANY, 'Emailtemplate', array('name'=>'message'))
		));
	}
	
	public function attributeLabels()
	{
		$labels = parent::attributeLabels();
		$labels['message'] = Yii::t('word', 'Name', SINGULAR);
		$labels['category'] = Yii::t('word', 'Type');
		return $labels;
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Emailtemplate the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

