<?php

/**
 * This is the model class for table "officeworkareas".
 *
 * The followings are the available columns in table 'officeworkareas':
 * @property int $id
 * @property integer $office
 * @property string $workArea
 */
class OfficeWorkarea extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'officeworkareas';
	}
	/**
	 * @return string prymary key name
	 */
	public function primaryKey()
	{
		return 'id';
	}
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('office, workArea', 'required'),
			array('id, office', 'numerical', 'integerOnly'=>true),
			array('workArea', 'length', 'max'=>2),
			// The following rule is used by search().
			array('id, office, workArea', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'parentOffice' => array(self::BELONGS_TO, 'Office', 'id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'office' => 'Office',
			'workArea' => 'Work Area'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('office',$this->office);
		$criteria->compare('workArea',$this->workArea,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OfficeWorkarea the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
