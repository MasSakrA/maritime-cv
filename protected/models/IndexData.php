<?php

/**
 * This is the model class for table "companies".
 *
 * The followings are the available columns in table 'companies':
 * @property integer $id
 * @property string $name
 *
 * The followings are the available model relations:
 * @property Office[] $offices
 * @property OrderStatus[] $orderStatuses
 */
class IndexData extends CFormModel
	
{	/**
	 * Quantity of offices
	 * @var int
	 */
	public $officesQty;
	
	/**
	 * @return int quantity of offices
	 */
	public function loadOfficesQty()
	{
		// Get application database
		$db = Yii::app()->db;
		// Connect
		$db->active = true;
		// Select companies list
		$officesQty = $db->createCommand()
			->select('COUNT(`email`) as count')
			->from('offices')
			->where('offices.inactive=0')
			->queryRow();			
		
		$this->officesQty = $officesQty['count'];	
			
	}
	public function getOfficesQty() {
	return $this->officesQty;
	}
	/**
	 * Quantity of countries
	 * @var int
	 */
	public $countryQty;
	/**
	 * @return int quantity of countries
	 */
	public function loadCountryQty()
	{
		// Get application database
		$db = Yii::app()->db;
		// Connect
		$db->active = true;
		// Select companies list
		$countryQty = $db->createCommand()
			->select('COUNT(DISTINCT `country`) as count')
			->from('offices')
			->where('offices.inactive=0')
            ->andWhere('offices.country != ""')
			->queryRow();			
		
		$this->countryQty = $countryQty['count'];	
			
	}
	
	
	
	public function getCountryQty() {
		return $this->countryQty;
	}
}

?>