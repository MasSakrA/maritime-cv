<?php

/**
 * This is the model class for table "SourceMessage".
 *
 * The followings are the available columns in table 'SourceMessage':
 * @property integer $id
 * @property string $category
 * @property string $message
 * @property mixed[] $search Array of values used for filtering.
 * @property int[] $translationsOrder The order of translations on different languages.
 *
 * The followings are the available model relations:
 * @property Message[] $messages
 */
class SourceMessage extends CActiveRecord
{
	public $search = array(
		"translation" => array(
			"en" => "",
			"ru" => ""
			)
		);
	
	private $translationsOrder = array(
		'en' => 0,
		'ru' => 1
	);
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'SourceMessage';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category', 'length', 'max'=>32),
			array('message', 'safe'),
			// The following rule is used by search().
			array('id, category, message, search', 'safe', 'on'=>'search, translation'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'messages' => array(self::HAS_MANY, 'Message', 'id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category' => Yii::t('word', 'Category', SINGULAR),
			'message' => Yii::t('word', 'Message', SINGULAR),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;
		$criteria->with = array('messages');

		$criteria->compare('id',$this->id);
		$criteria->compare('category',$this->category);
		$criteria->compare('message',$this->message,true);
		
		/**
		 * Select specified categories depending on scenario
		 */
		switch($this->scenario)
		{
			case 'translation':
				if(Yii::app()->user->getName() !== 'developer')
				$criteria->addInCondition('category', array(
					'word',
					'phrase',
					'region',
					'country',
					'sector',
					'language',
					'parameter',
					'Parametercategory'));
				break;
			
			case 'emailtemplate':
				$criteria->addInCondition('category', array(
					'emailTemplateName',
					'emailTemplateContent',
					'emailTemplateSubject'));
				break;
                            
                        case 'page':
				$criteria->addInCondition('category', array(
                        'pagePart',
                        'filesPage',
                        'homePage',
                        'licensePage'
					));
				break;

			default:
				break;
		}
		
		/**
		 * Search translations for one of the languages.
		 */
		foreach(Yii::app()->params['availableLanguages'] as $language)
		{
			// Search translation language
			if( ! empty($this->search['translation'][$language]))
			{
				// Join with other tables from criteria "with" parameter
				$criteria->together = true;
				// Add language condition
				$criteria->compare("language", $language);
				// Add translation message
				$criteria->compare("translation", $this->search['translation'][$language], true);
				// Finding can be only by one of the languages.
				break;
			}
		}
                if(get_class($this) === 'EmailtemplateMessage')
                {
                    return new CActiveDataProvider($this, array(
                            'criteria'=>$criteria,
                            'sort' => array(
                                    'defaultOrder' => array(
                                            'message' => CSort::SORT_DESC
                                    )
                            ),
                            'pagination' => array(
                                            'pageSize' => Yii::app()->Settings->Get('rowsPerPage', 'adminPanel')
                            )
                    ));
                } 
                else 
                {
                    return new CActiveDataProvider($this, array(
                            'criteria'=>$criteria,
                            'sort' => array(
                                    'defaultOrder' => array(
                                            'category' => CSort::SORT_DESC
                                    )
                            ),
                            'pagination' => array(
                                            'pageSize' => Yii::app()->Settings->Get('rowsPerPage', 'adminPanel')
                            )
                    ));
                }
	}
	
	/**
	 * Gets translation message of this source message
	 * in specified language.
	 * 
	 * @param string $language Language of the translation.
	 * @return string|bool Translation in specified language or false if no translation.
	 */
	public function GetTranslation($language)
	{
		foreach($this->messages as $Message)
		{
			if($Message->language === $language)
			{
				return $Message->translation;
			}
		}
		return false;
	}
	
	/**
	 * Method checks for existing all of each avialable languages.
	 * If it not founds one of the languages the model of the message
	 * of this language will be created and associated with
	 * this source message. Also all of messages will be sorted by translations
	 * language order.
	 * 
	 * @return void
	 */
	public function ControlMessages()
	{
		/**
		 * Check for existing messages for
		 * all of available languages.
		 * 
		 * @var Message[] $newMessagesList Sorted messages including new models of the unexistings messages fro some language.
		 * @var string $language Current avialable language in this iteration.
		 * @var Message|bool $languageTranslation Found message for current language or false if not found.
		 * @var Message $Message Current message of the all related messages.
		 */
		$newMessagesList = array();
		foreach(Yii::app()->params['availableLanguages'] as $language)
		{
			$languageTranslation = false;
			foreach($this->messages as $Message)
			{
				/**
				 * Finding and sorting the message with current language.
				 */
				if($Message->language === $language)
				{
					/**
					 * Found.
					 */
					$languageTranslation = $Message;
					break;
				}				
			}
			/**
			 * Not found.
			 * 
			 * Translation not found for current language,
			 * so create new message with empty translation
			 * for this language and this source message.
			 */
			if( ! $languageTranslation)
			{
				// Create new message.
				$languageTranslation = new Message;
				// Set language such as current language.
				$languageTranslation->language = $language;
				// And relate to current source message.
				$languageTranslation->id = $this->id;				
			}
			/**
			 * Set new message order.
			 * 
			 * @var int $order New order of message in the messages list.
			 */
			$order = $this->translationsOrder[$language];
			$newMessagesList[$order] = $languageTranslation;
		}
		/**
		 * Sort the messages by order(index in current version) and 
		 * assign new messages list to old related messages list.
		 */
		ksort($newMessagesList);
		$this->messages = $newMessagesList;
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SourceMessage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
