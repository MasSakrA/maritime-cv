<?php

/**
 * This is the model class for table "userEmailContents".
 *
 * The followings are the available columns in table 'userEmailContents':
 * @property integer $id
 * @property integer $user
 * @property string $subject
 * @property string $body
 * @property string $colorScheme
 * @property Colorscheme $ColorSchemeDetails
 *
 * The followings are the available model relations:
 * @property Users $user
 */
class UserEmailContent extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'userEmailContents';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user, subject, body', 'required'),
			array('id, user', 'numerical', 'integerOnly'=>true),
			array('subject', 'length', 'max'=>255),
			array('colorScheme', 'length', 'max'=>45),
			// The following rule is used by search().
			array('id, user, subject, body', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user'),
			'ColorSchemeDetails' => array(self::HAS_ONE, 'Colorscheme', array('name'=>'colorScheme'))
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user' => 'User',
			'subject' => 'Subject',
			'body' => 'Body',
			'colorScheme' => 'Color scheme'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user',$this->user);
		$criteria->compare('subject',$this->subject,true);
		$criteria->compare('body',$this->body,true);		
		$criteria->compare('colorScheme',$this->colorScheme,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserEmailContent the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
