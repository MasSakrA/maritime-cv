<?php
/**
 * Class helper to work with strings
 */
Class Str extends CComponent
{
	public function init(){}
	
	/**
	 * Returns capitalized string
	 * @param string $string String to capitalize
	 * @return string
	 */
	public function Ucfirst($string)
	{
		return mb_convert_case($string, MB_CASE_TITLE, 'UTF-8');
	}
}
