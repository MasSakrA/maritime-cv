<?php
/**
 * Class helper to work with files
 */
Class File extends CComponent
{
	/**
	 * Needed for the component work.
	 */
	public function init(){}
	
	/**
	 * This method is wrapper for native php function,
	 * @see fopen() documentation for the details.
	 * @return resource|bool File poiner or false on failure.
	 */
	public function Open($fileName, $mode = 'w', $useIncludePath = false, $context = null)
	{
		if(is_null($context))
		{
			return fopen($fileName, $mode, $useIncludePath);
		}
		else
		{
			return fopen($fileName, $mode, $useIncludePath, $context);
		}
	}
	
	/**
	 * This method is wrapper for native php function,
	 * @see fclose() documentation for the details.
	 * @return bool File closing success.
	 */
	public function Close($fileResource)
	{
		return fclose($fileResource);
	}
	
	/**
	 * This method is wrapper for native php function,
	 * @see file_get_contents() documentation for the details.
	 * @return string File content.
	 */
	public function GetContents($filename, $use_include_path = false, $context = null, $offset = -1, $maxlen = null)
	{
		if(is_null($context))
		{
			return file_get_contents($filename, $use_include_path);
		}
		else
		{
			return file_get_contents($filename, $use_include_path, $context, $offset, $maxlen);
		}
	}
	
	/**
	 * This method is wrapper for native php function,
	 * @see fputcsv() documentation for the details.
	 * @return bool Content writing success.
	 */
	public function PutCsv($resource, $fields, $delimiter = ';', $enclosure = '"')
	{
		return fputcsv($resource, $fields, $delimiter, $enclosure);
	}
	
	/**
	 * This method is wrapper for native php function,
	 * @see fgetcsv() documentation for the details.
	 * @return string|bool CSV row or false.
	 */
	public function GetCsvRow($resource, $length = 0, $delimiter = ';', $enclosure = '"', $escape = '\\')
	{
		return fgetcsv($resource, $length, $delimiter, $enclosure, $escape);
	}
	
	/**
	 * This method is wrapper for native php function,
	 * @see str_getcsv() documentation for the details.
	 * @return array Fields of the csv row.
	 */
	public function ParseCsvRow($csvString, $delimiter = ';', $enclosure = '"', $escape = '\\')
	{
		return str_getcsv($csvString, $delimiter, $enclosure, $escape);
	}
	
	/**
	 * Creates csv row from fields array.
	 * 
	 * @param array $fields An array of fields for csv row.
	 * @param string $delimeter One symbol for delimiting the fields.
	 * @param string $enclosure One symbol for string escaping. 
	 * @return array Fields of the csv row.
	 */
	public function CreateCsvRow($fields, $delimiter = ';', $enclosure = '"')
	{
		/**
		 * @var string $scvRow Formated csv row.
		 * @var string $field Field which must be added to the row, used in cycle.
		 * @var int $addedFieldsCount Number of fields already added to row.
		 */
		$scvRow = '';
		$addedFieldsCount = 0;
		foreach($fileds as $field)
		{
			// If field contain the delimiter then escape the string.
			if(strpos($field, $delimiter) !== false)
			{
				$field = $enclosure.$field.$enclosure;
			}
			// Add field to csv row.
			$scvRow .= $field;
			$addedFieldsCount++;
			// If it is not last field then add the delimeter.
			if($addedFieldsCount !== count($fileds))
			{
				$scvRow .= $delimiter;
			}
		}
		// Return result.
		return $scvRow;
	}
	
	/**
	 * Deletes specified file
	 * @return bool Deleting success.
	 */
	public function Delete($fileName)
	{
		return unlink($fileName);
	}
}
