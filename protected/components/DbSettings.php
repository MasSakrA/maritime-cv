<?php
/**
 * Setting comnetnt.
 * 
 * Responsible for the application settings, which is stored
 * in the database but not in the config file.
 * 
 * @property array[] $cache Multidimensional associative aray of cached values of parameters. ($cahe[category][name])
 */
class DbSettings extends CComponent
{	
	public $cache = array();
	/**
	 * Method needed for component worked
	 */
	public function init(){}
	
	/**
	 * Gets setting from the database.
	 * 
	 * Method checks cache and if no cached value then returns the new.
	 * 
	 * @param string $name A name of the setting.
	 * @param string $category A category to which setting is related.
	 * @return string The value of requested parameter
	 */
	public function Get($name, $category = null)
	{
		/**
		 * Check parameters.
		 */
		if(empty($name))
		{
			throw new CException('Wrong parameters.');
		}
		
		/**
		 * Getting a value.
		 * 
		 * Trying to get cached value or get new value.
		 * In case of getting new value it will be cached.
		 */
		$value = $this->GetCached($category, $name);
		if(empty($value))
		{
			/**
			 * Getting
			 */
			$value = Parameter::model()
				->findByAttributes(array(
					'name' => $name,
					'category' => $category))
				->value;
			
			/**
			 * Caching
			 */
			// No category
			if(empty($category))
			{
				$category = 'nocategory';
			}
			$this->cache[$category][$name] = $value;			
		}
		
		// Return value
		return $value;
	}


    public function Set($name, $category, $value)
    {
        $parameter = Parameter::model()->findByAttributes(array('name' => $name, 'category' => $category));
        if ($parameter) {
            $parameter->setAttribute('value', $value);
            $parameter->update();
            $result = true;
        }
        else {
            $result = false;
        }

        return $result;
    }
	
	/**
	 * Method trying to get value from cache.
	 * 
	 * @param string $category A category to which setting is related.
	 * @param string $name A name of the setting.
	 * @retun string|bool Cached value or false if no cache
	 */
	public function GetCached($category, $name)
	{
		// No value by default.
		$value = false;
		// No category
		if(empty($category))
		{
			$category = 'nocategory';
		}
		// Try to get a value.
		if( ! empty($this->cache[$category][$name]))
		{
			$value = $this->cache[$category][$name];
		}
		// Return value.
		return $value;
	}
}
