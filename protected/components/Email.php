<?php
/**
 * Class helper to work withemail methods such as sending email
 */
Class Email extends CComponent
{
	/**
	 * The object to control a sending email
	 * @var EMailer
	 */
	public $Mailer;
	/**
	 * Init method. Needed for Yii components
	 */
	public function init()
	{
		// Initialize Mailer object
		$this->Mailer = Yii::createComponent('application.extensions.mailer.EMailer');
		/**
		 * Set sending parameters
		 */
		$this->Mailer->Host = Yii::app()->Settings->Get('host', 'email');
		$this->Mailer->Port = Yii::app()->Settings->Get('port', 'email');
		$this->Mailer->Username = Yii::app()->Settings->Get('user', 'email');
		$this->Mailer->Password = Yii::app()->Settings->Get('password', 'email');
		if(Yii::app()->Settings->Get('smtp', 'email'))
		{
			$this->Mailer->IsSMTP();
			$this->Mailer->SMTPAuth=Yii::app()->Settings->Get('smtpAuth', 'email');
			$this->Mailer->SMTPSecure = Yii::app()->Settings->Get('smtpSecure', 'email');
		}		
		$this->Mailer->IsHTML(true);
		$this->Mailer->CharSet = Yii::app()->Settings->Get('charset', 'email');
	}
	
	/**
    * Call a EMailer function
    *
    * @param string $method the method to call
    * @param array $params the parameters
    * @return mixed
    */
	public function __call($method, $params)
	{
		if (is_object($this->Mailer) && get_class($this->Mailer)==='EMailer') return call_user_func_array(array($this->Mailer, $method), $params);
		else throw new CException(Yii::t('EMailer', 'Can not call a method of a non existent object'));
	}
	
	/**
	 * Method to initialize parameters.
	 * Available parameters:
	 * string $params['to']				Recipient email address.
	 * string $params['fromEmail']		Sender email address.
	 * string $params['fromName']		Sender name.
	 * string $params['subject']		Email subject.
	 * string $params['body']			Email body.
	 * 
	 * @param array $params				The parameters for the sending email
	 */
	public function SetParameters($params)
	{
		// Set the sender's email
		if( ! empty($params['fromEmail']))
		{
			$this->Mailer->From = $params['fromEmail'];	
		}			
		// Add the email addresses to the send list
		if( ! empty($params['to']))
		{
			if( is_array($params['to']))
			{
				// Add each of the addresses if the parameter "to" is an array
				foreach($params['to'] as $email)
				{
					// Add the address
					$this->Mailer->AddAddress($email);
				}
			}
			else
			{
				// Just add the single email
				$this->Mailer->AddAddress($params['to']);
			}
		}
		// The sender's name
		if( ! empty($params['fromName']))
		{
			$this->Mailer->FromName = $params['fromName'];	
		}
		// Set a subject
		if( ! empty($params['subject']))
		{
			$this->Mailer->Subject = $params['subject'];
		}
		// Set a body
		if( ! empty($params['body']))
		{
			$this->Mailer->Body = $params['body'];
		}
	}
	/**
	 * Common method for sending email.
	 * 
	 * @param array $params		The parameters for the sending email. @see SetParameters() Description for the details.
	 */
	public function Send($params = array())
	{
		/**
		 * BEGIN Tests. For the tests only.
		 * Temporary show message instead of sending mail.
		 * @todo Remove this code after the tests.
		 */
		//print('Sent '.$params['to'].'<br>');
		//return true;
		/* END Tests */	
		
		
		// Check the input parameters
		if( ! is_array($params))
		{
			Yii::log(__FUNCTION__.': Parameters error!', CLogger::LEVEL_ERROR);
			return false;
		}
		// Set sending parameters if given
		else if( ! empty($params))
		{
			$this->SetParameters($params);
		}
		// Return the send successful
		//return $this->Mailer->Send();
		$result = $this->Mailer->Send();

		Yii::log('Error info' . $this->Mailer->ErrorInfo );
		return $result;
	}
	
	/**
	 * Sends each of the emails from queue.
	 * 
	 * @param EmailQueue[] $queue		Array of the email queue rows.
	 * @return void
	 */
	private function SendQueue($queue = array())
	{

        if (!$this->CheckIfEmailAllowed()) return false;

        /**
		 * @var int		$lastEmailQueueId			Last row 'queueId' value.
		 * @var int		$companyNotifyTemplateId	The id of template for company notify.
		 *
		 * @var string	$fromEmail					Sender emeail.
		 * @var string	$fromName					Sender name.
		 * @var string	$subject					Email subject for the queue group.
		 * @var string	$body						Email body for the queue group.
		 */

        $fromEmail = '';
        $fromName = '';
        $subject = '';
        $body = '';
		$lastEmailQueueId = -1;
		$companyNotifyTemplateId = Emailtemplate::model()->findByAttributes(array('name' => 'company-notify'))->id;

		foreach($queue as $EmailQueue)
		{
			/**
			 * Get new parameters if the 'queueId' was changed,
			 * else use old parameters for queue group.
			 */
			if($EmailQueue->queueId != $lastEmailQueueId)
			{
				/**
				 * Forming an array of parameters for Yii internationalization and templating.
				 * 
				 * $EmailQueue->EmailTemplateParams is lazy loads the email parameters.
				 * The parameters will used for all queue group, but takes from one queue row only.
				 * 
				 * @var string[]			$templateParameters		Array of template parameters.	
				 * @var EmailQueueParam		$TemplateParam			A parameter of email template.
				 */
				$templateParameters = array();
				foreach($EmailQueue->EmailTemplateParams as $TemplateParam)
				{
					// Wrap parameter name and set it as the index of value.
					$templateParameters['{'.$TemplateParam->name.'}'] = $TemplateParam->value;
				}

				/**
				 * The special case. Notifying for a company (the user order execution).
				 * 
				 * Get order email details from user's order email content.
				 * @var User $User		User to which order belongs. Used in company notifying.
				 */
				$User = null;
				if($EmailQueue->emailTemplate == $companyNotifyTemplateId)
				{
					/**
					 * Get user
					 */
					$User = User::model()
						->findByAttributes(array('id' => $templateParameters['{user}']));
					
					/**
					 * Get parameters
					 */
					$fromEmail = $User->email;
					$fromName = $User->email;
					$subject = $User->EmailContent->subject;
					$body = $User->EmailContent->body;

					/**
					 * Add CSS
					 * 
					 * @var string $CSS
					 */
//					$CSS = '<style type="text/css">'.$User->EmailContent->ColorSchemeDetails->scheme.'</style>';
					$body = '<div>'.$body.'</div>';

                    $bodyColorScheme = explode('|@|', str_replace(array('<!--', '-->'), '', $User->EmailContent->ColorSchemeDetails->scheme));
                    foreach($bodyColorScheme as $schemeRow)
                    {
                        $assoc = explode('|#|', $schemeRow);
                        $colorSchemeArray[trim(array_shift($assoc))] = trim(array_shift($assoc));
                    }
                    foreach($colorSchemeArray as $key => $colorSchemeRow)
                    {
                        $body = preg_replace('/(<'.$key.'\b[^><]*)>/i', '$1 style="'.$colorSchemeRow.'">', $body);
                    }

					/**
					 * Add attachments
					 * 
					 * @var UserFile[] $attachments		An array of user file objects.
					 * @var string $fileStore			A path to users uploads.
					 * @var UserFile $Attachment		User file object.
					 */
					$attachments = UserFile::model()->findAllByAttributes(array('user' => $User->id));
                    $fileStore = str_replace('protected', 'uploads/', Yii::app()->getBasePath()).$User->identificator;
					//$fileStore = '/home3/maritim8/public_html/maritimecv/uploads/'.$User->identificator;

//                    $this->AddAttachment('/home3/maritim8/public_html/maritimecv/uploads/veg0ejc0la9s3bfdl8p4m8flj1/slider-slide2.jpg');
//                    $this->AddAttachment('/home3/maritim8/public_html/maritimecv/uploads/veg0ejc0la9s3bfdl8p4m8flj1/back-to-top1.zip');
//                    $this->AddAttachment('/home3/maritim8/public_html/maritimecv/uploads/veg0ejc0la9s3bfdl8p4m8flj1/yii-imagine-master.zip');

      				foreach($attachments as $Attachment)
					{
                        $this->AddAttachment($fileStore . DIRECTORY_SEPARATOR . $Attachment->name);
                    }
				}
                else
                {

                    /**
                     * Get the default values for the parameters which had not set.
                     *
                     * Mail from: by default is the email address of an administrator.
                     * Name from: by default is "Maritime CV".
                     * Subject: by default is the subject of the template .
                     * Body: by default is the translating of the html template with the email parameters replacement.
                     */
                    $fromEmail = Yii::app()->Settings->Get('email', 'admin');
                    $fromName = 'Maritime CV';
                    $subject = Yii::t('emailTemplateSubject', $EmailQueue->EmailTemplateDetails->name, array(), null, $templateParameters['{lang}']);
                    $body = Yii::t('emailTemplateContent', $EmailQueue->EmailTemplateDetails->name, $templateParameters, null, $templateParameters['{lang}']);
                }

				// Remember last queue group id.
				$lastEmailQueueId = $EmailQueue->queueId;
			}

			/**
			 * Set the parameters and send email.
			 */
			$params = array(
				'fromEmail' => $fromEmail,
				'fromName' => $fromName,
				'to' => $EmailQueue->address,
				'subject' => $subject,
				'body' => $body
			);

            // Send email
            if($this->Send($params))
//            if(true)
            {
                // Clear addresses manually - it is do not automatically.
                $this->ClearAllRecipients();
                // Remove email from queue.
                if($EmailQueue->Remove())
//                if(true)
                {
                    /**
                     * The special case. Notifying for a company (the user order execution).
                     *
                     * Check if all companies from user order are notified
                     * @var Emailtemplate $Template		The template of user notification
                     */
                    if($EmailQueue->emailTemplate == $companyNotifyTemplateId)
                    {
                        if($this->IsOrderDone($User->id))
                        {
                            $Template = Emailtemplate::model()->findByAttributes(array('name' => 'completed-order-notify'));
                            // Send user notification about all companies are mailed
                            $this->Queue(array(
                                'emails' => array($User->email),
                                'emailsTemplate' => $Template->id,
                                'templateParameters' => $this->getOrderDoneParams($User->id)
                            ));
                            $this->clearAttachments();
                        }
                    }
                }
                else
                {
                    // Removing error.
                    Yii::log(__FUNCTION__.': Removing from queue was failed. ID in queue: '.$EmailQueue->id);
                }
            }
            else
            {
                // Sending error.
                Yii::log(__FUNCTION__.': Sending email was failed. ID in queue: '.$EmailQueue->id);

                //remove from queque cause error with status 2
                $EmailQueue->RemoveWithError();
            }

		}

	}

    /**
     * Check if emailing process is allowed by admin.
     * It is changed from admin panel.
     * @return bool
     */
    private function CheckIfEmailAllowed()
    {
        return (bool)Yii::app()->Settings->Get('status', 'queue');
    }

    private function IsOrderDone($id = null) {
        $UserTest = Yii::app()->db->createCommand()
            ->select('inQueue')
            ->from('emailQueue')
            ->join('emailQueueParams', 'emailQueueParams.emailsQueueId = emailQueue.queueId')
            ->where('emailQueueParams.name = "user"
                and
                emailQueueParams.value = :userId', array(':userId' => $id))
            ->andWhere('emailQueue.inQueue = 1')
            ->queryRow();

        empty($UserTest) ? $IsOrderDone = true : $IsOrderDone = false;

        return $IsOrderDone;
    }
    private function getOrderDoneParams($id = null) {
        $ParamID = Yii::app()->db->createCommand()
            ->select('emailsQueueId')
            ->from('emailQueueParams')
            ->where('emailQueueParams.name = "user_id"')
            ->andWhere('emailQueueParams.value = :userId', array(':userId' => $id))
            ->queryRow();
        $lang = Yii::app()->db->createCommand()
            ->select('value')
            ->from('emailQueueParams')
            ->where('emailQueueParams.emailsQueueId = :emailsQueueId', array(':emailsQueueId' => $ParamID['emailsQueueId']))
            ->andWhere('emailQueueParams.name = "lang"')
            ->queryRow();
        $orderAmount = Yii::app()->db->createCommand()
            ->select('value')
            ->from('emailQueueParams')
            ->where('emailQueueParams.emailsQueueId = :emailsQueueId', array(':emailsQueueId' => $ParamID['emailsQueueId']))
            ->andWhere('emailQueueParams.name = "orderAmount"')
            ->queryRow();
        $orderNumber = Yii::app()->db->createCommand()
            ->select('value')
            ->from('emailQueueParams')
            ->where('emailQueueParams.emailsQueueId = :emailsQueueId', array(':emailsQueueId' => $ParamID['emailsQueueId']))
            ->andWhere('emailQueueParams.name = "orderNumber"')
            ->queryRow();
        $accountLink = Yii::app()->db->createCommand()
            ->select('value')
            ->from('emailQueueParams')
            ->where('emailQueueParams.emailsQueueId = :emailsQueueId', array(':emailsQueueId' => $ParamID['emailsQueueId']))
            ->andWhere('emailQueueParams.name = "accountLink"')
            ->queryRow();
        return array('orderAmount' => $orderAmount['value'], 'orderNumber' => $orderNumber['value'], 'accountLink' => $accountLink['value'], 'lang' => $lang['value']);
    }

	/**
	 * @return int Last queue Id
	 */
	public function GetLastQueueId()
	{
		/**
		 * @var EmailQueue	$EmailQueue		The last row of an email queue.
		 */
		$EmailQueue = EmailQueue::model()->findBySql('SELECT * FROM emailQueue WHERE queueId = (SELECT MAX(queueId) FROM emailQueue)');
		// No rows - returned zero id
		if(is_null($EmailQueue))
		{
			return 0;
		}
		return $EmailQueue->queueId;
	}
	
	/**
	 * Gets the available quantity of the email sendings.
	 * @return int[] $availableSendings	The array of the available quantity of important and other sendings.
	 */
	public function GetAvailableSendings()
	{
		/**
		 * Get the used sendings.
		 * @var int $notImportantUsedSendings		The quantity of not important emails which already was sent.
		 * @var int $importantUsedSendings			The quantity of important emails which already was sent.
		 */
		// Get used all sendings
		$UsedSendings = Yii::app()->db->createCommand()
			->select('COUNT(emailQueue.id) as usedImportantLimit')
			->from('emailQueue')
			->join('emailtemplates', 'emailQueue.emailTemplate = emailtemplates.id')
			->where('
				`datetimeOut` BETWEEN "' . date('Y-m-d H:i:s', strtotime('-1 hour')) . '" AND "' . date('Y-m-d H:i:s') . '"
				-- Not in queue(already sent)
				AND inQueue = 0')
			->queryScalar();
        //Available limit to send in CURRENT hour
        $AvailableToSendCount = Yii::app()->Settings->Get('sendingsPerHour', 'queue') - $UsedSendings;
		// Return results
		return $AvailableToSendCount;
	}
	
	/**
	 * Gets the rows of emails in queue with important priority.
	 * 
	 * @param int $limit		The queue rows limit.
	 * @return EmailQueue[]		An array of the email queue rows.
	 */
	public function GetImportantQueue($limit = null)
	{
		$emailsQueue = EmailQueue::model()->with(array(
			'EmailTemplateDetails'=>array(
				'joinType'=>'INNER JOIN',
				'condition'=>'EmailTemplateDetails.order = 0'
			)))->findAll(array(
				'condition' => 'inQueue = 1',
				'limit' => $limit ? $limit : ''));
		return $emailsQueue;
	}
	
	/**
	 * Gets the rows of emails in queue with not important priority.
	 * 
	 * @param int $limit		The queue rows limit.
	 * @return EmailQueue[]		An array of the email queue rows.
	 */
	public function GetNotImportantQueue($limit = null)
	{
		$emailsQueue = EmailQueue::model()->with(array(
			'EmailTemplateDetails' => array(
				'select' => false,
				'joinType' => 'INNER JOIN',
				'condition' => 'EmailTemplateDetails.order != 0'
			)))->findAll(array(
				'condition' => 'inQueue = 1',
				'order' => 'EmailTemplateDetails.order DESC',
				'limit' => $limit ? $limit : ''));
		return $emailsQueue;
	}
	
	/**
	 * Add email to queue.
	 * 
	 * Adds the specified email with the parameters
	 * and the priority setups of a template.
	 * Available parameters:
	 * string[] $parameters['emails']				An array of the emails that must be added to queue.
	 * string	$parameters['emailsTemplate']		A template type for the emails.
	 * string[] $parameters['templateParameters']	The parameters for the template of an emails type.
	 * 
	 * @param string[] $parameters					The parameters of queue.
	 * @return bool									A success of enqueue.
	 */
	public function Queue($parameters = array())
	{
		/**
		 * The parameters check.
		 */
		if( ! is_array($parameters)
			|| empty($parameters['emails'])
			|| ! is_array($parameters['emails']))
		{
			Yii::log(__FUNCTION__.': the parameters are incorrect.', CLogger::LEVEL_ERROR);
			return false;
		}
		
		/**
		 * Add the emails to the queue.
		 * 
		 * Add the specified emails to the queue in the database using a multiple inserting.
		 * 
		 * @var	array[]			$emails		Array of the values of columns for the each of emails for a multile inserting.
		 * @var int				$queueId	An id of a queue.
		 * @var string			$email		An email address. Used in cycle.
		 * @var CDbCommand		$command	Yii sql comand.
		 */
		$emails = array();
		$queueId = $this->GetLastQueueId() + 1;
		foreach($parameters['emails'] as $email)
		{
			$emails[] = array(
				'address' => $email,
				'queueId' => $queueId,
				'datetimeIn' => date('Y-m-d H:i:s'),
				'emailTemplate' => $parameters['emailsTemplate']
			);
		}
		$command = Yii::app()->db->schema->commandBuilder->createMultipleInsertCommand('emailQueue', $emails);
		$command->execute();
		
		/**
		 * Save the parameters of emails.
		 * 
		 * Save the parameters to database if they are set.
		 * Parsing an array into other array as the keys and the values.
		 * Creating an array for multiple inserting.
		 * 
		 * @var array[]		$templateParams		An array with template parameters.
		 * @var string		$parameterName		The name of the parameter. Used in cycle.
		 * @var string		$value				The value of thr parameter. Used in cycle.
		 */
		if( ! empty($parameters['templateParameters']) && is_array($parameters['templateParameters']))
		{
			$templateParams = array();
			foreach($parameters['templateParameters'] as $parameterName => $value)
			{
				$templateParams[] = array(
					'name' => $parameterName,
					'value' => $value,					
					'emailsQueueId' => $queueId
				);
			}
			$command = Yii::app()->db->schema->commandBuilder->createMultipleInsertCommand('emailQueueParams', $templateParams);
			$command->execute();
		}
		// If all finished return true
		return true;
	}
	
	/**
	 * Start sending the emails from queue.
	 * @param int	$queueId	The ID of queue which must be started. If null then the queue starts from first row.
	 * @return void
	 */
	public function StartQueue($queueId = null)
	{
		/**
		 * Get available quantity of emails which we can send.
		 * 
		 * @var	int[] $limit		The remaining limits of the sendings of emails('important' and 'notImportant' indexes)
		 */
		$limit = $this->GetAvailableSendings();

		/**
		 * Get the quantity of the queued emails if the limit allow.
		 * 
		 * @var int[] $queue		The quantity of the emails in the queue('important' and 'notImportant' indexes)
		 */
		// Get the important queue.
		if($limit > 0)
		{
			$queue['important'] = $this->GetImportantQueue($limit);
		}
        // Count not important after getting max of important is done
        $notImportantLimit = $limit - count($queue['important']);
		// Get the not important queue.
		if($notImportantLimit > 0)
		{
			$queue['notImportant'] = $this->GetNotImportantQueue($notImportantLimit);
		}

		/**
		 * Send the emails from the queue.
		 * @todo Remove test echos.
		 */
		// Send important emails
		if($limit > 0)
		{
			$this->SendQueue($queue['important']);
		}
		// Send not important emails
		if($notImportantLimit > 0)
		{
			$this->SendQueue($queue['notImportant']);
		}
	}
}
