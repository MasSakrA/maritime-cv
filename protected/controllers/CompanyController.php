<?php

class CompanyController extends Controller
{
	/**
	 * Default action. Displays companies list
	 */
//	public function actionIndex()
//	{
//		/**
//		 * Active offices list
//		 * @var array
//		 */
//		$CompanyModel = new Company();
//		// Load count company 
//		$CompanyModel->loadOfficesQty();
//		$offices = Office::model()->with('parentCompany')->findAll('`inactive` = 0');
//		foreach($offices as $Office)
//		{
//			$this->SaveTextToImg($Office->parentCompany->name, 40, 120, 170, 10, 'lib/skin/default/font/Segoe_UI.ttf'); // generate png image using company name (string)
//		}		
//		$this->render('index', array('offices' => $offices, 'officesQty' => $CompanyModel->getOfficesQty()));
//	}

	/**
	 * Displays specified company details
	 * @param int $value Specified company office id
	 */
	//public function ActionDetails($value)
	//{
		/**
		 * Specified office
		 * @var Office
		 */
		//$Office = Office::model()->findByPk($value);
		// Render office details		
		//$this->render('details', array('Office' => $Office));
	//}
	
	/**
	 * Creating image frome some text if it does not exist
	 */
	public static function SaveTextToImg($text, $r, $g, $b, $font_size, $font_file, $status = '')
	{	
		$md5_file_name = md5($text.Yii::app()->controller->id.Yii::app()->controller->action->id).$status;
		$filename = "images/$md5_file_name.png";
		if( ! file_exists($filename) ) 
		{							
			$bbox = imagettfbbox($font_size, 0, $font_file, $text);
			$width = abs($bbox[0]) + abs($bbox[2]+1);
			$height = abs($bbox[5]) + abs($bbox[1]);
			$y = abs($bbox[5]);

			$im = imagecreatetruecolor( $width, $height );		
			imagesavealpha($im, true);
			$transparent = imagecolorallocatealpha($im, 0, 0, 0, 127);
			imagefill($im, 0, 0, $transparent);
			$blue = imagecolorallocate($im, $r, $g, $b);

			imagettftext($im, $font_size, 0, 0, $y, $blue, $font_file, $text);		
			
			imagepng($im, $filename);
			imagedestroy($im);
		}
	}
}