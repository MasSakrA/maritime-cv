<?php
class OrderController extends Controller
{
	/**
	 * Override layout to use order form layout
	 * @var string
	 */
	public $layout = 'orderForm';/**
	 * Previous step name
	 * @var string 
	 */
	public $previousStep = '';/**
	 * Next step name
	 * @var string 
	 */
	public $nextStep = '';/**
	 * Form title
	 * @var string 
	 */
	public $formTitle = '';/**
	 * Name of the sessions variables which will be removed on the reseting
	 * @var array
	 */
	public $dataToReset = array(
		'OrderFilesForm',
		'OrderContentForm',
		'OrderCompaniesForm',
		'OrderPreviewForm',
		'OrderPaymentForm',
		'order'
	);
	
	public function beforeAction($action)
	{
		// action
		$this->CheckAgreement();		
		return parent::beforeAction($action);
	}
	
	public function ActionIndex()
	{
		// By default show files selecting step
		$this->actionFiles();
	}
	
	
	/**
	* License agreements step
	*/
	public function ActionLicense()
	{
		if(isset($_POST['agree']))
		{
			Yii::app()->session['agree'] = $_POST['agree'];
		}
		if(isset(Yii::app()->session['agree']))
		{
			$this->redirect('/order/content');
		}
		$this->formTitle = Yii::t('phrase','License Agreements');
		$this->render('stepLicense');
	}	
	
	
	/**
	 * File selecting step
	 */
	public function ActionFiles()
	{
		// If order preview was submited go to finish step
		$orderPreviewForm = Yii::app()->session->get('OrderPreviewForm');
		if( ! is_null($orderPreviewForm) && $orderPreviewForm['finished'] && ! isset($_POST['ajax']))
		{
			$this->GoStep('finish');
		}
		$this->nextStep = 'content';
		$orderFilesFormModel = new OrderFilesForm;
		// Check for previously saved file name
		$this->UnsetNonExistentFileName('OrderFilesForm', 'CVFile');
		$this->UnsetNonExistentFileName('OrderFilesForm', 'attachment1');
		$this->UnsetNonExistentFileName('OrderFilesForm', 'attachment2');		
		
		// Was been form data sent?
		if(isset($_POST['OrderFilesForm']))
		{

            // Ajax validation
			if(isset($_POST['ajax']) && $_POST['ajax']==='order-form')
			{
				echo CActiveForm::validate($orderFilesFormModel, array('email'));
				Yii::app()->end();
			}

			// Set model attributes
			$orderFilesFormModel->attributes=$_POST['OrderFilesForm'];
			// Get files
            $CVFile = CUploadedFile::getInstance($orderFilesFormModel,'CVFile');
            $attachment1 = CUploadedFile::getInstance($orderFilesFormModel,'attachment1');
            $attachment2 = CUploadedFile::getInstance($orderFilesFormModel,'attachment2');

			// Get orders file form session data
			$orderFilesForm = Yii::app()->session->get('OrderFilesForm', array());
			// Set email
			$orderFilesForm['email'] = $orderFilesFormModel->email;
			// Form is finished
			$orderFilesForm['finished'] = true;
			// Save order files form in session
			Yii::app()->session->add('OrderFilesForm', $orderFilesForm);

			// If cv file was previously loaded do not validate it field
			if( ! empty($orderFilesForm['CVFile']))
			{
				$attributes = array('email');
			}
			// Else validate all
			else
			{
				$attributes = NULL;
			}

			// Validate needed form atributes
			if($orderFilesFormModel->validate($attributes))
			{
				// Check for selected files
				$CVFileSelected = ! is_null($CVFile);
				$attachment1Selected = ! is_null($attachment1);
				$attachment2Selected = ! is_null($attachment2);

				// If one of file is selected then create directory
				if($CVFileSelected || $attachment1Selected || $attachment2Selected)
				{
					// Get uploads path
					$uploadsPath = Yii::getPathOfAlias('uploads') . DIRECTORY_SEPARATOR . Yii::app()->session->getSessionID();
					// Check session directory existing
					if( ! file_exists($uploadsPath))
					{
						// Create session directory
						mkdir($uploadsPath, 0777);
					}
				}

				// Save selected files to user folder and temporary save those name in session
				if($CVFileSelected)
				{
					$this->SaveFile('OrderFilesForm', 'CVFile', $CVFile);
				}

				if($attachment1Selected)
				{
					$this->SaveFile('OrderFilesForm', 'attachment1', $attachment1);
				}

				if($attachment2Selected)
				{
					$this->SaveFile('OrderFilesForm', 'attachment2', $attachment2);
				}

                                if(!isset(Yii::app()->session['agree']))
                                {
                                    $this->redirect('/order/license');
                                }

				// Go to next step
				$this->GoStep($this->nextStep);
			}
		} else {
            if(!isset(Yii::app()->session['agree']))
            {
                $this->redirect('/order/license');
            }
		}
		
		// Get orders file form session data
		$orderFilesForm = Yii::app()->session->get('OrderFilesForm');
		if( !is_null($orderFilesForm))
		{
			// Load attributes from existing session
			$orderFilesFormModel->email = $orderFilesForm['email'];
		}
		
		$this->formTitle = Yii::t('phrase','CV and Attachments');
		$this->render('stepFiles', array('orderFilesFormModel'=>$orderFilesFormModel));
	}
	
	/**
	 * Order content step
	 */
	public function ActionContent()
	{		
		// If order preview was submited go to finish step
		$orderPreviewForm = Yii::app()->session->get('OrderPreviewForm');
		if( ! is_null($orderPreviewForm) && $orderPreviewForm['finished'])
		{
			$this->GoStep('finish');
		}
		$this->previousStep = 'files';
		// Check is previous form is finished
		$previousForm = Yii::app()->session->get('OrderFilesForm');
		if(is_null($previousForm) || ! $previousForm['finished'])
		{
			// Go to previous step
			$this->GoStep($this->previousStep);
		}
		$this->nextStep = 'companies';
		$this->formTitle = Yii::t('phrase','Design and text of the letter');
		$orderContentFormModel = new OrderContentForm;
		if(isset($_POST['OrderContentForm']))
		{
			// Set model attributes
			$orderContentFormModel->attributes=$_POST['OrderContentForm'];
			
			// Ajax validation
			if(isset($_POST['ajax']) && $_POST['ajax']==='order-form-content')
			{
				echo CActiveForm::validate($orderContentFormModel);
				Yii::app()->end();
			}
			
			// Validate form
			if($orderContentFormModel->validate())
			{
				/**
				 *  Check for text existing in the body (not only tags)
				 */
				/**
				 * User text for email body
				 * @var string	
				 */		
				$body = $_POST['OrderContentForm']['body'];	
				$body = strip_tags($body);
				// Trim space symbols (include &nbsp)
				$body =  str_replace('&nbsp;', '', $body);
				$body = trim($body);
				// Check if body empty now
				if(empty($body))
				{
					$orderContentFormModel->addError('body', Yii::t('yii',"{attribute} cannot be blank.", array('{attribute}'=>'body')));
				}
				else
				{
					// Get attributes
					$orderContentForm = $orderContentFormModel->attributes;
					// Form is finished
					$orderContentForm['finished'] = true;
					// Store a text and a color templates
					$orderContentForm['textTemplate'] = $_POST['template-text'];
					$orderContentForm['colorTemplate'] = $_POST['template-color'];
					// Save data in session
					Yii::app()->session->add('OrderContentForm', $orderContentForm);
					// Go to next step
					$this->GoStep($this->nextStep);
				}
			}
		}
		
		// Load color schemes
		$orderContentFormModel->LoadColorSchemes();
		// Load email templates
		$orderContentFormModel->LoadEmailTemplates('cv');
		
		// Load attributes from existing session
		if(isset(Yii::app()->session['OrderContentForm']))
		{
			// Attributes
			$orderContentFormModel->attributes = Yii::app()->session['OrderContentForm'];
		}

		$this->render('stepContent', array('orderContentFormModel'=>$orderContentFormModel));
	}
	
	/**
	 * Order comanies selecting step
	 */
	public function ActionCompanies()
	{
        if( !isset(Yii::app()->session['OrderCompaniesForm']) ) {
            $session_companies = array('companies' => array(), 'finished' => '1');
            Yii::app()->session['OrderCompaniesForm'] = $session_companies;
        }

		// If order preview was submited go to finish step
		$orderPreviewForm = Yii::app()->session->get('OrderPreviewForm');
		if( ! is_null($orderPreviewForm) && $orderPreviewForm['finished'])
		{
			$this->GoStep('finish');
		}
		$this->previousStep = 'content';
		// Check is previous form is finished
		$previousForm = Yii::app()->session->get('OrderContentForm');
		if(is_null($previousForm) || ! $previousForm['finished'])
		{
			// Go to previous step
			$this->GoStep($this->previousStep);
		}
//		$this->nextStep = 'preview';
		$this->nextStep = 'payment';
		$this->formTitle = Yii::t('phrase', 'Selecting crewing agencies');
		$orderCompaniesFormModel = new OrderCompaniesForm;

		// Load company list
		$orderCompaniesFormModel->LoadCompanies();
		// Load work area filters
		$orderCompaniesFormModel->LoadWorkAreaFilters();
		// Load country filters
		$orderCompaniesFormModel->LoadCountryFilters();
		// Load region filters
		$orderCompaniesFormModel->LoadRegionFilters();
		// Load regions
		$orderCompaniesFormModel->LoadRegions();
		// Render form
        $workAreaFiltersPrice = array();
        foreach($orderCompaniesFormModel->workAreaFilters as $filter) {
            $workAreaFiltersPrice[] = array(
                'name' => $filter['name'],
                'price' => $this->getPriceByWorkarea($filter['name']),
                'count' => $this->getCompaniesCountByWorkarea($orderCompaniesFormModel->companiesList, $filter['name'])
            );
        }
        $orderCompaniesFormModel->workAreaFilters = $workAreaFiltersPrice;


        // pagination for companies

        $criteria = new CDbCriteria();
        $criteria->select = array('
            company,
            country,
            companies.name
        ');
        $criteria->join = '
            LEFT JOIN companies ON companies.id = company
        ';
        $criteria->condition = 'inactive=0';
        $criteria->order = 'companies.name ASC';

        $count = Office::model()->count($criteria);
        $pages = new CPagination($count);

        // results per page
        $pages->pageSize = 75;
        $pages->applyLimit($criteria);
        $models = Office::model()->findAll($criteria);

		Yii::import('application.controllers.CompanyController');	$offices = Office::model()->with('parentCompany')->findAll('`inactive` = 0');
		foreach($offices as $Office)
		{
			CompanyController::SaveTextToImg($Office->parentCompany->name, 170, 170, 170, 10, 'lib/skin/default/font/Segoe_UI.ttf'); // generate png image using company name (string)
			CompanyController::SaveTextToImg($Office->parentCompany->name, 33, 80, 100, 10, 'lib/skin/default/font/Segoe_UI.ttf', 'hover');
		}

        if(isset( Yii::app()->session['OrderCompaniesForm']['companies'] )) {
            $regions_from_session = $this->getSectorsByCompanies(Yii::app()->session['OrderCompaniesForm']['companies']);
            $counties_from_session = $this->getCountriesByCompanies(Yii::app()->session['OrderCompaniesForm']['companies']);
            $counties_regions_from_session = $this->getRegionsByCountry(Yii::app()->session['OrderCompaniesForm']['companies']);
        } else {
            $regions_from_session = array();
            $counties_from_session = array();
            $counties_regions_from_session = array();
        }

        $all_companies_count = count($this->getAllCompanies());


		$this->render('stepCompanies', array(
            'orderCompaniesForm' => $orderCompaniesFormModel,
            'models' => $models,
            'pages' => $pages,
            'regions_from_session' => $regions_from_session,
            'counties_from_session' => $counties_from_session,
            'all_companies_count' => $all_companies_count,
            'counties_regions_from_session' => $counties_regions_from_session
            )
        );
	}

    private function getCompaniesCountByWorkarea($companiesList, $workarea)
    {
        $count = 0;
        foreach($companiesList as $company)
        {
            foreach(explode(',', $company['sectors']) as $area)
            {
                $area === $workarea ? $count++ : $count;
            }
        }
        return $count;
    }
	
	/**
	 * Order data preview step
	 */
	public function ActionPreview()
	{
		// If order preview was submited go to finish step
		$orderPreviewForm = Yii::app()->session->get('OrderPreviewForm');
		if( ! is_null($orderPreviewForm) && $orderPreviewForm['finished'])
		{
			$this->GoStep('finish');
		}
		$this->previousStep = 'companies';
		// Check is previous form is finished

        if(empty(Yii::app()->session['OrderCompaniesForm']['companies'])) {
            $this->GoStep($this->previousStep);
        }

		$previousForm = Yii::app()->session->get('OrderCompaniesForm');
		if(is_null($previousForm) || ! $previousForm['finished'])
		{
			// Go to previous step
			$this->GoStep($this->previousStep);
		}
		$this->nextStep = 'payment';
		$this->formTitle = Yii::t('phrase', 'confirm data');
		$orderPreviewFormModel = new OrderPreviewForm;
		if(isset($_POST['OrderPreviewForm']))
		{
			// Set model attributes
			$orderPreviewFormModel->attributes = $_POST['OrderPreviewForm'];
			if($orderPreviewFormModel->validate())
			{
				// Save data in database
				if( ! $orderPreviewFormModel->SaveUserDataToDatabase())
				{
					Yii::log('Can not create new user!');
					return false;
				}
				// Get attributes
				$orderPreviewForm = $orderPreviewFormModel->attributes;
				// Form is finished
				$orderPreviewForm['finished'] = true;
				// Save data in session
				Yii::app()->session->add('OrderPreviewForm', $orderPreviewForm);
				// Go to next step
				$this->GoStep($this->nextStep);
			}
		}
		// Load the order content form's the color schemes
		$ContentModel = new OrderContentForm;
		// Load color schemes
		$ContentModel->LoadColorSchemes();

//        Yii::import('application.controllers.CompanyController');	$offices = $orderPreviewFormModel->companyList;
//        foreach($offices as $Office)
//        {
//            CompanyController::SaveTextToImg($Office['name'], 101, 112, 115, 10, 'lib/skin/default/font/Segoe_UI.ttf'); // generate png image using company name (string)
//        }

		// Render form
		$this->render('stepPreview', array('orderPreviewForm' => $orderPreviewFormModel, 'ContentModel' => $ContentModel));
	}
	
	/**
	 * Order payment step
	 */
	public function ActionPayment()
	{	
		 
		// Check is previous form is finished
		$previousForm = Yii::app()->session->get('OrderPreviewForm');
		if(is_null($previousForm) || ! $previousForm['finished'])
		{
			// Go to previous step
			$this->GoStep('preview');
		}
		$this->previousStep = '';
		$this->nextStep = 'finish';
		$this->formTitle = Yii::t('phrase','payment services');
		/**
		 * Current user info
		 * @var array
		 */
		$user = Yii::app()->session->get('user');
		/**
		 * Payment success flag. Default true (no payment error)
		 * @var bool
		 */
		$paymentSuccess = true;
		/**
		 * Current user order
		 * @var Order
		 */	
		 
		$Order = new Order;	
		
		$OrderAttr = $Order->findByPk(Yii::app()->session['order']['id']);			
		if(!empty($OrderAttr))
		{
			$ToStore = $OrderAttr->attributes;
			$ToStore['orderedCompaniesCount'] = Yii::app()->session['order']['orderedCompaniesCount'];
			Yii::app()->session['order'] = $ToStore;		
		}
		// If no stored order data
		if( ! $Order->Restore())
		{
			// Create new order
			$Order = $this->CreateOrder();			
		}
		// else 
		// {				
			// $OrderAttr = $Order->findByPk(Yii::app()->session['order']['id']);			
			// $ToStore = $OrderAttr->attributes;
			// $ToStore['orderedCompaniesCount'] = Yii::app()->session['order']['orderedCompaniesCount'];
			// Yii::app()->session['order'] = $ToStore;
			// print_r(Yii::app()->session['order']);
		// }
		// If successful order creation
		if( ! is_null($Order))
		{
			// Send notification email to user
			$this->SendUserNotification($user, $Order);
		}
		
		/**
		 * Interkassa shop id
		 * @var string
		 */
		$IKShopId = Yii::app()->Settings->Get('shopId', 'interkassa');
		// A currency is USD
		$Order->currency = 'USD';

		Yii::log('Order amount check: ' . $Order->number);

		// Render form
		$this->render('stepPaymentOrder', array('Order' => $Order, 'IKShopId' => $IKShopId, 'user' => $user));
	}/**
	 * Method sends email notification about order creation
	 * and account details to user
	 * @param array $user	User information
	 * @param Order $Order	Order information
	 * @return void
	 */
	public function SendUserNotification($user, $Order)
	{		
		// Chek parameters
		if( ! $user || ! $Order)
		{
			Yii::log(__FUNCTION__.' Wrong parameters');
			return false;
		}
		/**
		 * @var array $templateParameters			The parameters of the email template.
		 * @var EmailTemplate $Template				The template of an email about user order.
		 * @var array $queueParameters				The parameters of queue.
		 * @var bool $notified						A status of notifying to user.
		 * 
		 * Parameters lsit:
		 * $templateParameters['accountLink']		A link to the account of the user.
		 * $templateParameters['orderNumber']		A number of the user order.
		 * $templateParameters['orderAmount']		An amount of the user order.
		 */
		$templateParameters = array(
			'accountLink' => Yii::app()->request->getHostInfo() . '/account/login?user=' . $user['identificator'],
			'orderNumber' => $Order->number,
			'orderAmount' => number_format($Order->amount, 2, '.', ''),
            'user_id' => $user['id'],
            'lang' => Yii::app()->language
		);
		$Template = Emailtemplate::model()->findByAttributes(array('name' => "new-order-notify"));
		$queueParameters = array(
			'emails' => array($user['email']),
			'emailsTemplate' => $Template->id,
			'templateParameters' => $templateParameters
		);
		$notified = isset($user['notified']) ? $user['notified'] : false;
		// If user was not notified yet
		if( ! $notified)
		{
			// Add email to queue and immediately start it.
			Yii::app()->Email->Queue($queueParameters);
			//Yii::app()->Email->StartQueue();
			// Remember what user already queued for notifying.
			$user['notified'] = true;
			Yii::app()->session->add('user', $user);
					
		}
	}/**
	 * Create new order method
	 * @return Order Created order or false on failure
	 */
	public function CreateOrder()
	{	
		/**
		 * Current user info
		 * @var array
		 */
		$user = Yii::app()->session->get('user');
		/**
		 * Current user order
		 * @var Order
		 */
		$Order = new Order;	
		
		// Check for active user
		if(is_null($user))
		{
			// No active user
			Yii::log('No active user!');
			return false;
		}
	
		
		// Set order data
		$Order->GenerateNumber();			
		$Order->GetOrderedCompaniesCount();
		$Order->CalculateAmount();
		$Order->user = $user['id'];
		$Order->creationDate = date("Y-m-d H:i:s");
		$Order->currency = 'USD';
		
		// Try to create order
		if($Order->save())
		{
			/**
			 * Order data to save in session
			 * @var array
			 */
			$orderToStore = $Order->attributes;
			$orderToStore['orderedCompaniesCount'] = $Order->orderedCompaniesCount; // TODO check if it gets from $Order->attributes then remove this line
			// Save order data in session
			Yii::app()->session['order'] = $orderToStore;
			
			Yii::log('Order Session CreateOrder:' . json_encode($Order->attributes));
			
			// Return order
			return $Order;
		}
		else
		{
			// Order not created
			Yii::log('Saving order error!');
			return false;
		}
	}
	
	/**
	 * Finish step
	 */
	public function ActionFinish()
	{
        $this->formTitle = Yii::t('phrase','Finished');
		// Check is previous form is finished
		// $previousForm = Yii::app()->session->get('OrderPaymentForm');

		if( !Yii::app()->session['order']['paid'] )
		{
			// Go to previous step
			$this->GoStep('payment');
		}
		
//		 echo '<pre>' . print_r(Yii::app()->Email->GetImportantQueue(), true) . '</pre>';
//		 echo '<pre>' . print_r(Yii::app()->Email->GetNotImportantQueue(), true) . '</pre>';

//		Yii::app()->Email->StartQueue();

        $user = Yii::app()->session->get('user');

		// Render form
		$this->render('stepFinish', array('user' => $user));
	}
	
	/**
	 * Method resets user's stored data and form's completion statuses
	 * @param string $value Name of form to reset, if null - resets all forms
	 */
	public function ActionReset($value = null)
	{
		/*
		 * Remove session data
		 */
		// Remove all session data by default
		if(is_null($value))
		{
			foreach($this->dataToReset as $data)
			{
				Yii::app()->session->remove($data);
			}
		}
		// Or remove specified form only
		else
		{
			Yii::app()->session->remove($value);
		}
		
		/*
		 *  Remove user files
		 */
		// Get uploads path
		/*$uploadsPath = Yii::getPathOfAlias('uploads') . DIRECTORY_SEPARATOR . Yii::app()->session->getSessionID();
		// Check for file existing
		if(file_exists($uploadsPath))
		{
			$dir = opendir($uploadsPath);
			while($file = readdir($dir))
			{
				if(in_array($file, array('.', '..')))
				{
					continue;
				}
				// Delete file
				unlink($uploadsPath . DIRECTORY_SEPARATOR . $file);			
			}						
		}*/
		
		/**
		 *  Clear database
		 */
		// Create criteria condition
		/*
		$Criteria = new CDbCriteria;
		$Criteria->condition = 'identificator=:userIdentify';
		$Criteria->params = array(':userIdentify' => Yii::app()->session->getSessionID());
		// Try to get existing user
		$User = User::model()->find($Criteria);
		// If user exist - delete user
		if( ! is_null($User))
		{
			$User->delete();
		}
		*/
		
		// And redirect to home
		$this->redirect('/order');
	}
	
	/**
	 * Method move uploaded file and remeber file name in session.
	 * Override old file if exists.
	 * @param string $formName name of form in session
	 * @param string $fileFieldName name of file field in session
	 * @param CUploadedFile $file uploaded file
	 */
	public function SaveFile($formName, $fileFieldName, $file)
	{
		// Get form session data
		$form = Yii::app()->session->get($formName);
		// Get uploads path
		$uploadsPath = Yii::getPathOfAlias('uploads') . DIRECTORY_SEPARATOR . Yii::app()->session->getSessionID();		
		// Check for previously saved file name
		if( ! empty($form[$fileFieldName]))
		{
			// Check for file existing
			if(file_exists($uploadsPath . DIRECTORY_SEPARATOR . $form[$fileFieldName]))
			{
				// Delete file
				if(unlink($uploadsPath . DIRECTORY_SEPARATOR . $form[$fileFieldName]))
				{
					// Unset old session file name
					unset($form[$fileFieldName]);
					// Save form session data
					Yii::app()->session->add($formName, $form);
				}
			}
		}
		
		// Upload new file
		if($file->saveAs($uploadsPath . DIRECTORY_SEPARATOR . $file->name))
		{
			// Save new file name in session
			$form[$fileFieldName] = $file->name;
			// Save form session data
			Yii::app()->session->add($formName, $form);
		}
	}
	
	/**
	 * Method check if file which name was been remebered in session is exists. 
	 * If not - remebered file name will be unset
	 * @param string $formName name of form in session
	 * @param string $fileFieldName name of file field in session
	 */
	public function UnsetNonExistentFileName($formName, $fileFieldName)
	{
		// Get form session data
		$form = Yii::app()->session->get($formName);
		// Get uploads path
		$uploadsPath = Yii::getPathOfAlias('uploads') . DIRECTORY_SEPARATOR . Yii::app()->session->getSessionID();
		// Check for remembered file name on file field name session variable
		if( ! empty($form[$fileFieldName]))
		{
			// Check for file existing with that name
			if( ! file_exists($uploadsPath . DIRECTORY_SEPARATOR . $form[$fileFieldName]))
			{
				// Unset wrong session file name
				unset($form[$fileFieldName]);	
				// Save form session data
				Yii::app()->session->add($formName, $form);
			}
		}
	}

    private function getPriceByWorkarea($name)
    {
        $price = Yii::app()->db->createCommand()
            ->select('value')
            ->from('parameters')
            ->where('category = "price"')
            ->andWhere('name = :name', array(':name' => $name))
            ->queryRow();
        return $price['value'];
    }
	
	/**
	 * Method redirects to specified step
	 * @param string $step Step name
	 */
	public function GoStep($step)
	{
		// Get controller name
		$controller = Yii::app()->controller->getId();
		// Get url to specified step
		$stepUrl = $controller . '/' . $step;
		// Normalize url
		$stepUrl = CHtml::normalizeUrl(array($stepUrl));
		// Redirect to specified step
		$this->redirect($stepUrl);
	}

    public function getSectorsById($id) {

        $sectors = Yii::app()->db->createCommand()
            ->select('group_concat(officeworkareas.workArea separator \',\') as sectors')
            ->from('officeworkareas')
            ->where('officeworkareas.office = :id', array(':id' => $id))
            ->queryRow();
        return $sectors['sectors'];
    }
    public function getRegionsById($id) {
        $regions = Yii::app()->db->createCommand()
            ->select('group_concat(officeregions.region separator \',\') as sectors')
            ->from('officeregions')
            ->where('officeregions.office = :id', array(':id' => $id))
            ->queryRow();
        return $regions['sectors'];
    }

	/**
	* Check License agreement function
	*/
	public function CheckAgreement()
	{
		if(Yii::app()->controller->id == 'order' && Yii::app()->controller->action->id != 'license' and Yii::app()->controller->id == 'order' && Yii::app()->controller->action->id != 'files') 
		{
			if(!isset(Yii::app()->session['agree']))
			{
				$this->redirect('/order/license');
			}
		}
	}

     private function getSectorsByCompanies($Companies) {
        $sectors = Yii::app()->db->createCommand()
            ->selectDistinct('workArea')
            ->from('offices')
            ->leftJoin('officeworkareas', 'officeworkareas.office = offices.id')
            ->where(array('in', 'offices.id', $Companies))
            ->queryColumn();
         return $sectors;
    }

    public function getAllCompaniesBySector($sector) {
        $companies = Yii::app()->db->createCommand()
            ->selectDistinct('office')
            ->from('officeworkareas')
            ->where('workArea = :sector', array(':sector' => $sector))
            ->queryColumn();
        return array_diff($companies, $this->getAllInactiveCompanies());
    }

    private function getAllCompanies() {
        $companies = Yii::app()->db->createCommand()
            ->selectDistinct('company')
            ->from('offices')
            ->where('inactive = 0')
            ->queryColumn();
        return $companies;
    }

    public function getCountriesByCompanies($Companies) {
        $countries = Yii::app()->db->createCommand()
            ->selectDistinct('country')
            ->from('offices')
            ->where(array('in', 'offices.id', $Companies))
            ->queryColumn();
        return $countries;
    }

    public function getRegionsByCountry($orderedCompanies) {

        $countries = $this->getCountriesByCompanies($orderedCompanies);

        $regions = Yii::app()->db->createCommand()
            ->select('region')
            ->from('countries')
            ->where(array('in', 'countries.country', $countries))
            ->queryColumn();
        return $regions;
    }

    public function getAllInactiveCompanies() {
        $companies = Yii::app()->db->createCommand()
            ->selectDistinct('company')
            ->from('offices')
            ->where('inactive = 1')
            ->queryColumn();
        return $companies;
    }

    private function getCountriesByRegion($region) {
        $countries = Yii::app()->db->createCommand()
            ->selectDistinct('country')
            ->from('countries')
            ->where('countries.region = :region', array(':region' => $region))
            ->queryColumn();
        return $countries;
    }

    public function getAllCompaniesByRegion($region) {

        $countries = $this->getCountriesByRegion($region);

        $companies = Yii::app()->db->createCommand()
            ->selectDistinct('company')
            ->from('offices')
            ->where(array('in', 'offices.country', $countries))
            ->queryColumn();
        return array_diff($companies, $this->getAllInactiveCompanies());
    }

    public function getAllCompaniesByCountry($country) {
        $companies = Yii::app()->db->createCommand()
            ->selectDistinct('company')
            ->from('offices')
            ->where(array('in', 'offices.country', $country))
            ->queryColumn();
        return array_diff($companies, $this->getAllInactiveCompanies());
    }
    public function CheckIfAllInArray($values_array, $root_array) {
        foreach($values_array as $value) {
            if(!in_array($value, $root_array)) {
                return false;
            }
        }
        return true;
    }

    public function countMatchesInArrays($array1, $array2) {
        return count(array_intersect($array1, $array2));
    }
    public function countPercent($a, $b) {
        return round($a*100/$b, 2);
    }

    public function ActionAjaxChangeSession() {
        if (!isset($_POST['id'])) {return false;}

        if( isset(Yii::app()->session['OrderCompaniesForm']) ) {
            $session_companies = Yii::app()->session->get('OrderCompaniesForm');
        } else {
            $session_companies = array('companies' => array(), 'finished' => '1');
        }

        if( isset($_POST['status']) ) {
            if($_POST['status'] === "work-area") {
                if ($_POST['id'] === 'all') {

                    if( count(Yii::app()->session['OrderCompaniesForm']['companies']) === count($this->getAllCompanies()) ) {
                        $session_companies['companies'] = array();
                    } else {
                        $session_companies['companies'] = $this->getAllCompanies();
                    }

                } elseif($_POST['id'] === 'none') {
                    $session_companies['companies'] = array();
                } else {
                    if ( !$this->CheckIfAllInArray( $this->getAllCompaniesBySector($_POST['id']), Yii::app()->session['OrderCompaniesForm']['companies'] ) ) {
                        foreach($this->getAllCompaniesBySector($_POST['id']) as $comp_id) {
                            if( !in_array($comp_id, $session_companies['companies']) ) {
                                $session_companies['companies'][] = $comp_id;
                            }
                        }
                    } else {
                        foreach ($this->getAllCompaniesBySector($_POST['id']) as $comp_id) {
                            if (($key = array_search($comp_id, $session_companies['companies'])) !== false) {
                                unset($session_companies['companies'][$key]);
                            }
                        }
                    }
                }
                Yii::app()->session['OrderCompaniesForm'] = $session_companies;
            }
            elseif($_POST['status'] === "work-region") {

                if ( !$this->CheckIfAllInArray( $this->getAllCompaniesByRegion($_POST['id']), Yii::app()->session['OrderCompaniesForm']['companies'] ) ) {
                    foreach($this->getAllCompaniesByRegion($_POST['id']) as $comp_id) {
                        if( !in_array($comp_id, $session_companies['companies']) ) {
                            $session_companies['companies'][] = $comp_id;
                        }
                    }
                } else {
                    foreach ($this->getAllCompaniesByRegion($_POST['id']) as $comp_id) {
                        if (($key = array_search($comp_id, $session_companies['companies'])) !== false) {
                            unset($session_companies['companies'][$key]);
                        }
                    }
                }
                Yii::app()->session['OrderCompaniesForm'] = $session_companies;
            }
            elseif($_POST['status'] === "work-country") {

                if ( !$this->CheckIfAllInArray( $this->getAllCompaniesByCountry($_POST['id']), Yii::app()->session['OrderCompaniesForm']['companies'] ) ) {
                    foreach($this->getAllCompaniesByCountry($_POST['id']) as $comp_id) {
                        if( !in_array($comp_id, $session_companies['companies']) ) {
                            $session_companies['companies'][] = $comp_id;
                        }
                    }
                } else {
                    foreach ($this->getAllCompaniesByCountry($_POST['id']) as $comp_id) {
                        if (($key = array_search($comp_id, $session_companies['companies'])) !== false) {
                            unset($session_companies['companies'][$key]);
                        }
                    }
                }
                Yii::app()->session['OrderCompaniesForm'] = $session_companies;
            } elseif($_POST['status'] === "single") {
                if (!in_array($_POST['id'], $session_companies['companies'])) {
                    $session_companies['companies'][] = $_POST['id'];
                } else {
                    if (($key = array_search($_POST['id'], $session_companies['companies'])) !== false) {
                        unset($session_companies['companies'][$key]);
                    }
                }
                Yii::app()->session['OrderCompaniesForm'] = $session_companies;
            }
        } else {
            if (!in_array($_POST['id'], $session_companies['companies'])) {
                $session_companies['companies'][] = $_POST['id'];
            } else {
                if (($key = array_search($_POST['id'], $session_companies['companies'])) !== false) {
                    unset($session_companies['companies'][$key]);
                }
            }
            Yii::app()->session['OrderCompaniesForm'] = $session_companies;
        }

        if( isset($_POST['status']) ) {
            if( !isset(Yii::app()->session['OrderCompaniesForm']) ) {
                $session_companies = array('companies' => array(), 'finished' => '1');
                Yii::app()->session['OrderCompaniesForm'] = $session_companies;
            }

            // If order preview was submited go to finish step
            $orderPreviewForm = Yii::app()->session->get('OrderPreviewForm');
            if( ! is_null($orderPreviewForm) && $orderPreviewForm['finished'])
            {
                $this->GoStep('finish');
            }
            $this->previousStep = 'content';
            // Check is previous form is finished
            $previousForm = Yii::app()->session->get('OrderContentForm');
            if(is_null($previousForm) || ! $previousForm['finished'])
            {
                // Go to previous step
                $this->GoStep($this->previousStep);
            }
        //		$this->nextStep = 'preview';
            $this->nextStep = 'payment';
            $this->formTitle = Yii::t('phrase', 'Selecting crewing agencies');
            $orderCompaniesFormModel = new OrderCompaniesForm;

            // Load company list
            $orderCompaniesFormModel->LoadCompanies();
            // Load work area filters
            $orderCompaniesFormModel->LoadWorkAreaFilters();
            // Load country filters
            $orderCompaniesFormModel->LoadCountryFilters();
            // Load region filters
            $orderCompaniesFormModel->LoadRegionFilters();
            // Load regions
            $orderCompaniesFormModel->LoadRegions();
            // Render form
            $workAreaFiltersPrice = array();
            foreach($orderCompaniesFormModel->workAreaFilters as $filter) {
                $workAreaFiltersPrice[] = array(
                    'name' => $filter['name'],
                    'price' => $this->getPriceByWorkarea($filter['name']),
                    'count' => $this->getCompaniesCountByWorkarea($orderCompaniesFormModel->companiesList, $filter['name'])
                );
            }
            $orderCompaniesFormModel->workAreaFilters = $workAreaFiltersPrice;


            // pagination for companies

            $criteria = new CDbCriteria();
            $criteria->select = array('
                company,
                country,
                companies.name
            ');
            $criteria->join = '
                LEFT JOIN companies ON companies.id = company
            ';
            $criteria->condition = 'inactive=0';
            $criteria->order = 'companies.name ASC';

            $count = Office::model()->count($criteria);
            $pages = new CPagination($count);

            // results per page
            $pages->pageSize = 75;
            $pages->currentPage = $_POST['page'];
            $pages->applyLimit($criteria);
            $models = Office::model()->findAll($criteria);

            Yii::import('application.controllers.CompanyController');	$offices = Office::model()->with('parentCompany')->findAll('`inactive` = 0');
            foreach($offices as $Office)
            {
                CompanyController::SaveTextToImg($Office->parentCompany->name, 170, 170, 170, 10, 'lib/skin/default/font/Segoe_UI.ttf'); // generate png image using company name (string)
                CompanyController::SaveTextToImg($Office->parentCompany->name, 33, 80, 100, 10, 'lib/skin/default/font/Segoe_UI.ttf', 'hover');
            }

            if(isset( Yii::app()->session['OrderCompaniesForm']['companies'] )) {
                $regions_from_session = $this->getSectorsByCompanies(Yii::app()->session['OrderCompaniesForm']['companies']);
                $counties_from_session = $this->getCountriesByCompanies(Yii::app()->session['OrderCompaniesForm']['companies']);
                $counties_regions_from_session = $this->getRegionsByCountry(Yii::app()->session['OrderCompaniesForm']['companies']);
            } else {
                $regions_from_session = array();
                $counties_from_session = array();
                $counties_regions_from_session = array();
            }

            $all_companies_count = count($this->getAllCompanies());


            $filter = $this->renderPartial('stepCompaniesCatFilter', array(
                    'orderCompaniesForm' => $orderCompaniesFormModel,
                    'models' => $models,
                    'pages' => $pages,
                    'regions_from_session' => $regions_from_session,
                    'counties_from_session' => $counties_from_session,
                    'all_companies_count' => $all_companies_count,
                    'counties_regions_from_session' => $counties_regions_from_session
                ), true, false
            );
            echo $filter;

        } else {
            $orderedCompanies = Yii::app()->session['OrderCompaniesForm']['companies'];
            $sectors = $this->getSectorsByCompanies($orderedCompanies);
            $country = $this->getCountriesByCompanies($orderedCompanies);
            $regions = $this->getRegionsByCountry($orderedCompanies);

            echo $_POST['id'].','.implode(';',$sectors).','.implode(';', $country).','.implode(';', $regions);
        }

    }
}