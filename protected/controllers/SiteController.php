<?php

class SiteController extends Controller
{
	public $layout = 'main';
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
        return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'height'=>'37',
				'backColor'=>0x000000,
				'transparent'=>true,
				'foreColor'=>0x4895d7,
				'padding'=>0
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);

	}
	
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// Contact form handle
		$contactFormModel=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$contactFormModel->attributes=$_POST['ContactForm'];
			// Ajax validation for contact-form
			if(isset($_POST['ajax']) && $_POST['ajax']==='contact-form')
			{
				echo CActiveForm::validate($contactFormModel);
				Yii::app()->end();
			}
			// Non ajax validation of contact-form
			if($contactFormModel->validate())
			{
				// If all is ok send mail
				if($this->SendContactEmail($contactFormModel))
				{
					// Set successful message
					Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
					$this->refresh();
				}	
				else
				{
					// Set error message
					Yii::app()->user->setFlash('contact','Sorry, error on sednig, please try again later.');
					$this->refresh();
				}
			}
		}
		
		// Order files form handle
		$orderFilesFormModel=new OrderFilesForm;
		if(isset($_POST['OrderFilesForm']))
		{
			$orderFilesFormModel->attributes=$_POST['OrderFilesForm'];
            $orderFilesFormModel->CVFile=CUploadedFile::getInstance($orderFilesFormModel,'CVFile');
            
			//$orderFilesFormModel->CVFile->saveAs('path/to/localFile');
		}
		
		// Get orders file form session data
		$orderFilesForm = Yii::app()->session->get('OrderFilesForm');
		if( !is_null($orderFilesForm))
		{
			// Load attributes from existing session
			$orderFilesFormModel->email = $orderFilesForm['email'];
		}
		
		$IndexDataModel = new IndexData();
		// Load count company 
		$IndexDataModel->loadOfficesQty();
		$IndexDataModel->loadCountryQty();
		// Render page
		$this->render('index', array('contactFormModel'=>$contactFormModel, 'orderFilesFormModel'=>$orderFilesFormModel,'indexDataModel'=>$IndexDataModel));
			
	}
	
	/**
	 * Send email from contact form
	 * @param ContactForm $contactFormModel
	 * @return bool Email sending successful
	 */
	private function SendContactEmail($contactFormModel)
	{
		/**
		 * Email parameters
		 * @var Array
		 */
		$emailParams = array(
			'fromEmail' => $contactFormModel->email,
			'fromName' => $contactFormModel->name,
			'to' => Yii::app()->Settings->Get('email', 'admin'),
			'subject' => Yii::app()->Settings->Get('contactEmailSubject', 'site'),
			'body' => $contactFormModel->body
		);
		// Return send successful
		return Yii::app()->Email->Send($emailParams);
	}
	
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
	public function actionLanguage($value = null)
	{
		// If value not set then return current language
		if(is_null($value))
		{
			return Yii::app()->language;
		}
		/**
		 * List of acessible languages
		 * @var array
		 */
		$aviableLangs = Yii::app()->params['availableLanguages'];
		// Check if specified language is in aviabled languages
		if( in_array($value, $aviableLangs))
		{
			// If it is, then remember this language
			Yii::app()->session->add('language', $value);
			// Create cookie object
			$cookie = new CHttpCookie('language', $value);
			// Set expire date in two months
			$cookie->expire = time() + 60 * 60 * 24 * 60; 
			// Wrtite cookie
			Yii::app()->request->cookies['language'] = $cookie;
		}
		// Else remember default language
		else
		{
			Yii::app()->session->add('language', 'ru');
			// Create cookie object
			$cookie = new CHttpCookie('language', 'ru');
			// Set expire date in two months
			$cookie->expire = time() + 60 * 60 * 24 * 60; 
			// Wrtite cookie
			Yii::app()->request->cookies['language'] = $cookie;
		}		
		// And redirect to previous page
		$this->redirect(Yii::app()->request->getUrlReferrer());
	}
	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
        // Contact form handle
        $contactFormModel=new ContactForm;
        if(isset($_POST['ContactForm']))
        {
            $contactFormModel->attributes=$_POST['ContactForm'];
            // Ajax validation for contact-form
            if(isset($_POST['ajax']) && $_POST['ajax']==='contact-form')
            {
                echo CActiveForm::validate($contactFormModel);
                Yii::app()->end();
            }
            // Non ajax validation of contact-form
            if($contactFormModel->validate())
            {
                // If all is ok send mail
                if($this->SendContactEmail($contactFormModel))
                {
                    // Set successful message
                    Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
                    $this->refresh();
                }
                else
                {
                    // Set error message
                    Yii::app()->user->setFlash('contact','Sorry, error on sednig, please try again later.');
                    $this->refresh();
                }
            }
        }

		$this->render('contact',array('contactFormModel'=>$contactFormModel));
	}

	/**
	 * Test action.
	 * @todo Delete after the tests.
	 */
	public function actionTest()
	{
		/* Email QUeue test
		echo 'This is test page for email queue. Remove this action after the tests.<br /><br />';
		$parameters['emails'] = array('kul_13@mail.ru', 'demnarctis@gmail.com', 'zamihovskiy@outsourcing-ukraine.com');
		$parameters['emailsTemplate'] = 1;
		$parameters['templateParameters'] = array('new'=>'yes', 'name' => 'Leo');
		if( ! Yii::app()->Email->Queue($parameters))
		{
			Yii:log('Email queue error', CLogger::LEVEL_ERROR);
		}
		 */
		
		
		/* Email StartQueue test */
		//Yii::app()->Email->StartQueue();
		
		
		/* OrderExecution test 
		$Order = Order::model()->findByPk(36);
		if($Order->executed == 0)
		{
			$Order->Execute();
		}
		 */
		
		
		/* Ordered sectors price test
		$Order = Order::model()->findByPk(44);
		print_r($Order->GetOrderedSectors());
		print_r($Order->CalculateAmount());
		 */
		
		
	}
}