<?php
/**
 * Controller for user's account
 * @property bool $viewStatus Flag for order status view detecting
 */
class AccountController extends Controller
{
	public $viewStatus = false;
	/**
	 * Current user data
	 * @var array
	 */
	public $user;
	/**
	 * Override layout to use order form layout
	 * @var string
	 */
	public $layout = 'account';
	/**
	 * Account page title
	 * @var string 
	 */
	public $title = '';
	
	/**
	 * Checks user auth befor all actions
	 */
	public function BeforeAction($action)
	{
		/**
		 * Active user
		 * @var array
		 */
		$storedUser = Yii::app()->session->get('user');
		/**
		 * Flag for active login page
		 * @var bool
		 */
		$loginPageActive = $action->id === 'login';
		/**
		 * User authorization flag
		 * @var boll
		 */
		$userAuthorized = ! is_null($storedUser) && ! empty($storedUser['authorized']);
		// If not login page and user not authorized
		if( ! $loginPageActive && ! $userAuthorized )
		{
			// Redirect to home
			$this->redirect('/');
		}
		else
		{
			$this->user = $storedUser;
			return true;
		}
	}
	
	/**
	 * Page with general info about user and his order
	 */
	public function actionIndex()
	{
		$this->title = Yii::t('phrase','order details');;
		/**
		 * User's order general data form.
		 * Automatically load all data when created
		 * @var AcAccountGeneralForm
		 */
		$GeneralForm = new AccountGeneralForm;
		/**
		 * Companies form model. Used for loading all companies list.
		 * @var OrderCompaniesForm
		 */
		$CompaniesForm = new OrderCompaniesForm;
		$CompaniesForm->LoadCompanies();
		$GeneralForm->companyList = $CompaniesForm->companiesList;
		// Set companies list to general form
		$GeneralForm->LoadGeneralData();
		// Load the order content form's the color schemes
		$ContentModel = new OrderContentForm;
		// Load color schemes
		$ContentModel->LoadColorSchemes();

//        Yii::import('application.controllers.CompanyController');	$offices = $GeneralForm->companyList;
//        foreach($offices as $Office)
//        {
//            CompanyController::SaveTextToImg($Office['name'], 101, 112, 115, 10, 'lib/skin/default/font/Segoe_UI.ttf'); // generate png image using company name (string)
//        }

		$this->render('index', array('GeneralForm' => $GeneralForm, 'ContentModel' => $ContentModel));
	}

	/**
	 * Method for user authorization
	 */
	public function actionLogin()
	{
		/**
		 * User identificator from request
		 * @var string
		 */
		$userIdentify = $_GET['user'];
		/**
		 * Existing user or null
		 * @var User
		 */
		$User = User::model()->find('identificator = :userIdent', array('userIdent' => $userIdentify));
		// Check user existing
		if( ! is_null($User))
		{
			/*
			 * Auth is ok. Set user as authorized
			 * and load user data
			 */
			
			/*
			 * Remember user
			 */
			// Set authorized status
			$User->authorized = true;
			// Save user data in session
			Yii::app()->session->add('user', $User->attributes);
			// Redirect to account
			$this->redirect('/account');			
		}
		else
		{
			/*
			 * Auth failed
			 */
			
			// Redirect to home
			$this->redirect('/');
		}
	}	

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		// Destroy session
		Yii::app()->session->destroy();
		// Redirect to home
		$this->redirect('/');
	}


    public function getNameByIdInArray($id, $array) {
        foreach($array as $arr) {
            if($arr['id'] === $id) {
                return $arr['name'];
            }
        }
    }
	/**
	 * Display page with user order data or order status
	 * @param string $vaue	If given display additional order info such as status
	 */
	public function actionOrder($value = null)
	{
		$this->title = Yii::t('phrase','account details');
		/**
		 * User order
		 * @var Order
		 */
		$Order = Order::model()->find('user = :userId', array('userId' => $this->user['id']));
		if(is_null($Order))
		{
			// No user order
			return false;
		}
		if($value === 'status')
		{
			$this->viewStatus = true;
			$this->title = Yii::t('phrase','notification companies');

//            print_r($Order->GetMailedCompanies());
//            Yii::app()->end();

            Yii::import('application.controllers.CompanyController');	$offices = $Order->GetMailedCompanies();
            foreach($offices as $Office)
            {
                CompanyController::SaveTextToImg($Office['name'], 101, 112, 115, 10, 'lib/skin/default/font/Segoe_UI.ttf'); // generate png image using company name (string)
            }

            $session_user = Yii::app()->session['user'];

            $criteria = new CDbCriteria();
            $criteria->select ='*';
            $criteria->join = '
                JOIN emailQueueParams ON emailQueueParams.emailsQueueId = queueId
                JOIN offices ON offices.email = address
			    JOIN companies ON offices.company = companies.id
            ';
            $criteria->condition = 'emailQueueParams.name = "user" AND inQueue = 0 AND emailQueueParams.value = :user_id';
            $criteria->params = array(':user_id'=>$session_user['id']);


            $count = EmailQueue::model()->count($criteria);
            $pages = new CPagination($count);

            // results per page
            $pages->pageSize = 10;
            $pages->applyLimit($criteria);
            $models = EmailQueue::model()->findAll($criteria);


			$this->render('orderStatus', array(
                    'Order' => $Order,
                    'models' => $models,
                    'pages' => $pages
                )
            );
		}
		else
		{
			// Display order details page
			$Order->GetOrderedCompaniesCount();
			/**
			* Interkassa shop id
			* @var string
			*/
			$IKShopId = Yii::app()->Settings->Get('shopId', 'interkassa');
			// Render
			$this->render('order', array('Order' => $Order, 'IKShopId' => $IKShopId));
		}
	}
}