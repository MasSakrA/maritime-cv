<?php

class PaymentController extends Controller
{
	/**
	 * Default action
	 */
	public function ActionIndex()
	{
		// TODO
	}



	/**
	 * The handle an interaction request from interkassa. 
	 * 
	 * Handles the request from the Interkassa and marks
	 * the order as the paid, if the payment was successful.
	 * 
	 * @return bool
	 */
	public function ActionInteraction()
	{
		Yii::log('PAYMENT Interaction start');
		// Test log for the background request

        //Yii::log('POST: ' . json_encode($_POST));
        $Order = Order::model()->find('number = :number', array(':number' => $_POST['ik_pm_no']));
        // Check for the order existing
        if(is_null($Order))
        {
            Yii::log('The order is null');
            return false;
        }

		/**
		 * Validate the Interkassa request.
		 * 
		 * Check what the request from the Interkassa and
		 * the request is valid and the request status is "success".
		 * If the request is valid then mark the order as paid.
		 * 
		 * @var Order	$Order		The order which must be marked as paid up.
		 */
		if($this->ValidateIkResponse($_POST, $Order)) {

            Yii::log('Request from Interkassa accepted');

			/**
			 *  Set the order as paid and start order execution
			 */
			$Order->paid = 1;		
			
			
			// Database
			if( ! $Order->save() )
			{
				Yii::log('The order saving database error');
				return false;
			}

			// Add order to session (if it's exists, first it is removed)
			// $order = Yii::app()->session->get('order');
			// $order['paid'] = 1;
			// Yii::app()->session->add('order', $order);	
			// Yii::log('Order Session B:' . json_encode(Yii::app()->session->get('order')));			
			
		    // Save order data in session		
			
		    Yii::app()->session['order'] = $Order->attributes;			
			Yii::log('Order Session A:' . json_encode(Yii::app()->session));
			
			// Start execution
			if($Order->executed == 0)
			{
                Yii::log('Execute');
				$Order->Execute(); // TODO: uncomment on production
			}

			// Response success header for the background request from the Interkassa.
			// Temporary return true
			if(Yii::app()->controller->action->id == 'interaction')
				Yii::app()->end(200);

			return true;
		}	
		else
		{
			// Response success header for the background request from the Interkassa.
			// Uncomment when it will be work. Temporary return false.
			// Yii::app()->end(500);

            Yii::log('Failed request from Interkassa: ' . json_encode($_POST));

			return false;
		}	
    }



    /**
	 * Shows success result page
	 */
	public function ActionSuccess()
	{
		// TODO Handle request and show success message
//		if($this->ActionInteraction())
		if(true)
		{
			$message = 'OK';
		}
		else
		{
			$message = 'NOT OK';
		}
		$this->render('success',array('message' => $message));
	}



    /**
	 * Shows pending result page
	 */
	public function ActionPending()
	{
		// TODO Handle request and show pending result message
		$this->render('pending');
	}



	/**
	 * Shows fialure result page
	 */
	public function ActionFail()
	{
		// TODO Handle request and show failure message
		$this->render('fail');
	}



    /**
     * Validation of a request from an Interkassa
     * @param bool $ikResponse
     * @return bool Validation status
     */
	public function ValidateIkResponse($ikResponse = false, $Order = false)
	{
        /**
		 * Get an request data.
		 *
		 * Get only the data from an Interkassa request.
		 *
		 * @var string[]		$IKRequest			Only the Interkassa request data.
		 * @var string|bool		$signature			A calculated signature from the request data. False on fail.
		 * @var string			$IKSignature		A signature from the request.
		 * @var bool			$IKRequestValid		A validation status of the request.
		 */

        if ($ikResponse === false || $Order === false) return false;
        $isValid = true;

        /**
         * Create a set of matches.
         */
        $matches = array(
            'signKey' => (isset($ikResponse['ik_sign']) && $ikResponse['ik_sign'] === $this->GetSignKey($ikResponse)),
            'coId' => ($ikResponse['ik_co_id'] == Yii::app()->Settings->Get('shopId', 'interkassa')),
            'amount' => ($ikResponse['ik_am']-0 == $Order->amount),
            'state' => ($ikResponse['ik_inv_st'] == 'success'),
        );
        // The check that all data is matched
        foreach($matches as $key => $match)
        {
            // If one of the comparisons is wrong
            if(!$match)
            {
                // Then all request is not valid
                $isValid = false;
                Yii::log("Interkassa request is not valid! Failed parameter: name -> $key; value -> $match");
                // Break the check
                break;
            }
        }
		
		// Return the validation status
		return $isValid;
	}


    /**
     * Generate sign key as described in Interkassa manual (https://www.interkassa.com/technical-documentation)
     * @param $response
     * @return string
     */
    private function GetSignKey($response)
    {
        unset($response['ik_sign']);
        ksort($response, SORT_STRING);
        $key = Yii::app()->Settings->Get('signKey', 'interkassa');
        array_push($response, $key);
        $signString = implode(':', $response);
        return base64_encode(md5($signString, true));
    }



	/**
	 * Creation payment
	 * @return bool Payment creation success
	 */
	public function CreatePayment()
	{
		/**
		 * Payment success
		 * @var bool
		 */
		$paymentSuccess = false;
		/**
		 * Order number from interkassa request
		 * @var string
		 */
		$orderNumber = $_POST['ik_pm_no'];
		/**
		 * Condition for order finding
		 * @var CDbCriteria
		 */
		$Condition = new CDbCriteria();
		$Condition->condition = 'number = :number';
		$Condition->params = array('number' => $orderNumber);
		/**
		 * Existing order or null
		 * @var Order
		 */
		$Order = Order::model()->find($Condition);
		if(is_null($Order))
		{
			// Order not exists
			$paymentSuccess = false;
			Yii::log('Requested order #'. $orderNumber .' not exists in database!' );
		}
		/**
		 * Paid amount from interkassa request
		 * @var float
		 */
		$paidAmount = (float) $_POST['ik_am'];
		/**
		 * Payment currency from interkassa request
		 * @var string
		 */
		$currency = $_POST['ik_cur'];
		// If currency already set in order and currency not coincides
		if( ! is_null($Order->currency) && $Order->currency != $currency)
		{
			$paymentSuccess = false;	
			Yii::log('Interkassa currency '. $currency . ' and order currency ' . $Order->currency . ' not coincides. Order #'. $orderNumber );
		}
		/**
		 * Payment from interkassa
		 * @var Payment
		 */
		$Payment = new Payment;
		$Payment->order = $Order->id;
		$Payment->amount = $_POST['ik_co_rfn'];
		$Payment->ikTransaction = $_POST['ik_trn_id'];
		$Payment->method = $_POST['ik_pw_via'];
		$Payment->date = $_POST['ik_inv_prc'];
		$Payment->status = $_POST['ik_inv_st'];
		if( ! $Payment->save())
		{
			// Can not create payment
			$paymentSuccess = false;
			Yii::log('Can not create payment! Order #' . $orderNumber .' Interkassa payment #' . $_POST['ik_inv_id']);
		}
		// Retun payment success
		return $paymentSuccess;
	}
}
