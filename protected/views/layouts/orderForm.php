<?php /* @var $this Controller */ ?>

<?php $this->beginContent('//layouts/main'); ?>

<div id="banner" class="container-fluid banner-0">
<div class="container">

	<div class="row-fluid-5">
		<!-- <div class="span1">
			<div class="square-box">
	
				<div class="square-content">
				<div class="dt">
				<div class="dc">
								
					<div class="glyph">
						<span class="glyph-item mega" aria-hidden="true" data-icon="&#xe034;" data-js-prompt="&#xe034;"></span>
					</div>
		
				</div>
				</div>
				</div>
					
			</div>	
		</div>
		<div class="span4">
			
			
		</div> -->
	</div>

</div>
</div>
<div id="wizard" class="container-fluid">
<?php if(Yii::app()->controller->id == 'order' && Yii::app()->controller->action->id != 'license') { ?>
<div class="container">
<?php
/**
 * Current wizzard action
 * @var string
 */
$action = Yii::app()->controller->action->id;
/**
 * Current class name
 * @var string
 */
$currentClassName = 'current';
/**
 * Variable has current class name if current action is that step,
 * else it must be empty
 * @var string
 */
$currentClass = '';
/**
 * Href of each step, if step is current then href must be empty.
 * @var string
 */
$currentHrefTag = '';
/**
 * Order controller url
 * @var string
 */
$orderControllerUrl = '/order';

?>
<div class="row-fluid-5">
	
	<?php
	// Detect is step below is current action
	if($action === 'files' || $action === 'index')
	{
		$currentClass = $currentClassName;
		$currentHrefTag = '';
	}
	else
	{
		$currentClass = '';
		$currentHrefTag = 'href="' . $orderControllerUrl . '/files"';
	}
	?>
	<div class="span1 <?php echo $currentClass; ?>">
	<a <?php echo $currentHrefTag; ?>>
		<div class="glyph">
			<span class="glyph-item mega" aria-hidden="true" data-icon="&#xe034;" data-js-prompt="&#xe034;"></span>
		</div>
		<h2><?php echo Yii::t('word', 'Step', 1); ?> 1.</h2>
		<p><?php echo Yii::t('phrase', 'CV and Attachments'); ?></p>
	</a>
	</div>
	
	<?php
	// Detect is step below is current action
	if($action === 'content')
	{
		$currentClass = $currentClassName;
		$currentHrefTag = '';
	}
	else
	{
		$currentClass = '';
		$currentHrefTag = 'href="' . $orderControllerUrl . '/content"';
	}
	?>
	<div class="span1 <?php echo $currentClass; ?>">
	<a <?php echo $currentHrefTag; ?>>
		<div class="glyph">
			<span class="glyph-item mega" aria-hidden="true" data-icon="&#xe05f;" data-js-prompt="&#xe05f;"></span>
		</div>
		<h2><?php echo Yii::t('word', 'Step', 1); ?> 2.</h2>
		<p><?php echo Yii::t('phrase', 'Design and Content'); ?></p>
	</a>
	</div>
	
	<?php
	// Detect is step below is current action
	if($action === 'companies')
	{
		$currentClass = $currentClassName;
		$currentHrefTag = '';
	}
	else
	{
		$currentClass = '';
		$currentHrefTag = 'href="' . $orderControllerUrl . '/companies"';
	}
	?>
	<div class="span1 <?php echo $currentClass; ?>">
	<a <?php echo $currentHrefTag; ?>>
		<div class="glyph">
			<span class="glyph-item mega" aria-hidden="true" data-icon="&#xe003;" data-js-prompt="&#xe003;"></span>
		</div>
		<h2><?php echo Yii::t('word', 'Step', 1); ?> 3.</h2>
		<p><?php echo Yii::t('phrase', 'Crewing Companies'); ?></p>
	</a>
	</div>
	
	<?php
	// Detect is step below is current action
	if($action === 'payment')
	{
		$currentClass = $currentClassName;
		$currentHrefTag = '';
	}
	else
	{
		$currentClass = '';
		$currentHrefTag = 'href="' . $orderControllerUrl . '/payment"';
	}
	?>
	<div class="span1 <?php echo $currentClass; ?>">
	<a <?php echo $currentHrefTag; ?>>
		<div class="glyph">
			<span class="glyph-item mega" aria-hidden="true" data-icon="&#xe02a;" data-js-prompt="&#xe02a;"></span>
		</div>
		<h2><?php echo Yii::t('word', 'Step', 1); ?> 4.</h2>
		<p><?php echo Yii::t('phrase', 'Order Payment'); ?></p>
	</a>
	</div>
	
	<?php
	// Detect is step below is current action
	if($action === 'finish')
	{
		$currentClass = $currentClassName;
	}
	else
	{
		$currentClass = '';
	}
	?>
	<div class="span1 last <?php echo $currentClass; ?>">
		<div class="glyph">
			<span class="glyph-item mega" aria-hidden="true" data-icon="&#xe01f;" data-js-prompt="&#xe01f;"></span>
		</div>
		<h2><?php echo Yii::t('word', 'Step', 1); ?> 5.</h2>
		<p><?php echo Yii::t('phrase', 'Email Campaign'); ?></p>
	</div>
</div>

</div>
<?php } ?>
</div>

 
<div class="container-fluid section light">
<div class="container">

	<div class="row-fluid-5">
		<div class="span5">
		<h1 class="page-header"><?php echo $this->formTitle; ?></h1>
		</div>
	</div>
	
	<?php echo $content; ?>

</div>
</div>

<?php $this->endContent(); ?>

<script>
    $('input.reset').on('click', function() {
        var r = confirm("<?php echo Yii::t('phrase', 'reset text'); ?>");
        if (r == true) {
            js:location.href='/order/reset'
        }
    });
</script>