<?php /* @var $this Controller */ ?>

<?php $this->beginContent('//layouts/main'); ?>

<div id="banner" class="container-fluid banner-0">
<div class="container">

	<div class="row-fluid-5">
		<!-- <div class="span1">
			<div class="square-box">
	
				<div class="square-content">
				<div class="dt">
				<div class="dc">
								
					<div class="glyph">
						<span class="glyph-item mega" aria-hidden="true" data-icon="&#xe034;" data-js-prompt="&#xe034;"></span>
					</div>
		
				</div>
				</div>
				</div>
					
			</div>	
		</div>
		<div class="span4">
			
			
		</div> -->
	</div>
</div>
</div>

<?php
/**
 * Current wizzard action
 * @var string
 */
$action = Yii::app()->controller->action->id;
/**
 * Current class name
 * @var string
 */
$currentClassName = 'current';
?>

<div id="wizard" class="container-fluid">
	<div class="container">
		<div class="row-fluid-5">
		
<?php
// Detect is step below is current action
if($action === 'index')
{
	$currentClass = $currentClassName;
}
else
{
	$currentClass = '';
}
?>
			<div class="span1 <?php echo $currentClass; ?>">
				<a href="/account">
					<div class="glyph">
						<span class="glyph-item mega" aria-hidden="true" data-icon="&#xe08b;" data-js-prompt="&#xe08b;"></span>
					</div>
					<h2><?php echo Yii::t('word', 'Order', SINGULAR); ?></h2>
					<p><?php echo Yii::t('phrase', 'order details'); ?></p>
				</a>
			</div>
<?php
if($action === 'order' && ! $this->viewStatus)
{
	$currentClass = $currentClassName;
}
else
{
	$currentClass = '';
}
?>
			<div class="span1 <?php echo $currentClass; ?>">
				<a href="/account/order">
					<div class="glyph">
						<span class="glyph-item mega" aria-hidden="true" data-icon="&#xe02a;" data-js-prompt="&#xe02a;"></span>
					</div>
					<h2><?php echo Yii::t('word', 'payment'); ?></h2>
					<p><?php echo Yii::t('phrase','payment status') ?></p>
				</a>
			</div>
<?php
if($action === 'order' && $this->viewStatus)
{
	$currentClass = $currentClassName;
}
else
{
	$currentClass = '';
}
?>
			<div class="span1  <?php echo $currentClass; ?>">
				<a href="/account/order/status">
					<div class="glyph">
						<span class="glyph-item mega" aria-hidden="true" data-icon="&#xe04c;" data-js-prompt="&#xe04c;"></span>
					</div>
					<h2><?php echo Yii::t('word', 'report'); ?></h2>
					<p><?php echo Yii::t('phrase', 'on the implementation of services'); ?></p>
				</a>
			</div>
		</div>
	</div>
</div>

<div class="container section light">
	<div class="row-fluid-5">
		<div class="span5">
		<h1 class="page-header"><?php echo $this->title; ?></h1>
		</div>
	</div>
	
	<?php echo $content; ?>

</div>


<?php $this->endContent(); ?>