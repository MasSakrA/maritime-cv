<?php 
	/**
	 * Method checks if the specified menu is currentlyactive and set it as active if it is true.
	 * Use it in inline html code of menu
	 * @param string|bool $controllerName		Name of a controller which must be checked as active menu
	 * @param string|bool $actionName			Name of a controller action which must be checked as active menu
	 */
	function SetActiveMenu($controllerName = 'site', $actionName = false)
	{
		/**
		 * Current controller action
		 * @var string
		 */
		$currentController = Yii::app()->controller->id;
		/**
		 * Current controller action
		 * @var string
		 */
		$currentAction = Yii::app()->controller->action->id;
		/**
		 * Active menu class name
		 * @var string
		 */
		$activeClass = 'active';
		/**
		 * Current menu
		 * @var string
		 */
		$currentMenu = $currentController;
		if($actionName !== false)
		{
			$currentMenu .= $currentAction;
		}
		/**
		 * Given menu
		 * @var string
		 */
		$menu = $controllerName;
		if($actionName !== false)
		{
			$menu .= $actionName;
		}
		/**
		 * Check is the specified menu is an active menu
		 */
		if( $menu === $currentMenu)
		{
			// Wrtie active class name
			echo $activeClass;
		}
	}

    function isCurrentCA($controller, $action) {
        if(Yii::app()->controller->id === $controller and Yii::app()->controller->action->id === $action) {
            return true;
        } else {
            return false;
        }

    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<link rel="shortcut icon" href="/lib/assets/icon/favicon.ico" />
	<link rel="stylesheet" href="/lib/bootstrap/css/bootstrap.min.css" />
	<link rel="stylesheet" href="/lib/bootstrap/custom/row-fluid-5.min.css" />
    <?php if(isCurrentCA('order', 'companies')) { ?>
    <link rel="stylesheet" href="/lib/bootstrap/custom/row-fluid-12.min.css" />
    <?php } ?>
    <?php if(isCurrentCA('site', 'index')) { ?>
	<link rel="stylesheet" href="/lib/bootstrap/custom/carousel.css" />
    <?php } ?>
	<link rel="stylesheet" href="/lib/skin/default/style.css" />
	
	<!-- The Yii jQuery including -->
	<?php Yii::app()->clientScript->registerCoreScript('jquery.ui') ?>
	<!-- The Bootstrap javascript -->
    <?php if(isCurrentCA('order', 'content') or isCurrentCA('site', 'index')) { ?>
    <script src="/lib/bootstrap/js/bootstrap.min.js"></script>
    <?php } ?>
	<script src="/lib/core.js"> </script>
</head>
<body>

<div id="navigation" class="navbar navbar-default navbar-static-top" role="navigation">
<div class="container">
    <div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="/">
			<h2><span class="text-blue">Maritime</span> <span class="text-orange">CV</span></h2>
		</a>
	</div>
	<div class="navbar-collapse collapse">
		<ul class="nav navbar-nav">
			<li class="<?php SetActiveMenu('site', 'index'); ?>"><a href="/">Maritime CV</a></li>
<!--			<li class="<?php SetActiveMenu('site', 'company'); ?>"><a href="/company"><?php echo Yii::t('phrase', 'list of companies'); ?></a></li>-->
			<li class="<?php SetActiveMenu('site', 'contact'); ?>"><a href="/contact"><?php echo Yii::t('word', 'contact'); ?></a></li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
            <li <?php echo Yii::app()->language === 'ru' ? 'class="active"' : ''; ?>><a href="/site/language/ru"><?php echo Yii::t('word', 'RU'); ?></a></li>
            <li <?php echo Yii::app()->language === 'en' ? 'class="active"' : ''; ?>><a href="/site/language/en"><?php echo Yii::t('word', 'EN'); ?></a></li>
		</ul>
	</div>
</div>
</div>

	<?php 
	// In this place shows content of layout
	echo $content; 
	?>
	

<div id="footer">
<div class="container">
	<ul class="list-inline pull-left">
		<li><a href="/">Maritime CV</a></li>
<!--		<li><a href="#"><?php echo Yii::t('phrase', 'list of companies'); ?></a></li>-->
		<li><a href="/contact"><?php echo Yii::t('word', 'contact'); ?></a></li>
	</ul>
	<ul class="list-inline pull-right">
		<li>Maritime CV &copy; 2014 <?php echo Yii::t('phrase', 'all rights reserved'); ?></li>
	</ul>
	<div class="clearfix"></div>
</div>
</div>	

</body>
</html>
