<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->pageTitle=Yii::app()->name . ' - Contact Us';
$this->breadcrumbs=array(
	'Contact',
);
?>
<div id="banner" class="container-fluid banner-0 contact">
    <div class="container">
        <div class="row-fluid-5">
        </div>
    </div>
</div>
<div class="container-fluid section contact-page">
    <div id="section-contact" class="container">
        <h1><?php echo Yii::t('phrase', 'Contact Us'); ?></h1>

        <?php if(Yii::app()->user->hasFlash('contact')): ?>

        <div class="flash-success">
            <?php echo Yii::app()->user->getFlash('contact'); ?>
        </div>

        <?php else: ?>

        <p>
        <?php echo Yii::t('phrase', 'If you have business inquiries or other questions, please fill out the following form to contact us. Thank you.'); ?>
        </p>

        <?php $this->renderPartial('forms/contactForm', array('contactFormModel'=>$contactFormModel)); ?>
    </div>
</div>
<?php endif; ?>