<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;

//$this->renderPartial('forms/orderInitialForm', array('orderFilesFormModel'=>$orderFilesFormModel));
$data = $indexDataModel;
?>

<!-- CAROUSEL -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">

    <!-- slides -->
    <div class="carousel-inner">
        <div id="slide-0" class="item active">
            <div class="container">
                <div class="carousel-caption">
                    <div class="text-box"><?php echo Yii::t('phrase', 'slide0'); ?></div>
                </div>
            </div>
        </div>

        <div id="slide-1" class="item">
            <div class="container">
                <div class="carousel-caption">
                    <h1 class="text-shadow"><?php echo Yii::t('phrase', 'companies in base'); ?></h1>

                    <div class="text-box-small">
                        <h1> <?php echo $indexDataModel-> officesQty; ?> </h1>
                        <p> <?php echo Yii::t('word', 'offices'); ?> </p>
                    </div>

                    <div class="text-box-small">
                        <h1> <?php echo $indexDataModel-> countryQty; ?> </h1>
                        <p> <?php echo Yii::t('word', 'countries'); ?> </p>
                    </div>
                </div>
            </div>
        </div>

        <div id="slide-2" class="item">
            <div class="container">
                <div class="carousel-caption">
                    <div class="text-box"><?php echo Yii::t('phrase', 'slide2'); ?></div>
                </div>
            </div>
        </div>

    </div>
    <!-- left right navigation -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
</div>

<!-- WELCOME -->
<div id="section-start" class="container-fluid section light">
	<div class="container">
	
		<div class="row-fluid-5">
			<h1 class="page-header"><?php echo Yii::t('phrase', 'Top Section Header'); ?></h1>
			<?php echo Yii::t('homePage', 'Top Section'); ?>
		</div>
		
		<?php
			// Render order form
			$this->renderPartial('../order/stepFiles', array('orderFilesFormModel'=>$orderFilesFormModel, 'currentPage'=>'home'));
		?>
		
	</div>
</div>


<!-- PAYMENTS -->
<div id="section-payments" class="container-fluid section">
	<div class="container">
	
		<div class="row-fluid-5">
			<h1 class="page-header"><?php echo Yii::t('phrase', 'Middle Section Header'); ?></h1>
			<?php echo Yii::t('homePage', 'Middle Section'); ?>
		</div>
		
		<div class="row-fluid-5">
            <div class="span2">
				<?php echo Yii::t('homePage', 'Middle Section Left'); ?>
			</div>
			<div class="span3">
				<?php echo Yii::t('homePage', 'Middle Section Right'); ?>
			</div>
		</div>
		
	</div>
</div>
	

<!-- HOW IT WORKS -->
<div id="section-info" class="container-fluid section light">
	<div class="container">
	
		<div class="row-fluid-5">
			<div class="span5">
				<h1 class="page-header"><?php echo Yii::t('phrase', 'How It Works'); ?></h1>
			</div>
		</div>
		
		<?php echo Yii::t('homePage', 'Bottom Section'); ?>
		
		<div class="row-fluid-5">		
			<div class="span4">		
			</div>
			<div class="span1">
				<button type="button" class="btn btn-warning btn-block"><?php echo Yii::t('phrase','More Info'); ?></button>
			</div>
		</div>
			
    </div>
</div>


<!-- CONTACT -->
<div id="section-contact" class="container-fluid section dark">
<div class="container">

	<div class="row-fluid-5">
		<div class="span5">
			<h1 class="page-header"><?php echo Yii::t('phrase','Contact Us'); ?></h1>
		</div>
	</div>
	
<?php
// Show contact form
$this->renderPartial('forms/contactForm', array('contactFormModel'=>$contactFormModel));
?>
	
</div>
</div>