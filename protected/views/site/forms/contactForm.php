<?php
/** 
 * Contact form template
 */
$form=$this->beginWidget('CActiveForm', array(
	"id"=>"contact-form",	
	"clientOptions"=>array(
		"validateOnType"=>true,
		"validateOnSubmit"=>true
	)
));
?>

<?php if(Yii::app()->user->hasFlash('contact')): ?>

	<div class="flash-success">
		<?php echo Yii::app()->user->getFlash('contact'); ?>
	</div>

<?php endif; ?>

<div class="row-fluid-5">		
	<div class="span2">

		<div class="form-group">

			<?php 
			echo $form->textField($contactFormModel, 'name', array(
				"class"=>"form-control",
				"placeholder"=> Yii::t('phrase', 'Name and Surname')));
			echo $form->error($contactFormModel, 'name'); 
			?>

		</div>

		<div class="form-group">

			<?php 
			echo $form->textField($contactFormModel, 'email', array(
				"class"=>"form-control",
				"placeholder"=> Yii::t('word', 'Email', SINGULAR)));
			echo $form->error($contactFormModel, 'email'); 
			?>

		</div>

		<div class="form-group">

			<?php 
			echo $form->textField($contactFormModel, 'phone', array(
				"class"=>"form-control",
				"placeholder"=> Yii::t('phrase', 'Phone Number')));
			echo $form->error($contactFormModel, 'phone'); 
			?>

		</div>	

	</div><!-- span2 -->	
	<div class="span3">	

		<div class="form-group">

			<?php 
			echo $form->textArea($contactFormModel, 'body',array(
				"rows"=>6,
				"class"=>"form-control",
				"placeholder"=> Yii::t('word', 'Message', SINGULAR)));
			echo $form->error($contactFormModel, 'body'); 
			?>

		</div>

	</div>
</div><!-- row-fluid-5 -->

<div class="row-fluid-5">
	<div class="span1">

		<div class="form-group">

			<?php 
			echo $form->textField($contactFormModel, 'verifyCode', array(
				"class"=>"form-control pull-left",
				"placeholder"=> Yii::t('phrase','Enter the Symbols')
				));
			echo $form->error($contactFormModel, 'verifyCode'); 
			?>

		</div>

	</div>
	<div class="span1">
		<?php 
		if(CCaptcha::checkRequirements())
		{
			 $this->widget('CCaptcha', array(
				'showRefreshButton'=>false,
				'clickableImage'=>true,
				'imageOptions'=>array(
					"title"=>Yii::t('phrase', 'Click to Refresh'))));				
		}?>
	</div>
	<div class="span2">

	</div>					
	<div class="span1">

		<?php 
		echo CHtml::submitButton( Yii::t('phrase','Send Message'), array(
			"submit"=>false,
			"class"=>"btn btn-primary btn-block"));
		?>

	</div>
</div><!-- row-fluid-5 -->

<?php $this->endWidget(); ?>
