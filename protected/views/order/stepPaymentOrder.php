<?php
/**
 * Order payment template
 */
// If order error
if( ! $Order)
{
	echo 'Error. Can not create order! ';
	// The reset button
	echo CHtml::button( Yii::t('word','Reset'), array(
		"class"=>"btn btn-primary",
		"onclick"=>"js:location.href='/order/reset'")); 	
}
// Show form if no errors
else
{//echo '<pre>' . print_r($Order->paid, true) . '</pre>';
?>

<form name="payment" action="https://sci.interkassa.com/" method="post" accept-charset="UTF-8" target="_blank">
	<input type="hidden" name="ik_co_id" value="<?php echo $IKShopId; ?>" />
	<input type="hidden" name="ik_am" value="<?php echo $Order->amount; ?>" />
	<input type="hidden" name="ik_cur" value="<?php echo $Order->currency; ?>" />
	<input type="hidden" name="ik_pm_no" value="<?php echo $Order->number; ?>" />
	<input type="hidden" name="ik_desc" value="<?php echo $Order->description; ?>" />
    <input type="hidden" name="ik_loc" value="<?php echo Yii::app()->language; ?>" />
	<input type="hidden" name="ik_ia_m" value="post" />
    


	<div style="line-height: 22px;margin-bottom:50px;">
	<div class="row">
		<label class="col-md-3 control-label"><?php echo Yii::t('phrase','number of order'); ?></label>
		<span class="form-control-static"><?php echo $Order->number; ?></span>
	</div>

	<div class="row">
		<label class="col-md-3 control-label"><?php echo Yii::t('phrase','selected companies'); ?></label>
		<span class="form-control-static"><?php echo $Order->orderedCompaniesCount; ?></span>
	</div>

	<div class="row">
		<label class="col-md-3 control-label"><?php echo Yii::t('word','cost'); ?></label>
		<span class="form-control-static"><?php
			echo number_format($Order->amount, 2, '.', '') // Value
			.' '.Yii::app()->locale->getCurrencySymbol($Order->currency); // Currency
			?></span>
	</div>

	<div class="row">
		<label class="col-md-3 control-label"><?php echo Yii::t('word','status'); ?></label>
		<span class="form-control-static"><?php echo $Order->paid ? Yii::t('word', 'paid') : Yii::t('word', 'unpaid')?></span>
	</div>
</div>
<div class="row-fluid-5">
	<div class="span5">

		<?php
		// If the order is paid do not show pay button
		if( ! $Order->paid)
		{
			echo CHtml::submitButton(Yii::t('phrase','go to payment'), array(
			"class"=>"btn btn-primary"));
		}
		else
		{
			echo CHtml::button(Yii::t('phrase','Next Step'), array(
			"class"=>"btn btn-primary",
			"onclick"=>"js:location.href='/order/finish'"));
		}
			
		// The reset button
		if ( false )
		{
			echo '<br /><br /><br /><br />';
			echo CHtml::button( Yii::t('phrase','Reset'), array(
				"class"=>"btn reset btn-primary",
				"onclick"=>"js:location.href='/order/reset'"));
		}
		?>	
		
	</div>
</div>
</form>
<?php 
}
?>



