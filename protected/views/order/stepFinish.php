<?php
/**
 * Finish template
 */
?>
<div class="row-fluid-5">
    <div class="span5">
        <p class="gratz"><?php echo Yii::t('phrase', 'Congratulations'); ?></p>
    </div>
    <hr>
    <hr>
</div>
<div class="row-fluid-5">
    <div class="span5">
        <?php
        echo CHtml::button(Yii::t('phrase','personal account'), array(
            "class"=>"btn btn-warning",
            "onclick"=>"js:location.href='/account/login?user=" . $user['identificator'] . "'"));
        ?>
    </div>
</div>


