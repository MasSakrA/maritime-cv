<?php
/**
 * Content filling order form step
 */
$form=$this->beginWidget('CActiveForm', array(
	"id"=>"order-form-content",
	"enableAjaxValidation"=>true,	
	"clientOptions"=>array(
		"validateOnType"=>true,
		"validateOnSubmit"=>true
	)
)); ?>

<script type="text/javascript" src="/lib/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
	/**
	 * A name of a template which means what the template has been changed
	 * @type {String}
	 */
	var DIRTY_TEMPLATE = 'dirty';
	
	tinymce.init({
		theme: "modern",
		skin: "light",
		selector: "div.editable",
		inline: true,
		plugins: [
			"advlist autolink lists link image charmap print preview anchor",
			"searchreplace visualblocks code fullscreen",
			"insertdatetime media table contextmenu paste textcolor"
		],
		toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
		language : '<?php echo Yii::app()->language; ?>',
		setup : function(editor) {
			/**
			 * Method to detect text template changing.
			 * If text template was changed then marks it as dirty
			 */
			editor.on('keyup', function(e) {
				// Mark what template is dirty now (for a server storing)
				$('input[name="template-text"]').val(DIRTY_TEMPLATE);
				// Unmark selected text template
				UnselectTemplate('text');
			  });
		}
	});
	// Add tinymce body value to form before form will be submitted
	/*$('#order-form-content').on('submit', function()
	{
		// Get value of the body
		bodyValue = $('div.editable').html();
		// Add hidden input with the body value
		bodyInput = '<input type="hidden" name="OrderContentForm[body]" value="' + bodyValue + '" />';
		$(this).append(bodyInput);
	});*/
	
	/**
	 * Method gets a class name which begins with the specified prefix
	 * @param {DOM|jQuery|String} el	A DOM element from which a class name will be taken
	 * @param {String} classPrefix		A class prefix which must be found
	 * @returns {String|Boolean}		A class name or FALSE if no such prefix
	 */
	function getClassName( el, classPrefix )
	{
		//console.log(el.length)
		/**
		 * @type {String} Found class name
		 */
		var s = '';
		$.each( ($(el).attr('class')).split(/\s+/), function( i, className )
		{
			if ( className.lastIndexOf( classPrefix, 0 ) === 0  ) s = className;
		});
		return ( s != '' ) ? s : false;
	}
	
	/**
	 * Mehtod to the restoring selected templates
	 */
	function RestoreSelectedTemplates()
	{
		/**
		 * Get stored templates
		 */
		/**
		 * @type {String}	A selected text template
		 */
		var textTemplate = $('input[name="template-text"]').val();
		/**
		 * @type {String}	A selected color template
		 */

		var colorTemplate = $('input[name="template-color"]').val();
		/**
		 * Set stored templates
		 */
		if(typeof textTemplate === 'string' && textTemplate.length > 0)
		{
			SelectTemplate('text', textTemplate);
		}
		if(typeof colorTemplate === 'string' && colorTemplate.length > 0)
		{
			SelectTemplate('color', colorTemplate);
		}
		ChangeColorTemplate(colorTemplate);
	}
	
	/**
	 * Method to unselect the template
	 * @param {String} type		The template type
	 */
	function UnselectTemplate(type)
	{
		/**
		 * Wrong arguments
		 */
		if(!type)
		{
			return false;
		}
		$('.template-' + type + '.selected').removeClass('selected');
	}
	/**
	 * Method to select templates
	 * @param {String} type		The template type
	 * @param {String} name		The template name
	 */
	function SelectTemplate(type, name)
	{
		/**
		 * Wrong arguments
		 */
		if(!type || !name)
		{
			return false;
		}
		// Unmark selected text template
		UnselectTemplate(type);
		// Mark selected template if it not was changed
		if( name !== DIRTY_TEMPLATE)
		{
			$('.template-' + type + '.' + name).addClass('selected');
		}		
	}
	
	/**
	 * Method to replace an old color scheme to new
	 * @param {String} colorTemplate		Color template name
	 */
	function ChangeColorTemplate(templateName)
	{
		/**
		 * @type {String}	An old color template class name
		 */
		var oldTemplateName = getClassName( $('div.textarea'), 'cs-' );
		// Replace an old color template to new
		$('div.textarea').removeClass( oldTemplateName ).addClass( templateName );
		// Use a hidden input to send the value to a server for the storing
		$('input[name="template-color"]').val(templateName);
	}
	
	/**
	 * Method to replace email body text template
	 * @param {String} templateName		Text template name
	 */
	function ChangeTextTemplate(templateName)
	{
		// Set selected template as new email body
		$('div.textarea').html( $('#text-tempaltes div.' + templateName).html() );
                if(templateName === 'tt-cv-default') {
                    $('.var-subject').val($('.subject-templates .cv-default').html());
                }
                if(templateName === 'tt-cv-exp') {
                    $('.var-subject').val($('.subject-templates .cv-exp').html());
                }
		// Use a hidden input to send the value to a server for the storing
		$('input[name="template-text"]').val(templateName);
	}
	
	$(document).ready(function()
	{
        if($('input[name="template-color"]').val().length === 0) { $('input[name="template-color"]').val(getClassName( $('div.template-color.selected'), 'cs-' )); }
		RestoreSelectedTemplates();
                $('.var-subject').val($('.subject-templates .cv-default').html());
		$('.template-text').click(function()
		{
			/**
			 * @type {String} A selected text template class name
			 */
			var textTemplate = getClassName( this, 'tt-' );
			/**
			 * @type {String} A template type
			 */
			var type = 'text';
			// Change the template to selected
			SelectTemplate(type, textTemplate);
			ChangeTextTemplate(textTemplate);
		});
		
		$('.template-color').click(function()
		{
			/**
			 * @type {String}	A new color template class name
			 */
			var colorTemplate = getClassName( this, 'cs-' );
			/**
			 * @type {String} A template type
			 */
			var type = 'color';
			// Mark selected template
			SelectTemplate(type, colorTemplate);
			// Replace an old color template to new
			ChangeColorTemplate(colorTemplate);
		});
		
	});
	
</script>


<style type="text/css">

<?php
// Display color schemes
foreach($orderContentFormModel->colorSchemes as $index => $item) echo $item['additionalCSS']."\n".$item['scheme']."\n\n\n";
?>




</style>

<div class="container stepContent">

	<div id="text-tempaltes">
	
	<?php
                
	foreach($orderContentFormModel->emailTemplates as $index => $template)
	{
		echo '
			<div class="tt-'. $template['name'] .'">
				'. Yii::t( 'emailTemplateContent', $template['name'] ) .'
			</div>';                
	}        
	?>
            
	</div>
        <div class="subject-templates">
        <?php 
        foreach($orderContentFormModel->emailTemplates as $index => $template) {
            echo '<span class="'.$template['name'].'">'.Yii::t('emailTemplateName', $template['name']).'</span>';
        }
        ?>
        </div>

	<div class="row-fluid-5">
	
		<div class="span1">
			<h3 class="page-header"><?php echo Yii::t('word', 'Template', SINGULAR); ?></h3>

			<?php
			foreach($orderContentFormModel->emailTemplates as $index => $template)
			{
				if( ! isset($firstTextTemplate))
				{
					$selectedClass = 'selected';
					$firstTextTemplate = false;
				}
				else
				{
					$selectedClass = '';
				}
				echo '
					<div class="template-text tt-'. $template['name'] .' '. $selectedClass .'">
						<div class="preview">A</div>
						'. Yii::t( 'emailTemplateName', $template['name'] ) .'
					</div>';
			}
			?>
			
		</div>
	
		<div class="span1">
			<h3 class="page-header selected"><?php echo Yii::t('phrase', 'Color Scheme'); ?></h3>
			
			<?php
			// Display color scheme selectors
			foreach($orderContentFormModel->colorSchemes as $index => $scheme)
			{
				if( ! isset($firstColorTemplate))
				{
					$selectedClass = 'selected';
					$firstColorTemplate = false;
				}
				else
				{
					$selectedClass = '';
				}
				// Show filter row
				echo '
					<div class="template-color cs-'. $scheme['name'] . ' ' . $selectedClass . '">
						<div class="preview"><span class="one"></span><span class="two"></span><span class="three"></span><span class="four"></span></div>
						'. Yii::t('colorScheme', $scheme['name']) .'
					</div>';	
			}
			?>
			
		</div>
		

		
		<div class="span3">
			
			<h3 class="page-header"><?php echo Yii::t('phrase', 'Email Subject'); ?></h3>

			<div class="form-group">
					<?php
					echo $form->textField($orderContentFormModel, 'subject', array(
						"class"=>"form-control var-subject", "value" => "some value here"));
					echo $form->error($orderContentFormModel, 'subject'); 
					?>
			</div>
			
			
			<h3 class="page-header"><?php echo Yii::t('phrase', 'Email Content'); ?></h3>
			<div class="form-group">
                <p><?php echo Yii::t('phrase', 'hint letter text'); ?></p>
				<div id="OrderContentForm[body]" class="textarea  form-control editable email-content">
				<?php
				// Restore body or display default
				$orderContentForm = Yii::app()->session->get('OrderContentForm');
				if( ! empty($orderContentForm['body']) || isset($_POST['OrderContentForm']['body']))
				{
					echo $orderContentForm['body'];
				}
				else
				{
					// Default text
					echo Yii::t( 'emailTemplateContent', $orderContentFormModel->emailTemplates[0]['name'] );
				}
				?>
				</div>
				<?php
				echo $form->error($orderContentFormModel, 'body'); 
				?>
			</div>			
			
		</div>
		
	</div>

	
<div class="row-fluid-5">
	<div class="span5 buttons">
		<?php
		// The reset button
		echo CHtml::button( Yii::t('phrase','Reset'), array(
			"class"=>"btn reset btn-primary pull-left"
			));
		?>
		<?php		
		echo CHtml::submitButton( Yii::t('phrase','Next Step'), array(
			"class"=>"btn btn-warning pull-right hide-input"));
		?>
        <?php
        echo CHtml::button( Yii::t('phrase','Next Step'), array(
            "class"=>"btn btn-warning pull-right next-step"));
        ?>
        <?php
		echo CHtml::button( Yii::t('phrase','Previous Step'), array(
			"class"=>"btn btn-primary pull-right",
			"onclick"=>"js:location.href='$this->previousStep'"));
		?>		
	</div>
</div>
</div>
<!-- Hidden fields to send to a server for the storing -->
<input name="template-text" type="hidden" value="<?php echo empty($orderContentForm['textTemplate']) ? '' : $orderContentForm['textTemplate'] ?>"/>
<input name="template-color" type="hidden" value="<?php echo empty($orderContentForm['colorTemplate']) ? '' : $orderContentForm['colorTemplate'] ?>"/>
<?php $this->endWidget(); ?>

</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo Yii::t('phrase', 'modal content title tip'); ?></h4>
            </div>
            <div class="modal-body">
                <?php echo Yii::t('phrase', 'modal content body tip'); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><?php echo Yii::t('word', 'Close'); ?></button>
                <?php
                echo CHtml::button( Yii::t('phrase','Next Step'), array(
                    "class"=>"btn btn-warning pull-right",
                    "onclick"=>"js:$('input.hide-input').trigger('click')"));
                ?>
            </div>
        </div>
    </div>
</div>