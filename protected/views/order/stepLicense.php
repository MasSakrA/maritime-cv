<?php echo Yii::t('licensePage', 'Lisense'); ?>
<div class="row-fluid-5">
	<div class="span5 buttons">
		<?php	
		echo CHtml::beginForm();
		echo CHtml::hiddenField('agree' , true, array('id' => 'hiddenInputAgree'));
		echo CHtml::submitButton( Yii::t('word', 'agree'), array(
				"class"=>"btn btn-success pull-right",
				"submit"=>"/order/license/")); 
		echo CHtml::endForm()
		?>
	</div>
</div>