<h3 class="page-header"><?php echo Yii::t('word', 'Sector', PLURAL); ?></h3>
<ul id="work-area-filters">
    <li>
        <?php

        if(empty(Yii::app()->session['OrderCompaniesForm']['companies']))
        {
            $checked = '';
        }

        if( count(Yii::app()->session['OrderCompaniesForm']['companies']) === $all_companies_count ) {
            $isAll = 'none';
            $checked = 'checkedByCookies';
        } else {
            $isAll = 'all';
            $checked = '';
        }

        echo CHtml::checkBox('', false, array(
            'ajax' => array(
                'type'=>'POST',
                'url'=>  Yii::app()->createUrl('order/AjaxChangeSession'),
                'data' => array('id' => 'all', 'status' => 'work-area', 'page' => $pages->getCurrentPage()),
                'success' => 'js:function(val){
                                    $("#render_companies_form").html(val);
                                    $("#companies-form").removeClass("disabled");
                                }'
            ),
            'class' => 'filter sector '.$checked.' '.$isAll,
            'id' => 'work-area-all',
            'value' => 'all'
        ));
        ?>
        <label for="work-area-all"><span><img src="<?php echo Yii::app()->request->baseUrl.'/lib/skin/img/check-companies.png'; ?>" class="check-companies" /></span><?php echo Yii::t('word', 'All', SINGULAR); ?></label>
    </li>
    <?php
    // Display work area filters
    foreach($orderCompaniesForm->workAreaFilters as $index => $filter)
    {
        // Show filter row
        if(empty($regions_from_session))
        {
            $checked = '';
        }
        // Check stored checkboxes
        elseif(in_array($filter['name'], $regions_from_session))
        {
            $checked = 'checkedByCookies';
        }
        // Don't check not stored checkboxes
        else
        {
            $checked = '';
        }

        if($this->CheckIfAllInArray($this->getAllCompaniesBySector($filter['name']), Yii::app()->session['OrderCompaniesForm']['companies'])) {
            $done = 'done';
        } else {
            $done = 'not-done';
        }

        echo '<li>';
        echo CHtml::checkBox('', false, array(
            'ajax' => array(
                'type'=>'POST',
                'url'=>  Yii::app()->createUrl('order/AjaxChangeSession'),
                'data' => array('id' => $filter['name'], 'status' => 'work-area', 'page' => $pages->getCurrentPage()),
                'success' => 'js:function(val){
                                    $("#render_companies_form").html(val);
                                    $("#companies-form").removeClass("disabled");
                                }'
            ),
            'class' => 'filter sector ' . $checked . ' '.$done,
            'id' => 'work-area-' . $index,
            'value' => $filter['name']
        ));

        echo '<label for="work-area-' . $index . '"
						        data-count-all="' . count($this->getAllCompaniesBySector($filter['name'])) . '"
						        data-count-current="' . $this->countMatchesInArrays($this->getAllCompaniesBySector($filter['name']), Yii::app()->session['OrderCompaniesForm']['companies']) . '"
						        data-percent="'. $this->countPercent( $this->countMatchesInArrays($this->getAllCompaniesBySector($filter['name']), Yii::app()->session['OrderCompaniesForm']['companies']), count($this->getAllCompaniesBySector($filter['name'])) ) .'">
									<span><img src="' . Yii::app()->request->baseUrl.'/lib/skin/img/check-companies.png' . '" class="check-companies" /></span>'
            . '<name class="area-name">' . Yii::t('workArea', $filter['name']) . '</name> $<price class="region-price">' . $filter['price'] . '</price>'
            .'</label>
							</li>';
    }
    ?>
</ul>

<h3 class="page-header"><?php echo Yii::t('word', 'Region', PLURAL); ?></h3>
<ul id="region-filters">
    <?php
    // Unset source language for translating abbriviatures to english language
    $storedSourceLanguage = Yii::app()->sourceLanguage;
    Yii::app()->sourceLanguage = '';
    // Display region filters
    foreach($orderCompaniesForm->regionFilters as $index => $filter)
    {
        // Show filter row
        if(empty($counties_regions_from_session))
        {
            $checked = '';
        }
        // Check stored checkboxes
        elseif(in_array($filter['name'], $counties_regions_from_session))
        {
            $checked = 'checkedByCookies';
        }
        // Don't check not stored checkboxes
        else
        {
            $checked = '';
        }
        if($this->CheckIfAllInArray($this->getAllCompaniesByRegion($filter['name']), Yii::app()->session['OrderCompaniesForm']['companies'])) {
            $done = 'done';
        } else {
            $done = 'not-done';
        }

        echo '<li title="'. Yii::t('region', $filter['name']) .'">';
        echo CHtml::checkBox('', false, array(
            'ajax' => array(
                'type'=>'POST',
                'url'=>  Yii::app()->createUrl('order/AjaxChangeSession'),
                'data' => array('id' => $filter['name'], 'status' => 'work-region', 'page' => $pages->getCurrentPage()),
                'success' => 'js:function(val){
                                    $("#render_companies_form").html(val);
                                    $("#companies-form").removeClass("disabled");
                                }'
            ),
            'class' => 'filter region ' . $checked . ' ' . $done,
            'id' => 'region-' . $index,
            'value' => $filter['name']
        ));
        echo '
								<label for="region-' . $index . '"
								        data-count-all="' . count($this->getAllCompaniesByRegion($filter['name'])) . '"
								        data-count-current="' . $this->countMatchesInArrays($this->getAllCompaniesByRegion($filter['name']), Yii::app()->session['OrderCompaniesForm']['companies']) . '">
									<span><img src="' . Yii::app()->request->baseUrl.'/lib/skin/img/check-companies.png' . '" class="check-companies" /></span>'
            .Yii::t('region', $filter['name'])
            .'</label>
							</li>';
    }
    ?>
</ul>

<h3 class="page-header"><?php echo Yii::t('word', 'Country', PLURAL); ?></h3>
<ul id="country-filters">
    <?php
    // Display country filters
    foreach($orderCompaniesForm->countryFilters as $index => $filter)
    {
        // Show filter row
        if(empty($counties_from_session))
        {
            $checked = '';
        }
        // Check stored checkboxes
        elseif(in_array($filter['name'], $counties_from_session))
        {
            $checked = 'checkedByCookies';
        }
        // Don't check not stored checkboxes
        else
        {
            $checked = '';
        }

        if($this->CheckIfAllInArray($this->getAllCompaniesByCountry($filter['name']), Yii::app()->session['OrderCompaniesForm']['companies'])) {
            $done = 'done';
        } else {
            $done = 'not-done';
        }

        echo '<li>';
        echo CHtml::checkBox('', false, array(
            'ajax' => array(
                'type'=>'POST',
                'url'=>  Yii::app()->createUrl('order/AjaxChangeSession'),
                'data' => array('id' => $filter['name'], 'status' => 'work-country', 'page' => $pages->getCurrentPage()),
                'success' => 'js:function(val){
                                   $("#render_companies_form").html(val);
                                   $("#companies-form").removeClass("disabled");
                                }'
            ),
            'class' => 'filter country ' . $checked . ' ' .$done,
            'id' => 'country-' . $index,
            'value' => $filter['name']
        ));

        echo '<label for="country-' . $index . '"
                        data-count-all="' . count($this->getAllCompaniesByCountry($filter['name'])) . '"
                        data-count-current="' . $this->countMatchesInArrays($this->getAllCompaniesByCountry($filter['name']), Yii::app()->session['OrderCompaniesForm']['companies']) . '">
									<span><img src="' . Yii::app()->request->baseUrl.'/lib/skin/img/check-companies.png' . '" class="check-companies" /></span>'
            .Yii::t('country', $filter['name'])
            .'</label>
							</li>';
    }
    // Restore current language
    Yii::app()->sourceLanguage = $storedSourceLanguage;
    ?>
</ul>