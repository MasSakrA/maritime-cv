<?php
/**
 * Order data preview template
 */
$form = $this->beginWidget('CActiveForm', array(
	"id"=>"order-form-preview"
));
?>

<style type="text/css">
<?php
// Display the styles for the color schemes
foreach($ContentModel->colorSchemes as $index => $item)
{
	echo $item['additionalCSS']."\n".$item['scheme']."\n\n\n";
}
?>
</style>

<!-- Two columns full width row -->
<div class="row">
	
	<!-- First column -->
	<div class="col-md-6">
		
		<div class="row form-group">
			<label class="col-md-4 control-label"><?php echo Yii::t('word', 'Email', SINGULAR); ?></label>
			<span class="col-md-6 form-control-static"><?php echo $orderPreviewForm->email ?></span>
		</div>

		<div class="row form-group">
			<label class="col-md-4 control-label"><?php echo Yii::t('word', 'CV', SINGULAR); ?></label>
			<p class="col-md-6 form-control-static"><?php echo $orderPreviewForm->CVFile ?></p>
			
		</div>
		
		<div class="row form-group">
			<label class="col-md-4 control-label"><?php echo Yii::t('phrase', 'CV File Name'); ?></label>
			<div class="col-md-6">
			<?php 
			echo $form->textField($orderPreviewForm, 'CVFileName', array(
				"class" => "form-control"
			));
			echo $form->error($orderPreviewForm, 'CVFileName');
			?>
			</div>
		</div>
		
		<?php if($orderPreviewForm->attachment1 !== '')
		{
		?>
		<div class="row form-group">
			<label class="col-md-4 control-label"><?php echo Yii::t('word', 'Attachment', SINGULAR); ?> 1</label>
			<p class="col-md-6 form-control-static"><?php echo $orderPreviewForm->attachment1 ?></p>
			
		</div>
		
		<div class="row form-group">
			<label class="col-md-4 control-label"><?php echo Yii::t('phrase', 'Attachment {number} Name', array( '{number}' => 1 )); ?></label>
			<div class="col-md-6">
			<?php 
			echo $form->textField($orderPreviewForm, 'attachment1Name', array(
				"class" => "form-control"
			));
			echo $form->error($orderPreviewForm, 'attachment1Name');
			?>
			</div>
		</div>
		<?php
		}
		?>
		
		<?php if($orderPreviewForm->attachment2 !== '')
		{
		?>
		<div class="row form-group">
			<label class="col-md-4 control-label"><?php echo Yii::t('word', 'Attachment', SINGULAR); ?> 2</label>
			<p class="col-md-6 form-control-static"><?php echo $orderPreviewForm->attachment2 ?></p>
			
		</div>
		
		<div class="row form-group">
			<label class="col-md-4 control-label"><?php echo Yii::t('phrase', 'Attachment {number} Name', array( '{number}' => 2 )); ?></label>
			<div class="col-md-6">
			<?php 
			echo $form->textField($orderPreviewForm, 'attachment2Name', array(
				"class" => "form-control"
			));
			echo $form->error($orderPreviewForm, 'attachment2Name');
			?>
			</div>
		</div>
		<?php
		}
		?>

	</div>
	
	<!-- Second column -->
	<div class="col-md-6">
		
		<div class="row form-group">
			<label class="col-md-4 control-label"><?php echo Yii::t('phrase', 'Email Subject'); ?></label>
			<span class="form-control-static"><?php echo $orderPreviewForm->subject ?></span>
		</div>
		
		<div class="row form-group">
			<label class="col-md-4 control-label"><?php echo Yii::t('phrase', 'Email Content'); ?></label>
			<div class="col-md-12 form-control-static form-control email-content <?php echo $orderPreviewForm->colorScheme; ?>"><?php echo $orderPreviewForm->body ?></div>
		</div>
		
		
	</div>
	
	<!-- Selected companies list below previous two columns -->
<!--	<div class="col-md-12 preview">-->
<!--		<h3 class="page-header">--><?php //echo Yii::t('word', 'Company', PLURAL); ?><!--</h3>-->
		<?php
//		// Show each company
//		$selectedCompanyIds = $orderPreviewForm->selectedCompanyIds;
//		$companyList = $orderPreviewForm->companyList;
//
//		// Check data type
//		if(isset($selectedCompanyIds) && is_array($selectedCompanyIds) && isset($companyList) && is_array($companyList))
//		{
//			foreach($selectedCompanyIds as $selectedCompanyId)
//			{
//				// Search company with current id in all company list
//				foreach($companyList as $company)
//				{
//					if($company['id'] == $selectedCompanyId)
//					{
//						echo '
//							<div class="col-md-3 company" title="'. $company['name'] .'">' .
//                            CHtml::image(Yii::app()->request->baseUrl . '/images/' . md5($company['name'].Yii::app()->controller->id.Yii::app()->controller->action->id) .
//                                '.png', '') . '</div>
//						';
//
//					}
//				}
//			}
//		}
		?>
<!--	</div>-->
	
</div>

<div class="row-fluid-5">
	<div class="span5 buttons">
		
		<?php
		// The reset button
		echo CHtml::button( Yii::t('phrase','Reset'), array(
			"class"=>"btn reset btn-primary"));
		?>
		<?php
		echo CHtml::submitButton( Yii::t('phrase','Submit'), array(
			"class"=>"btn btn-warning pull-right")); 
		?>
		<?php
		echo CHtml::button( Yii::t('phrase','Previous Step'), array(
			"class"=>"btn btn-primary pull-right",
			"onclick"=>"js:location.href='$this->previousStep'"));
		?>
		
	</div>
</div>
<?php $this->endWidget(); ?>



