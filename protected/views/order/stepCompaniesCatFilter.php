<input type="hidden" name="OrderCompaniesForm" />

<p><?php echo Yii::t('pagePart', 'This is companies select page. Please, select companies that you are interested. You may use filters in left part of page or set checkboxes manually.'); ?></p>

<div id="progress-bars"></div>

<div id="placeholder-agencies" class="row-fluid-5">

<div class="span1">
<div id="filters">

<h3 class="page-header"><?php echo Yii::t('word', 'Sector', PLURAL); ?></h3>
<ul id="work-area-filters">
    <li>
        <?php

        if(empty(Yii::app()->session['OrderCompaniesForm']['companies']))
        {
            $checked = '';
        }

        if( count(Yii::app()->session['OrderCompaniesForm']['companies']) === $all_companies_count ) {
            $isAll = 'none';
            $checked = 'checkedByCookies';
        } else {
            $isAll = 'all';
            $checked = '';
        }

        echo CHtml::checkBox('', false, array(
            'ajax' => array(
                'type'=>'POST',
                'url'=>  Yii::app()->createUrl('order/AjaxChangeSession'),
                'data' => array('id' => 'all', 'status' => 'work-area', 'page' => $pages->getCurrentPage()),
                'success' => 'js:function(val){
                                    $("#render_companies_form").html(val);
                                    $("#companies-form").removeClass("disabled");
                                }'
            ),
            'class' => 'filter sector '.$checked.' '.$isAll,
            'id' => 'work-area-all',
            'value' => 'all'
        ));
        ?>
        <label for="work-area-all"><span><img src="<?php echo Yii::app()->request->baseUrl.'/lib/skin/img/check-companies.png'; ?>" class="check-companies" /></span><?php echo Yii::t('word', 'All', SINGULAR); ?></label>
    </li>
    <?php
    // Display work area filters
    foreach($orderCompaniesForm->workAreaFilters as $index => $filter)
    {
        // Show filter row
        if(empty($regions_from_session))
        {
            $checked = '';
        }
        // Check stored checkboxes
        elseif(in_array($filter['name'], $regions_from_session))
        {
            $checked = 'checkedByCookies';
        }
        // Don't check not stored checkboxes
        else
        {
            $checked = '';
        }

        if($this->CheckIfAllInArray($this->getAllCompaniesBySector($filter['name']), Yii::app()->session['OrderCompaniesForm']['companies'])) {
            $done = 'done';
        } else {
            $done = 'not-done';
        }

        echo '<li>';
        echo CHtml::checkBox('', false, array(
            'ajax' => array(
                'type'=>'POST',
                'url'=>  Yii::app()->createUrl('order/AjaxChangeSession'),
                'data' => array('id' => $filter['name'], 'status' => 'work-area', 'page' => $pages->getCurrentPage()),
                'success' => 'js:function(val){
                                    $("#render_companies_form").html(val);
                                    $("#companies-form").removeClass("disabled");
                                }'
            ),
            'class' => 'filter sector ' . $checked . ' '.$done,
            'id' => 'work-area-' . $index,
            'value' => $filter['name']
        ));

        echo '<label for="work-area-' . $index . '"
						        data-count-all="' . count($this->getAllCompaniesBySector($filter['name'])) . '"
						        data-count-current="' . $this->countMatchesInArrays($this->getAllCompaniesBySector($filter['name']), Yii::app()->session['OrderCompaniesForm']['companies']) . '"
						        data-percent="'. $this->countPercent( $this->countMatchesInArrays($this->getAllCompaniesBySector($filter['name']), Yii::app()->session['OrderCompaniesForm']['companies']), count($this->getAllCompaniesBySector($filter['name'])) ) .'">
									<span><img src="' . Yii::app()->request->baseUrl.'/lib/skin/img/check-companies.png' . '" class="check-companies" /></span>'
            . '<name class="area-name">' . Yii::t('workArea', $filter['name']) . '</name> $<price class="region-price">' . $filter['price'] . '</price>'
            .'</label>
							</li>';
    }
    ?>
</ul>

<h3 class="page-header"><?php echo Yii::t('word', 'Region', PLURAL); ?></h3>
<ul id="region-filters">
    <?php
    // Unset source language for translating abbriviatures to english language
    $storedSourceLanguage = Yii::app()->sourceLanguage;
    Yii::app()->sourceLanguage = '';
    // Display region filters
    foreach($orderCompaniesForm->regionFilters as $index => $filter)
    {
        // Show filter row
        if(empty($counties_regions_from_session))
        {
            $checked = '';
        }
        // Check stored checkboxes
        elseif(in_array($filter['name'], $counties_regions_from_session))
        {
            $checked = 'checkedByCookies';
        }
        // Don't check not stored checkboxes
        else
        {
            $checked = '';
        }
        if($this->CheckIfAllInArray($this->getAllCompaniesByRegion($filter['name']), Yii::app()->session['OrderCompaniesForm']['companies'])) {
            $done = 'done';
        } else {
            $done = 'not-done';
        }

        echo '<li title="'. Yii::t('region', $filter['name']) .'">';
        echo CHtml::checkBox('', false, array(
            'ajax' => array(
                'type'=>'POST',
                'url'=>  Yii::app()->createUrl('order/AjaxChangeSession'),
                'data' => array('id' => $filter['name'], 'status' => 'work-region', 'page' => $pages->getCurrentPage()),
                'success' => 'js:function(val){
                                    $("#render_companies_form").html(val);
                                    $("#companies-form").removeClass("disabled");
                                }'
            ),
            'class' => 'filter region ' . $checked . ' ' . $done,
            'id' => 'region-' . $index,
            'value' => $filter['name']
        ));
        echo '
								<label for="region-' . $index . '">
									<span><img src="' . Yii::app()->request->baseUrl.'/lib/skin/img/check-companies.png' . '" class="check-companies" /></span>'
            .Yii::t('region', $filter['name'])
            .'</label>
							</li>';
    }
    ?>
</ul>

<h3 class="page-header"><?php echo Yii::t('word', 'Country', PLURAL); ?></h3>
<ul id="country-filters">
    <?php
    // Display country filters
    foreach($orderCompaniesForm->countryFilters as $index => $filter)
    {
        // Show filter row
        if(empty($counties_from_session))
        {
            $checked = '';
        }
        // Check stored checkboxes
        elseif(in_array($filter['name'], $counties_from_session))
        {
            $checked = 'checkedByCookies';
        }
        // Don't check not stored checkboxes
        else
        {
            $checked = '';
        }

        if($this->CheckIfAllInArray($this->getAllCompaniesByCountry($filter['name']), Yii::app()->session['OrderCompaniesForm']['companies'])) {
            $done = 'done';
        } else {
            $done = 'not-done';
        }

        echo '<li>';
        echo CHtml::checkBox('', false, array(
            'ajax' => array(
                'type'=>'POST',
                'url'=>  Yii::app()->createUrl('order/AjaxChangeSession'),
                'data' => array('id' => $filter['name'], 'status' => 'work-country', 'page' => $pages->getCurrentPage()),
                'success' => 'js:function(val){
                                   $("#render_companies_form").html(val);
                                   $("#companies-form").removeClass("disabled");
                                }'
            ),
            'class' => 'filter country ' . $checked . ' ' .$done,
            'id' => 'country-' . $index,
            'value' => $filter['name']
        ));

        echo '<label for="country-' . $index . '">
									<span><img src="' . Yii::app()->request->baseUrl.'/lib/skin/img/check-companies.png' . '" class="check-companies" /></span>'
            .Yii::t('country', $filter['name'])
            .'</label>
							</li>';
    }
    // Restore current language
    Yii::app()->sourceLanguage = $storedSourceLanguage;
    ?>
</ul>

</div>
</div>
<div class="span4" id="company-img">

    <h3 class="page-header">
        <?php echo Yii::t('word', 'Company', PLURAL) .
            ' (<span class="companies-count-selected">' .
            count(Yii::app()->session['OrderCompaniesForm']['companies']).
            '</span>/<span class="companies-count-all">' .
            $all_companies_count .
            '</span>)'.
            ' $<price class="total">10.00'.
            '</price>';
        ?>
    </h3>
    <?php
    // Get total items
    $itemsCount = count($models);
    // Columns count
    $columnsCount = 3;
    // Calculate items per column
    $itemsPerColumn = round($itemsCount / $columnsCount);
    // Separate companies for parts
    $companiesColumns = array_chunk($models, $itemsPerColumn);
    // Display each of the parts as a column
    foreach($companiesColumns as $companiesColumn)
    {
        // Column start
        echo '<div class="col-md-4">';
        // Show column items
        foreach($companiesColumn as $index => $company)
        {
            // Get stored form data
            $storedOrderCompaniesForm= Yii::app()->session->get('OrderCompaniesForm');
            // Get form companies data
            $storedCompanies = $storedOrderCompaniesForm['companies'];
            // If no stored data for companies then check all checkboxes by default

            if(empty($storedCompanies))
            {
                $checked = '';
            }
            // Check stored checkboxes
            elseif(in_array($company['company'], $storedCompanies))
            {
                $checked = 'checkedByCookies';
            }
            // Don't check not stored checkboxes
            else
            {
                $checked = '';
            }
            // Show item row
            echo '<div class="form-group">';
            echo CHtml::checkBox('OrderCompaniesForm[companies][]', false, array(
                'ajax' => array(
                    'type'=>'POST',
                    'url'=>  Yii::app()->createUrl('order/AjaxChangeSession'),
                    'data' => array('id' => $company['company'], 'status' => 'single', 'page' => $pages->getCurrentPage()),
                    'success' => 'js:function(val){
                                    $("#render_companies_form").html(val);
                                }'
                ),
                'class' => 'company ' . $checked,
                'id' => 'company-' . $company['company'],
                'sectors' => $this->getSectorsById($company['company']),
                'country"' => $company['country'],
                'value' => $company['company']
            ));
            echo '
							<label for="company-' . $company['company'] . '">
								<span><img src="' . Yii::app()->request->baseUrl.'/lib/skin/img/check-companies-white.png' . '" class="check-companies" /></span> '
                .CHtml::image(Yii::app()->request->baseUrl . '/images/' . md5($company['name'].Yii::app()->controller->id.Yii::app()->controller->action->id) . '.png', '', array(
                    'data-src' => Yii::app()->request->baseUrl . '/images/' . md5($company['name'].Yii::app()->controller->id.Yii::app()->controller->action->id).'.png',
                    'data-src-hover' => Yii::app()->request->baseUrl . '/images/' . md5($company['name'].Yii::app()->controller->id.Yii::app()->controller->action->id).'hover.png',
                ))
                .'</label>';
            echo '</div>';
        }
        // Column end
        echo '</div>';
    }
    ?>
</div>
<?php
//echo $form->error($orderCompaniesForm, 'companies');
?>
<div class="clear"></div>

<div class="pagination-block">
    <?php $this->widget('CLinkPager', array(
        'pages' => $pages,
    )) ?>
</div>

</div>

<script>
    loadCompaniesSelect();
</script>