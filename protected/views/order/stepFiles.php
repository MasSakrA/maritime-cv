﻿<?php
/** 
 * Order initial form (Step 1) template
 */

// Set default page
if( ! isset($currentPage))
{
	$currentPage = 'order/files';
}

// Pages statuses
$homePage = false;
if( $currentPage === 'home')
{
	$homePage = true;
}

?>

<div class="row-fluid-5">
	<div class="span2 padding-right">
		<?php echo Yii::t('filesPage', 'File Upload Explanation'); ?>
	</div>
	<div class="span3">
<?php


$form=$this->beginWidget('CActiveForm', array(
	"id"=>"order-form",
	"enableAjaxValidation"=>true,
	"clientOptions"=>array(
		"validationUrl"=>"/order/files",
		"validateOnType"=>true,
		"validateOnSubmit"=>true,
                'onsubmit'=>"return false;",
                'onkeypress'=>" if(event.keyCode == 13){ send(); } "                             
	),
	'htmlOptions'=>array(
		'enctype'=>'multipart/form-data')
));


?>
	<div>
		<div class="col-md-4">
			<div id="cv-file-select" class='square-box'>

				<div class='square-content' title="<?php if(isset(Yii::app()->session['OrderFilesForm']['CVFile'])) echo Yii::app()->session['OrderFilesForm']['CVFile']; ?>">
				<div class="dt">
				<div class="dc">

					<div class="file-loaded-mark glyph glyphicon glyphicon-ok"><span><?php echo pathinfo( @Yii::app()->session['OrderFilesForm']['CVFile'], PATHINFO_EXTENSION ); ?></span></div>
					<div class="glyph">
						<span class="glyph-item mega" aria-hidden="true" data-icon="&#xe085;" data-js-prompt="&#xe085;"></span>
					</div>
					<span><?php echo Yii::t('phrase', 'Click to upload'); ?></span>
					<span class="lead"><?php echo Yii::t('phrase', 'Your CV'); ?></span>

				</div>
				</div>
				</div>

			</div>
			<div class="errorMessage file" id="error-cv"><?php echo Yii::t('phrase', 'Allowed file types'); ?><span></span></div>
			<div class="errorMessage file" id="error-cv-empty"><?php echo Yii::t('phrase', 'Attach your CV'); ?><span></span></div>
            <div class="errorMessage file" id="error-cv-size"><?php echo Yii::t('phrase', 'File size error'); ?><span></span></div>
		</div>

		<div class="col-md-4">
			<div id="attachment1-select" class='square-box'>

				<div class='square-content'>
				<div class="dt">
				<div class="dc">
					
					<div class="file-loaded-mark glyph glyphicon glyphicon-ok"><span><?php echo pathinfo( @Yii::app()->session['OrderFilesForm']['attachment1'], PATHINFO_EXTENSION ); ?></span></div>
					<div class="glyph">
						<span class="glyph-item mega" aria-hidden="true" data-icon="&#xe093;" data-js-prompt="&#xe093;"></span>
					</div>
					<span><?php echo Yii::t('phrase', 'Click to upload'); ?></span>
					<span class="lead"><?php echo Yii::t('phrase', 'File 1'); ?></span>

				</div>
				</div>
				</div>

			</div>
			<div class="errorMessage file" id="error-file-1"><?php echo Yii::t('phrase', 'Allowed file types'); ?><span></span></div>
            <div class="errorMessage file" id="error-file-size-1"><?php echo Yii::t('phrase', 'File size error'); ?><span></span></div>
		</div>

		<div class="col-md-4">
			<div id="attachment2-select" class='square-box'>

				<div class='square-content'>
				<div class="dt">
				<div class="dc">

					<div class="file-loaded-mark glyph glyphicon glyphicon-ok"><span><?php echo pathinfo( @Yii::app()->session['OrderFilesForm']['attachment2'], PATHINFO_EXTENSION ); ?></span></div>
					<div class="glyph">
						<span class="glyph-item mega" aria-hidden="true" data-icon="&#xe093;" data-js-prompt="&#xe093;"></span>
					</div>
					<span><?php echo Yii::t('phrase', 'Click to upload'); ?></span>
					<span class="lead"><?php echo Yii::t('phrase', 'File 2'); ?></span>

				</div>
				</div>
				</div>

			</div>
			<div class="errorMessage file" id="error-file-2"><?php echo Yii::t('phrase', 'Allowed file types'); ?><span></span></div>
            <div class="errorMessage file" id="error-file-size-2"><?php echo Yii::t('phrase', 'File size error'); ?><span></span></div>
		</div>

		<div class="clearfix"></div>
	</div>
	
	<div class="row-fluid-5">
		<div class="form-group hidden">
			<?php
			// File input
			echo $form->fileField($orderFilesFormModel, 'CVFile', array(
                                'id'=>'cv-file'
                            )
                        );
			// Hidden inut with  file name from session
			echo CHTML::hiddenField('CVFile-name', @Yii::app()->session['OrderFilesForm']['CVFile'], array(
				"id"=>"cv-file-name"));				
			// File input error
			echo $form->error($orderFilesFormModel, 'CVFile'); 
			
			// File input
			echo $form->fileField($orderFilesFormModel, 'attachment1', array(
				"id"=>"attachment1"));
			// Hidden inut with  file name from session
			echo CHTML::hiddenField('attachment1-name', @Yii::app()->session['OrderFilesForm']['attachment1'], array(
				"id"=>"attachment1-name"));		
			// File input error
			echo $form->error($orderFilesFormModel, 'attachment1'); 
			
			// File input
			echo $form->fileField($orderFilesFormModel, 'attachment2', array(
				"id"=>"attachment2"));
			// Hidden inut with  file name from session
			echo CHTML::hiddenField('attachment2-name', @Yii::app()->session['OrderFilesForm']['attachment2'], array(
				"id"=>"attachment2-name"));	
			// File input error
			echo $form->error($orderFilesFormModel, 'attachment2'); 
			?>
		</div>

		<div class="<?php echo $homePage ? 'col-md-8 ' : 'email-margin '; ?>form-group">
			<?php 
			echo $form->textField($orderFilesFormModel, 'email', array(
				"class"=>"form-control",
				"placeholder"=> Yii::t('phrase', 'Your Email') ));
			echo $form->error($orderFilesFormModel, 'email'); 
			?>
		</div>	
		
		<?php if($homePage): ?>
		<div class="col-md-4 form-group">
			<?php 
			echo CHtml::submitButton( Yii::t('phrase', 'Continue'), array(
				"class"=>"btn btn-warning btn-block",
				"submit"=>'/order/files')); 
			?>
		</div>
		<?php endif; ?>
		
	</div><!-- row-fluid-5 -->
	</div>
	
	<?php if( ! $homePage): ?>

<div class="row-fluid-5">
<div class="span5 buttons">

	<?php
	// The reset button
	echo CHtml::button( Yii::t('phrase','Reset'), array(
		"class"=>"btn reset btn-primary pull-left"));
	?>
	<?php		
	echo CHtml::submitButton( Yii::t('phrase', 'Next Step'), array(
			"class"=>"btn btn-warning pull-right")); 
	?>
	

</div>
</div>
	
<?php endif; ?>
	
	<?php $this->endWidget(); ?>
</div>	



