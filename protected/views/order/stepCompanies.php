<?php

//$orderedCompanies = Yii::app()->session['OrderCompaniesForm']['companies'];

/**
 * Order Companies (Step 3) template
 */
$form=$this->beginWidget('CActiveForm', array(
    "id"=>"companies-form"
));
?>
<img src="<?php echo Yii::app()->request->baseUrl.'/lib/skin/img/preloader.gif'; ?>" id="loader-img" />
<div id="loader"></div>
<div id="render_companies_form">

    <?php
    $filter = $this->renderPartial('stepCompaniesCatFilter', array(
            'orderCompaniesForm' => $orderCompaniesForm,
            'models' => $models,
            'pages' => $pages,
            'regions_from_session' => $regions_from_session,
            'counties_from_session' => $counties_from_session,
            'all_companies_count' => $all_companies_count,
            'counties_regions_from_session' => $counties_regions_from_session
        ), true, false
    );
    echo $filter;
    ?>

</div>

	<div class="row-fluid-5">



		<div class="span5 buttons">

<!--            <div class="progress"></div>-->
		
		<?php
		// The reset button
		echo CHtml::button( Yii::t('phrase','Reset'), array(
			"class"=>"btn reset btn-primary pull-left"));

        echo CHtml::ajaxLink(
            Yii::t('phrase', 'clear form'),
            Yii::app()->createUrl('order/AjaxChangeSession'),
            array(
                'type' => 'POST',
                'beforeSend' => "function( request )
                 {
                    $('#companies-form').addClass('disabled');
                 }",
                'success' => "function( val )
                {
                    $('#render_companies_form').html(val);
                    $('#companies-form').removeClass('disabled');
                }",
                'data' => array( 'id' => 'none', 'status' => 'work-area', 'page' => $pages->getCurrentPage()  )
            ),
            array(
                'class' => "clear-companies-session btn btn-primary pull-left"
            )
        );

		?>
		<?php
		echo CHtml::button(Yii::t('phrase', 'Next Step'), array(
			"class"=>"btn btn-warning pull-right",
            "onclick"=>"js:location.href='$this->nextStep'"
        ));
		?>
		<?php 
		echo CHtml::button(Yii::t('phrase', 'Previous Step'), array(
			"class"=>"btn btn-primary pull-right",
			"onclick"=>"js:location.href='$this->previousStep'"
        ));
		?>
		
		</div>
	</div>
<?php $this->endWidget(); ?>

<a href="#0" class="cd-top" id="toTop">Top</a>
