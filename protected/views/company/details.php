<?php
/**
 * Company office details page template
 */
?>
<!-- CAROUSEL -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">

	<!-- indicators -->
	<!-- <ol class="carousel-indicators">
		<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		<li data-target="#myCarousel" data-slide-to="1"></li>
	</ol> -->

	<!-- slides -->
	<div class="carousel-inner">
	
		<div id="slide-1" class="item active"> 
			<div class="container">
				<div class="carousel-caption">
					<!-- <h1>Example headline.</h1>
					<p>Note: If you're viewing this page via a <code>file://</code> URL, the "next" and "previous" Glyphicon buttons on the left and right might not load/display properly due to web browser security rules.</p>
					<p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p> -->
				</div>
			</div>
		</div>
		
		<!-- <div id="slide-1" class="item">
			<div class="container">
				<div class="carousel-caption">
					<h1>Another example headline.</h1>
					<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
					<p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>
				</div>
			</div>
        </div> -->
		
	</div>

	<!-- left right navigation -->
	<!-- <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
	<a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a> -->
	
</div>
<div class="container">
	<div class="row-fluid-5">
		<h1 class="page-header"><?php echo $Office->parentCompany->name; ?></h1>
	</div>
	<?php
	echo 'Email: ' . $Office->email . '<br />';
	echo Yii::t('word', 'Country', SINGULAR) . ': ' . Yii::t('country', $Office->country) . '<br />';
	echo Yii::t('word', 'Sector', SINGULAR) . ': ' . ($Office->workarea ? Yii::t('workarea', $Office->workarea[0]->workArea) : Yii::t('word', 'No')) . '<br /><br />';
	echo '<a href="/company"><button class="btn btn-warning">'.Yii::t('phrase', 'to company list').'</button></a>';
	?>
</div>