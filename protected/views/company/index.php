<?php
/**
 * Companies list template
 */

?>
<!-- CAROUSEL -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">

	<!-- indicators -->
	<!-- <ol class="carousel-indicators">
		<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		<li data-target="#myCarousel" data-slide-to="1"></li>
	</ol> -->

	<!-- slides -->
	<div class="carousel-inner">
	
		<div id="slide-1" class="item active"> 
			<div class="container">
				<div class="carousel-caption">
					<!-- <h1>Example headline.</h1>
					<p>Note: If you're viewing this page via a <code>file://</code> URL, the "next" and "previous" Glyphicon buttons on the left and right might not load/display properly due to web browser security rules.</p>
					<p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p> -->
				</div>
			</div>
		</div>
		
		<!-- <div id="slide-1" class="item">
			<div class="container">
				<div class="carousel-caption">
					<h1>Another example headline.</h1>
					<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
					<p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>
				</div>
			</div>
        </div> -->
		
	</div>

	<!-- left right navigation -->
	<!-- <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
	<a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a> -->
	
</div>
<div class="container" id="nocopy">
	<div class="row-fluid-5">
		<h1 class="page-header">Companies list - <?php echo $officesQty; ?></h1>
	</div>
<?php

// Display each company
foreach($offices as $Office)
{
	echo '<a href="/company/details/' . $Office->id . '" class="underline-img">' . CHtml::image(Yii::app()->request->baseUrl . '/images/' . md5($Office->parentCompany->name.Yii::app()->controller->id.Yii::app()->controller->action->id) . '.png') . '</a><br />';
}
?>
</div>