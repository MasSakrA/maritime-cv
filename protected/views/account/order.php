<?php
/*
 * User account order page template
 */
?>
	<div class="account-order" style="line-height: 22px;margin-bottom:50px;">
		<div class="row">
			<label class="col-md-3 control-label"><?php echo Yii::t('phrase','number of order'); ?></label>
			<span class="form-control-static"><?php echo $Order->number; ?></span>
		</div>

		<div class="row">
			<label class="col-md-3 control-label"><?php echo Yii::t('phrase', 'selected companies'); ?></label>
			<span class="form-control-static"><?php echo $Order->orderedCompaniesCount; ?></span>
		</div>

		<div class="row">
			<label class="col-md-3 control-label"><?php echo Yii::t('word','cost'); ?></label>
			<span class="form-control-static"><?php echo Yii::app()->locale->getCurrencySymbol($Order->currency) . ' ' .number_format($Order->amount, 2, '.', ''); ?></span>
		</div>

		<div class="row">
			<label class="col-md-3 control-label"><?php echo Yii::t('word','status'); ?></label>
			<span class="form-control-static"><?php echo $Order->paid ? Yii::t('word', 'paid') : Yii::t('word', 'unpaid')?></span>
		</div>
	</div>

<br />

<?php
// If the order is paid do not show pay button
if( ! $Order->paid)
{
	echo CHtml::submitButton(Yii::t('phrase', 'go to payment'), array(
		"class"=>"btn btn-primary"));
	echo ' ';
}
echo CHtml::button(Yii::t('phrase', 'view status'), array(
	"class"=>"btn btn-warning",
	"onclick"=>"js:location.href='/account/order/status'"));
?>
