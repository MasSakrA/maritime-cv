<?php
/**
 * General user order data preview template
 */
?>

<style type="text/css">
<?php
// Display the styles for the color schemes
foreach($ContentModel->colorSchemes as $index => $item)
{
	echo $item['additionalCSS']."\n".$item['scheme']."\n\n\n";
}
?>
</style>

<!-- Two columns full width row -->
<div class="row">
	
	<!-- First column -->
	<div class="col-md-6">
		
		<div class="row form-group">
			<label class="col-md-4 control-label"><?php echo Yii::t('word', 'Email', SINGULAR); ?></label>
			<span class="form-control-static"><?php echo $GeneralForm->email ?></span>
		</div>

		<div class="row form-group">
			<label class="col-md-4 control-label"><?php echo Yii::t('word', 'CV', SINGULAR); ?></label>
			<p class="form-control-static"><?php echo $GeneralForm->CVFile ?></p>
			
		</div>
		
		<div class="row form-group">
			<label class="col-md-4 control-label"><?php echo Yii::t('phrase', 'CV File Name'); ?></label>
			<p class="form-control-static"><?php echo $GeneralForm->CVFileName ?></p>			
		</div>
		
		<?php if($GeneralForm->attachment1 !== '')
		{
		?>
		<div class="row form-group">
			<label class="col-md-4 control-label"><?php echo Yii::t('word', 'Attachment', SINGULAR); ?> 1</label>
			<p class="form-control-static"><?php echo $GeneralForm->attachment1 ?></p>
			
		</div>
		
		<div class="row form-group">
			<label class="col-md-4 control-label"><?php echo Yii::t('phrase', 'Attachment {number} Name', array( '{number}' => 1 )); ?></label>
			<p class="form-control-static"><?php echo $GeneralForm->attachment1Name ?></p>
		</div>
		<?php
		}
		?>
		
		<?php if($GeneralForm->attachment2 !== '')
		{
		?>
		<div class="row form-group">
			<label class="col-md-4 control-label"><?php echo Yii::t('word', 'Attachment', SINGULAR); ?> 2</label>
			<p class="form-control-static"><?php echo $GeneralForm->attachment2 ?></p>
			
		</div>
		
		<div class="row form-group">
			<label class="col-md-4 control-label"><?php echo Yii::t('phrase', 'Attachment {number} Name', array( '{number}' => 2 )); ?></label>
			<p class="form-control-static"><?php echo $GeneralForm->attachment2Name ?></p>
		</div>
		<?php
		}
		?>

	</div>
	
	<!-- Second column -->
	<div class="col-md-6">
		
		<div class="row form-group">
			<label class="col-md-4 control-label"><?php echo Yii::t('phrase', 'Email Subject'); ?></label>
			<span class="form-control-static"><?php echo $GeneralForm->subject ?></span>
		</div>
		
		<div class="row form-group">
			<label class="col-md-4 control-label"><?php echo Yii::t('phrase', 'Email Content'); ?></label>
			<div class="col-md-12 form-control-static form-control email-content <?php echo 'cs-' . $GeneralForm->colorScheme; ?>"><?php echo $GeneralForm->body ?></div>
		</div>
		
		
	</div>
	
	<!-- Selected companies list below previous two columns -->
<!--	<div class="col-md-12 preview">-->
<!--		<h3 class="page-header">--><?php //echo Yii::t('word', 'Company', PLURAL); ?><!--</h3>-->
		<?php
//		// Show each company
//		$selectedCompanyIds = $GeneralForm->selectedCompanyIds;
//		$companyList = $GeneralForm->companyList;
//		// Check data type
//		if(isset($selectedCompanyIds) && is_array($selectedCompanyIds) && isset($companyList) && is_array($companyList))
//		{
//			foreach($selectedCompanyIds as $selectedCompanyId)
//			{
//				// Search company with current id in all company list
//				foreach($companyList as $company)
//				{
//					if($company->id == $selectedCompanyId)
//					{
//						echo '
//							<div class="col-md-3 company" title="">' . CHtml::image(Yii::app()->request->baseUrl . '/images/' . md5($company->name.Yii::app()->controller->id.Yii::app()->controller->action->id) .
//                                '.png', '') . '</div>
//						';
//					}
//				}
//			}
//		}
		?>
<!--	</div>-->
	
</div>