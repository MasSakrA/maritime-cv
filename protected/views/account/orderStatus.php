<?php
/*
 * Service status page template
 */
?>
<div style="line-height: 22px;">
<?php

/**
 * @var array[]		A list of mailed companies
 */
//$company['name']
$mailedCompanies = $Order->GetMailedCompanies();
foreach($models as $company)
{
	echo date('d.m.Y H:i:s', strtotime($company['datetimeOut'])) . '&nbsp;&nbsp;&nbsp;&nbsp;';
	echo CHtml::image(Yii::app()->request->baseUrl . '/images/' . md5($this->getNameByIdInArray($company['id'], $mailedCompanies).Yii::app()->controller->id.Yii::app()->controller->action->id) .
            '.png', '') . '<br />';
}
?>
<div class="pagination-block">
<?php $this->widget('CLinkPager', array(
    'pages' => $pages,
)) ?>
</div>
<?php
echo '<hr><b> '.Yii::t('phrase', 'total notification companies').': ' . count($mailedCompanies) . '/' . $Order->GetOrderedCompaniesCount() . '</b><br />';


if(count($mailedCompanies) === $Order->GetOrderedCompaniesCount())
{
	$statusSuccess = Yii::t('word', 'completed');
	$leftToNotify = '';
}
else
{
	$statusSuccess = Yii::t('phrase', 'not completed');
	$leftToNotify = '<b>'.Yii::t('phrase', 'unnotificated companies left').': ' . ($Order->GetOrderedCompaniesCount() - count($mailedCompanies)) .'</b><br />';
}
	echo $leftToNotify;
	echo '<b>'.Yii::t('phrase', 'order status').': ' . $statusSuccess .'</b>';
?>
</div>