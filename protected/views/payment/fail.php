<?php
/**
 * The fail payment page
 */
?>
<div id="banner" class="container-fluid banner-0">
    <div class="container">
        <div class="row-fluid-5">
        </div>
    </div>
</div>
<div class="container">
	<h1><?php echo Yii::t('phrase', 'the order pay is fail'); ?></h1>
	<?php echo Yii::t('phrase','pay error');  ?><br />
        <?php 
        $call_back_url = Yii::app()->createUrl("order/payment");        
        ?>
        <p class="countdown_row">
            <?php echo Yii::t('phrase', 'You will be redirected in').': '; ?>
            <span class="countdown"></span>
            <?php echo '. <a href="'.$call_back_url.'" title="'.Yii::t('phrase', 'or just click here, for instant redirect').'">'.Yii::t('phrase', 'or just click here, for instant redirect').'</a>'; ?>
        </p>
</div>
<?php Yii::app()->clientScript->registerMetaTag("10;url={$call_back_url}", null, 'refresh'); ?>