<?php
/**
 * The success payment page
 */
?>
<div id="banner" class="container-fluid banner-0">
    <div class="container">
        <div class="row-fluid-5">
        </div>
    </div>
</div>
<div class="container payment-message">
	<h1><?php echo Yii::t('phrase','successfully paid up'); ?></h1>
	<?php echo Yii::t('phrase','thank you paid up'); ?>
        <?php 
        $call_back_url = Yii::app()->createUrl("order/payment");
        Yii::app()->clientScript->registerMetaTag("10;url={$call_back_url}", null, 'refresh'); 
        ?>
        <p class="countdown_row">
            <?php echo Yii::t('phrase', 'You will be redirected in').': '; ?>
            <span class="countdown"></span>
            <?php echo '. <a href="'.$call_back_url.'" title="'.Yii::t('phrase', 'or just click here, for instant redirect').'">'.Yii::t('phrase', 'or just click here, for instant redirect').'</a>'; ?>
        </p>
</div>