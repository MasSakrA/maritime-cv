<?php

class AdminModule extends CWebModule
{
	/**
	 * Module assets url
	 * @var string 
	 */
	private $assetsUrl;
	
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'admin.models.*',
			'admin.components.*',
		));
	}

	/**
	 * Publish module assets and return published url
	 * 
	 * @return string	Published url
	 */
	public function GetAssetsUrl()
	{
		if($this->assetsUrl === null)
		{
			/** @var bool Whether the published directory should be named as the hashed basename*/
			$hashByName = false;
			/** @var int level of recursive copying when the asset is a directory. Level -1 means publishing all subdirectories and files. */
			$level = -1;
			/** @var bool Whether we should copy the asset file or directory even if it is already published before. Sets to TRUE on development. */
			$forceCopy = YII_DEBUG;
			$this->assetsUrl = Yii::app()->getAssetManager()->publish(
				Yii::getPathOfAlias('admin.assets'), $hashByName, $level, $forceCopy);
		}
		return $this->assetsUrl;
    }
	
	public function beforeControllerAction($controller, $action)
	{
		// Set layout for all module controllers
		Yii::app()->controller->layout='/layouts/admin';
		// Set user login url
		Yii::app()->user->loginUrl = array('/admin/login/');
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
