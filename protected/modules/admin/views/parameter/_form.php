<?php
/* @var $this ParameterController */
/* @var $model Parameter */
/* @var $form CActiveForm */
?>

<div class="form-horizontal">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'parameter-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php
	if($this->action->id == 'create')
	{
	?>
        <?php echo Yii::t('phrase', 'required fields'); ?>
	<?php
	}
	?>

	<?php echo $form->errorSummary($model); ?>

	<div class="">
		<?php echo $form->labelEx($model,'category') . ': '; ?>
		<?php
		if($this->action->id == 'create')
		{
			echo $form->textField($model,'category',array('size'=>50,'maxlength'=>50));
		}
		else
		{
			echo $model->category;
		}
		?>
		<?php echo $form->error($model,'category'); ?>
	</div>

	<div class="">
		<?php echo $form->labelEx($model,'name') . ': '; ?>
		<?php
		if($this->action->id == 'create')
		{
			echo $form->textField($model,'name',array('size'=>60,'maxlength'=>100));
		}
		else
		{
			echo $model->name;
		}
		?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="">
		<?php echo $form->labelEx($model,'value'); ?>
		<?php echo $form->textField($model,'value',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'value'); ?>
	</div>

	<div class="buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('word', 'Create') : Yii::t('word', 'Save')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->