<?php
/* @var $this ParameterController */
/* @var $model Parameter */

$this->breadcrumbs=array(
	'Parameters'=>array('index'),
	'Manage',
);

$this->menu=array(
	//array('label'=>'List Parameter', 'url'=>array('index')),
	//array('label'=>'Create Parameter', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#parameter-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('phrase', 'Manage Parameters'); ?></h1>

<p>
<?php echo Yii::t('phrase', 'search guid text'); ?>
</p>

<?php echo CHtml::link(Yii::t('phrase', 'Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
	'categoryFilterData' => $categoryFilterData
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'parameter-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
			'name' => 'category',
			'header' => Yii::t('word', 'Category', SINGULAR),
			'value' => 'Yii::t("Parametercategory", $data->category, SINGULAR)',
			'filter' => CHtml::dropDownList(
				'Parameter[category]',
				$model->category,
				$categoryFilterData,
				array('empty' => Yii::t('word', 'All'))
			)
		),
		array(
			'name' => 'name',
			'value' => 'Yii::t("Parameter", $data->name)',
			'filter' => '',
			'header' => Yii::t('word', 'Name', SINGULAR)
		),
		array(
			'name' => 'value',
			'header' => Yii::t('word', 'Value', SINGULAR)
		),
		array(
			'class' => 'CButtonColumn',
			'template' => '{update}'
		)
	)
)); ?>
