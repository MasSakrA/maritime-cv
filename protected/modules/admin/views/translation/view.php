<?php
/* @var $this TranslationController */
/* @var $model SourceMessage */

$this->breadcrumbs=array(
	'Source Messages'=>array('index'),
	$model->id,
);

$menu = array();
if(Yii::app()->user->getName() === 'developer')
{
	$menu[] = array('label'=>Yii::t('phrase', 'Create SourceMessage'), 'url'=>array('create'));
	$menu[] = array('label'=>Yii::t('phrase', 'Delete SourceMessage'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'));
}
$menu[] = array('label'=>Yii::t('phrase', 'Update SourceMessage'), 'url'=>array('update', 'id'=>$model->id));
$menu[] = array('label'=>Yii::t('phrase', 'Manage SourceMessage'), 'url'=>array('admin'));

$this->menu = $menu;
?>

<h1><?php echo Yii::t('phrase', 'View SourceMessage'). ' #' . $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'category',
		'message',
		array(
			'name' => Yii::t('language', 'en'),
			'value' => $model->GetTranslation("en")
		),
		array(
			'name' => Yii::t('language', 'ru'),
			'value' => $model->GetTranslation("ru")
		)
	),
)); ?>
