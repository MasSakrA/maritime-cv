<?php
/* @var $this TranslationController */
/* @var $SourceMessage SourceMessage */

$this->breadcrumbs=array(
	'Source Messages'=>array('index'),
	'Create',
);

$this->menu=array(
	//array('label'=>'List SourceMessage', 'url'=>array('index')),
	array('label'=>Yii::t('phrase', 'Manage SourceMessage'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('phrase', 'Create Source Message'); ?></h1>

<?php $this->renderPartial('_form', array('SourceMessage'=>$SourceMessage)); ?>