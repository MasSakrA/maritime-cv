<?php
/* @var $this TranslationController */
/* @var $SourceMessage SourceMessage */

$this->breadcrumbs=array(
	'Source Messages'=>array('index'),
	$SourceMessage->id=>array('view','id'=>$SourceMessage->id),
	'Update',
);

$menu = array();
if(Yii::app()->user->getName() === 'developer')
{
	$menu[] = array('label'=>Yii::t('phrase', 'Create SourceMessage'), 'url'=>array('create'));
	$menu[] = array('label'=>Yii::t('phrase', 'Delete SourceMessage'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$SourceMessage->id),'confirm'=>'Are you sure you want to delete this item?'));
}
$menu[] = array('label'=>Yii::t('phrase', 'Manage SourceMessage'), 'url'=>array('admin'));

$this->menu = $menu;
?>

<h1><?php echo Yii::t('phrase', 'Update SourceMessage') . ' #' .$SourceMessage->id; ?></h1>

<?php $this->renderPartial('_form', array('SourceMessage'=>$SourceMessage)); ?>