<?php
/**
 * @var TranslationController $this
 * @var SourceMessage $SourceMessage
 */

$this->breadcrumbs=array(
	'Source Messages'=>array('index'),
	'Manage',
);

$menu = array();
if(Yii::app()->user->getName() === 'developer')
{
	$menu[] = array('label'=>Yii::t('phrase', 'Create SourceMessage'), 'url'=>array('create'));
}
$this->menu = $menu;

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#source-message-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});

/**
 * On seraching english translation disable russian
 * translation filter and conversely.
 */
$('.search-language-en').live('click',function()
{
	$('input', this).prop('disabled', false).focus();
	$('.search-language-ru input').val('').prop('disabled', true);
});
$('.search-language-ru').live('click',function()
{
	$('input', this).prop('disabled', false).focus();
	$('.search-language-en input').val('').prop('disabled', true);
});
");
?>

<h1><?php echo Yii::t('phrase', 'Manage Source Messages'); ?></h1>

<p>
<?php echo Yii::t('phrase', 'search guid text'); ?>
</p>

<?php echo CHtml::link(Yii::t('phrase', 'Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'SourceMessage' => $SourceMessage,
	'categoryFilterData' => $categoryFilterData
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'source-message-grid',
	'dataProvider'=>$SourceMessage->search(),
	'filter'=>$SourceMessage,
	'columns'=>array(
		array(
			'name' => 'category',
			'header' => Yii::t('word', 'Category', SINGULAR),
			'value' => 'Yii::t("word", Yii::app()->Str->Ucfirst($data->category), SINGULAR)',
			'filter' => CHtml::dropDownList(
				'SourceMessage[category]',
				$SourceMessage->category,
				$categoryFilterData,
				array('empty' => Yii::t('word', 'All'))
			)
		),
		array(
			'name' => 'message',
			'header' => Yii::t('word', 'Message', SINGULAR)
		),
		array(
			'name' => 'search[translation][en]',
			'header' => Yii::t('language', 'en'),
			'value'=> '$data->GetTranslation("en")',
			'filterHtmlOptions' => array(
				'class' => 'search-language-en'
			)
		),
		array(
			'name' => 'search[translation][ru]',
			'header' => Yii::t('language', 'ru'),
			'value'=> '$data->GetTranslation("ru")',
			'filterHtmlOptions' => array(
				'class' => 'search-language-ru'
			)
		),
		array(
			'class'=>'CButtonColumn',
			// Show update button only
			'template' => '{view} {update}'
		)
	)
)); ?>
