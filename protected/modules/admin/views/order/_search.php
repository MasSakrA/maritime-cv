<?php
/* @var $this OrderController */
/* @var $model Order */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user'); ?>
		<?php echo $form->textField($model,'user'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'number'); ?>
		<?php echo $form->textField($model,'number',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'creationDate'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model'=>$model,
			'attribute'=>'creationDate',
			'options' =>array('dateFormat' => 'yy-mm-dd')
			)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'amount'); ?>
		<?php echo $form->textField($model,'amount',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'currency'); ?>
		<?php echo $form->textField($model,'currency',array('size'=>3,'maxlength'=>3)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'paid'); ?>
		<?php echo $form->dropdownList($model,'paid', array( 1 =>Yii::t('word', 'Yes'), 0 => Yii::t('Word', 'No')), array('empty' => Yii::t('word', 'All'))); ?>
	</div>
	
	<div class="row">
		<?php echo $form->label($model,'executed'); ?>
		<?php echo $form->dropdownList($model,'executed', array( 1 =>Yii::t('word', 'Yes'), 0 => Yii::t('Word', 'No')), array('empty' => Yii::t('word', 'All'))); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton(Yii::t('word', 'Search')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->