<?php
/* @var $this OrderController */
/* @var $model Order */

$this->breadcrumbs=array(
	'Orders'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>Yii::t('phrase', 'List Order'), 'url'=>array('index')),
	array('label'=>Yii::t('phrase', 'Create Order'), 'url'=>array('create')),
	array('label'=>Yii::t('phrase', 'View Order'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>Yii::t('phrase', 'Manage Order'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('phrase', 'Update Order') . ' #' . $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>