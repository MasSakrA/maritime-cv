<?php
/* @var $this OrderController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Orders',
);

$this->menu=array(
	array('label'=>Yii::t('phrase', 'Create Order'), 'url'=>array('create')),
	array('label'=>Yii::t('phrase', 'Manage Order'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('phrase', 'Manage Order'); ?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
