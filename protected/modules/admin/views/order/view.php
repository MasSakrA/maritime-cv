<?php
/* @var $this OrderController */
/* @var $model Order */

$this->breadcrumbs=array(
	'Orders'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('phrase', 'List Order'), 'url'=>array('index')),
	array('label'=>Yii::t('phrase', 'Create Order'), 'url'=>array('create')),
	array('label'=>Yii::t('phrase', 'Update Order'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('phrase', 'Delete Order'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('phrase', 'Manage Order'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('phrase', 'View Order') . ' #' . $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user',
		'number',
		'creationDate',
		'amount',
		'currency',
		'paid',
		'executed',
	),
)); ?>
