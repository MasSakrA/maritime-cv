<?php
/* @var $this OrderController */
/* @var $model Order */

$this->breadcrumbs=array(
	'Orders'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>Yii::t('phrase', 'List Order'), 'url'=>array('index')),
	array('label'=>Yii::t('phrase', 'Create Order'), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#order-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('phrase', 'Manage Orders'); ?></h1>

<p>
<?php echo Yii::t('phrase', 'search guid text'); ?>
</p>

<?php echo CHtml::link(Yii::t('phrase', 'Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'order-grid',
	'dataProvider'=>$model->search(),
	'afterAjaxUpdate' => 'function(){$("#creationDateFilter").datepicker()}',
	'filter'=>$model,
	'columns'=>array(
		'id',
		array(
			'name' => 'user',
			'type' => 'raw',
			'value' => 'CHtml::link($data->User->email, "/admin/user/view?id=".$data->User->id, array("target"=>"blank"))'
		),
		'number',
		array(
			'name' => 'creationDate',
			'value' => 'date("d.m.Y", strtotime($data->creationDate))',			
			'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'name' => 'creationDateFilter',
				'model'=>$model,
				'attribute'=>'creationDate',
				'language'=>'en',
				'i18nScriptFile' => 'jquery.ui.datepicker-en.js',
				'defaultOptions' =>array('dateFormat' => 'dd.mm.yy')
			),true)
		),
		array(
			'name' => 'amount',
			'value' => '$data->amount . " " . Yii::app()->locale->getCurrencySymbol($data->currency)'
		),
		array(
			'name' => 'paid',
			'value' => '$data->paid ? Yii::t("word", "Yes") : Yii::t("word", "No")',
			'filter' => CHtml::dropDownList(
				'Order[paid]',
				$model->paid,
				array(1 =>Yii::t('word', 'Yes'), 0 => Yii::t('Word', 'No')),
				array('empty' => Yii::t('word', 'All'))
			)
		),
		array(
			'name' => 'executed',
			'value' => '$data->executed ? Yii::t("word", "Yes") : Yii::t("word", "No")',
			'filter' => CHtml::dropDownList(
				'Order[executed]',
				$model->executed,
				array( 1 =>Yii::t('word', 'Yes'), 0 => Yii::t('Word', 'No')),
				array('empty' => Yii::t('word', Yii::t('word', 'All')))
			)
		),
		array(
			'class'=>'CButtonColumn',
			'template' => '{view} {update} {delete} {run}',
			'buttons'=>array(
				'run'=>array(
					'label' => Yii::t('word', 'Run'),
					'url' => '"execute?id=$data->id"',
					'visible' => ' ! $data->executed',
					'click' => 'function(){return confirm("'.Yii::t('phrase', 'Start order execution').'?")}'				
		))),
	),
)); ?>
