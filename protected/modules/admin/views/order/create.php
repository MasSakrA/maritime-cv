<?php
/* @var $this OrderController */
/* @var $model Order */

$this->breadcrumbs=array(
	'Orders'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>Yii::t('phrase', 'List Order'), 'url'=>array('index')),
	array('label'=>Yii::t('phrase', 'Manage Order'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('phrase', 'Create Order'); ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>