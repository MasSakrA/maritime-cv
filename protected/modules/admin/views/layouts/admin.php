<?php

    $emailingStatus = (bool)Yii::app()->Settings->Get('status', 'queue');
    $emailingStatusName = ($emailingStatus) ? 'enabled' : 'disabled';

	/**
	 * Method checks if the specified menu is currentlyactive and set it as active if it is true.
	 * Use it in inline html code of menu
	 * @param string|bool $controllerName		Name of a controller which must be checked as active menu
	 * @param string|bool $actionName			Name of a controller action which must be checked as active menu
	 */
	function SetActiveMenu($controllerName = 'site', $actionName = false)
	{
		/**
		 * Initiallizing.
		 * 
		 * @var string $currentController Current controller name
		 * @var string $currentAction Current controller action
		 * @var string $activeClass Active menu class name
		 * @var string $currentMenu Current menu id
		 * @var string $menu Given menu id
		 */
		$currentController = Yii::app()->controller->id;
		$currentAction = Yii::app()->controller->action->id;
		$activeClass = 'active';
		$currentMenu = $currentController;
		if($actionName !== false)
		{
			$currentMenu .= $currentAction;
		}
		$menu = $controllerName;
		if($actionName !== false)
		{
			$menu .= $actionName;
		}
		/**
		 * Check is the specified menu is an active menu
		 */
		if($menu === $currentMenu)
		{
			// Wrtie active class name
			echo $activeClass;
		}
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<link rel="shortcut icon" href="/lib/assets/icon/favicon.ico" />
	<link rel="stylesheet" href="/lib/bootstrap/css/bootstrap.min.css" />
	<link rel="stylesheet" href="/lib/bootstrap/custom/row-fluid-5.min.css" />
	<link rel="stylesheet" href="/lib/bootstrap/custom/carousel.css" />
	<link rel="stylesheet" href="/lib/skin/default/style.css" />
	
	<?php
	/**
	 * Load admin module resources
	 * @var string $adminModuleResources	Url to module resources
	 */
	$adminModuleResources = $this->module->GetAssetsUrl();
	?>
	<link rel="stylesheet" href="<?php echo $adminModuleResources; ?>/css/adminpanel.css" />
	<link rel="stylesheet" href="<?php echo $adminModuleResources; ?>/css/form.css" />	
	
	<!-- The Yii jQuery including -->
	<?php Yii::app()->clientScript->registerCoreScript('jquery.ui') ?>
	<!-- The Bootstrap javascript -->
	<script src="/lib/bootstrap/js/bootstrap.min.js"></script>
	
	<script src="/lib/core.js"> </script>

</head>
<body id="admin-area">

<div id="navigation" class="navbar navbar-default navbar-static-top" role="navigation">
<div class="container">
    <div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="/">
			<h2><span class="text-blue">Maritime</span> <span class="text-orange">CV</span> <span class="text-blue">Administration</span></h2>
		</a>
	</div>
	<div class="navbar-collapse collapse">
		<ul class="nav navbar-nav navbar-right">
            <li <?php echo Yii::app()->language === 'ru' ? 'class="active"' : ''; ?>><a href="/site/language/ru"><?php echo Yii::t('word', 'RU'); ?></a></li>
            <li <?php echo Yii::app()->language === 'en' ? 'class="active"' : ''; ?>><a href="/site/language/en"><?php echo Yii::t('word', 'EN'); ?></a></li>
		</ul>
	</div>
</div>
</div>
	<div class="container">
		<?php if( ! Yii::app()->user->isGuest) 
		{
		?>

		<ul id="admin-menu" class="nav navbar-nav">
			<li class="<?php SetActiveMenu('office'); ?>"><a href="/admin/office/admin"><?php echo Yii::t('word', 'Company', PLURAL); ?></a></li>
			<li class="<?php SetActiveMenu('order'); ?>"><a href="/admin/order/admin"><?php echo Yii::t('word', 'Order', PLURAL); ?></a></li>
			<li class="<?php SetActiveMenu('user'); ?>"><a href="/admin/user/admin"><?php echo Yii::t('word', 'User', PLURAL); ?></a></li>
			<li class="<?php SetActiveMenu('parameter'); ?>"><a href="/admin/parameter/admin"><?php echo Yii::t('word', 'Parameter', PLURAL); ?></a></li>
			<li class="<?php SetActiveMenu('translation'); ?>"><a href="/admin/translation/admin"><?php echo Yii::t('word', 'Translation', PLURAL); ?></a></li>
			<li class="<?php SetActiveMenu('emailtemplate'); ?>"><a href="/admin/emailtemplate/admin"><?php echo Yii::t('phrase', 'Email templates'); ?></a></li>
            <li class="<?php SetActiveMenu('page'); ?>"><a href="/admin/page/admin"><?php echo Yii::t('word', 'Pages'); ?></a></li>
            <li class="emailing-status-<?=$emailingStatusName?>"><a href="/admin/triggeremailing"><?php echo Yii::t('phrase', 'emailing '.$emailingStatusName); ?></a></li>
            <li class="<?php SetActiveMenu('logout'); ?>"><a href="/admin/logout"><?php echo Yii::t('word', 'logout'); ?></a></li>
		</ul>
		<?php
		}
		?>
		<div id="content" class="<?php if(Yii::app()->controller->action->id === 'importpreview') {echo Yii::app()->controller->action->id;} ?>">
            <?php if(Yii::app()->user->hasFlash('admin')): ?>
                    <?php echo Yii::app()->user->getFlash('admin'); ?>
            <?php endif; ?>
		<?php 
		// In this place shows content of layout
		echo $content; 
		?>
		</div>
        <?php if(Yii::app()->controller->action->id != 'importpreview') { ?>
		<div id="sidebar">
		<?php
			$this->beginWidget('zii.widgets.CPortlet', array(
				'title'=>Yii::t('word','Operations'),
			));
			$this->widget('zii.widgets.CMenu', array(
				'items'=>$this->menu,
				'htmlOptions'=>array('class'=>'operations'),
			));
			$this->endWidget();
		?>
		</div><!-- sidebar -->
        <?php } ?>
	</div>
<div class="clearfix"></div>
<br /><br /><br /><br />
<div id="footer">
<div class="container">
	<ul class="list-inline pull-left">
		<li class="<?php SetActiveMenu(); ?>"><a href="/">Maritime CV</a></li>
	</ul>
	<ul class="list-inline pull-right">
		<li>Maritime CV &copy; 2014 <?php echo Yii::t('phrase', 'all rights reserved'); ?></li>
	</ul>
	<div class="clearfix"></div>
</div>
</div>	

</body>
</html>
