<?php
/* @var $this TranslationController */
/* @var $SourceMessage SourceMessage */

$this->breadcrumbs=array(
	'Source Messages'=>array('index'),
	$SourceMessage->id=>array('view','id'=>$SourceMessage->id),
	'Update',
);

$menu = array();
if(Yii::app()->user->getName() === 'developer')
{
	$menu[] = array('label'=>'Create SourceMessage', 'url'=>array('create'));
	$menu[] = array('label'=>'Delete SourceMessage', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$SourceMessage->id),'confirm'=>'Are you sure you want to delete this item?'));
}
$menu[] = array('label'=>'Manage SourceMessage', 'url'=>array('admin'));

$this->menu = $menu;
?>

<h1>Update SourceMessage <?php echo $SourceMessage->id; ?></h1>

<?php $this->renderPartial('_form', array('SourceMessage'=>$SourceMessage)); ?>