<?php
/* @var $this TranslationController */
/* @var $SourceMessage SourceMessage */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($SourceMessage,'category'); ?>
		<?php echo $form->dropDownList($SourceMessage,'category', $categoryFilterData, array('empty' => Yii::t('word', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($SourceMessage,'message'); ?>
		<?php echo $form->textArea($SourceMessage,'message',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->