<?php
/* @var $this TranslationController */
/* @var $SourceMessage SourceMessage */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'source-message-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <?php echo Yii::t('phrase', 'required fields'); ?>

	<?php echo $form->errorSummary($SourceMessage); ?>

	<div class="row">
		<?php echo $form->labelEx($SourceMessage,'category'); ?>
		<?php
			if(Yii::app()->user->getName() === 'developer')
			{
				echo $form->textField($SourceMessage,'category');
			}
			else
			{
				echo Yii::t('messageCategory', $SourceMessage->category);
			}
		?>
		<?php echo $form->error($SourceMessage,'category'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($SourceMessage,'message'); ?>
		<?php
			if(Yii::app()->user->getName() === 'developer')
			{
				echo $form->textArea($SourceMessage,'message',array('rows'=>6, 'cols'=>50));
			}
			else
			{
				echo $SourceMessage->message;
			}
		?>
		<?php  ?>
		<?php echo $form->error($SourceMessage,'message'); ?>
	</div>
	
	
	<?php
	foreach($SourceMessage->messages as $index => $Message)
	{
	?>
	<div class="row">
		<?php echo CHtml::label(Yii::t('language', $Message->language), "Messages_{$index}_translation"); ?>
		<?php echo $form->textArea($Message,'translation',array(
			'rows' => 6,
			'cols' => 50,
			'name' => "Messages[$index][translation]")); ?>
		<?php echo $form->error($Message,'translation'); ?>
	</div>
	<?php
	}
	?>

	<div class="row buttons">
		<?php echo CHtml::submitButton($SourceMessage->isNewRecord ? Yii::t('word', 'Create') : Yii::t('word', 'Save')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->