<?php
/* @var $this OfficeworkareaController */
/* @var $model OfficeWorkarea */

$this->breadcrumbs=array(
	'Office Workareas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List OfficeWorkarea', 'url'=>array('index')),
	array('label'=>'Create OfficeWorkarea', 'url'=>array('create')),
	array('label'=>'View OfficeWorkarea', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage OfficeWorkarea', 'url'=>array('admin')),
);
?>

<h1>Update OfficeWorkarea <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>