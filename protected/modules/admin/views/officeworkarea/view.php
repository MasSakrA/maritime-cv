<?php
/* @var $this OfficeworkareaController */
/* @var $model OfficeWorkarea */

$this->breadcrumbs=array(
	'Office Workareas'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List OfficeWorkarea', 'url'=>array('index')),
	array('label'=>'Create OfficeWorkarea', 'url'=>array('create')),
	array('label'=>'Update OfficeWorkarea', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete OfficeWorkarea', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage OfficeWorkarea', 'url'=>array('admin')),
);
?>

<h1>View OfficeWorkarea #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'office',
		'workArea',
	),
)); ?>
