<?php
/* @var $this OfficeworkareaController */
/* @var $model OfficeWorkarea */

$this->breadcrumbs=array(
	'Office Workareas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List OfficeWorkarea', 'url'=>array('index')),
	array('label'=>'Manage OfficeWorkarea', 'url'=>array('admin')),
);
?>

<h1>Create OfficeWorkarea</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>