<?php
/* @var $this OfficeworkareaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Office Workareas',
);

$this->menu=array(
	array('label'=>'Create OfficeWorkarea', 'url'=>array('create')),
	array('label'=>'Manage OfficeWorkarea', 'url'=>array('admin')),
);
?>

<h1>Office Workareas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
