<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>Yii::t('phrase','List User'), 'url'=>array('index')),
	array('label'=>Yii::t('phrase','Create User'), 'url'=>array('create')),
	array('label'=>Yii::t('phrase','View User'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>Yii::t('phrase','Manage User'), 'url'=>array('admin')),
);
?>

<h1>Update User <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>