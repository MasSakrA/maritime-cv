<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('phrase','List User'), 'url'=>array('index')),
	array('label'=>Yii::t('phrase','Create User'), 'url'=>array('create')),
	array('label'=>Yii::t('phrase','Update User'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('phrase','Delete User'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('phrase','Manage User'), 'url'=>array('admin')),
);
?>

<h1>View User #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'identificator',
		'email',
	),
)); ?>
