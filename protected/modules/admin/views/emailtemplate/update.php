<?php
/* @var $this EmailtemplateController */
/* @var $EmailtemplateMessage EmailtemplateMessage */

$this->breadcrumbs=array(
	'Source Messages'=>array('index'),
	$EmailtemplateMessage->id=>array('view','id'=>$EmailtemplateMessage->id),
	'Update',
);

$menu = array();
if(Yii::app()->user->getName() === 'developer')
{
	$menu[] = array('label'=>Yii::t('phrase', 'Create Emailtemplate'), 'url'=>array('create'));
	$menu[] = array('label'=>Yii::t('phrase', 'Delete Emailtemplate'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$EmailtemplateMessage->id),'confirm'=>'Are you sure you want to delete this item?'));
}
$menu[] = array('label'=>Yii::t('phrase', 'Manage Emailtemplates'), 'url'=>array('admin'));

$this->menu = $menu;
?>

<h1><?php echo Yii::t('phrase', 'Update Emailtemplate') . ' #' . $EmailtemplateMessage->id; ?></h1>

<?php $this->renderPartial('_form', array('EmailtemplateMessage'=>$EmailtemplateMessage, 'typeFilterData' => $typeFilterData)); ?>