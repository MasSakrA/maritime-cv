<?php
/**
 * @var EmailtemplateController $this
 * @var EmailtemplateMessage $EmailtemplateMessage
 */

$this->breadcrumbs=array(
	'Email Templates'=>array('index'),
	'Manage',
);

$menu = array();
if(Yii::app()->user->getName() === 'developer')
{
	$menu[] = array('label'=>Yii::t('phrase', 'Create Email Template'), 'url'=>array('create'));
}
$this->menu = $menu;

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#source-message-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});

/**
 * On seraching english translation disable russian
 * translation filter and conversely.
 */
$('.search-language-en').live('click',function()
{
	$('input', this).prop('disabled', false).focus();
	$('.search-language-ru input').val('').prop('disabled', true);
});
$('.search-language-ru').live('click',function()
{
	$('input', this).prop('disabled', false).focus();
	$('.search-language-en input').val('').prop('disabled', true);
});
");
?>

<h1><?php echo Yii::t('phrase', 'Manage Email Templates'); ?></h1>

<p>
<?php echo Yii::t('phrase', 'search guid text'); ?>
</p>

<?php //echo CHtml::link(Yii::t('phrase', 'Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php /*$this->renderPartial('_search',array(
	'EmailtemplateMessage' => $EmailtemplateMessage,
	'nameFilterData' => $nameFilterData,
	'typeFilterData' => $typeFilterData
)); */?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'source-message-grid',
	'dataProvider'=>$EmailtemplateMessage->search(),
	'filter'=>$EmailtemplateMessage,
	'columns'=>array(
		array(
			'name' => 'message',
			'header' => Yii::t('word', 'Name', SINGULAR),
			'value' => 'Yii::t("emailTemplateName", $data->message, SINGULAR)',
			'filter' => CHtml::dropDownList(
				'EmailtemplateMessage[message]',
				$EmailtemplateMessage->message,
				$nameFilterData,
				array('empty' => Yii::t('word', 'All'))
			),
			'htmlOptions' => array(
				'width' => '250px'
			)
		),
		array(
			'name' => 'category',
			'header' => Yii::t('word', 'Type', SINGULAR),
			'value' => 'Yii::t("emailTemplateType", $data->category, SINGULAR)',
			'filter' => CHtml::dropDownList(
				'EmailtemplateMessage[category]',
				$EmailtemplateMessage->category,
				$typeFilterData,
				array('empty' => Yii::t('word', 'All'))
			)
		),
//		array(
//			'name' => 'search[translation][en]',
//			'header' => Yii::t('language', 'en'),
//			'value'=> '$data->GetTranslation("en")',
//			'filterHtmlOptions' => array(
//				'class' => 'search-language-en'
//			),
//			'htmlOptions' => array(
//				'class' => 'table-content'
//			)
//		),
//		array(
//			'name' => 'search[translation][ru]',
//			'header' => Yii::t('language', 'ru'),
//			'value'=> '$data->GetTranslation("ru")',
//			'filterHtmlOptions' => array(
//				'class' => 'search-language-ru'
//			),
//			'htmlOptions' => array(
//				'class' => 'table-content'
//			)
//		),
		array(
			'class'=>'CButtonColumn',
			// Show update button only
			'template' => '{view} {update}'
		)
	)
)); ?>
