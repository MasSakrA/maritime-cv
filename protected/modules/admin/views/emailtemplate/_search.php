<?php
/* @var $this EmailtemplateController */
/* @var $EmailtemplateMessage EmailtemplateMessage */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($EmailtemplateMessage,'category'); ?>
		<?php echo $form->dropDownList($EmailtemplateMessage,'category', $nameFilterData, array('empty' => Yii::t('word', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($EmailtemplateMessage,'message'); ?>
		<?php echo $form->textArea($EmailtemplateMessage,'message',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->