<?php
/* @var $this EmailtemplateController */
/* @var $EmailtemplateMessage EmailtemplateMessage */

$this->breadcrumbs=array(
	'Source Messages'=>array('index'),
	'Create',
);

$this->menu=array(
	//array('label'=>'List Emailtemplate', 'url'=>array('index')),
	array('label'=>Yii::t('phrase', 'Manage Emailtemplates'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('phrase', 'Create Emailtemplate'); ?></h1>

<?php $this->renderPartial('_form', array('EmailtemplateMessage'=>$EmailtemplateMessage, 'typeFilterData' => $typeFilterData)); ?>