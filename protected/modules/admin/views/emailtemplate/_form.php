<?php
/* @var $this EmailtemplateController */
/* @var $EmailtemplateMessage EmailtemplateMessage */
/* @var $form CActiveForm */
?>
<script type="text/javascript" src="/lib/tinymce/tinymce.min.js"></script>
<?php
if($EmailtemplateMessage->category === 'emailTemplateContent')
{
?>
<script type="text/javascript">
tinymce.init({
		theme: "modern",
		skin: "light",
		selector: ".content",
		inline: true,
		plugins: [
			"advlist autolink lists link image charmap print preview anchor",
			"searchreplace visualblocks code fullscreen",
			"insertdatetime media table contextmenu paste textcolor"
		],
		toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify |source-message-form bullist numlist outdent indent | link image",
		language : '<?php echo Yii::app()->language; ?>'
	});
	// Add tinymce body value to form before form will be submitted
	$('#source-message-form').on('submit', function()
	{
		// Get value of the body
		bodyContents = new Array;
		$('.content').each(function(index, element)
		{
			bodyContents[element.name] = $(this).html();
		});
		// Add hidden input with the body value
		$.each(bodyContents, function(index, bodyContent)
		{
			contentInput = '<input type="hidden" name="'+index+'" value="' + bodyContent + '" />';
		});
		
		$(this).append(bodyInput);
	});
</script>
<?php
}
?>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'source-message-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <?php echo Yii::t('phrase', 'required fields'); ?>

	<?php echo $form->errorSummary($EmailtemplateMessage); ?>

	<div class="row">
		<?php echo Yii::t('word', $form->labelEx($EmailtemplateMessage,'message'), SINGULAR); ?>
		<?php
//			if(Yii::app()->user->getName() === 'developer')
//			{
				echo $form->textField($EmailtemplateMessage,'message');
//			}
//			else
//			{
//				echo Yii::t('emailTemplateName', $EmailtemplateMessage->message);
//			}
		?>
		<?php  ?>
		<?php echo $form->error($EmailtemplateMessage,'message'); ?>
	</div>
	
	<div class="row">
		<?php echo Yii::t('word', $form->labelEx($EmailtemplateMessage,'category'), SINGULAR); ?>
		<?php
//			if(Yii::app()->user->getName() === 'developer')
//			{
				echo $form->dropDownList($EmailtemplateMessage,'category', $typeFilterData, array('empty' => Yii::t('word', 'All')));
//			}
//			else
//			{
//				echo Yii::t('emailTemplateType', $EmailtemplateMessage->category);
//			}
		?>
		<?php echo $form->error($EmailtemplateMessage,'category'); ?>
	</div>

	<?php
	foreach($EmailtemplateMessage->messages as $index => $Message)
	{
		if($EmailtemplateMessage->category === 'emailTemplateContent')
		{
	?>
	<br />
	<div class="row">
		<?php echo CHtml::label(Yii::t('language', $Message->language), "Messages_{$index}_translation"); ?>
		<?php echo '<div id="Messages['.$index.'][translation]" class="content">'.$Message->translation.'</div>'; ?>
		<?php echo $form->error($Message,'translation'); ?>
	</div>
	<br /><br />
	<?php
		}
		else
		{
		echo $form->textArea($Message,'translation',array(
			'rows' => 6,
			'cols' => 50,
			'name' => "Messages[$index][translation]"));
		}
	}
	?>

	<div class="row buttons">
		<?php echo CHtml::submitButton($EmailtemplateMessage->isNewRecord ? Yii::t('word', 'Create') : Yii::t('word', 'Save')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->