<?php
/* @var $this EmailtemplateController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Source Messages',
);

$menu = array();
if(Yii::app()->user->getName() === 'developer')
{
	$menu[] = array('label'=>'Create Emailtemplate', 'url'=>array('create'));
}
$menu[] = array('label'=>'Manage Emailtemplates', 'url'=>array('admin'));
$this->menu = $menu;
?>

<h1>Source Messages</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
