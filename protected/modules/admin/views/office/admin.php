<?php if(!isset(Yii::app()->session['export_access'])) { ?>

    <div class="get-export-acces-block">
        <?php
        echo Yii::t('phrase', 'Please enter expor password') . ': ';
        echo '<input type="text" vlaue="" id="export_pass">';
        echo CHtml::ajaxButton(Yii::t('word', 'Continue'),Yii::app()->createUrl('admin/office/CheckExportPass'),
            array(
                'type'=>'POST',
                'data'=> 'js:{"pass": $("#export_pass").val()}',
                'success'=>'js:function(string){ $(".get-export-acces-block").remove(); $(".alert-block").remove(); }',
                'error'=>'js:function(string) {console.log(string)}'
            ),array('class'=>'check-export-pass-ajax-button'));

        ?>

    </div>

<?php } ?>
<script>
    if( $(".alert-error").length > 0 ) {
        $('.get-export-acces-block').show();
    }
</script>
<?php
/* @var $this OfficeController */
/* @var $model Office */

$this->breadcrumbs=array(
	'Offices'=>array('index'),
	'Manage',
);

$this->menu=array(
	array(
		'label'=>Yii::t('phrase', 'Csv import'),
		'url'=>array(''),
		'template' => '
			<form 
				name="csv-form"
				action="'.Yii::app()->urlManager->createUrl('admin/office/ImportCsv').'"
				method="POST"
				enctype="multipart/form-data">
				<input type="file" name="csvFile" accept=".csv" class="hide" />
				{menu}
			</form>',
		'linkOptions' => array(
			'class' => 'select-csv'
		)
	),
	array('label'=>Yii::t('phrase', 'Csv export'), 'url'=>array('ExportCsv')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#office-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
// Csv file selecting
$('.select-csv').click(function()
{
	$('input[name=\"csvFile\"]').trigger('click');
	return false;
});
$('input[name=\"csvFile\"]').change(function()
{
	$('form[name=\"csv-form\"]').submit();
})
");
?>

<h1><?php echo Yii::t('phrase', 'Manage Companies'); ?></h1>

<p>
<?php echo Yii::t('phrase', 'search guid text'); ?>
</p>

<?php echo CHtml::link(Yii::t('phrase', 'Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
	'regionFilterData' => $regionFilterData,
	'countryFilterData' => $countryFilterData,
	'sectorFilterData' => $sectorFilterData
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'office-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(		
		array(
			'name' => 'name',
			'value' => '$data->parentCompany->name'
		),
		'email',
		array(
			'name' => 'regionSearch',
			'value' => 'str_replace(";", "; ", $data->GetRegionsString())',
			'header' => Yii::t('word', 'Region', SINGULAR),
			'filter' => CHtml::dropDownList(
				'Office[regionSearch]',
				$model->regionSearch,
				$regionFilterData,
				array('empty' => Yii::t('word', 'All'))
			)
		),
		array(
			'name' => 'country',
			'value' => 'Yii::t("country", $data->country)',
			'header' => Yii::t('word', 'Country', SINGULAR),
			'filter' => CHtml::dropDownList(
				'Office[country]',
				$model->country,
				$countryFilterData,
				array('empty' => Yii::t('word', 'All'))
			)
		),
		array(
            'name' => 'workAreaSearch',
            'value' => 'str_replace(";", "; ", $data->GetSectorsString())',
            'header' => Yii::t('word', 'Sector', SINGULAR),
            'filter' => CHtml::dropDownList(
                'Office[workAreaSearch]',
                $model->workAreaSearch,
                $sectorFilterData,
                array('empty' => Yii::t('word', 'All'))
            )
        ),
        array(
            'name' => 'inactive',
            'value' => 'CHtml::CheckBox("office-".$data->id, !$data->inactive, array(
                "ajax" => array(
                    "type" => "POST",
                    "url" => "'. Yii::app()->createUrl("/admin/office/switchStatus") .'",
                    "dataType"=>"text",
                    "data" => array("id" => $data->id),
                    "success" => "js:function(result){
                        /*
                        if(result==\"1\"){
                            $(\"#office-$data->id\").attr(\"checked\",\"checked\");
                            //window.location.reload();
                        }
                        else if(result==\"0\"){
                            $(\"#office-$data->id\").attr(\"checked\",false);
                            //window.location.reload();
                        }
                        */
                        var checkbox = $(\"#office-$data->id\");
                        checkbox.prop(\"checked\", !checkbox.prop(\"checked\"));
                    }",
                    "error"=>"js:function (xhr, ajaxOptions, thrownError){
                        alert(xhr.statusText);
                        alert(thrownError);
                    }",
                ),
                "style"=>"width:50px;",
                "id"=>"office-".$data->id,)
            )',
            'type' => 'raw',
            'htmlOptions'=>array("width"=>"50px"),
            'header' => Yii::t('word', 'status', SINGULAR),
            'filter' => CHtml::dropDownList(
                'Office[inactive]',
                $model->inactive,
                $inactive,
                array('empty' => Yii::t('word', 'All'))
            )
        ),
        array(
            'name' => 'comment',
            'value' => '$data->comment',
            'htmlOptions'=>array("class"=>"magic-textarea")
        ),
		/*array(
			'class'=>'CButtonColumn',
		),*/
	),
));
?>
<script>
    function getCommentId() {
        return $('td.magic-textarea[data-status=changed]').prev('td').find('input').attr('id').replace('office-', '');
    }
    function getCommentText() {
        return $('td.magic-textarea[data-status=changed]').text();
    }
</script>
<div class="hidden-block">
<?php
echo CHtml::ajaxSubmitButton('change-comment',Yii::app()->createUrl('admin/office/AjaxComments'),
    array(
        'type'=>'POST',
        'data'=> 'js:{"id": getCommentId(), "value": getCommentText()}',
        'success'=>'js:function(string){}',
        'error'=>'js:function(string) {console.log(string)}'
    ),
    array('class'=>'change-comment-ajax-button')
);
?>
</div>