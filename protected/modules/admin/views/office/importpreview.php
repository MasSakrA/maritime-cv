<?php
//    echo '<pre>';
//    print_r(Yii::app()->session['import_data_array']);
//    echo '</pre>';
?>

<?php if( !empty($no_translate_array) ) { ?>
<div class="alert alert-info alert-block">
    <a class="close" data-dismiss="alert" href="#">×</a>
    <h4 class="alert-heading"><?php echo Yii::t('word', 'warning') ?>!</h4>
    <?php if(!empty($no_translate_array['region'])) { ?>
        <?php
        echo '<p>' . Yii::t('phrase', 'regions will be added') . ': ';
        echo implode(';', $no_translate_array['region']) . '</p>';
        ?>
    <? } ?>
    <?php if(!empty($no_translate_array['country'])) { ?>
        <?php
        echo '<p>' . Yii::t('phrase', 'country will be added') . ': ';
        echo implode(';',$no_translate_array['country']) . '</p>';
        ?>
    <? } ?>
    <?php if(!empty($no_translate_array['sector'])) { ?>
        <?php
        echo '<p>' . Yii::t('phrase', 'workarea will be added') . ': ';
        print implode(';',$no_translate_array['sector']) . '</p>';
        ?>
    <? } ?>
</div>
<? } ?>

<?php if(!empty($new_emails)) { ?>
<div class="import-block new-emails-import block">
    <div class="navbar navbar-inner block-header">
        <div class="muted pull-left"><span class="badge badge-success new-record pull-right"><?php echo count($new_emails); ?></span><?php echo Yii::t('phrase', 'new email'); ?></div>
    </div>
    <?php
    echo '<img src="' . Yii::app()->request->baseUrl.'/lib/skin/img/export-csv.png' . '" class="export-to-scv-img new-records-ajax-img" /><div class="block-content collapse in"><div class="span12">';
    echo '<table class="new-records table margin-bottom-none"><tbody>';
    echo '<tr class="all-ui"><td colspan="3">' . CHtml::checkBox('new-records', 'true', array('class' => 'new-records-all-check check-uncheck-all')) . '<td></tr>';
    foreach($new_emails as $email) 
    {
            if( empty($email['name']) ) { $new_name_validation = 'error'; $new_title = Yii::t('phrase', 'Missing company name'); } else {$new_name_validation = ''; $new_title = ''; }
                echo '<tr class="' . $new_name_validation . ' active" title="' . $new_title . '" data-id="' . $email['id'] . '"><td class="check-box-col">';
        echo CHtml::checkBox($email['email'], 'true', array('class' => 'get-new-hidden-input'));
        echo '</td><td class="magic-input" data-status="" data-id="' . $email['id'] . '">' . $email['name'] . '</td><td>' . $email['email'] . '</td></tr>';
    }
    echo '</tbody></table>';
    unset($email);
echo '</div></div></div>';
} ?>

<?php if(!empty($csv_existent_domain_emails)) { ?>
<?php
$withNewDomainCount = 0;
foreach($csv_existent_domain_emails as $domain) {
    $withNewDomainCount = $withNewDomainCount + count($domain);
}
?>
<div class="import-block existent-domains-import block">
    <div class="navbar navbar-inner block-header">
        <div class="muted pull-left"><span class="badge badge-success new-with-domain pull-right"><?php echo $withNewDomainCount; ?></span><?php echo Yii::t('phrase', 'existent domain name'); ?></div>
    </div>
    <?php echo '<img src="' . Yii::app()->request->baseUrl.'/lib/skin/img/export-csv.png' . '" class="export-to-scv-img new-domain-records-ajax-img" />'; ?>
    <div class="block-content collapse in">
        <div class="span12">
            <table class="ex-email table"><tbody>
            <?php echo '<tr class="all-ui"><td colspan="3">' . CHtml::checkBox('existent-domains-import', 'true', array('class' => 'existent-domains-import-all-check check-uncheck-all')) . '<td></tr>'; ?>
    <?php
    foreach($csv_existent_domain_emails as $key => $email) 
    {
        $isFirst = true;
        foreach($email as $mail)
        {
            if ($isFirst) {
                $additionalClass = 'firs-padding';
                $isFirst = false;
            } else {
                $additionalClass = '';
            }
            if( empty($mail['company']) ) { $new_existent_domain_name_validation = 'error'; $new_existent_domain_title = Yii::t('phrase', 'Missing company name'); } else {$new_existent_domain_name_validation = ''; $new_existent_domain_title = '';}
            echo '<tr class="prime-email ' . $additionalClass . ' ' . $new_existent_domain_name_validation . ' active" title="' . $new_existent_domain_title . '" data-id="' . $mail['id'] . '"><td class="check-box-col">';
            echo CHtml::checkBox($mail['email'], 'true', array('class' => 'get-new-hidden-input'));
            echo '</td><td class="magic-input" data-status="" data-id="' . $mail['id'] . '">' . $mail['company'] . '</td><td>' . $mail['email'] . '</td></tr>';
        }
        echo '<tr class="dropdown">';
        echo '<td class="click_to_show_dropdown navbar navbar-inner-blue block-header" colspan="3">
                <div class="muted pull-left">
                    <img src="' . Yii::app()->request->baseUrl . '/lib/skin/img/1downarrow.png" class="right-dropdown-arrow" />' .
                    Yii::t('phrase', 'click here to see all emails with domain') . ':
                    <strong>' . $key . '</strong>
                </div>
              </td>';
        echo '</tr>';
        foreach($db_domain_emails[$key] as $mail) {
            echo '<tr class="hidden_list"><td colspan="2">' . $mail['company'] . '</td><td>' . $mail['email'] . '</td></tr>';
        }
    }
    unset($email);
    echo '</tbody></table>';
echo '</div></div></div>';
} ?>

<?php if(!empty($update_emails)) { ?>
<div class="import-block update-emails-import block">
    <div class="navbar navbar-inner block-header">
       <div class="muted pull-left"><span class="badge badge-success will-be-updated pull-right"><?php echo count($update_emails); ?></span><?php echo Yii::t('phrase', 'will be updated'); ?></div>
    </div>
    <?php echo '<img src="' . Yii::app()->request->baseUrl.'/lib/skin/img/export-csv.png' . '" class="export-to-scv-img update-records-ajax-img" />'; ?>
    <div class="block-content collapse in">
        <div class="span12">
            <table class="update_compare table"><tbody>
                <?php echo '<tr class="all-ui"><td colspan="3">' . CHtml::checkBox('update-records', 'true', array('class' => 'update-records-all-check check-uncheck-all')) . '<td></tr>'; ?>
    <?php
    foreach($update_emails as $key => $email)
    {
        if( !empty($email['name_old']) and empty($email['name_new']) ) { $update_name_validation = 'error'; $update_domain_title = Yii::t('phrase', 'Missing company name'); } else {$update_name_validation = ''; $update_domain_title = '';}
        echo '<tr class="border-none update-header-mail ' . $update_name_validation . ' active" title="' . $update_domain_title . '" data-id="' . $email['id'] . '">';
        echo '<td colspan="3">' . CHtml::checkBox( $key , 'true', array('class' => 'get-update-hidden-input'))  . $email['company'] . ' - ' . $key . '</td>';
        echo '</tr>';
        if(!empty($email['name_old']) or !empty($email['name_new']))
        {
            echo '<tr data-email="' . $key . '">';
            echo '<td>'.
                    Yii::t('word', 'Name', SINGULAR).
                    ': </td><td class="old">'.
                    $email['name_old'].
                    '</td><td class="new">'.
                    $email['name_new'].
                    '</td>';
            echo '</tr>';
        }
        if(!empty($email['country_old']) or !empty($email['country_new']))
        {
            echo '<tr  data-email="' . $key . '">';
                echo '<td>'.
                    Yii::t('word', 'Country', SINGULAR).
                    ': </td><td class="old">';
                    if(!empty($email['country_old']))
                    {
                        echo Yii::t('country',$email['country_old']). ' (' . $email['country_old'] . ')';
                    }
                echo '</td><td class="new">';
                    if(!empty($email['country_new']))
                    {
                        echo Yii::t('country',$email['country_new']). ' (' . $email['country_new'] . ')';
                    }
                echo '</td>';
            echo '</tr>';
        }
        if(!empty($email['region_old']) or !empty($email['region_new']))
        {
            echo '<tr  data-email="' . $key . '">';
            echo '<td>'.
                    Yii::t('word', 'Region', SINGULAR).
                    ': </td><td class="old">';
                    if(!empty($email['region_old'])) {
                        foreach (explode(';', $email['region_old']) as $region_old) {
                            echo Yii::t('region', $region_old) . ' (' . $region_old . '); ';
                        }
                    }
                    echo '</td><td class="new">';
                    if(!empty($email['region_new'])) {
                        foreach (explode(';', $email['region_new']) as $region_new) {
                            echo Yii::t('region', $region_new) . ' (' . $region_new . '); ';
                        }
                    }
                    echo '</td>';
            echo '</tr>';
        }
        if(!empty($email['workArea_old']) or !empty($email['workArea_new']))
        {
            echo '<tr  data-email="' . $key . '">';
            echo '<td>'.
                    Yii::t('word', 'WorkArea', SINGULAR).
                    ': </td><td class="old">';
                    if(!empty($email['workArea_old'])) {
                        foreach (explode(';', $email['workArea_old']) as $workarea_old) {
                            echo Yii::t('workArea', $workarea_old) . ' (' . $workarea_old . '); ';
                        }
                    }
                    echo '</td><td class="new">';
                    if(!empty($email['workArea_new'])) {
                        foreach (explode(';', $email['workArea_new']) as $workarea_new) {
                            echo Yii::t('workArea', $workarea_new) . ' (' . $workarea_new . '); ';
                        }
                    }
                    echo '</td>';
            echo '</tr>';
        }
    }
    unset($email);
    echo '</tbody></table>';
    echo '</div></div>';

//    if(!empty($same_rows_count))
//    {
//        echo '<h4 class="right bold">'. Yii::t('phrase', 'ident rows csv') .':' . $same_rows_count . '</h4>';
//    }

echo '</div>';
} ?>

<div class="import-block block">
    <div class="navbar navbar-inner block-header total">
        <div class="muted pull-left"><?php echo Yii::t('phrase', 'Total import'); ?></div>
    </div>
    <div class="block-content collapse in total">
        <div class="span12">
            <table class="update_compare table margin-bottom-none">
                <tbody>
                    <?php
                    if(!empty($new_emails))
                    {
                    ?>
                    <tr>
                        <td><?php echo Yii::t('phrase', 'new email'); ?></td>
                        <td class="new-record"><?php echo count($new_emails); ?></td>
                    </tr>
                    <?php } ?>
                    <?php
                    if(!empty($withNewDomainCount))
                    {
                    ?>
                    <tr>
                        <td><?php echo Yii::t('phrase', 'existent domain name'); ?></td>
                        <td class="new-with-domain"><?php echo $withNewDomainCount; ?></td>
                    </tr>
                    <?php } ?>
                    <?php
                    if(!empty($update_emails))
                    {
                        ?>
                        <tr>
                            <td><?php echo Yii::t('phrase', 'will be updated'); ?></td>
                            <td class="will-be-updated"><?php echo count($update_emails); ?></td>
                        </tr>
                    <?php } ?>
                    <?php
                    if(!empty($same_rows_count))
                    {
                    ?>
                    <tr>
                        <td><?php echo Yii::t('phrase', 'ident rows csv'); ?></td>
                        <td class="same_rows_total"><?php echo $same_rows_count; ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php
//if(empty($new_emails) and empty($csv_existent_domain_emails) and empty($update_emails))
//{
//    echo '<div class="import-block new-emails-import"><h2>' . Yii::t('phrase', 'This csv file same with your companies') . '</h2></div>';
//}
?>
<div class="row-fluid-5">
    <div class="span5 buttons">
	<?php	
	echo CHtml::beginForm('', 'post', array('id' => 'continue-import-form'));
	echo CHtml::hiddenField('continue' , true, array('id' => 'hiddenInputAgree'));
	echo CHtml::submitButton( Yii::t('phrase', 'Continue'), array(
			"class"=>"btn btn-success pull-right",
			"submit"=>"/admin/office/importpreview")); 
	echo CHtml::endForm();
	?>
        <?php	
	echo CHtml::beginForm();
	echo CHtml::hiddenField('cancel' , true, array('id' => 'hiddenInputAgree'));
	echo CHtml::submitButton( Yii::t('word', 'Cancel'), array(
			"class"=>"btn btn-warning pull-right left-button",
			"submit"=>"/admin/office/importpreview")); 
	echo CHtml::endForm();
	?>        
    </div>
</div>


<div id="getCSV">
    <script>
        function getNewRecordsData() {
            ids = [];
            $('.new-records tr[data-id].active').each(function () {
                id = $(this).attr('data-id');
                ids.push(id);
            });
            return ids;
        }
        function getNewDomainRecordsData() {
            ids = [];
            $('.ex-email tr[data-id].active').each(function () {
                id = $(this).attr('data-id');
                ids.push(id);
            });
            return ids;
        }
        function getUpdateRecordsData() {
            ids = [];
            $('.update_compare tr[data-id].active').each(function () {
                id = $(this).attr('data-id');
                ids.push(id);
            });
            return ids;
        }

        function getChangeRecordId() {
            return  $('td.magic-input[data-status = changed]').attr('data-id');
        }
        function getChangeRecordName() {
            return  $('td.magic-input[data-status = changed]').text();
        }
    </script>
    <?php
    echo CHtml::ajaxSubmitButton('new_records',Yii::app()->createUrl('admin/office/GetCSV'),
        array(
            'type'=>'POST',
            'data'=> 'js:{"ids": getNewRecordsData(), "action": "new_records"}',
            'success'=>'js:function(string){ window.open("http://maritime-cv.com/"+string, "", "width=400px, height=200px") }'
        ),array('class'=>'new-records-ajax-button'));

    echo CHtml::ajaxSubmitButton('new_domain_records',Yii::app()->createUrl('admin/office/GetCSV'),
        array(
            'type'=>'POST',
            'data'=> 'js:{"ids": getNewDomainRecordsData(), "action": "new-domain-records"}',
            'success'=>'js:function(string){ window.open("http://maritime-cv.com/"+string, "", "width=400px, height=200px") }'
        ),array('class'=>'new-domain-records-ajax-button'));

    echo CHtml::ajaxSubmitButton('update_records',Yii::app()->createUrl('admin/office/GetCSV'),
        array(
            'type'=>'POST',
            'data'=> 'js:{"ids": getUpdateRecordsData(), "action": "update-records"}',
            'success'=>'js:function(string){ window.open("http://maritime-cv.com/"+string, "", "width=400px, height=200px") }',
            'error'=>'js:function(string) {console.log(string)}'
        ),array('class'=>'update-records-ajax-button'));

    echo CHtml::ajaxSubmitButton('change-records',Yii::app()->createUrl('admin/office/AjaxEditImportData'),
        array(
            'type'=>'POST',
            'data'=> 'js:{"id": getChangeRecordId(), "action": "change-record", "value": getChangeRecordName()}',
            'success'=>'js:function(string){ $("#result").html(string); }',
            'error'=>'js:function(string) {console.log(string)}'
        ),array('class'=>'change-records-ajax-button'));
    ?>

</div>

<div id="result">

</div>