<?php
/* @var $this OfficeController */
/* @var $model Office */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255)); ?>
	</div>
	
	<div class="row">
		<?php echo $form->label($model, 'regionSearch'); ?>
		<?php echo $form->dropDownList($model,'regionSearch', $regionFilterData, array('empty' => Yii::t('word', 'All'))); ?>
	</div>
	
	<div class="row">
		<?php echo $form->label($model, 'country'); ?>
		<?php echo $form->dropDownList($model,'country', $countryFilterData, array('empty' => Yii::t('word', 'All'))); ?>
	</div>
	<div class="row">
		<?php echo $form->label($model, 'workAreaSearch'); ?>
		<?php echo $form->dropDownList($model,'workAreaSearch', $sectorFilterData, array('empty' => Yii::t('word', 'All'))); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton(Yii::t('word','Search')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->