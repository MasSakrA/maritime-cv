<?php
/* @var $this EmailqueueController */
/* @var $data EmailQueue */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('queueId')); ?>:</b>
	<?php echo CHtml::encode($data->queueId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emailTemplate')); ?>:</b>
	<?php echo CHtml::encode($data->emailTemplate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address')); ?>:</b>
	<?php echo CHtml::encode($data->address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datetimeIn')); ?>:</b>
	<?php echo CHtml::encode($data->datetimeIn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datetimeOut')); ?>:</b>
	<?php echo CHtml::encode($data->datetimeOut); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('inQueue')); ?>:</b>
	<?php echo CHtml::encode($data->inQueue); ?>
	<br />


</div>