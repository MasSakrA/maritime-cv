<?php
/* @var $this EmailqueueController */
/* @var $model EmailQueue */

$this->breadcrumbs=array(
	'Email Queues'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List EmailQueue', 'url'=>array('index')),
	array('label'=>'Create EmailQueue', 'url'=>array('create')),
	array('label'=>'Update EmailQueue', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete EmailQueue', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EmailQueue', 'url'=>array('admin')),
);
?>

<h1>View EmailQueue #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'queueId',
		'emailTemplate',
		'address',
		'datetimeIn',
		'datetimeOut',
		'inQueue',
	),
)); ?>
