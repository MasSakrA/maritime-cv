<?php
/* @var $this EmailqueueController */
/* @var $model EmailQueue */

$this->breadcrumbs=array(
	'Email Queues'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EmailQueue', 'url'=>array('index')),
	array('label'=>'Manage EmailQueue', 'url'=>array('admin')),
);
?>

<h1>Create EmailQueue</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>