<?php
/* @var $this EmailqueueController */
/* @var $model EmailQueue */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'email-queue-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'queueId'); ?>
		<?php echo $form->textField($model,'queueId'); ?>
		<?php echo $form->error($model,'queueId'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'emailTemplate'); ?>
		<?php echo $form->textField($model,'emailTemplate'); ?>
		<?php echo $form->error($model,'emailTemplate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'address'); ?>
		<?php echo $form->textField($model,'address',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'address'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'datetimeIn'); ?>
		<?php echo $form->textField($model,'datetimeIn'); ?>
		<?php echo $form->error($model,'datetimeIn'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'datetimeOut'); ?>
		<?php echo $form->textField($model,'datetimeOut'); ?>
		<?php echo $form->error($model,'datetimeOut'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'inQueue'); ?>
		<?php echo $form->textField($model,'inQueue'); ?>
		<?php echo $form->error($model,'inQueue'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('word', 'Create') : Yii::t('word', 'Save')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->