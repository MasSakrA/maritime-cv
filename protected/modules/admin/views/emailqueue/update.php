<?php
/* @var $this EmailqueueController */
/* @var $model EmailQueue */

$this->breadcrumbs=array(
	'Email Queues'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List EmailQueue', 'url'=>array('index')),
	array('label'=>'Create EmailQueue', 'url'=>array('create')),
	array('label'=>'View EmailQueue', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage EmailQueue', 'url'=>array('admin')),
);
?>

<h1>Update EmailQueue <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>