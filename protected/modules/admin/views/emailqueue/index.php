<?php
/* @var $this EmailqueueController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Email Queues',
);

$this->menu=array(
	array('label'=>'Create EmailQueue', 'url'=>array('create')),
	array('label'=>'Manage EmailQueue', 'url'=>array('admin')),
);
?>

<h1>Email Queues</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
