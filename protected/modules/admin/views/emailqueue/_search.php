<?php
/* @var $this EmailqueueController */
/* @var $model EmailQueue */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'queueId'); ?>
		<?php echo $form->textField($model,'queueId'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'emailTemplate'); ?>
		<?php echo $form->textField($model,'emailTemplate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'address'); ?>
		<?php echo $form->textField($model,'address',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'datetimeIn'); ?>
		<?php echo $form->textField($model,'datetimeIn'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'datetimeOut'); ?>
		<?php echo $form->textField($model,'datetimeOut'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'inQueue'); ?>
		<?php echo $form->textField($model,'inQueue'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->