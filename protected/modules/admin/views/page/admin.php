<?php
/**
 * @var PageController $this
 * @var PageMessage $PageMessage
 */

$this->breadcrumbs=array(
	'Email Templates'=>array('index'),
	'Manage',
);

$menu = array();
if(Yii::app()->user->getName() === 'developer')
{
	$menu[] = array('label'=>Yii::t('phrase', 'Create Page'), 'url'=>array('create'));
}
$this->menu = $menu;

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#source-message-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});

/**
 * On seraching english translation disable russian
 * translation filter and conversely.
 */
$('.search-language-en').live('click',function()
{
	$('input', this).prop('disabled', false).focus();
	$('.search-language-ru input').val('').prop('disabled', true);
});
$('.search-language-ru').live('click',function()
{
	$('input', this).prop('disabled', false).focus();
	$('.search-language-en input').val('').prop('disabled', true);
});
");
?>

<h1><?php echo Yii::t('phrase', 'Manage Page Translation'); ?></h1>

<p>
<?php echo Yii::t('phrase', 'search guid text'); ?>
</p>

<?php //echo CHtml::link(Yii::t('phrase', 'Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php /*$this->renderPartial('_search',array(
	'PageMessage' => $PageMessage,
	'nameFilterData' => $nameFilterData
)); */?>
</div><!-- search-form -->
<?php 
//print_r($PageMessage);
//Yii::app()->end();
?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'source-message-grid',
	'dataProvider'=>$PageMessage->search(),
	'filter'=>$PageMessage,
	'columns'=>array(
		array(
			'name' => 'message',
			'header' => Yii::t('word', 'Name', SINGULAR),
			'value' => 'Yii::t("page", $data->message, SINGULAR)',
		),
                array(
			'name' => 'message',
			'header' => Yii::t('word', 'Category', SINGULAR),
			'value' => 'Yii::t("page", $data->category, SINGULAR)',
            'filter' => CHtml::dropDownList(
                'PageMessage[category]',
                $PageMessage->category,
                $nameFilterData,
                array('empty' => Yii::t('word', 'All'))
            ),
            'htmlOptions' => array(
                'width' => '250px'
            )
			
		),
//		array(
//			'name' => 'search[translation][en]',
//			'header' => Yii::t('language', 'en'),
//			'value'=> '$data->GetTranslation("en")',
//			'filterHtmlOptions' => array(
//				'class' => 'search-language-en'
//			),
//			'htmlOptions' => array(
//				'class' => 'table-content'
//			)
//		),
//		array(
//			'name' => 'search[translation][ru]',
//			'header' => Yii::t('language', 'ru'),
//			'value'=> '$data->GetTranslation("ru")',
//			'filterHtmlOptions' => array(
//				'class' => 'search-language-ru'
//			),
//			'htmlOptions' => array(
//				'class' => 'table-content'
//			)
//		),
		array(
			'class'=>'CButtonColumn',
			// Show update button only
			'template' => '{view} {update}'
		)
	)
)); ?>