<?php
/* @var $this PageController */
/* @var $PageMessage PageMessage */

$this->breadcrumbs=array(
	'Source Messages'=>array('index'),
	'Create',
);

$this->menu=array(
	//array('label'=>'List Page', 'url'=>array('index')),
	array('label'=>Yii::t('phrase', 'Manage Pages'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('phrase', 'Create Page'); ?></h1>

<?php $this->renderPartial('_form', array('PageMessage'=>$PageMessage, 'typeFilterData' => $typeFilterData)); ?>