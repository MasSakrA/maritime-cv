<?php
/* @var $this PageController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Source Messages',
);

$menu = array();
if(Yii::app()->user->getName() === 'developer')
{
	$menu[] = array('label'=>Yii::t('phrase', 'Create Page'), 'url'=>array('create'));
}
$menu[] = array('label'=>Yii::t('phrase', 'Manage Pages'), 'url'=>array('admin'));
$this->menu = $menu;
?>

<h1><?php Yii::t('phrase', 'Manage Pages'); ?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
