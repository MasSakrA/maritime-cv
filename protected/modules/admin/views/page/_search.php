<?php
/* @var $this PageController */
/* @var $PageMessage PageMessage */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($PageMessage,'category'); ?>
		<?php echo $form->dropDownList($PageMessage,'category', $nameFilterData, array('empty' => Yii::t('word', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($PageMessage,'message'); ?>
		<?php echo $form->textArea($PageMessage,'message',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->