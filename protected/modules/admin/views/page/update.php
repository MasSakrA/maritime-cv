<?php
/* @var $this PageController */
/* @var $PageMessage PageMessage */

$this->breadcrumbs=array(
	'Source Messages'=>array('index'),
	$PageMessage->id=>array('view','id'=>$PageMessage->id),
	'Update',
);

$menu = array();
if(Yii::app()->user->getName() === 'developer')
{
	$menu[] = array('label'=>Yii::t('phrase', 'Create Page'), 'url'=>array('create'));
	$menu[] = array('label'=>Yii::t('phrase', 'Delete Page'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$PageMessage->id),'confirm'=>'Are you sure you want to delete this item?'));
}
$menu[] = array('label'=>Yii::t('phrase', 'Manage Pages'), 'url'=>array('admin'));

$this->menu = $menu;
?>

<h1><?php echo Yii::t('phrase', 'Update Page') . ' #' . $PageMessage->id; ?></h1>

<?php $this->renderPartial('_form', array('PageMessage'=>$PageMessage, 'typeFilterData' => $typeFilterData)); ?>