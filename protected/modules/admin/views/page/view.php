<?php
/* @var $this PageController */
/* @var $model PageMessage */

$this->breadcrumbs=array(
	'Source Messages'=>array('index'),
	$model->id,
);

$menu = array();
if(Yii::app()->user->getName() === 'developer')
{
	$menu[] = array('label'=>Yii::t('phrase', 'Create Page'), 'url'=>array('create'));
	$menu[] = array('label'=>Yii::t('phrase', 'Delete Page'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'));
}
$menu[] = array('label'=>Yii::t('phrase', 'Update Page'), 'url'=>array('update', 'id'=>$model->id));
$menu[] = array('label'=>Yii::t('phrase', 'Manage Pages'), 'url'=>array('admin'));

$this->menu = $menu;
?>

<h1><?php echo Yii::t('phrase', 'View Page') . ' #' . $model->id; ?></h1>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'source-message-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
	
<div class="row">
		<?php echo Yii::t('word', $form->labelEx($model,'message'), SINGULAR); ?>
		<?php echo Yii::t('emailTemplateName', $model->message); ?>
		<?php  ?>
		<?php echo $form->error($model,'message'); ?>
	</div>
	
	<div class="row">
		<?php echo Yii::t('word', $form->labelEx($model,'category'), SINGULAR); ?>
		<?php echo Yii::t('emailTemplateType', $model->category);	 ?>
		<?php echo $form->error($model,'category'); ?>
	</div>

	<?php
	foreach($model->messages as $index => $Message)
	{
	?>
	<br />
	<div class="row">
		<?php echo CHtml::label(Yii::t('language', $Message->language), "Messages_{$index}_translation"); ?>
		<?php echo '<div id="Messages['.$index.'][translation]" class="content">'.$Message->translation.'</div>'; ?>
		<?php echo $form->error($Message,'translation'); ?>
	</div>
	<br /><br />
	<?php
	}
	?>
<?php $this->endWidget(); ?>

</div><!-- form -->

