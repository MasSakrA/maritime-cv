<?php
/**
 * Parameters controller
 * 
 * @property string[] $existingCategories An array of existing parameters categories. Used for caching.
 */
class ParameterController extends Controller
{
	public $existingCategories = array();
	
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index', 'view', 'update', 'admin'/* 'create', 'delete',*/ ),
				'users'=>array(Yii::app()->Settings->get('login', 'admin'), 'developer'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			)
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Parameter('create');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Parameter']))
		{
			$model->attributes=$_POST['Parameter'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Parameter']))
		{
			$model->attributes=$_POST['Parameter'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Parameter');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Parameter('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Parameter']))
			$model->attributes=$_GET['Parameter'];
		/**
		 * Formate the categories for dropdown filter.
		 * @var string[] $categoryFilterData Associative array of the categories (category => translation).
		 * @var string $category Current category in cycle.
		 */

		$this->LoadCategories();
		$categoryFilterData = array();
		foreach($this->existingCategories as $category)
		{
            if($category != 'admin') {
                $categoryFilterData[$category] = Yii::t('parameterCategory', $category);
            }
		}
		$this->render('admin',array(
			'model'=>$model,
			'categoryFilterData' => $categoryFilterData
		));
	}
	
	/**
	 * Loads categories from database and caches they.
	 * @return string[] Existing categories.
	 */
	public function LoadCategories()
	{
		// Check cache.
		if(empty($this->existingCategories))
		{
			// Load new data if cache is empty.
			$this->existingCategories = Yii::app()->db->createCommand()
				->selectDistinct('category')
				->from('parameters')
				->queryColumn();
		}
		// Return data.
		return $this->existingCategories;
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Parameter the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Parameter::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Parameter $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='parameter-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
