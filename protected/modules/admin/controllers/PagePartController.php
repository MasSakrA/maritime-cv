<?php
/**
 * Translations controller.
 */
class TranslationController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions' => array('index', 'admin', 'view', 'update'),
				'users' => array(Yii::app()->Settings->get('login', 'admin'), 'developer'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions' => array('create', 'delete'),
				'users' => array('developer'),
			),
			array('deny',  // deny all users
				'users' => array('*'),
			)
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		/**
		 * Create new source message model
		 * and control for messages order, existing, etc.
		 */
		$SourceMessage = new SourceMessage;
		$SourceMessage->ControlMessages();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($SourceMessage);
		// $this->performAjaxValidation($Message);

		/**
		 * Validate and save source message with related messages.
		 */
		if(isset($_POST['SourceMessage']))
		{
			// Begin transaction.
			$Transaction = Yii::app()->db->beginTransaction();			
			try{
				/**
				 * Set attributes and save source message.
				 */
				$SourceMessage->attributes = $_POST['SourceMessage'];
				if($SourceMessage->save())
				{
					/**
					 * Save related messages on success.
					 */
					$messagesSaved = true;
					foreach($SourceMessage->messages as $index => $Message)
					{
						$Message->attributes = $_POST['Messages'][$index];
						$Message->id = $SourceMessage->id;
						if( ! $Message->save())
						{
							// Message saving error
							$messagesSaved = false;							
						}
					}
					// Saving the messages is successfull.
					if($messagesSaved)
					{
						// Commit transaction.					
						$Transaction->commit();
						// Redirect to viewing the changes.
						$this->redirect(array('view','id'=>$SourceMessage->id));
					}
					else
					{
						// Rollback the changes on errors.
						$Transaction->rollback();
					}
				}
			}
			catch( Exception $e)
			{				
				// Rollback the changes on excption.
				$Transaction->rollback();
				throw new CDbException('Translation saving error: '.$e->getMessage());
			}		
		}
		// Render the view.
		$this->render('create',array(
			'SourceMessage'=>$SourceMessage,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		/**
		 * Create new source message model
		 * and control for messages order, existing, etc.
		 */
		$SourceMessage = $this->loadModel($id);
		$SourceMessage->ControlMessages();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($SourceMessage);
		// $this->performAjaxValidation($Message);

		/**
		 * Validate and save source message with related messages.
		 */
		if(isset($_POST['SourceMessage']) || isset($_POST['Messages']))
		{
			echo 1;
			// Begin transaction.
			$Transaction = Yii::app()->db->beginTransaction();			
			try{
				/**
				 * Set attributes and save source message.
				 */
				$savedSourceMessage = true;
				if(isset($_POST['SourceMessage']))
				{
					$SourceMessage->attributes = $_POST['SourceMessage'];
					$savedSourceMessage = $SourceMessage->save();
				}
				if($savedSourceMessage)
				{
					/**
					 * Save related messages on success.
					 */
					$messagesSaved = true;
					foreach($SourceMessage->messages as $index => $Message)
					{
						$Message->attributes = $_POST['Messages'][$index];
						$Message->id = $SourceMessage->id;
						if( ! $Message->save())
						{
							// Message saving error
							$messagesSaved = false;							
						}
					}
					// Saving the messages is successfull.
					if($messagesSaved)
					{
						// Commit transaction.					
						$Transaction->commit();
						// Redirect to viewing the changes.
						$this->redirect(array('view','id'=>$SourceMessage->id));
					}
					else
					{
						// Rollback the changes on errors.
						$Transaction->rollback();
					}
				}
			}
			catch( Exception $e)
			{				
				// Rollback the changes on excption.
				$Transaction->rollback();
				throw new CDbException('Translation saving error: '.$e->getMessage());
			}		
		}
		// Render the view.
		$this->render('update',array(
			'SourceMessage'=>$SourceMessage,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('SourceMessage');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
	
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		/**
		 * Create source message and message models and assign  the attributes if exists.
		 * 
		 * @var SourceMessage $SourceMessage
		 */
		// Source message
		$SourceMessage = new SourceMessage('translation');
		$SourceMessage->unsetAttributes();  // clear any default values
		if(isset($_GET['SourceMessage']))
		{
			$SourceMessage->attributes = $_GET['SourceMessage'];
		}
		// Message
		$Message = new Message('translation');
		$Message->unsetAttributes();  // clear any default values
		if(isset($_GET['Message']))
		{
			$Message->attributes = $_GET['Message'];
		}

		/**
		 * Formate the categories for dropdown filter.
		 * @var string[] $categoryFilterData Associative array of the categories (category => translation).
		 * @var string $category Current category in cycle.
		 */
		$categoryFilterData = array(
			'word' => Yii::t('word', 'Word', SINGULAR),
			'phrase' => Yii::t('word', 'Phrase', SINGULAR),
			'region' => Yii::t('word', 'Region', SINGULAR),
			'country' => Yii::t('word', 'Country', SINGULAR),
			'sector' => Yii::t('word', 'Sector', SINGULAR),			
			'language' => Yii::t('word', 'Language', SINGULAR),	
			'parameter' => Yii::t('word', 'Parameter', SINGULAR),	
			'Parametercategory' => Yii::t('word', 'Parametercategory', SINGULAR)
		);
		$this->render('admin',array(
			'SourceMessage' => $SourceMessage,
			'Message' => $Message,
			'categoryFilterData' => $categoryFilterData
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return SourceMessage the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=SourceMessage::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param SourceMessage $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='source-message-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
