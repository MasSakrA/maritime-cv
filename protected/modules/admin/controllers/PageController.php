<?php
/**
 * Email template controller.
 */
class PageController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions' => array('index', 'admin', 'view', 'update'),
				'users' => array(Yii::app()->Settings->get('login', 'admin'), 'developer'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions' => array('create', 'delete'),
				'users' => array('developer'),
			),
			array('deny',  // deny all users
				'users' => array('*'),
			)
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		/**
		 * Create new email template message model
		 * and control for messages order, existing, etc.
		 */
		$PageMessage = new PageMessage;
		$PageMessage->ControlMessages();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($PageMessage);
		// $this->performAjaxValidation($Message);

		/**
		 * Validate and save email template message with related messages.
		 */
		if(isset($_POST['PageMessage']))
		{
			// Begin transaction.
			$Transaction = Yii::app()->db->beginTransaction();			
			try{
				/**
				 * Set attributes and save email template message.
				 */
				$PageMessage->attributes = $_POST['PageMessage'];
				if($PageMessage->save())
				{
					/**
					 * Save related messages on success.
					 */
					$messagesSaved = true;
					foreach($PageMessage->messages as $index => $Message)
					{
						$Message->attributes = $_POST['Messages'][$index];
						$Message->id = $PageMessage->id;
						if( ! $Message->save())
						{
							// Message saving error
							$messagesSaved = false;							
						}
					}
					// Saving the messages is successfull.
					if($messagesSaved)
					{
						// Commit transaction.					
						$Transaction->commit();
						// Redirect to viewing the changes.
						$this->redirect(array('view','id'=>$PageMessage->id));
					}
					else
					{
						// Rollback the changes on errors.
						$Transaction->rollback();
					}
                                }
			}
			catch( Exception $e)
			{				
				// Rollback the changes on excption.
				$Transaction->rollback();
				throw new CDbException('Translation saving error: '.$e->getMessage());
			}		
		}
		
		/**
		 * Formate the categories for dropdown filter.
		 * @var string[] $typeFilterData Associative array of the email template types (type => translation).
		 * @var string $category Current category in cycle.
		 */
		$typeFilterData = array(
			'pagePart' => Yii::t('page', 'pagePart', SINGULAR),
                        'filesPage' => Yii::t('page', 'filesPage', SINGULAR),
                        'homePage' => Yii::t('page', 'homePage', SINGULAR),
                        'licensePage' => Yii::t('page', 'licensePage', SINGULAR),
		);
		// Render the view.
		$this->render('create',array(
			'PageMessage'=>$PageMessage,
			'typeFilterData' => $typeFilterData
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		/**
		 * Create new email template message model
		 * and control for messages order, existing, etc.
		 */
		$PageMessage = $this->loadModel($id);
		$PageMessage->ControlMessages();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($PageMessage);
		// $this->performAjaxValidation($Message);

		/**
		 * Validate and save email template message with related messages.
		 */
		if(isset($_POST['PageMessage']) || isset($_POST['Messages']))
		{
			// Begin transaction.
			$Transaction = Yii::app()->db->beginTransaction();			
			try{
				/**
				 * Set attributes and save email template message.
				 */
				$savedPageMessage = true;
				if(isset($_POST['PageMessage']))
				{
					$PageMessage->attributes = $_POST['PageMessage'];
					$savedPageMessage = $PageMessage->save();
				}
				if($savedPageMessage)
				{
					/**
					 * Save related messages on success.
					 */
					$messagesSaved = true;
					foreach($PageMessage->messages as $index => $Message)
					{
						$Message->attributes = $_POST['Messages'][$index];
						$Message->id = $PageMessage->id;
						if( ! $Message->save())
						{
							// Message saving error
							$messagesSaved = false;							
						}
					}
					// Saving the messages is successfull.
					if($messagesSaved)
					{
						// Commit transaction.					
						$Transaction->commit();
						// Redirect to viewing the changes.
						$this->redirect(array('view','id'=>$PageMessage->id));
					}
					else
					{
						// Rollback the changes on errors.
						$Transaction->rollback();
					}
				}
			}
			catch( Exception $e)
			{				
				// Rollback the changes on excption.
				$Transaction->rollback();
				throw new CDbException('Translation saving error: '.$e->getMessage());
			}		
		}
		
		/**
		 * Formate the categories for dropdown filter.
		 * @var string[] $typeFilterData Associative array of the email template types (type => translation).
		 * @var string $category Current category in cycle.
		 */
		$typeFilterData = array(
			'pagePart' => Yii::t('page', 'pagePart', SINGULAR),
                        'filesPage' => Yii::t('page', 'filesPage', SINGULAR),
                        'homePage' => Yii::t('page', 'homePage', SINGULAR),
                        'licensePage' => Yii::t('page', 'licensePage', SINGULAR),
		);
		// Render the view.
		$this->render('update',array(
			'PageMessage'=>$PageMessage,
			'typeFilterData' => $typeFilterData
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('PageMessage');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
	
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		/**
		 * Create email template message and message models and assign  the attributes if exists.
		 * 
		 * @var PageMessage $PageMessage
		 */
		// Source message
		$PageMessage = new PageMessage('page');
		$PageMessage->unsetAttributes();  // clear any default values
		if(isset($_GET['PageMessage']))
		{
			$PageMessage->attributes = $_GET['PageMessage'];
		}

		/**
		 * Formate the names and types for dropdown filters.
		 * @var string[] $templateNames The names of email templates.
		 * @var string[] $nameFilterData Associative array of the template names (name => translation).
		 * @var string[] $typeFilterData Associative array of the email template types (type => translation).
		 * @var string $name Current template name in cycle.
		 */
		// Category
		$nameFilterData = array(
			'pagePart' => Yii::t('page', 'pagePart', SINGULAR),
                        'filesPage' => Yii::t('page', 'filesPage', SINGULAR),
                        'homePage' => Yii::t('page', 'homePage', SINGULAR),
                        'licensePage' => Yii::t('page', 'licensePage', SINGULAR),
		);
                
		$this->render('admin',array(
			'PageMessage' => $PageMessage,
			'nameFilterData' => $nameFilterData
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return PageMessage the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=PageMessage::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param PageMessage $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='email template-message-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
