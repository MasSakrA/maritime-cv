<?php

class DefaultController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('login, logout'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index'),
				'users'=>array(Yii::app()->Settings->get('login', 'admin'), 'developer'),
			),
			array('deny',  // deny all users
				'actions' => array('index'),
				'users'=>array('*'),
			),
		);
	}
	
	public function actionIndex()
	{
		$this->redirect('/admin/office/admin');
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login()) {
                unset(Yii::app()->session['login_validate']);
                $this->redirect('/admin');
            } else {
                if(isset(Yii::app()->session['login_validate'])) {
                    $login_validate = Yii::app()->session['login_validate'];
                } else {
                    $login_validate = 0;
                }
                Yii::app()->session['login_validate'] = $login_validate + 1;
            }
		}
        if(isset(Yii::app()->session['login_validate'])) {
            if(Yii::app()->session['login_validate'] >= 3) {
                $banned = true;

                if(!isset(Yii::app()->session['login_validate_sent'])) {
                    $params = array(
                        'fromEmail' => Yii::app()->Settings->Get('email', 'admin'),
                        'fromName' => 'Maritime CV',
                        'to' => Yii::app()->Settings->Get('email', 'admin'),
                        'subject' => Yii::t('phrase', 'Maritime CV - access control report'),
                        'body' => Yii::t('phrase', 'Someone tried to access admin area and failed')
                    );

                    if(Yii::app()->Email->Send($params)) {
                        Yii::app()->session['login_validate_sent'] = true;
                    }
                }


            } else {
                $banned = false;
            }
        } else {
            $banned = false;
        }
		// display the login form
		$this->render('login',array('model'=>$model, 'banned' => $banned));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

    /**
     * Triggers emailing process.
     * If emailing is enabled, it will be disabled.
     * Or if emailing is disabled, it will be enabled.
     */
    public function actionTriggerEmailing()
    {
        $status = (bool)Yii::app()->Settings->Get('status', 'queue');
        $status = !$status;
        Yii::app()->Settings->Set('status', 'queue', (int)$status);

        $this->redirect('/admin');
    }
}