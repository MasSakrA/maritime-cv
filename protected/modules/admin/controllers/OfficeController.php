<?php
/**
 * Office controller.
 * @property string $layout The layout for the views.
 * @property string[] $this->existingSectors An array of existing sectors codes. Used for caching.
 * @property string[] $this->existingRegions An array of existing regions codes. Used for caching.
 * @property string[] $this->existingCountries An array of existing countries codes. Used for caching.
 */
class OfficeController extends Controller
{
	public $layout='/layouts/admin';
	public $existingSectors = array();
	public $existingRegions = array();
	public $existingCountries = array();

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index', 'view', 'ExportCsv', 'ImportCsv', 'SwitchStatus', /*'create', 'update', 'delete',*/ 'admin', 'importpreview', 'GetCSV', 'AjaxEditImportData', 'AjaxComments', 'CheckExportPass'),
				'users'=>array(Yii::app()->Settings->get('login', 'admin'), 'developer'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			)
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Office;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Office']))
		{
			$model->attributes=$_POST['Office'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Office']))
		{
			$model->attributes=$_POST['Office'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Office');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Office('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Office']))
			$model->attributes=$_GET['Office'];

		/**
		 * Formate the regions for dropdown filter.
		 * @var string[] $regionFilterData Associative array of the regions (code => name).
		 * @var string $code Current region code in cycle.
		 */
		$this->LoadRegions();
		$regionFilterData = array();
		foreach($this->existingRegions as $code)
		{
			$regionFilterData[$code] = Yii::t('region', $code);
		}

		/**
		 * Formate the countries for dropdown filter.
		 * @var string[] $countryFilterData Associative array of the countries (code => name).
		 * @var string $code Current country code in cycle.
		 */
		$this->LoadCountries();
		$countryFilterData = array();
		foreach($this->existingCountries as $code)
		{
			$countryFilterData[$code] = Yii::t('country', $code);
		}

		/**
		 * Formate the sectors for dropdown filter.
		 * @var string[] $sectorFilterData Associative array of the sectors (code => name).
		 * @var string $code Current sector code in cycle.
		 */
		$this->LoadSectors();
		$sectorFilterData = array();
		foreach($this->existingSectors as $code)
		{
			$sectorFilterData[$code] = Yii::t('workarea', $code);
		}
        $inactive = array('0' => Yii::t("OfficesFlagInactive", '0'), '1' => Yii::t("OfficesFlagInactive", '1'));

		$this->render('admin',array(
			'model'=>$model,
			'regionFilterData' => $regionFilterData,
			'countryFilterData' => $countryFilterData,
			'sectorFilterData' => $sectorFilterData,
            'inactive' => $inactive
		));
	}

	public function actionExportCsv()
	{
		/**
		 * Create the csv file.
		 *
		 * @var array[] $content Array of file rows which will be put to file.
		 * @var string $fileName Name of the file.
		 * @var string $filePath Path to the file.
		 * @var resource $file File php resource.
		 */

        if(!isset(Yii::app()->session['export_access'])) {
            Yii::app()->user->setFlash('admin', '<div class="alert margin-top-20 alert-error alert-block">
            <a class="close" data-dismiss="alert" href="#">×</a>
            <h4 class="alert-heading">' . Yii::t('word', 'warning') . '!</h4>
            <p>
                '. Yii::t('phrase', 'No rights to access export') .'
            </p>
            </div>');
            $this->redirect('/admin/office/admin');
        }

		$content = Yii::app()->db->createCommand()
			->select('
				companies.name as name,
				email,
				GROUP_CONCAT(DISTINCT region SEPARATOR ";") as region,
                                country,
				GROUP_CONCAT(DISTINCT officeworkareas.workArea SEPARATOR ";") as workareas')
			->from('offices')
			->leftJoin('companies', 'companies.id = offices.company')
			->leftJoin('officeworkareas', 'officeworkareas.office = offices.id')
			->leftJoin('officeregions', 'officeregions.office = offices.id')
			->group('offices.id')
			->order('name')
			->queryAll();

		$fileName = 'companies.csv';
		$filePath = Yii::getPathOfAlias('uploads').DIRECTORY_SEPARATOR.$fileName;
		$file = Yii::app()->File->Open($filePath, 'w');

        $header_line = true;
		foreach($content as $row)
		{
                        if($header_line) {
                            fputcsv($file, array_keys($row));
                            $header_line = false;
                        }
			fputcsv($file, $row);
		}
        // Russian utf-8
        //fwrite($file, "\xEF\xBB\xBF" );

		Yii::app()->File->Close($file);

		/**
		 * Send the file to the user and delete it after sending.
		 *
		 * @var string $mimeType Type of the file.
		 * @var bool $terminate Flag of application ending after yii sends file to the user.
		 * @var string $stringContent Content of file which was created from $content.
		 */
		$mimeType = 'text/csv';
		$terminate = false;
		$stringContent = Yii::app()->File->GetContents($filePath);
		Yii::app()->request->sendFile($fileName, $stringContent, $mimeType, $terminate);
		Yii::app()->File->Delete($filePath);
		Yii::app()->end(200);
	}

	/**
	 * Imports companies list from csv file.
	 */
	public function actionImportCsv()
	{
		/**
		 * Load csv file.
		 *
		 * @var CUploadedFile $UploadedFile An instance of file uloaded by user.
		 * @var resource $csvFile Opened user csv file.
		 */
		$UploadedFile = CUploadedFile::getInstanceByName('csvFile');
		if($UploadedFile->hasError)
		{
			throw new CHttpException(500,'File uploading error.');
		}
        if( 'text/csv' == $UploadedFile->type ||  'application/vnd.ms-excel' == $UploadedFile->type )
        {

        }
        else
        {
            Yii::app()->user->setFlash('admin', '<div class="alert margin-top-20 alert-error alert-block">
            <a class="close" data-dismiss="alert" href="#">×</a>
            <h4 class="alert-heading">' . Yii::t('word', 'warning') . '!</h4>
            <p>
                '. Yii::t('phrase', 'wrong file type') .'
            </p>
            </div>');
            $this->redirect('/admin/office/admin');
        }

		$csvFile = fopen($UploadedFile->getTempName(), 'r');

        $try_to_get_attr = fgetcsv($csvFile, 0, ',', '"');
        if(count($try_to_get_attr) > 1)
        {
            $delimiter = ',';
            $enclosure = '"';
        }

        $csvFile = fopen($UploadedFile->getTempName(), 'r');

        $try_to_get_attr = fgetcsv($csvFile, 0, ';', '"');
        if(count($try_to_get_attr) > 1)
        {
            $delimiter = ';';
            $enclosure = '"';
        }

        if(empty($delimiter) and empty($enclosure))
        {
            Yii::app()->user->setFlash('admin', '<div class="alert margin-top-20 alert-error alert-block">
            <a class="close" data-dismiss="alert" href="#">×</a>
            <h4 class="alert-heading">' . Yii::t('word', 'warning') . '!</h4>
            <p>
                '. Yii::t('phrase', 'cannot get delimiter or enclosure') .'
            </p>
            </div>');
            $this->redirect('/admin/office/admin');
        }




		/**
		 * Save each row data.
		 *
		 * @var array[] $companiesRows Array of the comapnies rows to inserting into database.
		 * @var array[] $officesRows Array of the offices rows to inserting into database.
		 * @var array[] $regionsRows Array of offices regions rows to inserting into database.
		 * @var array[] $sectorsRows Array of offices sectors rows to inserting into database.
		 * @var int $currentCompanyId An id of current comapny which will be inserted.
		 */

        $CommandBuilder = Yii::app()->db;
            $select_emails = $CommandBuilder->createCommand()
                        ->select('*')
                        ->from('offices')
                        ->order('id desc')
                        ->queryAll();

        $db_domains = array();
        foreach($select_emails as $email)
        {
            $db_domains[] = $email['email']; // $db_emails array
        }

		$companiesRows = array();
        $companiesRowsEmails = array();
		$officesRows = array();
		$regionsRows = array();
		$sectorsRows = array();

        $new_companiesRows = array();
		$new_officesRows = array();
		$new_regionsRows = array();
		$new_sectorsRows = array();
        $new_preview_companiesRows = array();

        $update_companiesRows = array();
		$update_officesRows = array();
		$update_regionsRows = array();
		$update_sectorsRows = array();
        $regions = array();
        $sectors= array();
        $new_emails = array();
        $update_emails = array();

		if(!empty($select_emails))
        {
            $currentCompanyId = $select_emails[0]['id'] + 1;
        }
        else
        {
            $currentCompanyId = 1;
        }
		// Preload countries and regions for future checks
		$this->LoadCountries();
		$this->LoadRegions();
		// Get each of rows

        $csvFile = fopen($UploadedFile->getTempName(), 'r');

		while($rowData = fgetcsv($csvFile, 0, $delimiter, $enclosure))
		{
            if(!empty($rowData[1]) and $rowData[1] != 'email') {
                /**
                 * Separate codes string to codes array and check
                 * is regions and countries codes are valid using
                 * preloaded existing regions and countries codes.
                 *
                 * @var string[] $codes Region and countries codes array.
                 */

                if (in_array($rowData[1], $db_domains) or in_array($rowData[1], $new_emails)) {
                    $is_new_email = false;
                } else {
                    $is_new_email = true;
                }
                if (in_array($rowData[1], $db_domains) and !in_array($rowData[1], $update_emails)) {
                    $is_update_email = true;
                } else {
                    $is_update_email = false;
                }
                if (!in_array($rowData[1], $companiesRowsEmails)) {
                    $companies_row_unique = true;
                } else {
                    $companies_row_unique = false;
                }


                $update_id = $this->getIdbyEmail($rowData[1]);
                $update_company_id = $this->getCompanyIdbyEmail($rowData[1]);
                //$codes = fgetcsv($rowData[0]);
                //			$codes = fgetcsv($rowData[$csvColumnsIndexes['country']]);

                /**
                 * Create companies data rows.
                 */
                if ($companies_row_unique) {
                    $companiesRowsEmails[] = $rowData[1];

                    $companiesRows[] = array(
                        'id' => $currentCompanyId,
                        'name' => $rowData[0]
                    );
                }

                if ($is_new_email) {
                    $new_emails[] = $rowData[1];

                    $new_companiesRows[] = array(
                        'id' => $currentCompanyId,
                        'name' => $rowData[0]
                    );
                    $new_preview_companiesRows[] = array(
                        'id' => $currentCompanyId,
                        'name' => $rowData[0],
                        'email' => $rowData[1]
                    );
                }
                if ($is_update_email) {
                    $update_emails[] = $rowData[1];

                    $update_companiesRows[] = array(
                        'id' => $update_id,
                        'name' => $rowData[0]
                    );
                }

                if ($companies_row_unique) {
                    $officesRows[] = array(
                        // Office id as a company id in current version.
                        'id' => $currentCompanyId,
                        'company' => $currentCompanyId,
                        'email' => $rowData[1],
                        'country' => $rowData[3]
                    );
                }
                if ($is_new_email) {
                    $new_officesRows[] = array(
                        // Office id as a company id in current version.
                        'id' => $currentCompanyId,
                        'company' => $currentCompanyId,
                        'email' => $rowData[1],
                        'country' => $rowData[3]
                    );
                }
                if ($is_update_email) {
                    $update_officesRows[] = array(
                        // Office id as a company id in current version.
                        'id' => $update_id,
                        'company' => $update_company_id,
                        'email' => $rowData[1],
                        'country' => $rowData[3]
                    );
                }

                /**
                 * Create company region linking.
                 */
                if ($companies_row_unique) {
                    foreach ($this->FindRegions($rowData[2]) as $region) {
                        $regionsRows[] = array(
                            // Office id as a company id in current version.
                            'region' => $region,
                            'office' => $currentCompanyId
                        );
                    }
                }

                if ($is_update_email) {
                    $regions[$rowData[1]]['office'] = $update_company_id;
                    $regions[$rowData[1]]['region'] = $rowData[2];
                }
                foreach ($this->FindRegions($rowData[2]) as $region) {
                    if ($is_new_email) {
                        $new_regionsRows[] = array(
                            // Office id as a company id in current version.
                            'office' => $currentCompanyId,
                            'region' => $region
                        );
                    }
                    if ($is_update_email) {
                        $update_regionsRows[] = array(
                            // Office id as a company id in current version.
                            'office' => $update_company_id,
                            'region' => $region
                        );
                    }
                }


                /**
                 * Create company sectors linking.
                 */
                if ($companies_row_unique)
                {
                    foreach ($this->FindSectors($rowData[4]) as $sector) {
                        $sectorsRows[] = array(
                            // Office id as a company id in current version.
                            'office' => $currentCompanyId,
                            'workArea' => $sector
                        );
                    }
                }

                if($is_update_email)
                {
                    $sectors[$rowData[1]]['office'] = $update_company_id;
                    $sectors[$rowData[1]]['sector'] = $rowData[4];
                }
                foreach($this->FindSectors($rowData[4]) as $sector)
                {
                    if($is_new_email)
                    {
                        $new_sectorsRows[] = array(
                            // Office id as a company id in current version.
                            'office' => $currentCompanyId,
                            'workArea' => $sector
                        );
                    }
                    if($is_update_email)
                    {
                        $update_sectorsRows[] = array(
                            // Office id as a company id in current version.
                            'office' => $update_company_id,
                            'workArea' => $sector
                        );
                    }
                }

                // Increase the company id.
                $currentCompanyId++;
            }
		}

        if(!empty($csvFile))
        {
            $import_data_array = array(
                'companies' => $companiesRows,
                'offices' => $officesRows,
                'officeregions' => $regionsRows,
                'officeworkareas' => $sectorsRows
            );

            Yii::app()->session['new_companies'] = $new_companiesRows;
            Yii::app()->session['new_offices'] = $new_officesRows;
            Yii::app()->session['new_officeregions'] = $new_regionsRows;
            Yii::app()->session['new_officeworkareas'] = $new_sectorsRows;
            Yii::app()->session['new_previewcompanies'] = $new_preview_companiesRows;

            Yii::app()->session['update_companies'] = $update_companiesRows;
            Yii::app()->session['update_offices'] = $update_officesRows;
            Yii::app()->session['update_officeregions'] = $update_regionsRows;
            Yii::app()->session['update_officeworkareas'] = $update_sectorsRows;

            Yii::app()->session['import_data_array'] = $import_data_array;

            Yii::app()->session['regions'] = $regions;
            Yii::app()->session['sectors'] = $sectors;
            $this->redirect('/admin/office/importpreview');
        }
        else
        {
            // Refresh page
            $this->redirect('/admin/office/admin');
        }

	}


        public function actionImportpreview()
        {
            if(!Yii::app()->request->isPostRequest)
            {
                if(isset(Yii::app()->session['import_data_array']))
                {
                    $import_data_array = Yii::app()->session['import_data_array'];
                    // empty values
                    $new_emails = array();
                    $update_emails = array();
                    $csv_existent_domain_emails = array();
                    $db_domain_emails = array();
                    $update_change_array = array();
                    $new_records = array();
                    $no_translate_array = array();
                    $db_domains = array();


                    // select all emails from db
                    $CommandBuilder = Yii::app()->db;
                    $select_emails = $CommandBuilder->createCommand()
                                ->select('email')
                                ->from('offices')
                                ->queryAll();

                    foreach($select_emails as $email)
                    {
                        $db_emails[] = $email['email']; // $db_emails array
                        $emails_domain = array_pop(explode('@', $email['email'])); // get domain from email
                        $db_domains[] = $emails_domain; // $db_domains array
                        $db_domain_emails[$emails_domain][] = array('email' => $email['email'], 'company' => $this->GetCompanyByEmail($email['email'])); // $db_domain_emails array
                    }
                    unset($emails_domain);
                    unset($email);
                    $db_domains = array_unique($db_domains); // $db_domains unique

                    foreach(Yii::app()->session['new_previewcompanies'] as $email)
                    {
                        $csv_emails[] = $email['email']; // $csv_emails array from csv file
                        $emails_domain = array_pop(explode('@', $email['email'])); // domains from csv
                        $csv_domains[] = $emails_domain; // set domains from csv to array $csv_domains
                        // check if emails domain form csv in db domain array and if it email name is unique set email from csv to existent domain emails
                        if(in_array($emails_domain, $db_domains, true) and !in_array($email['email'], $db_emails, true))
                        {
                            $csv_existent_domain_emails[$emails_domain][] = array('email' => $email['email'], 'company' => $email['name'], 'id' => $email['id']);
                        }
                        else
                        {
                            $new_records[] = array('name' => $email['name'], 'email' => $email['email'], 'id' => $email['id']);
                        }
                    }
                    unset($emails_domain);
                    unset($email);

                    foreach(Yii::app()->session['update_companies'] as $company)
                    {
                        if(!$this->isSameUpdate('id', $company['id'], 'name', 'companies', $company['name'], 'name'))
                        {
                            $update_change_array[$this->getEmailById($company['id'])]['name_new'] = $company['name'];
                            $update_change_array[$this->getEmailById($company['id'])]['name_old'] = $this->getOldValueUpdate('id', $company['id'], 'name', 'companies', 'name');
                        }
                    }

                    foreach(Yii::app()->session['update_offices'] as $office)
                    {
                        if(!$this->isSameUpdate('id', $office['id'], 'country', 'offices', $office['country'], 'country'))
                        {
                            $update_change_array[$this->getEmailById($office['id'])]['country_new'] = $office['country'];
                            $update_change_array[$this->getEmailById($office['id'])]['country_old'] = $this->getOldValueUpdate('id', $office['id'], 'country', 'offices', 'country');
                        }
                    }

                    foreach(Yii::app()->session['regions'] as $key => $officeregion)
                    {
                        if(!$this->isSameUpdate('office',
                                $officeregion['office'],
                                'GROUP_CONCAT(DISTINCT region SEPARATOR ";") as region',
                                'officeregions',
                                $officeregion['region'],
                                'region'))
                        {
                            $update_change_array[$this->getEmailById($officeregion['office'])]['region_new'] = $officeregion['region'];
                            $update_change_array[$this->getEmailById($officeregion['office'])]['region_old'] = $this->getOldValueUpdate('office', $officeregion['office'], 'GROUP_CONCAT(DISTINCT region SEPARATOR ";") as region', 'officeregions', 'region');
                        }
                    }

                    foreach(Yii::app()->session['sectors'] as $officesector)
                    {
                        if(!$this->isSameUpdate('office',
                            $officesector['office'],
                            'GROUP_CONCAT(DISTINCT workArea SEPARATOR ";") as workArea',
                            'officeworkareas',
                            $officesector['sector'],
                            'workArea'))
                        {
                            $update_change_array[$this->getEmailById($officesector['office'])]['workArea_new'] = $officesector['sector'];
                            $update_change_array[$this->getEmailById($officesector['office'])]['workArea_old'] = $this->getOldValueUpdate('office', $officesector['office'], 'GROUP_CONCAT(DISTINCT workArea SEPARATOR ";") as workArea', 'officeworkareas', 'workArea');
                        }
                    }

                    $warning_array = array(
                        array(
                            'session' => array(
                                'update_offices',
                                'new_offices'
                            ),
                            'from' => 'offices',
                            'where' => 'country'
                        ),
                        array(
                            'session' => array(
                                'regions',
                                'new_officeregions'
                            ),
                            'from' => 'officeregions',
                            'where' => 'region'
                        ),
                        array(
                            'session' => array(
                                'sectors',
                                'new_officeworkareas'
                            ),
                            'from' => 'officeworkareas',
                            'where' => 'sector'
                        )
                    );

                    // new companies, regions and sectors (no translations)
                    foreach($warning_array as $warning_row)
                    {
                        foreach($warning_row['session'] as $session_name)
                        {
                            foreach(Yii::app()->session[$session_name] as $row_update)
                            {
                                if($session_name === 'new_officeworkareas')
                                {
                                    $row_update_val = explode(';', $row_update['workArea']);
                                }
                                else
                                {
                                    $row_update_val = explode(';', $row_update[$warning_row['where']]);
                                }
                                if($session_name === 'new_officeworkareas' or $session_name === 'sectors')
                                {
                                    $row_update_lang = 'workArea';
                                }
                                else
                                {
                                    $row_update_lang = $warning_row['where'];
                                }
                                foreach($row_update_val as $value_for_where) {
                                    if ($this->isNewSortValue($warning_row['from'], $warning_row['where'], $value_for_where)
                                        and !empty($value_for_where)
                                        and Yii::t($row_update_lang, $value_for_where) === $value_for_where
                                    )
                                    {
                                        $no_translate_array[$warning_row['where']][] = $value_for_where;
                                    }
                                }
                            }
                        }
                    }
                    foreach($no_translate_array as $key => $no_translate)
                    {
                        $no_translate_array[$key] = array_unique($no_translate);
                    }

                    // skiped count
                    foreach($update_change_array as $key => $email)
                    {
                        $update_change_array[$key]['company'] = $this->GetCompanyByEmail($key);
                        $update_change_array[$key]['id'] = $this->getIdByEmailInArray($key, Yii::app()->session['import_data_array']['offices'], 'id');
                    }
                    $withNewDomainCount = 0;
                    foreach($csv_existent_domain_emails as $domain) {
                        $withNewDomainCount = $withNewDomainCount + count($domain);
                    }
                    $same_rows = count(Yii::app()->session['import_data_array']['companies']) - count($update_change_array) - count($new_records) - $withNewDomainCount;

                    $this->render('importpreview',array(
                            'new_emails' => $new_records,
                            'update_emails' => $update_change_array,
                            'csv_existent_domain_emails' => $csv_existent_domain_emails,
                            'db_domain_emails' => $db_domain_emails,
                            'same_rows_count' => $same_rows,
                            'no_translate_array' => $no_translate_array
                        ));
                }
                else
                {
                    // Refresh page
                    $this->redirect('/admin/office/admin');
                }
            }
            else
            {
                if(!empty($_POST['continue']))
                {
                    //unset unselected records
                    $pk = $this->getPkFromPost($_POST);
                    $this->unsetFromSessionsByPk($pk);

//                        echo '<pre>';
//                        print_r(Yii::app()->session['new_companies']);
//                        print_r(Yii::app()->session['new_offices']);
//                        print_r(Yii::app()->session['new_officeregions']);
//                        print_r(Yii::app()->session['new_officeworkareas']);
//
//                        print_r(Yii::app()->session['update_companies']);
//                        print_r(Yii::app()->session['update_offices']);
//                        print_r(Yii::app()->session['update_officeregions']);
//                        print_r(Yii::app()->session['update_officeworkareas']);
//                        echo '</pre>';
//                        Yii::app()->end();
//                    Yii::app()->end();
                    isset(Yii::app()->session['import_data_array']) ? $import_data_array = Yii::app()->session['import_data_array'] : $import_data_array = array();
                    isset(Yii::app()->session['new_emails']) ? $new_emails = Yii::app()->session['new_emails'] : $new_emails = array();
                    isset(Yii::app()->session['update_emails']) ? $update_emails = Yii::app()->session['update_emails'] : $update_emails = array();

                    if(!empty(Yii::app()->session['new_companies'])
                            and
                            !empty(Yii::app()->session['new_offices'])
                            and
                            !empty(Yii::app()->session['new_officeregions'])
                            and
                            !empty(Yii::app()->session['new_officeworkareas']))
                    {
//                        echo '<pre>';
//                        print_r(Yii::app()->session['new_companies']);
//                        print_r(Yii::app()->session['new_offices']);
//                        print_r(Yii::app()->session['new_officeregions']);
//                        print_r(Yii::app()->session['new_officeworkareas']);
//                        echo '</pre>';
//                        Yii::app()->end();
                        $CommandBuilder_new = Yii::app()->db->schema->commandBuilder;
                        $Transaction_new = Yii::app()->db->beginTransaction();
                        try
                        {
                                //$CommandBuilder_new->createDeleteCommand('companies', new CDbCriteria())->execute();
                                $CommandBuilder_new->createMultipleInsertCommand('companies', Yii::app()->session['new_companies'])->execute();
                                $CommandBuilder_new->createMultipleInsertCommand('offices', Yii::app()->session['new_offices'])->execute();
                                $CommandBuilder_new->createMultipleInsertCommand('officeregions', Yii::app()->session['new_officeregions'])->execute();
                                $CommandBuilder_new->createMultipleInsertCommand('officeworkareas', Yii::app()->session['new_officeworkareas'])->execute();

                                unset(Yii::app()->session['new_companies']);
                                unset(Yii::app()->session['new_offices']);
                                unset(Yii::app()->session['new_officeregions']);
                                unset(Yii::app()->session['new_officeworkareas']);

                                $Transaction_new->commit();
                        }
                        catch(Exception $e)
                        {
                           $Transaction_new->rollback();
                           throw new CDbException('Comanies insert error.');
                        }
                    }
                    if(!empty(Yii::app()->session['update_companies'])
                            and
                            !empty(Yii::app()->session['update_offices'])
                            and
                            !empty(Yii::app()->session['update_officeregions'])
                            and
                            !empty(Yii::app()->session['update_officeworkareas']))
                    {
//                        echo 'Update<pre>';
//                        print_r(Yii::app()->session['update_companies']);
//                        print_r(Yii::app()->session['update_offices']);
//                        print_r(Yii::app()->session['update_officeregions']);
//                        print_r(Yii::app()->session['update_officeworkareas']);
//                        echo '</pre>';
//                        Yii::app()->end();
                        $Transaction_update = Yii::app()->db->beginTransaction();
                        try
                        {
                            foreach(Yii::app()->session['update_companies'] as $update_company)
                            {
                                $CompanyModel = Company::model()->findByPk($update_company['id']);
                                $CompanyModel->attributes = $update_company;
                                $CompanyModel->save();
                            }
                            foreach(Yii::app()->session['update_offices'] as $update_offices)
                            {
                                $OfficeModel = Office::model()->findByPk($update_offices['id']);
                                unset($update_offices['id']);
                                unset($update_offices['company']);
                                $OfficeModel->attributes = $update_offices;
                                $OfficeModel->save();
                            }

                            $criteria = new CDbCriteria;
                            $OfficeRegion = new OfficeRegion;
                            $OfficeWorkarea = new OfficeWorkarea;

                            foreach(Yii::app()->session['update_officeregions'] as $update_officeregions)
                            {
                                $OfficeRegion->deleteAll('office = :office', array(':office'=>$update_officeregions['office']));
                            }
                            foreach(Yii::app()->session['update_officeregions'] as $update_officeregions)
                            {
                                $OfficeRegion = new OfficeRegion;
                                $OfficeRegion->attributes = $update_officeregions;
                                $OfficeRegion->save();
                            }

                            foreach(Yii::app()->session['update_officeworkareas'] as $update_officeworkareas)
                            {
                                $OfficeWorkarea->deleteAll('office = :office', array(':office'=>$update_officeworkareas['office']));
                            }
                            foreach(Yii::app()->session['update_officeworkareas'] as $update_officeworkareas)
                            {
                                $OfficeWorkarea = new OfficeWorkarea;
                                $OfficeWorkarea->attributes = $update_officeworkareas;
                                $OfficeWorkarea->save();
                            }

                            unset(Yii::app()->session['update_companies']);
                            unset(Yii::app()->session['update_offices']);
                            unset(Yii::app()->session['update_officeregions']);
                            unset(Yii::app()->session['update_officeworkareas']);

                            $Transaction_update->commit();

                        }
                        catch(Exception $e)
                        {
                           $Transaction_update->rollback();
                           throw new CDbException('Companies update error.');
                        }
                        //$this->redirect('/admin/office/admin');
                    }
                    $this->redirect('/admin/office/admin');
                }
                elseif(!empty($_POST['cancel']))
                {
                    unset(Yii::app()->session['update_emails']);
                    unset(Yii::app()->session['new_emails']);
                    unset(Yii::app()->session['import_data_array']);

                    unset(Yii::app()->session['update_companies']);
                    unset(Yii::app()->session['update_offices']);
                    unset(Yii::app()->session['update_officeregions']);
                    unset(Yii::app()->session['update_officeworkareas']);

                    unset(Yii::app()->session['new_companies']);
                    unset(Yii::app()->session['new_offices']);
                    unset(Yii::app()->session['new_officeregions']);
                    unset(Yii::app()->session['new_officeworkareas']);
                    unset(Yii::app()->session['new_previewcompanies']);

                    $this->redirect('/admin/office/admin');
                }
            }
        }

        private function isNewSortValue($from, $where, $val) {
            if($where === 'sector') {
                $where = 'workArea';
            }
            $value = Yii::app()->db->createCommand()
                        ->select('id')
                        ->from($from)
                        ->where($where . ' = ' . ':val', array(':val' => $val))
                        ->queryRow();
            if(!empty($value['id']))
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }
        private function getPkFromPost($post) {
            foreach($post as $email)
            {
                if($this->getIdByEmailInArray($email, Yii::app()->session['new_offices'], 'id')) {$pk[] = $this->getIdByEmailInArray($email, Yii::app()->session['new_offices'], 'id');}
                if($this->getIdByEmailInArray($email, Yii::app()->session['update_offices'], 'id')) {$pk[] = $this->getIdByEmailInArray($email, Yii::app()->session['update_offices'], 'id');}
            }
            if(!empty($pk))
            {
                return $pk;
            }
            else
            {
                return false;
            }
        }
        private function getIdByEmailInArray($needle, $haystack, $selector) {
            foreach($haystack as $value) {
                if($needle === $value['email'])
                {
                    return $value[$selector];
                }
            }
            return false;
        }

        private function getKeyByIdFromArray($needle, $haystack, $selector) {
            foreach($haystack as $key=>$value) {
                if($needle === $value[$selector])
                {
                    $key_vals[] = $key;
                }
            }
            if(!empty($key_vals))
            {
                return $key_vals;
            }
            else
            {
                return false;
            }
        }

        private function unsetFromSessionsByPk($pk)
        {
            if(!empty($pk)) {
                $sessions_array = array(
                    array('session' => 'new_companies', 'selector' => 'id'),
                    array('session' => 'new_offices', 'selector' => 'id'),
                    array('session' => 'new_officeregions', 'selector' => 'office'),
                    array('session' => 'new_officeworkareas', 'selector' => 'office'),
                    array('session' => 'update_companies', 'selector' => 'id'),
                    array('session' => 'update_offices', 'selector' => 'id'),
                    array('session' => 'update_officeregions', 'selector' => 'office'),
                    array('session' => 'update_officeworkareas', 'selector' => 'office')
                );
                foreach ($sessions_array as $session) {
                    foreach ($pk as $pkey) {
                        if ($this->getKeyByIdFromArray($pkey, Yii::app()->session[$session['session']], $session['selector'])) {
                            $key_to_unset = $this->getKeyByIdFromArray($pkey, Yii::app()->session[$session['session']], $session['selector']);
                            foreach ($key_to_unset as $unset_key) {
                                $arr = Yii::app()->session[$session['session']];
                                unset($arr[$unset_key]);
                                Yii::app()->session[$session['session']] = $arr;
                            }
                        }
                    }
                }
            }
        }

        public function isSameUpdate($where, $id, $select, $from, $check, $select_name)
        {
            $CommandBuilder = Yii::app()->db;
            $valueToUpdate = $CommandBuilder->createCommand()
                                ->select($select)
                                ->from($from)
                                ->where($where.'=:'.$where, array(':'.$where=>$id))
                                ->queryRow();
            if(!empty($valueToUpdate[$select_name]))
            {
                return $valueToUpdate[$select_name] === $check;
            }
            else
            {
                return empty($check);
            }

        }
        public function getOldValueUpdate($where, $id, $select, $from, $select_name)
        {
            $CommandBuilder = Yii::app()->db;
            $valueToUpdate = $CommandBuilder->createCommand()
                                ->select($select)
                                ->from($from)
                                ->where($where.'=:'.$where, array(':'.$where=>$id))
                                ->queryRow();
            return $valueToUpdate[$select_name];
        }
        public function getEmailById($id)
        {
            $CommandBuilder = Yii::app()->db;
                    $select_email_by_id = $CommandBuilder->createCommand()
                                ->select('email')
                                ->from('offices')
                                ->where('id=:id', array(':id'=>$id))
                                ->queryRow();
            return $select_email_by_id['email'];
        }

        private function GetCompanyByEmail($email)
        {
            $CommandBuilder = Yii::app()->db;
            $get_company_by_email = $CommandBuilder->createCommand()
                ->select('companies.name')
                ->from('companies')
                ->join('offices', 'offices.company = companies.id')
                ->where('email=:email', array(':email'=>$email))
                ->queryRow();
            return $get_company_by_email['name'];
        }

        public function getIdbyEmail($email)
        {
            $CommandBuilder = Yii::app()->db;
                    $select_emails_id = $CommandBuilder->createCommand()
                                ->select('id')
                                ->from('offices')
                                ->where('email=:email', array(':email'=>$email))
                                ->queryRow();
                    return $select_emails_id['id'];
        }
        public function getCompanyIdbyEmail($email)
        {
            $CommandBuilder = Yii::app()->db;
                    $select_emails_id = $CommandBuilder->createCommand()
                                ->select('company')
                                ->from('offices')
                                ->where('email=:email', array(':email'=>$email))
                                ->queryRow();
                    return $select_emails_id['company'];
        }

        public function getIdbyRegion($office, $region)
        {
            $CommandBuilder = Yii::app()->db;
                    $select_regoins_id = $CommandBuilder->createCommand()
                                ->select('id')
                                ->from('officeregions')
                                ->where('office = :office', array(':office'=>$office))
                                ->andWhere('region = :region', array(':region'=>$region))
                                ->queryRow();
                    return $select_regoins_id['id'];
        }
        public function getIdbyWorkArea($office)
        {
            $CommandBuilder = Yii::app()->db;
                    $select_workareas_id = $CommandBuilder->createCommand()
                                ->select('id')
                                ->from('officeworkareas')
                                ->where('office = :office', array(':office'=>$office))
                                ->queryRow();
                    return $select_workareas_id['id'];
        }

        public function compareArrays($array1, $array2)
        {
            return array_intersect($array1, $array2);
        }

	/**
	 * Loads sectros from database and caches they.
	 * @return string[] Existing sectors.
	 */
	public function LoadSectors()
	{
		// Check cache.
		if(empty($this->existingSectors))
		{
			// Load new data if cache is empty.
			$this->existingSectors = Yii::app()->db->createCommand()
				->selectDistinct('workArea')
				->from('officeworkareas')
				->queryColumn();
		}
		// Return data.
		return $this->existingSectors;
	}

	/**
	 * Loads regions from database and caches they.
	 * @return string[] Existing regions.
	 */
	public function LoadRegions()
	{
		// Check cache.
		if(empty($this->existingRegions))
		{
			// Load new data if cache is empty.
			$this->existingRegions = Yii::app()->db->createCommand()
				->selectDistinct('region')
				->from('countries')
				->queryColumn();
		}
		// Return data.
		return $this->existingRegions;
	}

	/**
	 * Loads countries from database and caches they.
	 * @return string[] Existing countries.
	 */
	public function LoadCountries()
	{
		// Check cache.
		if(empty($this->existingCountries))
		{
			// Load new data if cache is empty.
			$this->existingCountries = Yii::app()->db->createCommand()
				->selectDistinct('country')
				->from('countries')
				->queryColumn();
		}
		// Return data.
		return $this->existingCountries;
	}

	/**
	 * Find each of sector from the csv string.
	 *
	 * @param string $sectorsString Csv row which contain one or more sectors.
	 * @
	 * @return string[] An array of sectors.
	 */
	public function FindSectors($sectorsString)
	{
		/**
		 * Get existing sectors from database and try to find
		 * one of them in the string.
		 *
		 * @var string[] $sectors An array of separated fields from input sectors string.
		 * @var string $sector Current sector used in cycle.
		 */
		$this->LoadSectors();
		// Trim spaces and delimiters and parse csv to array
		$sectors = explode(';', trim($sectorsString));
		// Find sectors
		foreach($sectors as $sector)
		{
			//if( ! in_array($sector, $this->existingSectors))
			//{
			//	throw new CException('Wrong sector: '.$sector);
			//}
		}
		return $sectors;
	}

	/**
	 * Find each of region from the csv string.
	 *
	 * @param string $regionsString Csv row which contain one or more regions.	 *
	 * @param string $delimiter A delimeter of input and returned fields strings.
	 * @return string[] An array of regions.
	 */
	public function FindRegions($regionsString)
	{
		/**
		 * Get existing regions from database and try to find
		 * one of them in the string.
		 *
		 * @var string[] $inputFields An array of separated fields from input regions string.
		 * @var string $regions An array of found regions.
		 * @var string $region Current region used in cycle.
		 */
		$this->LoadRegions();
		// Trim spaces and delimiters and parse csv to array
		$inputFields = explode(';', trim($regionsString));
		$regions = array();
		// Find regions.
		foreach($inputFields as $region)
		{
			//if(in_array($region, $this->existingRegions))
			//{
				// Add region to the found.
				$regions[] = $region;
			//}
		}
		return $regions;
	}

	/**
	 * Find a country from the csv string.
	 *
	 * Country can be only one. If will found more then one country exception will be thrown.
	 *
	 * @param string $countriesString Csv row which contain one or more countries.
	 * @param string $delimiter A delimeter of input and returned fields strings.
	 * @return string|bool One country, empty string or false if more then one country found.
	 */
	public function FindCountries($countriesString)
	{
		/**
		 * Get existing countries from database and try to find
		 * one of them in the string.
		 *
		 * @var string[] $inputFields An array of separated fields from input countries string.
		 * @var string $foundCountry The found country.
		 * @var string $country Current country used in cycle.
		 */
		$this->LoadCountries();
		// Trim spaces and delimiters and parse csv to array
		$inputFields = explode(';', trim($countriesString));
		$foundCountry = '';
		// Find country
		foreach($inputFields as $country)
		{
			if(in_array($country, $this->existingCountries))
			{
				// If found more then one country then throw exception
				if( ! empty($foundCountry))
				{
					return false;
				}
				// Store found country.
				$foundCountry = $country;
			}
		}
		return $foundCountry;
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Office the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Office::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Office $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='office-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    /**
     * Change company status.
     * @return bool
     */
    public function actionSwitchStatus()
    {
        if (!isset($_POST['id'])) return false;

        $office = Office::model()->findByPk($_POST['id']);
        if ($office) {
            $office->setAttribute('inactive', !(bool)$office->getAttribute('inactive'));
            $office->update();
            $result = true;
        }
        else {
            $result = false;
        }

        return $result;
    }


    public function actionGetCSV() {
        if (!isset($_POST['ids'])) return false;

        $sessions_new_array = array(
            array('session' => 'new_companies', 'selector' => 'id'),
            array('session' => 'new_offices', 'selector' => 'id'),
            array('session' => 'new_officeregions', 'selector' => 'office'),
            array('session' => 'new_officeworkareas', 'selector' => 'office')
        );

        $sessions_update_array = array(
            array('session' => 'companies', 'selector' => 'id'),
            array('session' => 'offices', 'selector' => 'id'),
            array('session' => 'officeregions', 'selector' => 'office'),
            array('session' => 'officeworkareas', 'selector' => 'office')
        );

        if($_POST['action'] === 'update-records')
        {
            foreach($sessions_update_array as $session)
            {
                foreach ($_POST['ids'] as $id) {
                    $data = array();
                    foreach ($this->getKeyByIdFromArray((int)$id, Yii::app()->session['import_data_array'][$session['session']], $session['selector']) as $key) {
                        $data[] = Yii::app()->session['import_data_array'][$session['session']][$key];
                    }
                    $export_array[$id][$session['session']] = $data;
                }
            }

            foreach($export_array as $array) {
                $regions_array = array();
                foreach($array['officeregions'] as $regions)
                {
                    $regions_array[] = $regions['region'];
                }
                $region = implode(';', $regions_array);

                $workareas_array = array();
                foreach($array['officeworkareas'] as $workareas)
                {
                    $workareas_array[] = $workareas['workArea'];
                }
                $workarea = implode(';', $workareas_array);

                $result[] = array(
                    'name' => $array['companies'][0]['name'],
                    'email' => $array['offices'][0]['email'],
                    'region' => $region,
                    'country' => $array['offices'][0]['country'],
                    'workareas' => $workarea
                );
            }
        }
        else
        {
            foreach($sessions_new_array as $session)
            {
                foreach ($_POST['ids'] as $id) {
                    $data = array();
                    foreach ($this->getKeyByIdFromArray((int)$id, Yii::app()->session[$session['session']], $session['selector']) as $key) {
                        $data[] = Yii::app()->session[$session['session']][$key];
                    }
                    $export_array[$id][$session['session']] = $data;
                }
            }

            foreach($export_array as $array) {
                $regions_array = array();
                foreach($array['new_officeregions'] as $regions)
                {
                    $regions_array[] = $regions['region'];
                }
                $region = implode(';', $regions_array);

                $workareas_array = array();
                foreach($array['new_officeworkareas'] as $workareas)
                {
                    $workareas_array[] = $workareas['workArea'];
                }
                $workarea = implode(';', $workareas_array);

                $result[] = array(
                    'name' => $array['new_companies'][0]['name'],
                    'email' => $array['new_offices'][0]['email'],
                    'region' => $region,
                    'country' => $array['new_offices'][0]['country'],
                    'workareas' => $workarea
                );
            }
        }
        $this->Export($_POST['action'].'.csv', $result);
    }

    private function Export($fileName, $content) {
        $filePath = Yii::getPathOfAlias('uploads').DIRECTORY_SEPARATOR.$fileName;
        $file = Yii::app()->File->Open($filePath, 'w');

        $header_line = true;
        foreach($content as $row)
        {
            if($header_line) {
                fputcsv($file, array_keys($row));
                $header_line = false;
            }
            fputcsv($file, $row);
        }

        print_r($filePath);

        Yii::app()->File->Close($file);
    }

    public function actionAjaxEditImportData() {
        if (!isset($_POST['id'])) return false;

        $key = $this->getKeyByIdFromArray((int)$_POST['id'], Yii::app()->session['new_previewcompanies'], 'id');

        $array_companies = Yii::app()->session['new_previewcompanies'];
        $array_companies[$key[0]]['name'] = $_POST['value'];
        Yii::app()->session['new_previewcompanies'] = $array_companies;

        $key_import = $this->getKeyByIdFromArray((int)$_POST['id'], Yii::app()->session['new_companies'], 'id');

        $array_companies_import = Yii::app()->session['new_companies'];
        $array_companies_import[$key_import[0]]['name'] = $_POST['value'];
        Yii::app()->session['new_companies'] = $array_companies_import;

    }

    public function actionAjaxComments() {
        if (!isset($_POST['id'])) return false;

        $office = Office::model()->findByPk((int)$_POST['id']);
        $office->setAttribute('comment', $_POST['value']);
        $office->update();
    }
    public function actionCheckExportPass() {
        if (!isset($_POST['pass'])) return false;

        $pass = 'qwerty';

        if( $_POST['pass'] === $pass) {
            Yii::app()->session['export_access'] = 1;
        } else {
            if(!isset(Yii::app()->session['export_validate_sent'])) {
                $params = array(
                    'fromEmail' => Yii::app()->Settings->Get('email', 'admin'),
                    'fromName' => 'Maritime CV',
                    'to' => Yii::app()->Settings->Get('email', 'admin'),
                    //'to' => 'pinchoalex@gmail.com',
                    'subject' => Yii::t('phrase', 'Maritime CV - access control report'),
                    'body' => Yii::t('phrase', 'Someone tried to access export and failed')
                );
                if(Yii::app()->Email->Send($params)) {
                    Yii::app()->session['export_validate_sent'] = true;
                }
            }
        }

        if(Yii::app()->session['export_access']){
            echo 'success';
        } else {
            echo 'fail';
        }


    }

}
