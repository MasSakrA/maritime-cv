/**
 * Main javascrit code file.
 * Contain common methods and code.
 */

/**
 * Method for adding file dialog opening on click to specified element
 * @param {string|jQuery} element Selector or dom element to wich will be added file uploading function
 * @param {string|jQuery} fileInput  Selector or dom element wich is file input
 * @returns {undefined} No return
 */
function AddFileUploadingEvent(element, fileInput)
{
	// Hide file inut if it is visible
	if($(fileInput).is(':visible'))
	{
		$(fileInput).hide();
	}
	// Trigger
	$(element).on('click', function()
	{
		$(fileInput).trigger('click');
	});
}

/**
 * Method shows one element if file selected and other one if not
 * @param {string|jQuery} fileInput input file type
 * @param {string|jQuery} fileSelectedElement shown when file selected
 * @param {string|jQuery} fileNotSelectedElement shown when file not selected
 * @param {string|jQuery} input with file name
 * @returns {undefined}
 */
 function AddFileSelectedEvent(fileInput, fileSelectedElement, fileNotSelectedElement, fileErrorElement, inutWithFileName, allowedExtIndex)
{

	// allowed extensions
	var exts = new Array(
		["doc","docx","xls","xlsx","rtf","pdf","zip","rar","jpg"], // 0
		["zip"] // 1
	);

	// Check file name existing
	if($(inutWithFileName).val() !== '')
	{
		// If not empty file already was loaded on server
		$(fileSelectedElement).show();
		$(fileNotSelectedElement).hide();
	}
	else
	{
		// Else file was not uploaded yet
		$(fileSelectedElement).hide();
		$(fileNotSelectedElement).show();
	}

	// When file input changed
	$(fileInput).on('change', function()
	{

		// Check for file selecting
		if($(fileInput).val() == '')
		{
			// If file not selected
			$(fileSelectedElement).hide();
			$(fileNotSelectedElement).show();

		}
		else
		{
			// If file selected

			var fileExt = getExt($(fileInput).val());
			if( isContains( exts[allowedExtIndex], fileExt) )
			{
				// allowed file extension
				$(fileSelectedElement + ' span').html(fileExt);

				$(fileSelectedElement).attr('title', getFileName($(fileInput).val())).show();
                $(fileSelectedElement).parents('.square-content').attr('title', getFileName($(fileInput).val()));
				$(fileNotSelectedElement).hide();
				$(fileErrorElement).hide();

                                $('.errorMessage').hide();
			}
			else
			{
				// disallowed file extension
				$(fileSelectedElement).hide();
				$('.errorMessage.file').hide();
				$(fileErrorElement + ' span').html(': ' + exts[allowedExtIndex].join(', ') + '.' );
				$(fileErrorElement).show();
				// Clear selected
				$(fileInput).val('');
			}
		}
	});
}


// ======================================= SOME USEFUL FUNCTIONS =============================
// returns extension of filename
function getExt( fname ){ return fname.toLowerCase().substr((~-fname.lastIndexOf(".") >>> 0) + 2); }
// returns filename
function getFileName( fullPath ){ return fullPath.replace(/^.*(\\|\/|\:)/, ''); }
// returns is array a contains value b
function isContains( a, b ){ return!!~a.indexOf(b); }



// Our countdown plugin takes a callback, a duration, and an optional message
$.fn.countdown = function (duration, message) {
    // If no message is provided, we use an empty string
    message = message || "";
    // Get reference to container, and set initial content
    var container = $(this[0]).html(duration + message);
    // Get reference to the interval doing the countdown
    var countdown = setInterval(function () {
        // If seconds remain
        if (--duration) {
            // Update our container's message
            container.html(duration + message);
        // Otherwise
        } else {
            // Clear the countdown interval
            clearInterval(countdown);

        }
    // Run interval every 1000ms (1 second)
    }, 1000);

};

function count_preview_plus( el ) {
    var bage =  el.parents('.import-block').find('.block-header').find('.badge-success');
    var count = bage.text()*1;
    bage.html(count + 1);
    refresh_total();
    $('.total .same_rows_total').html($('.total .same_rows_total').text()*1 - 1);
}
function count_preview_minus( el ) {
    var bage =  el.parents('.import-block').find('.block-header').find('.badge-success');
    var count = bage.text()*1;
    bage.html(count - 1);
    refresh_total();
    $('.total .same_rows_total').html($('.total .same_rows_total').text()*1 + 1);

}
function refresh_total() {
    var new_record_val = $('.new-emails-import .new-record').text();
    var new_with_domain = $('.existent-domains-import .new-with-domain').text();
    var will_be_update = $('.update-emails-import .will-be-updated').text();
    $('.total .new-record').html(new_record_val);
    $('.total .new-with-domain').html(new_with_domain);
    $('.total .will-be-updated').html(will_be_update);
}

function makeDiagrm() {
    $('.progress').html('');
    var all = 0;
    $("#work-area-filters li input:checked").each(function() {
        if($(this).val() != 'all') {
           all = all + $(this).next('label').attr('data-count-all')*1;
        }
    });

    $("#work-area-filters li input:checked").each(function() {
        if($(this).val() != 'all') {
            var percent = $(this).next('label').attr('data-count-current') * 100 / all;
            $('.progress').append('<div class="progress-bar progress-bar-' + $(this).val() + '" style="width: ' + percent.toFixed(2) + '%">' +
            $(this).next('label').find('name.area-name').text() + ' (' + $(this).next('label').attr('data-count-current') + '/' + $(this).next('label').attr('data-count-all') + ')' +
            '</div>');
        }
    });
}

function case_print_boxes(count) {

    switch (count) {
        case(5):
            $('#progress-bars').html('<div class="row-fluid-5">' +
            '<div class="span1 box-1"></div>' +
            '<div class="span1 box-2"></div>' +
            '<div class="span1 box-3"></div>' +
            '<div class="span1 box-4"></div>' +
            '<div class="span1 box-5"></div>' +
            '</div>');
            break;
        case(4):
            $('#progress-bars').html('<div class="row-fluid-12">' +
            '<div class="span3 box-1"></div>' +
            '<div class="span3 box-2"></div>' +
            '<div class="span3 box-3"></div>' +
            '<div class="span3 box-4"></div>' +
            '</div>');
            break;
        case(3):
            $('#progress-bars').html('<div class="row-fluid-12">' +
            '<div class="span4 box-1"></div>' +
            '<div class="span4 box-2"></div>' +
            '<div class="span4 box-3"></div>' +
            '</div>');
            break;
        case(2):
            $('#progress-bars').html('<div class="row-fluid-12">' +
            '<div class="span6 box-1"></div>' +
            '<div class="span6 box-2"></div>' +
            '</div>');
            break;
        case(1):
            $('#progress-bars').html('<div class="row-fluid-12">' +
            '<div class="span12 box-1"></div>' +
            '</div>');
            break;
        default:
            $('#progress-bars').html('<div class="row-fluid-12">' +
            '<div class="span4 box-1"></div>' +
            '<div class="span4 box-2"></div>' +
            '<div class="span4 box-3"></div>' +
            '</div>');
            break;
    }
}




function progressBars() {

    var cat_length = $("#work-area-filters li input:checked").not('#work-area-all').length;

    if( cat_length % 5 === 0 ) {
        var count_box = 5;
        case_print_boxes(count_box);
    }
    else if( cat_length % 4 === 0 ) {
        var count_box = 4;
        case_print_boxes(count_box);
    }
    else if( cat_length % 3 === 0 ) {
        var count_box = 3;
        case_print_boxes(count_box);
    }
    else if( cat_length === 2 ) {
        var count_box = 2;
        case_print_boxes(count_box);
    }
    else if( cat_length === 1 ) {
        var count_box = 1;
        case_print_boxes(count_box);
    }
    else {
        var v1 = count_box % 5;
        var v2 = count_box % 4;
        var v3 = count_box % 3;
        var max = Math.max(v1, v2, v3);

        if(v1 === max) {
            case_print_boxes(5);
        }
        else if(v2 === max) {
            case_print_boxes(4);
        }
        else if(v3 === max) {
            case_print_boxes(3);
        }

    }

    $('#progress-bars .span1').html('');
    $('#progress-bars .span3').html('');
    $('#progress-bars .span4').html('');
    var box = 0;
    $("#work-area-filters li input:checked").not('#work-area-all').each(function() {
        if($(this).val() != 'all') {
            box = box + 1;
            var percent =  $(this).next('label').attr('data-percent');
            $('#progress-bars .box-' + box).append('<div class="progress-block"><p><h5>' +
            $(this).next('label').find('name.area-name').text() +
            ' (' +
            $(this).next('label').attr('data-count-current') +
            '/' +
            $(this).next('label').attr('data-count-all') +
            ')</h5></p>' +
            '<div class="progress"><div class="progress-bar" role="progressbar" aria-valuenow="' + percent + '" aria-valuemin="0" aria-valuemax="100" style="width: ' + percent + '%;"></div></div>' +
            '</div></div>');
            if(box === count_box) { box = 0; }
        }
    });
}

function loadCompaniesSelect() {
    if ($('#companies-form .checkedByCookies').length > 0) {
        $('#companies-form .checkedByCookies').attr('checked', 'checked');

        var total = 0;
        $("input[checked=checked] ").next("label").find("price.region-price").each(function() {
            total += parseFloat(this.innerHTML);
        });
        if(total.toFixed(2) < 10) {
            $("price.total").html("10.00");
        } else {
            $("price.total").html(total.toFixed(2));
        }
    }

    $('#companies-form .yiiPager li a').each(function() {
        $(this).attr('href', $(this).attr('href').replace('ajaxchangesession', 'companies'));
    });

    if($("#companies-form ").length > 0) {
        //makeDiagrm();
        progressBars();

        $('#company-img .form-group label').on('click', function() {
            $(this).parent().addClass('disabled');
        });

        $('#filters label').on('click', function() {
            $('#companies-form').addClass('disabled');
        });

        $('#work-area-filters li label').on('mouseenter', function() {
            if($(this).prev('input').val() != 'all') {
                var allcount = $(this).attr('data-count-all');
                var currentcount = $(this).attr('data-count-current');
                $(this).parent().prepend('<p class="tip">' + currentcount + '/' + allcount + '</p>');
            }
        });
        $('#work-area-filters li label').on('mouseleave', function() {
            if($(this).prev('input').val() != 'all') {
                $(this).parent().find('p.tip').remove();
            }
        });
    }
}

function onValidationFailed(fileInput, fileSelectedElement, fileErrorElement, size) {
    // disallowed file extension
    $(fileSelectedElement).hide();
    $('.errorMessage.file').hide();
    $(fileErrorElement + ' span').html(': ' + size + '.' );
    $(fileErrorElement).show();
    // Clear selected
    $(fileInput).val('');
}

function validateFileSize() {
    if($('.form-group.hidden').length > 0) {
        var valid_cv_file_size = 1024000;
        var valid_attachment_file_size = 512000;

        $('.form-group.hidden #cv-file').on('change', function () {
            if (this.files[0].size > valid_cv_file_size) {
                onValidationFailed('#cv-file', '#cv-file-select .file-loaded-mark', '#error-cv-size', '1MB');
            }
        });
        $('.form-group.hidden #attachment1').on('change', function () {
            if (this.files[0].size > valid_attachment_file_size) {
                onValidationFailed('#attachment1', '#attachment1-select .file-loaded-mark', '#error-file-size-1', '0.5MB');
            }
        });
        $('.form-group.hidden #attachment2').on('change', function () {
            if (this.files[0].size > valid_attachment_file_size) {
                onValidationFailed('#attachment2', '#attachment2-select .file-loaded-mark', '#error-file-size-2', '0.5MB');
            }
        });
    }
}

$(document).ready(function()
{
	// Show file dialog on click on this elements
	AddFileUploadingEvent('#cv-file-select', '#cv-file');
	AddFileUploadingEvent('#attachment1-select', '#attachment1');
	AddFileUploadingEvent('#attachment2-select', '#attachment2');

	// Display what file selected for this elements
    AddFileSelectedEvent('#cv-file', '#cv-file-select .file-loaded-mark', '', '#error-cv', '#cv-file-name', 0);
	AddFileSelectedEvent('#attachment1', '#attachment1-select .file-loaded-mark', '', '#error-file-1', '#attachment1-name', 1);
	AddFileSelectedEvent('#attachment2', '#attachment2-select .file-loaded-mark', '', '#error-file-2', '#attachment2-name', 1);

	$('.errorMessage').on('click', function(){ $(this).hide(); });

    if($('body#admin-area').length === 0) {
        $('#order-form').on('submit', function (e) {

            if ($('.file-loaded-mark').children('span').first().text().length === 0) {
                e.preventDefault();
                $('.errorMessage').hide();
                $('#error-cv-empty').show();
            }
        });
    }
    //import csv admin area preview
    if($('.dropdown').length > 0) {
        $('.dropdown .click_to_show_dropdown').next().hide();
        $('.dropdown .click_to_show_dropdown').on('click', function() {
            $(this).next().toggle();
        });
    }
    if($(".countdown").length > 0) {
        // Use p.countdown as container, pass redirect, duration, and optional message
        $(".countdown").countdown(10, "");
    }

    $('#admin-area .block-header').on('click', function() {
        $(this).parent().find('.block-content').toggle();
        $(this).toggleClass('visible');
    });
    $('#admin-area .navbar-inner-blue').on('click', function() {
        $(this).parent().nextAll('.hidden_list').toggle();
        $(this).children().find('img').toggleClass('rotated');
    });

    $('#admin-area input.check-uncheck-all').on('click', function() {
        $(this).parents('tr').nextAll('tr').find('input[type=checkbox]').trigger('click')
    });

    $('input.get-new-hidden-input').on('change', function() {
        //
        if(this.checked) {
            $('form#continue-import-form input[value="' + this.name + '"]').remove();
            $(this).parents('tr').css('opacity', '1');
            $(this).parents('tr').addClass('active');
            count_preview_plus( $(this) );
        } else {
            $('#continue-import-form').prepend('<input type="hidden" name="' + this.name + '" value="' + this.name + '"/>');
            $(this).parents('tr').css('opacity', '0.5');
            $(this).parents('tr').removeClass('active');
            count_preview_minus( $(this) );
        }
    });

    $('input.get-update-hidden-input').on('change', function() {
        //
        if(this.checked) {
            $('form#continue-import-form input[value="' + this.name + '"]').remove();
            $(this).parents('tr').nextAll('tr[data-email="'+this.name+'"]').css('opacity', '1');
            $(this).parents('tr').css('opacity', '1');

            count_preview_plus( $(this) );
        } else {
            $('#continue-import-form').prepend('<input type="hidden" name="' + this.name + '" value="' + this.name + '"/>');
            $(this).parents('tr').nextAll('tr[data-email="'+this.name+'"]').css('opacity', '0.5');
            $(this).parents('tr').css('opacity', '0.5');

            count_preview_minus( $(this) );
        }
    });

    $('img.new-records-ajax-img').on('click', function() {
        $('input.new-records-ajax-button').trigger('click');
    });
    $('img.new-domain-records-ajax-img').on('click', function() {
        $('input.new-domain-records-ajax-button').trigger('click');
    });
    $('img.update-records-ajax-img').on('click', function() {
        $('input.update-records-ajax-button').trigger('click');
    });

    $('.importpreview td.magic-input').on('dblclick', function() {
        if($(this).find('input').length < 1) {
            var value_text = $(this).text();
            var id = $(this).parents('tr').attr('data-id');
            $(this).html('<input name="id-'+id+'" type="text" value="'+value_text+'" id="id-'+id+'">');
        }
    });
    $('.importpreview td.magic-input').on('focusout', function() {
        var text = $(this).find('input').val();
        $(this).attr('data-status', 'changed');
        $(this).html(text);
        $('.change-records-ajax-button').trigger('click');
        $(this).attr('data-status', '');
    });

    $('#admin-area td.magic-textarea').on('dblclick', function() {
        if($(this).find('textarea').length < 1) {
            var value_text = $(this).text();
            var id = $(this).prev('td').find('input').attr('id');
            $(this).html('<textarea name="comment-' + id + '" id="id-' + id + '">' + value_text + '</textarea>');
        }
    });
    $('#admin-area td.magic-textarea').on('focusout', function() {
        var text = $(this).find('textarea').val();
        $(this).attr('data-status', 'changed');
        $(this).html(text);
        $('.change-comment-ajax-button').trigger('click');
        $(this).attr('data-status', '');
    });


    $('.stepContent .next-step').on('click', function () {
        if($('.template-text.selected').length === 1) {
            $('.modal').modal('toggle');
        } else {
            $('input.hide-input').trigger('click');
        }
    });
    if($('#myModal').length > 0) {
        $('#wizard a[href="/order/companies"]').on('click', function () {
            if ($('.template-text.selected').length === 1) {
                $('.modal').modal('toggle');
            } else {
                e.preventDefault();
                $('input.hide-input').trigger('click');
            }
            return false;
        });
        $('#wizard a[href="/order/payment"]').on('click', function () {
            if ($('.template-text.selected').length === 1) {
                $('.modal').modal('toggle');
            } else {
                event.preventDefault();
                $('input.hide-input').trigger('click');
            }
            return false;
        });
    }

    if($('#company-img').length > 0) {
        $(".form-group").on({
            "mouseover" : function() {
                $(this).find('img').attr('src', $(this).find('img').attr('data-src-hover'));
            },
            "mouseout" : function() {
                $(this).find('img').attr('src', $(this).find('img').attr('data-src'));
            }
        });
    }

    if($('#toTop').length > 0) {
        $(window).scroll(function(){
            if ($(this).scrollTop() < 100) {
                $('#toTop').removeClass('cd-is-visible');
            } else {
                $('#toTop').addClass('cd-is-visible');
            }
        });
        $('#toTop').click(function(){
            $('html, body').animate({scrollTop : 0},800);
            return false;
        });
    }


    loadCompaniesSelect();
    validateFileSize();

})