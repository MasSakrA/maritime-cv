-- Add new email template for user notify about order creation
INSERT INTO `emailtemplates` 
(`name`, `htmlTemplate`, `type`, `inactive`)
VALUES ( 'new-order-notify', 'new-order-notify', 'nf', '0');

-- Inser traslates for email template
INSERT INTO `SourceMessage`
(`id`, `category`, `message`)
VALUES ('67', 'htmlTemplate', 'new-order-notify');
INSERT INTO `Message`
(`id`, `language`, `translation`)
VALUES ('67', 'ru', 'Здравствуйте!<br /><br />Был успешно осуществлён заказ рассылки Вашего резюме по нашей базе компаний.<br />Номер вашего заказа: {orderNumber}<br />Сумма заказа: {orderAmount}<br />Более подробные детали вы можете просмотреть в вашем <a href="{accountLink}">личном кабинете</a><br /><br />С уважением, Maritime');
INSERT INTO `Message`
(`id`, `language`, `translation`)
VALUES ('67', 'en', 'Hello! <br /> <br /> Been successfully implemented an order to mailing your resume to our database companies. <br /> Your order number: {orderNumber} <br /> Order amount: {orderAmount} <br /> Further details you can view in your <a href="{accountLink}">personal account</a> <br /> <br /> Regards, Maritime');

-- Renamesome tables to camel case
RENAME TABLE  `orderstatus` TO  `orderStatus` ;
RENAME TABLE  `userorderedcompanies` TO  `userOrderedCompanies` ;
RENAME TABLE  `userfiles` TO  `userFiles` ;
RENAME TABLE  `useremailcontents` TO  `userEmailContents` ;
RENAME TABLE  `sourcemessage` TO  `SourceMessage` ;
RENAME TABLE  `message` TO  `Message` ;