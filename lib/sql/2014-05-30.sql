-- Change fields name
ALTER TABLE `userFiles`
CHANGE COLUMN `fileName` `name` VARCHAR(255) NOT NULL,
CHANGE COLUMN `fileType` `category` VARCHAR(100) NOT NULL;

-- Add AI to id
ALTER TABLE `userEmailContents`
CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT;

-- Create selected companies table
CREATE  TABLE `userOrderedCompanies`(
  `id` INT NOT NULL AUTO_INCREMENT,
  `user` INT NOT NULL,
  `companies` TEXT NOT NULL,
  PRIMARY KEY(`id`),
  INDEX `FKUserOrderedCompaniesUser_idx`(`user` ASC),
  CONSTRAINT `FKUserOrderedCompaniesUser`
    FOREIGN KEY(`user`)
    REFERENCES `users`(`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

