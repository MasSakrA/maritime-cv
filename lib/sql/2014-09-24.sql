UPDATE SourceMessage 
SET `category` = 'emailTemplateContent' 
WHERE `category` = 'htmlTemplate';

UPDATE SourceMessage 
SET `category` = 'emailTemplateName' 
WHERE `category` = 'emailTemplate';

UPDATE SourceMessage 
SET `message` = 'cv-default' 
WHERE `category` = 'emailTemplateName'
AND `message` = 'default';

UPDATE SourceMessage 
SET `message` = 'cv-exp' 
WHERE `category` = 'emailTemplateName'
AND `message` = 'exp';

-- Remove parameter
DELETE FROM parameters WHERE name = 'agreementFile';

ALTER TABLE emailtemplates
DROP COLUMN `name`,
DROP COLUMN `subject`,
CHANGE COLUMN `htmlTemplate` `name` VARCHAR(32) NOT NULL;

