ALTER TABLE `orders`
DROP COLUMN `transactionId`,
DROP COLUMN `paymentMethod`,
DROP COLUMN `paidAmount`,
DROP COLUMN `paymentDate`,
DROP COLUMN `status`;

CREATE  TABLE `payments` (
  `id` INT NOT NULL ,
  `amount` DECIMAL(10,2) NOT NULL DEFAULT '0.00',
  `ikTransaction` VARCHAR(100) NULL DEFAULT NULL,
  `method` VARCHAR(100) NULL DEFAULT NULL,
  `date` DATE NULL DEFAULT NULL,
  `status` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`));

ALTER TABLE `payments` ADD COLUMN `order` INT(10) NOT NULL  AFTER `id` , 
  ADD CONSTRAINT `fk_payment_order`
  FOREIGN KEY (`order` )
  REFERENCES `orders` (`id` )
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD INDEX `fk_payment_order_idx` (`order` ASC) ;
