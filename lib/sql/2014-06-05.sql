-- Add user foreign key

ALTER TABLE `useremailcontents` 
  ADD CONSTRAINT `fk_useremailcontents_user`
  FOREIGN KEY (`user` )
  REFERENCES `users` (`id` )
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `userfiles` 
  ADD CONSTRAINT `fk_userfiles_user`
  FOREIGN KEY (`user` )
  REFERENCES `users` (`id` )
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `userorderedcompanies` 
  ADD CONSTRAINT `fk_userorderedcompanies_user`
  FOREIGN KEY (`user` )
  REFERENCES `users` (`id` )
  ON DELETE CASCADE
  ON UPDATE CASCADE;

