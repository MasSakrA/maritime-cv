-- Create table for SOURCE internationalize messages
CREATE TABLE SourceMessage
(
    id INTEGER PRIMARY KEY,
    category VARCHAR(32),
    message TEXT
);
-- Create table for internationalize messages
CREATE TABLE Message
(
    id INTEGER,
    language VARCHAR(16),
    translation TEXT,
    PRIMARY KEY (id, language),
    CONSTRAINT FK_Message_SourceMessage FOREIGN KEY (id)
         REFERENCES SourceMessage (id) ON DELETE CASCADE ON UPDATE RESTRICT
);

-- Create work areas table
CREATE  TABLE `workAreas` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(100) NOT NULL ,
  PRIMARY KEY(`id`));
-- Insert work areas
INSERT INTO `workAreas` (`id`, `name`) VALUES (1, 'Dry cargo');
INSERT INTO `workAreas` (`id`, `name`) VALUES (2, 'Offshore');
INSERT INTO `workAreas` (`id`, `name`) VALUES (3, 'Tanker');
INSERT INTO `workAreas` (`id`, `name`) VALUES (4, 'Passenger');

-- Update compnay workarea names to ids
UPDATE `companyWorkAreas` SET `workArea`='1' WHERE `id`='1';
UPDATE `companyWorkAreas` SET `workArea`='1' WHERE `id`='5';
UPDATE `companyWorkAreas` SET `workArea`='2' WHERE `id`='6';
UPDATE `companyWorkAreas` SET `workArea`='3' WHERE `id`='7';
UPDATE `companyWorkAreas` SET `workArea`='1' WHERE `id`='8';
UPDATE `companyWorkAreas` SET `workArea`='3' WHERE `id`='9';
UPDATE `companyWorkAreas` SET `workArea`='1' WHERE `id`='10';
UPDATE `companyWorkAreas` SET `workArea`='4' WHERE `id`='11';
UPDATE `companyWorkAreas` SET `workArea`='2' WHERE `id`='12';

-- Change column type befor add foreign key
ALTER TABLE `companyWorkAreas` CHANGE COLUMN `workArea` `workArea` INT NOT NULL;

-- Add foreign work area key
ALTER TABLE `companyWorkAreas` 
  ADD CONSTRAINT `FKCompanyWorkAreasWorkArea`
  FOREIGN KEY (`workArea` )
  REFERENCES `workAreas` (`id` )
, ADD INDEX `FKCompanyWorkAreasWorkArea_idx` (`workArea` ASC);

-- Translations
INSERT INTO `SourceMessage` (`id`, `category`, `message`) VALUES
(1, 'word', 'All'),
(2, 'workArea', 'Dry cargo'),
(3, 'workArea', 'Offshore'),
(4, 'workArea', 'Tanker'),
(5, 'workArea', 'Passenger'),
(6, 'region', 'ee'),
(7, 'region', 'eu'),
(8, 'country', 'ru'),
(9, 'country', 'en'),
(10, 'country', 'ua'),
(11, 'word', 'Sector'),
(12, 'word', 'Region'),
(13, 'word', 'Country'),
(14, 'word', 'Company'),
(15, 'phrase', 'Previous Step'),
(16, 'phrase', 'Next Step'),
(17, 'phrase', 'Selecting crewing agencies'),
(18, 'pagePart', 'This is companies select page. Please, select companies that you are interested. You may use filters in left part of page or set checkboxes manually.'),
(19, 'phrase', 'Crewing Agencies'),
(20, 'word', 'Step');
INSERT INTO `Message` (`id`, `language`, `translation`) VALUES
(1, 'ru', 'Все'),
(2, 'ru', 'Сухогруз'),
(3, 'ru', 'Офшор'),
(4, 'ru', 'Танкер'),
(5, 'ru', 'Пассажирский'),
(6, 'en', 'East Europe'),
(6, 'ru', 'Восточная Европа'),
(7, 'en', 'Europe'),
(7, 'ru', 'Европа'),
(8, 'en', 'Russia'),
(8, 'ru', 'Россия'),
(9, 'en', 'England'),
(9, 'ru', 'Англия'),
(10, 'en', 'Ukraine'),
(10, 'ru', 'Украина'),
(11, 'ru', 'Сектор'),
(12, 'ru', 'Регион'),
(13, 'ru', 'Страна'),
(14, 'ru', 'Компания'),
(15, 'ru', 'Предыдущий шаг'),
(16, 'ru', 'Следующий шаг'),
(17, 'ru', 'Выбор крюинговых компаний'),
(18, 'ru', 'Это страница выбора компаний. Пожалуйста, выберите компаний, которые вас интересуют. Вы можете использовать фильтры в левой части страницы или отметить их вручную.'),
(19, 'ru', 'Агентства'),
(20, 'ru', 'Шаг');


