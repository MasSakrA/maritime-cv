-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 29, 2014 at 12:23 PM
-- Server version: 5.5.25
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `maritime-cv`
--

-- --------------------------------------------------------

--
-- Table structure for table `colorschemes`
--

CREATE TABLE IF NOT EXISTS `colorschemes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `scheme` text NOT NULL,
  `additionalCSS` text NOT NULL,
  `order` int(11) NOT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `colorschemes`
--

INSERT INTO `colorschemes` (`id`, `name`, `scheme`, `additionalCSS`, `order`, `inactive`) VALUES
(1, 'mz', '.cs-mz.email-content{font-family:Tahoma,Arail,sans-serif;color:#295e60;background:#effdff;background:-moz-linear-gradient(top,#effdff 0,#fcfeff 100%);background:-webkit-gradient(linear,left top,left bottom,color-stop(0%,#effdff),color-stop(100%,#fcfeff));background:-webkit-linear-gradient(top,#effdff 0,#fcfeff 100%);background:-o-linear-gradient(top,#effdff 0,#fcfeff 100%);background:-ms-linear-gradient(top,#effdff 0,#fcfeff 100%);background:linear-gradient(to bottom,#effdff 0,#fcfeff 100%);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=''#effdff'', endColorstr=''#fcfeff'', GradientType=0)}\r\n.cs-mz.email-content a{color:#0083ae}\r\n.cs-mz.email-content h1{font-size:26px;font-family:Georgia,serif;color:#0d1519;text-transform:none}\r\n.cs-mz.email-content h2{font-size:20px;font-family:Georgia,serif;color:#0d1519;letter-spacing:normal;text-transform:none}\r\n.cs-mz.email-content h3{font-size:17px;font-family:Georgia,serif;color:#0d1519}\r\n.cs-mz.email-content h4{font-size:18px;font-family:''Times New Roman'',serif;font-style:italic;letter-spacing:normal;color:#0d1519;text-transform:none}\r\n.cs-mz.email-content strong{color:#1c9490}', '.cs-mz.template-color .one{background: #effdff}\r\n.cs-mz.template-color .two{background: #0083ae}\r\n.cs-mz.template-color .three{background: #1c9490}\r\n.cs-mz.template-color .four{background: #295e60}', 2, 0),
(2, 'exp', '.cs-exp.email-content{font-family:''Segoe UI'',Tahoma,Arail,sans-serif;color:#666;background: #f5f5f5;\r\nbackground: -moz-linear-gradient(top,  #f5f5f5 0%, #ffffff 100%);\r\nbackground: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f5f5f5), color-stop(100%,#ffffff));\r\nbackground: -webkit-linear-gradient(top,  #f5f5f5 0%,#ffffff 100%);\r\nbackground: -o-linear-gradient(top,  #f5f5f5 0%,#ffffff 100%);\r\nbackground: -ms-linear-gradient(top,  #f5f5f5 0%,#ffffff 100%);\r\nbackground: linear-gradient(to bottom,  #f5f5f5 0%,#ffffff 100%);\r\nfilter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#f5f5f5'', endColorstr=''#ffffff'',GradientType=0);}\r\n.cs-exp.email-content a{color:#F56B28}\r\n.cs-exp.email-content h1{font-size:26px;font-family:''Segoe UI'',sens-serif;color:#0d1519;text-transform:none}\r\n.cs-exp.email-content h2{font-size:20px;font-family:''Segoe UI'',sens-serif;color:#0d1519;letter-spacing:normal;text-transform:none}\r\n.cs-exp.email-content h3{font-size:17px;font-family:''Segoe UI'',sens-serif;color:#0d1519}\r\n.cs-exp.email-content h4{font-size:18px;font-family:''Segoe UI'',sens-serif;font-style:italic;letter-spacing:normal;color:#0d1519;text-transform:none}\r\n.cs-exp.email-content strong{color:#0093be}', '.cs-exp.template-color .one { background: #DDD }\r\n.cs-exp.template-color .two { background: #F56B28 }\r\n.cs-exp.template-color .three { background: #0093be }\r\n.cs-exp.template-color .four { background: #666 }', 3, 0),
(3, 'eco', '.cs-eco.email-content{font-family:Arail,sans-serif;color:#44504C;background: #fcfcfc;\r\nbackground: -moz-linear-gradient(top,  #fcfcfc 0%, #f5f9ef 100%);\r\nbackground: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#fcfcfc), color-stop(100%,#f5f9ef));\r\nbackground: -webkit-linear-gradient(top,  #fcfcfc 0%,#f5f9ef 100%);\r\nbackground: -o-linear-gradient(top,  #fcfcfc 0%,#f5f9ef 100%);\r\nbackground: -ms-linear-gradient(top,  #fcfcfc 0%,#f5f9ef 100%);\r\nbackground: linear-gradient(to bottom,  #fcfcfc 0%,#f5f9ef 100%);\r\nfilter: progid:DXImageTransform.Microsoft.gradient( startColorstr=''#fcfcfc'', endColorstr=''#f5f9ef'',GradientType=0 );}\r\n.cs-eco.email-content a{color:#35A887}\r\n.cs-eco.email-content h1{font-size:26px;font-family:''Times New Roman'',Georgia,serif;color:#46892F;text-transform:none}\r\n.cs-eco.email-content h2{font-size:20px;font-family:''Times New Roman'',Georgia,serif;color:#0d1519;letter-spacing:normal;text-transform:none}\r\n.cs-eco.email-content h3{font-size:17px;font-family:Georgia,serif;color:#0d1519}\r\n.cs-eco.email-content h4{font-size:18px;font-family:''Times New Roman'',serif;font-style:italic;letter-spacing:normal;color:#0d1519;text-transform:none}\r\n.cs-eco.email-content strong{color:#46892F}', '.cs-eco.template-color .one{background: #EEE}\r\n.cs-eco.template-color .two{background: #35A887}\r\n.cs-eco.template-color .three{background: #46892F}\r\n.cs-eco.template-color .four{background: #44504C}', 4, 0),
(4, 'default', '', '.cs-default.template-color .one{background: #effdff}\r\n.cs-default.template-color .two{background: #0083ae}\r\n.cs-default.template-color .three{background: #1c9490}\r\n.cs-default.template-color .four{background: #657073}', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE IF NOT EXISTS `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `name`) VALUES
(1, 'A-Crew Management International'),
(2, 'A1 Lighghouse of the Black Sea'),
(3, 'Baltik Maritime Consulting OU'),
(4, 'Caravella Odessa'),
(5, 'AG Maritime company'),
(6, 'Adriatico Mariupol'),
(7, 'Baltic Sea Management'),
(8, 'Bibby Ship Management');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `country` char(2) NOT NULL,
  `region` char(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`country`, `region`) VALUES
('ru', 'ee'),
('en', 'eu'),
('lv', 'ba'),
('lv', 'eu'),
('lv', 'ee'),
('se', 'sk'),
('', 'we');

-- --------------------------------------------------------

--
-- Table structure for table `emailtemplates`
--

CREATE TABLE IF NOT EXISTS `emailtemplates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `htmlTemplate` varchar(32) NOT NULL,
  `type` varchar(2) NOT NULL,
  `order` int(11) NOT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `emailtemplates`
--

INSERT INTO `emailtemplates` (`id`, `name`, `htmlTemplate`, `type`, `order`, `inactive`) VALUES
(1, 'default', 'cv-default', 'cv', 1, 0),
(2, 'exp', 'cv-exp', 'cv', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `id` int(11) NOT NULL DEFAULT '0',
  `language` varchar(16) NOT NULL DEFAULT '',
  `translation` text,
  PRIMARY KEY (`id`,`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `language`, `translation`) VALUES
(1, 'ru', 'Все'),
(2, 'en', 'Dry Cargo'),
(2, 'ru', 'Сухогрузы'),
(3, 'en', 'Offshore'),
(3, 'ru', 'Оффшор'),
(4, 'en', 'Tankers'),
(4, 'ru', 'Танкера'),
(5, 'en', 'Passengers'),
(5, 'ru', 'Пассажирский флот'),
(6, 'en', 'East Europe'),
(6, 'ru', 'Восточная Европа'),
(7, 'en', 'European Union'),
(7, 'ru', 'Европейский Союз'),
(8, 'en', 'Russia'),
(8, 'ru', 'Россия'),
(9, 'en', 'England'),
(9, 'ru', 'Англия'),
(10, 'en', 'Ukraine'),
(10, 'ru', 'Украина'),
(11, 'en', 'Sector|Sectors'),
(11, 'ru', 'Сектор|Сектора|Секторов|Сектора'),
(12, 'en', 'Region|Regions'),
(12, 'ru', 'Регион|Региона|Регионов|Регионы'),
(13, 'en', 'Country|Countries'),
(13, 'ru', 'Страна|Страны|Стран|Страны'),
(14, 'en', 'Company|Companies'),
(14, 'ru', 'Компания|Компании|Компаний|Компании'),
(15, 'ru', 'Предыдущий шаг'),
(16, 'ru', 'Следующий шаг'),
(17, 'ru', 'Выбор крюинговых компаний'),
(18, 'ru', 'Это страница выбора компаний. Пожалуйста, выберите компаний, которые вас интересуют. Вы можете использовать фильтры в левой части страницы или отметить их вручную.'),
(19, 'ru', 'Агентства'),
(20, 'en', 'Step|Steps'),
(20, 'ru', 'Шаг|Шага|Шагов|Шаги'),
(21, 'en', 'CV and Attachments'),
(21, 'ru', 'Анкета и Документы'),
(22, 'en', 'Design and Content'),
(22, 'ru', 'Оформление и Тексты'),
(23, 'en', 'Crewing Companies'),
(23, 'ru', 'Выбор компаний'),
(24, 'en', 'Order Payment'),
(24, 'ru', 'Оплата услуги'),
(25, 'en', 'Email Campaign'),
(25, 'ru', 'Рассылка писем'),
(26, 'en', 'Template|Templates'),
(26, 'ru', 'Шаблон|Шаблона|Шаблонов|Шаблоны'),
(27, 'en', 'Design Scheme'),
(27, 'ru', 'Оформление'),
(28, 'en', 'Email Subject'),
(28, 'ru', 'Тема письма'),
(29, 'en', 'Email Content'),
(29, 'ru', 'Текст письма'),
(30, 'en', 'How Our System Works'),
(30, 'ru', 'Как работает сервис Maritime CV?'),
(31, 'en', 'Norway'),
(31, 'ru', 'Норвегия'),
(32, 'en', 'Georgia'),
(32, 'ru', 'Грузия'),
(33, 'en', 'Latvia'),
(33, 'ru', 'Латвия'),
(34, 'en', 'Baltic Countries'),
(34, 'ru', 'Прибалтика'),
(35, 'en', 'Marine'),
(35, 'ru', 'Море'),
(36, 'en', 'Default Text'),
(36, 'ru', 'Стандартный текст'),
(37, 'en', '<h1>Header 1</h1>\r\n<h2>Header 2</h2>\r\n<h3>Header 3</h3>\r\n<h4>Header 4</h4>\r\n<h5>Header 5</h5>\r\n<h6>Header 6</h6>\r\n<p>Donec quis pellentesque mi, ac consequat nibh. Ut lacinia est ac augue tincidunt ultricies. Proin ac erat ac diam dapibus gravida. Proin sodales tellus vitae nisi volutpat fermentum. Phasellus facilisis quam vel faucibus placerat. Proin egestas nibh lectus, <strong>aliquam dapibus dolor</strong> ultricies vitae. Cras nec varius lectus.</p>\r\n<p>Suspendisse malesuada ut purus vel feugiat. Aenean faucibus nulla nec ligula tempus, eget varius nulla dapibus. Nam ultrices sem elit, ac iaculis nulla adipiscing eget. Vestibulum non nibh congue, elementum turpis et, feugiat massa. Fusce magna lectus, eleifend ac ante sit amet, feugiat euismod felis. Ut sit amet eros quis quam placerat porta. Morbi a nisi arcu.</p>\r\n<a href="http://aaa.aa">aaa@aa.aa</a>'),
(37, 'ru', 'Добрый день!\nЯ хочу отправить вам своё резюме!\n\nС уважением,\nморяк'),
(38, 'en', 'Good day sirs!\r\n\r\nI''m a well-experienced seaman who wants to work with your company. You offer the greatest conditions.\r\n\r\nRegards,\r\nseaman.'),
(38, 'ru', 'Добрый день!\r\nЯ моряк, прослужил на флоте более 20 лет.\r\nХочу работать именно в вашей компании, т.к. вы можете предложить мне лучшие условия работы и оплату труда.\r\n\r\nС уважением,\r\nморяк.'),
(39, 'en', 'Experience'),
(39, 'ru', 'Для опытных моряков'),
(40, 'en', 'Default'),
(40, 'ru', 'Стандарт'),
(41, 'en', 'Ecology'),
(41, 'ru', 'Трава'),
(42, 'en', 'Exposition'),
(42, 'ru', 'Экспозиция'),
(43, 'en', 'Click to upload'),
(43, 'ru', 'Прикрепить'),
(44, 'en', 'Your CV'),
(44, 'ru', 'Анкету'),
(45, 'en', 'File 1'),
(45, 'ru', 'Вложение 1'),
(46, 'en', 'File 2'),
(46, 'ru', 'Вложение 2'),
(47, 'en', 'Your Email'),
(47, 'ru', 'Ваш Email'),
(48, 'en', 'Continue'),
(48, 'ru', 'Продолжить'),
(49, 'en', 'Allowed file types'),
(49, 'ru', 'Допустимые типы файлов'),
(50, 'en', 'Attach your CV'),
(50, 'ru', 'Прикрепите анкету моряка'),
(51, 'en', '<p class="lead">Follow These Steps</p>\r\n<ol>\r\n<li>Upload your CV</li>\r\n<li>Upload attachments</li>\r\n<li>Enter your email</li>\r\n<li>Click the button</li>\r\n</ol>\r\n<p class="text-justify">Praesent ut dignissim orci, a congue diam. Maecenas sit amet est tellus. Morbi vitae malesuada purus. Duis consectetur urna congue elit scelerisque, vel tincidunt leo commodo. Aliquam cursus sit amet sem et pharetra.</p>'),
(51, 'ru', '<p class="lead">Форма загрузки ваших документов</p>\r\n<p>Воспользуйтесь формой для загрузки анкеты моряка и других необходимых документов. </p>\r\n\r\n<p>\r\n<ol>\r\n<li>Загрузите анкету моряка</li>\r\n<li>Загрузите другие документы (вложения)</li>\r\n<li>Введите свой email</li>\r\n<li>Нажмите на кнопку "Продолжить"</li>\r\n</ol>\r\n\r\n<hr />\r\n\r\n<p>Допустимые типы файлов анкеты моряка и вложений:<br />doc, docx, xls, xlsx, rtf, pdf, jpg, zip, rar.<br />\r\nМаксимальный размер анкеты составляет 1 МБ.<br />\r\nМаксимальный размер каждого из вложений составляет 500 Кб.</p>'),
(52, 'en', 'Send your CV right now'),
(52, 'ru', 'Начните рассылку своей анкеты прямо сейчас'),
(53, 'en', 'Why Maritime CV?'),
(53, 'ru', 'Почему стоит воспользоваться нашим сервисом?'),
(54, 'en', '<p class="lead">Benefits of Maritime CV</p>'),
(54, 'ru', '			<p class="lead">Преимущества сервиса Maritime CV</p>\r\n			<ul>\r\n				<li><span>Только проверенные email-адреса компаний</span></li> \r\n				<li><span>Рассылка по региону</span></li> \r\n				<li><span>Рассылка по сектору судоходства</span></li> \r\n				<li><span>Редактирование списка компаний-получателей</span></li> \r\n				<li><span>Множество вариантов оформления письма</span></li> \r\n				<li><span>Отчет о выполнении услуги</span></li> \r\n			</ul>'),
(55, 'en', '<p class="lead">We accept different payment systems</p>'),
(55, 'ru', '			<div class="padding" id="payment-systems">\r\n				<p class="lead">Мы работаем со многими платёжными системами</p>\r\n				\r\n				<div>\r\n					<div class="col-md-3">\r\n						<div class="payment-system">\r\n							<img src="lib/skin/img/wallet-one.png" title="Wallet One" alt="Wallet One" />\r\n							<span>Wallet<br />One</span>\r\n							<div class="clearfix"></div>\r\n						</div>\r\n					</div>\r\n					\r\n					<div class="col-md-3">\r\n						<div class="payment-system">\r\n							<img src="lib/skin/img/megaphone.png" title="Мегафон" alt="Мегафон" />\r\n							<span>Мегафон</span>\r\n							<div class="clearfix"></div>\r\n						</div>\r\n					</div>\r\n					\r\n					<div class="col-md-3">\r\n						<div class="payment-system">\r\n							<img src="lib/skin/img/telemoney.png" title="TeleMoney" alt="TeleMoney" />\r\n							<span>TeleMoney</span>\r\n							<div class="clearfix"></div>\r\n						</div>\r\n					</div>\r\n					\r\n					<div class="col-md-3">\r\n						<div class="payment-system">\r\n							<img src="lib/skin/img/terminals.png" title="Терминалы России" alt="Терминалы России" />\r\n							<span>Терминалы<br />России</span>\r\n							<div class="clearfix"></div>\r\n						</div>\r\n					</div>\r\n				</div>\r\n				<div>\r\n					<div class="col-md-3">\r\n						<div class="payment-system">\r\n							<img src="lib/skin/img/perfect-money.png" title="Perfect Money" alt="Perfect Money" />\r\n							<span>Perfect<br />Money</span>\r\n							<div class="clearfix"></div>\r\n						</div>\r\n					</div>\r\n					\r\n					<div class="col-md-3">\r\n						<div class="payment-system">\r\n							<img src="lib/skin/img/paxum.png" title="Paxum" alt="Paxum" />\r\n							<span>Paxum</span>\r\n							<div class="clearfix"></div>\r\n						</div>\r\n					</div>\r\n					\r\n					<div class="col-md-3">\r\n						<div class="payment-system">\r\n							<img src="lib/skin/img/mts.png" title="МТС" alt="МТС" />\r\n							<span>МТС</span>\r\n							<div class="clearfix"></div>\r\n						</div>\r\n					</div>\r\n					\r\n					<div class="col-md-3">\r\n						<div class="payment-system">\r\n							<img src="lib/skin/img/terminals.png" title="Терминалы Украины" alt="Терминалы Украины" />\r\n							<span>Терминалы<br />Украины</span>\r\n							<div class="clearfix"></div>\r\n						</div>\r\n					</div>\r\n				</div> \r\n				\r\n			</div>'),
(56, 'en', 'Contact Us'),
(56, 'ru', 'Обратная связь'),
(57, 'en', 'Enter the Symbols'),
(57, 'ru', 'Антиспам'),
(58, 'en', 'Phone Number (optional)'),
(58, 'ru', 'Номер телефона (опционально)'),
(59, 'en', 'Email'),
(59, 'ru', 'Email'),
(60, 'en', 'First Name / Last Name'),
(60, 'ru', 'Ваше имя'),
(61, 'en', 'Message|Messages'),
(61, 'ru', 'Сообщение|Сообщения|Сообщений|Сообщения'),
(62, 'en', 'Send Message'),
(62, 'ru', 'Отправить сообщение'),
(63, 'en', 'Click to Refresh'),
(63, 'ru', 'Нажмите для обновления'),
(64, 'en', 'More Information'),
(64, 'ru', 'Подробнее'),
(65, 'en', '<p>Instead of many other services we provide you with ... </p>'),
(65, 'ru', '<p class="text-justify">В отличие от других подобных сервисов помимо анкеты моряка вы можете прикреплять к письму любые документы, а общий размер вложений может составлять до 2 МБ. Наш сервис подскажет и поможет вам подготовить индивидуально оформленное письмо с логично составленным текстом и презентабельным внешним видом. Расширенная система фильтров по регионам мира и секторам судоходства с лёгкостью позволит вам выбрать лишь нужных получателей из нашей огромной базы крюинговых компаний. Мы работаем со множеством платёжных систем, поэтому вы без труда сможете оплатить наши услуги удобным для вас способом. Также вы имеете возможность следить за статусом выполнения услуги, а в конце получить подробный отчёт о её выполнении.</p>'),
(66, 'en', '<p class="lead">If you would like </p>'),
(66, 'ru', '<p class="lead">Если вы хотите быстро встать на учёт во множестве различных крюинговых компаний, разослать свою анкету работодателям нужного вам сектора судоходства и региона или даже точечно разослать свои документы в нужные агентства, то этот сервис именно для вас!</p>');

-- --------------------------------------------------------

--
-- Table structure for table `offices`
--

CREATE TABLE IF NOT EXISTS `offices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `country` varchar(2) DEFAULT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FKCompanyGeographicAreasCompanyIndex` (`company`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `offices`
--

INSERT INTO `offices` (`id`, `company`, `name`, `email`, `country`, `inactive`) VALUES
(1, 1, NULL, 'email1@m.ru', 'ua', 0),
(2, 2, NULL, 'email2@m.ru', 'ua', 0),
(3, 3, NULL, 'email3@m.ru', 'ru', 0),
(4, 4, NULL, 'email4@m.ru', 'no', 0),
(5, 5, NULL, 'email5@m.ru', 'ru', 0),
(6, 6, NULL, 'email6@m.ru', 'lv', 0),
(7, 7, NULL, 'email7@m.ru', 'lv', 0),
(8, 8, NULL, 'email8@m.ru', 'ge', 1);

-- --------------------------------------------------------

--
-- Table structure for table `officeworkareas`
--

CREATE TABLE IF NOT EXISTS `officeworkareas` (
  `office` int(11) NOT NULL,
  `workArea` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `officeworkareas`
--

INSERT INTO `officeworkareas` (`office`, `workArea`) VALUES
(1, 'dc'),
(2, 'os'),
(3, 'tk'),
(4, 'ps'),
(5, 'tk'),
(7, 'ps'),
(8, 'os'),
(2, 'ps'),
(5, 'dc');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `number` varchar(100) NOT NULL,
  `creationDate` date NOT NULL,
  `expireDate` date DEFAULT NULL,
  `amount` decimal(10,2) NOT NULL,
  `currency` char(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_orders_user_idx` (`user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user`, `number`, `creationDate`, `expireDate`, `amount`, `currency`) VALUES
(1, 1, '2651402928607', '2014-06-16', NULL, '0.60', 'UAH');

-- --------------------------------------------------------

--
-- Table structure for table `orderstatus`
--

CREATE TABLE IF NOT EXISTS `orderstatus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order` int(11) NOT NULL,
  `companiesMailed` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `success` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_orderStatus_order_idx` (`order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
  `id` int(11) NOT NULL,
  `order` int(10) NOT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `ikTransaction` varchar(100) DEFAULT NULL,
  `method` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_payment_order_idx` (`order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sourcemessage`
--

CREATE TABLE IF NOT EXISTS `sourcemessage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(32) DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=67 ;

--
-- Dumping data for table `sourcemessage`
--

INSERT INTO `sourcemessage` (`id`, `category`, `message`) VALUES
(0, 'phrase', 'CV and Attachments'),
(1, 'word', 'All'),
(2, 'workArea', 'dc'),
(3, 'workArea', 'os'),
(4, 'workArea', 'tk'),
(5, 'workArea', 'ps'),
(6, 'region', 'ee'),
(7, 'region', 'eu'),
(8, 'country', 'ru'),
(9, 'country', 'en'),
(10, 'country', 'ua'),
(11, 'word', 'Sector'),
(12, 'word', 'Region'),
(13, 'word', 'Country'),
(14, 'word', 'Company'),
(15, 'phrase', 'Previous Step'),
(16, 'phrase', 'Next Step'),
(17, 'phrase', 'Selecting crewing agencies'),
(18, 'pagePart', 'This is companies select page. Please, select companies that you are interested. You may use filters in left part of page or set checkboxes manually.'),
(19, 'phrase', 'Crewing Agencies'),
(20, 'word', 'Step'),
(21, 'phrase', 'CV and Attachments'),
(22, 'phrase', 'Design and Content'),
(23, 'phrase', 'Crewing Companies'),
(24, 'phrase', 'Order Payment'),
(25, 'phrase', 'Email Campaign'),
(26, 'word', 'Template'),
(27, 'phrase', 'Color Scheme'),
(28, 'phrase', 'Email Subject'),
(29, 'phrase', 'Email Content'),
(30, 'phrase', 'How It Works'),
(31, 'country', 'no'),
(32, 'country', 'ge'),
(33, 'country', 'lv'),
(34, 'region', 'ba'),
(35, 'colorScheme', 'mz'),
(36, 'emailTemplate', 'default'),
(37, 'htmlTemplate', 'cv-default'),
(38, 'htmlTemplate', 'cv-exp'),
(39, 'emailTemplate', 'exp'),
(40, 'colorScheme', 'default'),
(41, 'colorScheme', 'eco'),
(42, 'colorScheme', 'exp'),
(43, 'phrase', 'Click to upload'),
(44, 'phrase', 'Your CV'),
(45, 'phrase', 'File 1'),
(46, 'phrase', 'File 2'),
(47, 'phrase', 'Your Email'),
(48, 'phrase', 'Continue'),
(49, 'phrase', 'Allowed file types'),
(50, 'phrase', 'Attach your CV'),
(51, 'pagePart', 'File Upload Explanation'),
(52, 'phrase', 'Top Section Header'),
(53, 'phrase', 'Middle Section Header'),
(54, 'pagePart', 'Middle Section Left'),
(55, 'pagePart', 'Middle Section Right'),
(56, 'phrase', 'Contact Us'),
(57, 'phrase', 'Enter the Symbols'),
(58, 'phrase', 'Phone Number'),
(59, 'word', 'Email'),
(60, 'phrase', 'Name and Surname'),
(61, 'word', 'Message'),
(62, 'phrase', 'Send Message'),
(63, 'phrase', 'Click to Refresh'),
(64, 'phrase', 'More Info'),
(65, 'pagePart', 'Middle Section'),
(66, 'pagePart', 'Top Section');

-- --------------------------------------------------------

--
-- Table structure for table `useremailcontents`
--

CREATE TABLE IF NOT EXISTS `useremailcontents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `body` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKUserEmailContentsUserIndex` (`user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `useremailcontents`
--

INSERT INTO `useremailcontents` (`id`, `user`, `subject`, `body`) VALUES
(1, 1, 'Subject', '\r\n				Добрый день!\r\nЯ моряк, прослужил на флоте более 20 лет.\r\nХочу работать именно в вашей компании, т.к. вы можете предложить мне лучшие условия работы и оплату труда.\r\n\r\nС уважением,\r\nморяк.\r\n			');

-- --------------------------------------------------------

--
-- Table structure for table `userfiles`
--

CREATE TABLE IF NOT EXISTS `userfiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `category` varchar(100) NOT NULL,
  `pseudonym` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKUserFilesUserIndex` (`user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `userfiles`
--

INSERT INTO `userfiles` (`id`, `user`, `name`, `category`, `pseudonym`) VALUES
(1, 1, '2.jpg', 'CVFile', 'Резюме');

-- --------------------------------------------------------

--
-- Table structure for table `userorderedcompanies`
--

CREATE TABLE IF NOT EXISTS `userorderedcompanies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `companies` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKUserOrderedCompaniesUser_idx` (`user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `userorderedcompanies`
--

INSERT INTO `userorderedcompanies` (`id`, `user`, `companies`) VALUES
(1, 1, '6,1,2,3,4,5');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identificator` varchar(40) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`,`identificator`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `identificator`, `email`) VALUES
(1, 'i10qvi8ee9cctmfh672tc6sp32', 'k@m.o');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `FK_Message_SourceMessage` FOREIGN KEY (`id`) REFERENCES `sourcemessage` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `offices`
--
ALTER TABLE `offices`
  ADD CONSTRAINT `FKCompanyGeographicAreasCompany` FOREIGN KEY (`company`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `fk_orders_user` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orderstatus`
--
ALTER TABLE `orderstatus`
  ADD CONSTRAINT `fk_orderStatus_order` FOREIGN KEY (`order`) REFERENCES `orders` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `fk_payment_order` FOREIGN KEY (`order`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `useremailcontents`
--
ALTER TABLE `useremailcontents`
  ADD CONSTRAINT `fk_useremailcontents_user` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `userfiles`
--
ALTER TABLE `userfiles`
  ADD CONSTRAINT `fk_userfiles_user` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `userorderedcompanies`
--
ALTER TABLE `userorderedcompanies`
  ADD CONSTRAINT `fk_userorderedcompanies_user` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
