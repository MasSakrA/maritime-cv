ALTER TABLE `offices` 
CHANGE COLUMN `name` `name` VARCHAR(255) NULL DEFAULT '';

ALTER TABLE `orders` 
DROP COLUMN `expireDate`;