-- Add the default subject field
ALTER TABLE `emailtemplates` 
ADD COLUMN `subject` VARCHAR(255) NULL DEFAULT NULL AFTER `htmlTemplate`;

-- Set the default subject for the user notification about his order.
UPDATE `emailtemplates` SET `subject`='Order details' WHERE `id`='3';
