ALTER TABLE `officeworkareas` 
ADD INDEX `fk_officeworkareas_office_idx` (`office` ASC);
ALTER TABLE `officeworkareas` 
ADD CONSTRAINT `fk_officeworkareas_office`
  FOREIGN KEY (`office`)
  REFERENCES `offices` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

CREATE TABLE IF NOT EXISTS `emailQueue` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `queueId` INT NOT NULL DEFAULT 0,
  `emailTemplate` INT NULL DEFAULT NULL,
  `address` VARCHAR(255) NOT NULL,
  `datetimeIn` DATETIME NOT NULL,
  `datetimeOut` DATETIME NULL,
  `inQueue` TINYINT(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  INDEX `fk_emailQueue_emailTemplate_idx` (`emailTemplate` ASC),
  INDEX `fk_emailQueue_queueId_idx` (`queueId` ASC),
  CONSTRAINT `fk_emailQueue_emailTemplate`
    FOREIGN KEY (`emailTemplate`)
    REFERENCES `emailtemplates` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = cp1250
COLLATE = cp1250_general_ci;

CREATE TABLE IF NOT EXISTS `emailQueueParams` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `emailsQueueId` INT NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `value` TEXT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_emailQueueParams_email_idx` (`emailsQueueId` ASC),
  CONSTRAINT `fk_emailQueueParams_email`
    FOREIGN KEY (`emailsQueueId`)
    REFERENCES `emailQueue` (`queueId`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = cp1250
COLLATE = cp1250_general_ci;