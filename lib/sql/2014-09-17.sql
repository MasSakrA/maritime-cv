-- Remove region field and change default value for country
ALTER TABLE `offices` 
DROP COLUMN `region`,
CHANGE COLUMN `country` `country` VARCHAR(2) NULL DEFAULT '';
-- Remove foreign keys from not used table
ALTER TABLE `orderStatus` 
DROP FOREIGN KEY `fk_orderStatus_order`,
DROP FOREIGN KEY `fk_orderStatus_company`;

CREATE TABLE `officeregions` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `office` INT NOT NULL,
  `region` VARCHAR(2) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_officeregions_office_idx` (`office` ASC),
  CONSTRAINT `fk_officeregions_office`
    FOREIGN KEY (`office`)
    REFERENCES `maritime-cv`.`offices` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

