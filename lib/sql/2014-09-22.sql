-- Add more parameters
INSERT INTO `parameters` (`category`, `name`, `value`) VALUES
('price', 'minimum', '10');

-- Use USD as default currency
ALTER TABLE `orders` 
CHANGE COLUMN `currency` `currency` CHAR(3) NULL DEFAULT 'USD' ;
