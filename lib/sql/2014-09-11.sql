CREATE TABLE `parameters` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `category` VARCHAR(50) NULL,
  `name` VARCHAR(100) NOT NULL,
  `value` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`));

INSERT INTO `parameters` VALUES
(1,'admin','email','ex@mp.le'),
(2,'email','smtp','1'),
(3,'email','smtpAuth','1'),
(4,'email','smtpSecure','ssl'),
(5,'email','host','smtp.gmail.com'),
(6,'email','port','465'),
(7,'email','user','laspiairlines@gmail.com'),
(8,'email','password','S1332jJK'),
(9,'email','charset','UTF-8'),
(10,'interkassa','shopId','538ec854bf4efc6719507933'),
(11,'interkassa','signKey','IcqdLHLZtg8rR8Gf'),
(12,'queue','sendingsPerHour','5'),
(13,'queue','importantSendingsPerHour','2'),
(14,'admin','login','admin'),
(15,'admin','password','!P@Ssw0rD#'),
(16,'user','accountTimelife','30'),
(17,'user','agreementFile','license.txt'),
(18,'adminPanle','rowsPerPage','10'),
(19,'site','contactEmailSubject','Contact Email');
