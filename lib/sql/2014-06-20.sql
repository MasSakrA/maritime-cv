CREATE  TABLE `orderStatus` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `companiesMailed` INT NOT NULL ,
  `date` DATE NULL ,
  `success` TINYINT(1) NULL ,
  PRIMARY KEY (`id`) );

ALTER TABLE `orderStatus` ADD COLUMN `order` INT NOT NULL  AFTER `id` , 
  ADD CONSTRAINT `fk_orderStatus_order`
  FOREIGN KEY (`order` )
  REFERENCES `maritime-cv`.`orders` (`id` )
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
, ADD INDEX `fk_orderStatus_order_idx` (`order` ASC) ;

