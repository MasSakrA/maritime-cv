CREATE  TABLE `orders` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `user` INT NOT NULL ,
  `number` VARCHAR(100) NOT NULL ,
  `creationDate` DATE NOT NULL ,
  `expireDate` DATE NULL ,
  `paymentDate` DATE NULL ,
  `amount` DECIMAL(10,2) NOT NULL ,
  `paidAmount` DECIMAL(10,2) NOT NULL DEFAULT 0.00 ,
  `currency` CHAR(3) NULL ,
  `paymentMethod` VARCHAR(100) NULL ,
  `transactionId` VARCHAR(100) NULL ,
  `status` VARCHAR(45) NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_orders_user_idx` (`user` ASC) ,
  CONSTRAINT `fk_orders_user`
    FOREIGN KEY (`user` )
    REFERENCES `users` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE);
