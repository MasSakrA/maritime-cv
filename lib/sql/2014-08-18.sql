-- Change the companies count to a company name and date to datetime
ALTER TABLE `orderStatus` 
CHANGE COLUMN `companiesMailed` `company` INT(11) NOT NULL,
CHANGE COLUMN `date` `datetime` DATETIME NULL DEFAULT NULL,

-- Add foreign keyfor a company
ADD INDEX `fk_orderStatus_company_idx` (`company` ASC);
ALTER TABLE `orderStatus` 
ADD CONSTRAINT `fk_orderStatus_company`
  FOREIGN KEY (`company`)
  REFERENCES `companies` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;
