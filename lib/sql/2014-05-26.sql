-- phpMyAdmin SQL Dump
-- version 3.4.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 26, 2014 at 11:51 PM
-- Server version: 5.5.37
-- PHP Version: 5.5.3-1ubuntu2.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `maritime-cv`
--

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE IF NOT EXISTS `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `companyGeographicAreas`
--

CREATE TABLE IF NOT EXISTS `companyGeographicAreas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company` int(11) NOT NULL,
  `name` VARCHAR(255) NULL DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `country` int(11) DEFAULT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FKCompanyGeographicAreasCompanyIndex` (`company`),
  KEY `FKCompanyGeographicAreasCountryIndex` (`country`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `companyWorkAreas`
--

CREATE TABLE IF NOT EXISTS `companyWorkAreas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyArea` int(11) NOT NULL,
  `workArea` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKCompanyWorkAreasGeographicAreaIndex` (`companyArea`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country` char(2) NOT NULL,
  `region` char(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `userEmailContents`
--

CREATE TABLE IF NOT EXISTS `userEmailContents` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `body` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKUserEmailContentsUserIndex` (`user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `userFiles`
--

CREATE TABLE IF NOT EXISTS `userFiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `fileName` varchar(255) NOT NULL,
  `fileType` varchar(100) NOT NULL,
  `pseudonym` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKUserFilesUserIndex` (`user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identificator` varchar(40) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`,`identificator`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `companyGeographicAreas`
--
ALTER TABLE `companyGeographicAreas`
  ADD CONSTRAINT `FKCompanyGeographicAreasCountry` FOREIGN KEY (`country`) REFERENCES `countries` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FKCompanyGeographicAreasCompany` FOREIGN KEY (`company`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `companyWorkAreas`
--
ALTER TABLE `companyWorkAreas`
  ADD CONSTRAINT `FKCompanyWorkAreasGeographicArea` FOREIGN KEY (`companyArea`) REFERENCES `companyGeographicAreas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `userEmailContents`
--
ALTER TABLE `userEmailContents`
  ADD CONSTRAINT `FKUserEmailContentsUser` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `userFiles`
--
ALTER TABLE `userFiles`
  ADD CONSTRAINT `FKUserFilesUser` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


-- Fill companies
INSERT INTO `companies` (`id`, `name`) VALUES (NULL, 'A-Crew Management International');
INSERT INTO `companies` (`name`) VALUES ('A1 Lighghouse of the Black Sea');
INSERT INTO `companies` (`name`) VALUES ('Baltik Maritime Consulting OU');
INSERT INTO `companies` (`name`) VALUES ('Caravella Odessa');
INSERT INTO `companies` (`name`) VALUES ('AG Maritime company');
INSERT INTO `companies` (`name`) VALUES ('Adriatico Mariupol');
INSERT INTO `companies` (`name`) VALUES ('Baltic Sea Management');
INSERT INTO `companies` (`name`) VALUES ('Bibby Ship Management');

-- Fill countries
INSERT INTO `countries` (`country`, `region`) VALUES ('ru', 'ee');
INSERT INTO `countries` (`country`, `region`) VALUES ('en', 'eu');
INSERT INTO `countries` (`country`, `region`) VALUES ('ua', 'eu');

-- Fill companies geographic areas
INSERT INTO `companyGeographicAreas` (`company`, `email`, `country`, `inactive`) VALUES ('1', 'email1@m.ru', '1', '0');
INSERT INTO `companyGeographicAreas` (`company`, `email`, `country`) VALUES ('2', 'email2@m.ru', '2');
INSERT INTO `companyGeographicAreas` (`company`, `email`, `country`) VALUES ('3', 'email3@m.ru', '1');
INSERT INTO `companyGeographicAreas` (`company`, `email`, `country`) VALUES ('4', 'email4@m.ru', '2');
INSERT INTO `companyGeographicAreas` (`company`, `email`, `country`) VALUES ('5', 'email5@m.ru', '2');
INSERT INTO `companyGeographicAreas` (`company`, `email`, `country`) VALUES ('6', 'email6@m.ru', '3');
INSERT INTO `companyGeographicAreas` (`company`, `email`, `country`) VALUES ('7', 'email7@m.ru', '2');
INSERT INTO `companyGeographicAreas` (`company`, `email`, `country`) VALUES ('8', 'email8@m.ru', '2');

-- Fill companies work areas
INSERT INTO `companyWorkAreas` (`companyArea`, `workArea`) VALUES ('1', 'dryСargo');
INSERT INTO `companyWorkAreas` (`companyArea`, `workArea`) VALUES ('2', 'offshore');
INSERT INTO `companyWorkAreas` (`companyArea`, `workArea`) VALUES ('3', 'tanker');
INSERT INTO `companyWorkAreas` (`companyArea`, `workArea`) VALUES ('4', 'dryСargo');
INSERT INTO `companyWorkAreas` (`companyArea`, `workArea`) VALUES ('5', 'tanker');
INSERT INTO `companyWorkAreas` (`companyArea`, `workArea`) VALUES ('6', 'dryСargo');
INSERT INTO `companyWorkAreas` (`companyArea`, `workArea`) VALUES ('7', 'passenger');
INSERT INTO `companyWorkAreas` (`companyArea`, `workArea`) VALUES ('8', 'offshore');

