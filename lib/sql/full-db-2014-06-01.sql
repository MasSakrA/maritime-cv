-- phpMyAdmin SQL Dump
-- version 3.4.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 01, 2014 at 10:55 PM
-- Server version: 5.5.37
-- PHP Version: 5.5.3-1ubuntu2.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `maritime-cv`
--

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE IF NOT EXISTS `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `name`) VALUES
(1, 'A-Crew Management International'),
(2, 'A1 Lighghouse of the Black Sea'),
(3, 'Baltik Maritime Consulting OU'),
(4, 'Caravella Odessa'),
(5, 'AG Maritime company'),
(6, 'Adriatico Mariupol'),
(7, 'Baltic Sea Management'),
(8, 'Bibby Ship Management');

-- --------------------------------------------------------

--
-- Table structure for table `companyGeographicAreas`
--

CREATE TABLE IF NOT EXISTS `companyGeographicAreas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `country` int(11) DEFAULT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FKCompanyGeographicAreasCompanyIndex` (`company`),
  KEY `FKCompanyGeographicAreasCountryIndex` (`country`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `companyGeographicAreas`
--

INSERT INTO `companyGeographicAreas` (`id`, `company`, `name`, `email`, `country`, `inactive`) VALUES
(1, 1, NULL, 'email1@m.ru', 1, 0),
(2, 2, NULL, 'email2@m.ru', 2, 0),
(3, 3, NULL, 'email3@m.ru', 1, 0),
(4, 4, NULL, 'email4@m.ru', 2, 0),
(5, 5, NULL, 'email5@m.ru', 2, 0),
(6, 6, NULL, 'email6@m.ru', 3, 0),
(7, 7, NULL, 'email7@m.ru', 2, 0),
(8, 8, NULL, 'email8@m.ru', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `companyWorkAreas`
--

CREATE TABLE IF NOT EXISTS `companyWorkAreas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyArea` int(11) NOT NULL,
  `workArea` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKCompanyWorkAreasGeographicAreaIndex` (`companyArea`),
  KEY `FKCompanyWorkAreasWorkArea_idx` (`workArea`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `companyWorkAreas`
--

INSERT INTO `companyWorkAreas` (`id`, `companyArea`, `workArea`) VALUES
(1, 1, 1),
(5, 1, 1),
(6, 2, 2),
(7, 3, 3),
(8, 4, 1),
(9, 5, 3),
(10, 6, 1),
(11, 7, 4),
(12, 8, 2);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country` char(2) NOT NULL,
  `region` char(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `country`, `region`) VALUES
(1, 'ru', 'ee'),
(2, 'en', 'eu'),
(3, 'ua', 'eu');

-- --------------------------------------------------------

--
-- Table structure for table `Message`
--

CREATE TABLE IF NOT EXISTS `Message` (
  `id` int(11) NOT NULL DEFAULT '0',
  `language` varchar(16) NOT NULL DEFAULT '',
  `translation` text,
  PRIMARY KEY (`id`,`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Message`
--

INSERT INTO `Message` (`id`, `language`, `translation`) VALUES
(1, 'ru', 'Все'),
(2, 'ru', 'Сухогруз'),
(3, 'ru', 'Офшор'),
(4, 'ru', 'Танкер'),
(5, 'ru', 'Пассажирский'),
(6, 'en', 'East Europe'),
(6, 'ru', 'Восточная Европа'),
(7, 'en', 'Europe'),
(7, 'ru', 'Европа'),
(8, 'en', 'Russia'),
(8, 'ru', 'Россия'),
(9, 'en', 'England'),
(9, 'ru', 'Англия'),
(10, 'en', 'Ukraine'),
(10, 'ru', 'Украина'),
(11, 'ru', 'Сектор'),
(12, 'ru', 'Регион'),
(13, 'ru', 'Страна'),
(14, 'ru', 'Компания'),
(15, 'ru', 'Предыдущий шаг'),
(16, 'ru', 'Следующий шаг'),
(17, 'ru', 'Выбор крюинговых компаний'),
(18, 'ru', 'Это страница выбора компаний. Пожалуйста, выберите компаний, которые вас интересуют. Вы можете использовать фильтры в левой части страницы или отметить их вручную.'),
(19, 'ru', 'Агентства'),
(20, 'ru', 'Шаг');

-- --------------------------------------------------------

--
-- Table structure for table `SourceMessage`
--

CREATE TABLE IF NOT EXISTS `SourceMessage` (
  `id` int(11) NOT NULL,
  `category` varchar(32) DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `SourceMessage`
--

INSERT INTO `SourceMessage` (`id`, `category`, `message`) VALUES
(1, 'word', 'All'),
(2, 'workArea', 'Dry cargo'),
(3, 'workArea', 'Offshore'),
(4, 'workArea', 'Tanker'),
(5, 'workArea', 'Passenger'),
(6, 'region', 'ee'),
(7, 'region', 'eu'),
(8, 'country', 'ru'),
(9, 'country', 'en'),
(10, 'country', 'ua'),
(11, 'word', 'Sector'),
(12, 'word', 'Region'),
(13, 'word', 'Country'),
(14, 'word', 'Company'),
(15, 'phrase', 'Previous Step'),
(16, 'phrase', 'Next Step'),
(17, 'phrase', 'Selecting crewing agencies'),
(18, 'pagePart', 'This is companies select page. Please, select companies that you are interested. You may use filters in left part of page or set checkboxes manually.'),
(19, 'phrase', 'Crewing Agencies'),
(20, 'word', 'Step');

-- --------------------------------------------------------

--
-- Table structure for table `userEmailContents`
--

CREATE TABLE IF NOT EXISTS `userEmailContents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `body` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKUserEmailContentsUserIndex` (`user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `userEmailContents`
--

INSERT INTO `userEmailContents` (`id`, `user`, `subject`, `body`) VALUES
(3, 11, 'Subject', '<p>Proin pulvinar a nisi id ultrices. Ut aliquet, magna eget tristique faucibus, neque justo bibendum ipsum, vel bibendum elit ligula non orci. Sed faucibus dolor a erat sollicitudin, euismod gravida diam adipiscing. Morbi non risus in mauris vulputate sodales id id arcu. Donec id molestie urna. Nam in tristique odio, a egestas massa. Mauris ac lorem ipsum. Proin posuere sapien eu dui volutpat, in tempus purus consequat. Duis posuere risus sit amet purus aliquam sagittis. Aliquam mattis faucibus mauris ac rhoncus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p><p>Vestibulum quis congue enim. Duis venenatis augue a magna mollis, ac venenatis nulla vestibulum. Etiam venenatis nisi molestie quam viverra facilisis eu sit amet nulla. Maecenas leo nunc, pulvinar non mollis vel, bibendum sit amet nulla. Vivamus consectetur volutpat est, sed convallis quam sodales et. Quisque dictum a purus et congue. Ut sem felis, egestas a ullamcorper eu, molestie ut nulla. Phasellus malesuada condimentum nibh, vitae ullamcorper ipsum hendrerit ut. Sed fringilla nec massa eu pharetra. Duis egestas ipsum eu odio dignissim venenatis. Mauris eleifend lacus libero, sit amet dapibus erat mattis eget. Duis condimentum ullamcorper tellus bibendum cursus. Vestibulum at varius ipsum. Curabitur eu dapibus magna, non rutrum purus. Pellentesque condimentum faucibus lacinia. Donec imperdiet metus sit amet ornare mollis.</p>'),
(4, 12, 'Subjecteee', '<p>Proin pulvinar a nisi id ultrices. Ut aliquet, magna eget tristique faucibus, neque justo bibendum ipsum, vel bibendum elit ligula non orci. Sed faucibus dolor a erat sollicitudin, euismod gravida diam adipiscing. Morbi non risus in mauris vulputate sodales id id arcu. Donec id molestie urna. Nam in tristique odio, a egestas massa. Mauris ac lorem ipsum. Proin posuere sapien eu dui volutpat, in tempus purus consequat. Duis posuere risus sit amet purus aliquam sagittis. Aliquam mattis faucibus mauris ac rhoncus</p>');

-- --------------------------------------------------------

--
-- Table structure for table `userFiles`
--

CREATE TABLE IF NOT EXISTS `userFiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `category` varchar(100) NOT NULL,
  `pseudonym` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKUserFilesUserIndex` (`user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `userFiles`
--

INSERT INTO `userFiles` (`id`, `user`, `name`, `category`, `pseudonym`) VALUES
(29, 11, '2.jpg', 'CVFile', 'Резюмеsdf'),
(30, 11, 'bootstrap-3.1.0-dist.zip', 'attachment1', 'Приложение 1dsfs'),
(31, 11, 'CRS.zip', 'attachment2', 'Приложение 2sdf'),
(32, 12, '2.jpg', 'CVFile', 'Re'),
(33, 12, 'ft-calendar.zip', 'attachment1', 'Rt'),
(34, 12, 'jquery-validation-1.11.1.zip', 'attachment2', 'ert');

-- --------------------------------------------------------

--
-- Table structure for table `userOrderedCompanies`
--

CREATE TABLE IF NOT EXISTS `userOrderedCompanies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `companies` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKUserOrderedCompaniesUser_idx` (`user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `userOrderedCompanies`
--

INSERT INTO `userOrderedCompanies` (`id`, `user`, `companies`) VALUES
(1, 11, '1,2,4,5,7,8'),
(2, 12, '5,6,7,8');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identificator` varchar(40) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`,`identificator`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `identificator`, `email`) VALUES
(11, '2d34r4jrbnv4kbubuplv8qoa41', 'sa@m.o'),
(12, 'g6kc945ulnmm058dpgd0otv7q7', 'g@d.y');

-- --------------------------------------------------------

--
-- Table structure for table `workAreas`
--

CREATE TABLE IF NOT EXISTS `workAreas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `workAreas`
--

INSERT INTO `workAreas` (`id`, `name`) VALUES
(1, 'Dry cargo'),
(2, 'Offshore'),
(3, 'Tanker'),
(4, 'Passenger');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `companyGeographicAreas`
--
ALTER TABLE `companyGeographicAreas`
  ADD CONSTRAINT `FKCompanyGeographicAreasCompany` FOREIGN KEY (`company`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FKCompanyGeographicAreasCountry` FOREIGN KEY (`country`) REFERENCES `countries` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `companyWorkAreas`
--
ALTER TABLE `companyWorkAreas`
  ADD CONSTRAINT `FKCompanyWorkAreasWorkArea` FOREIGN KEY (`workArea`) REFERENCES `workAreas` (`id`),
  ADD CONSTRAINT `FKCompanyWorkAreasGeographicArea` FOREIGN KEY (`companyArea`) REFERENCES `companyGeographicAreas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Message`
--
ALTER TABLE `Message`
  ADD CONSTRAINT `FK_Message_SourceMessage` FOREIGN KEY (`id`) REFERENCES `SourceMessage` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `userEmailContents`
--
ALTER TABLE `userEmailContents`
  ADD CONSTRAINT `FKUserEmailContentsUser` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `userFiles`
--
ALTER TABLE `userFiles`
  ADD CONSTRAINT `FKUserFilesUser` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `userOrderedCompanies`
--
ALTER TABLE `userOrderedCompanies`
  ADD CONSTRAINT `FKUserOrderedCompaniesUser` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;