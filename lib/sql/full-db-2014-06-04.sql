-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 04, 2014 at 07:37 PM
-- Server version: 5.5.25
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `maritime-cv`
--

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE IF NOT EXISTS `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `name`) VALUES
(1, 'A-Crew Management International'),
(2, 'A1 Lighghouse of the Black Sea'),
(3, 'Baltik Maritime Consulting OU'),
(4, 'Caravella Odessa'),
(5, 'AG Maritime company'),
(6, 'Adriatico Mariupol'),
(7, 'Baltic Sea Management'),
(8, 'Bibby Ship Management');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `country` char(2) NOT NULL,
  `region` char(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`country`, `region`) VALUES
('ru', 'ee'),
('en', 'eu'),
('lv', 'ba'),
('lv', 'eu'),
('lv', 'ee'),
('se', 'sk'),
('', 'we');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `id` int(11) NOT NULL DEFAULT '0',
  `language` varchar(16) NOT NULL DEFAULT '',
  `translation` text,
  PRIMARY KEY (`id`,`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `language`, `translation`) VALUES
(1, 'ru', 'Все'),
(2, 'en', 'Dry Cargo'),
(2, 'ru', 'Сухогрузы'),
(3, 'en', 'Offshore'),
(3, 'ru', 'Оффшор'),
(4, 'en', 'Tankers'),
(4, 'ru', 'Танкера'),
(5, 'en', 'Passengers'),
(5, 'ru', 'Пассажирский флот'),
(6, 'en', 'East Europe'),
(6, 'ru', 'Восточная Европа'),
(7, 'en', 'European Union'),
(7, 'ru', 'Европейский Союз'),
(8, 'en', 'Russia'),
(8, 'ru', 'Россия'),
(9, 'en', 'England'),
(9, 'ru', 'Англия'),
(10, 'en', 'Ukraine'),
(10, 'ru', 'Украина'),
(11, 'en', 'Sector|Sectors'),
(11, 'ru', 'Сектор|Сектора|Секторов|Сектора'),
(12, 'en', 'Region|Regions'),
(12, 'ru', 'Регион|Региона|Регионов|Регионы'),
(13, 'en', 'Country|Countries'),
(13, 'ru', 'Страна|Страны|Стран|Страны'),
(14, 'en', 'Company|Companies'),
(14, 'ru', 'Компания|Компании|Компаний|Компании'),
(15, 'ru', 'Предыдущий шаг'),
(16, 'ru', 'Следующий шаг'),
(17, 'ru', 'Выбор крюинговых компаний'),
(18, 'ru', 'Это страница выбора компаний. Пожалуйста, выберите компаний, которые вас интересуют. Вы можете использовать фильтры в левой части страницы или отметить их вручную.'),
(19, 'ru', 'Агентства'),
(20, 'en', 'Step|Steps'),
(20, 'ru', 'Шаг|Шага|Шагов|Шаги'),
(21, 'en', 'CV and Attachments'),
(21, 'ru', 'Анкета и Документы'),
(22, 'en', 'Design and Content'),
(22, 'ru', 'Оформление и Тексты'),
(23, 'en', 'Crewing Companies'),
(23, 'ru', 'Выбор компаний'),
(24, 'en', 'Order Payment'),
(24, 'ru', 'Оплата услуги'),
(25, 'en', 'Email Campaign'),
(25, 'ru', 'Рассылка писем'),
(26, 'en', 'Template|Templates'),
(26, 'ru', 'Шаблон|Шаблона|Шаблонов|Шаблоны'),
(27, 'en', 'Color Scheme'),
(27, 'ru', 'Цветовая схема'),
(28, 'en', 'Email Subject'),
(28, 'ru', 'Тема письма'),
(29, 'en', 'Email Content'),
(29, 'ru', 'Текст письма'),
(30, 'en', 'How Our System Works'),
(30, 'ru', 'Как работает наша система?'),
(31, 'en', 'Norway'),
(31, 'ru', 'Норвегия'),
(32, 'en', 'Georgia'),
(32, 'ru', 'Грузия'),
(33, 'en', 'Latvia'),
(33, 'ru', 'Латвия'),
(34, 'en', 'Baltic Countries'),
(34, 'ru', 'Прибалтика');

-- --------------------------------------------------------

--
-- Table structure for table `offices`
--

CREATE TABLE IF NOT EXISTS `offices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `country` varchar(2) DEFAULT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FKCompanyGeographicAreasCompanyIndex` (`company`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `offices`
--

INSERT INTO `offices` (`id`, `company`, `name`, `email`, `country`, `inactive`) VALUES
(1, 1, NULL, 'email1@m.ru', 'ua', 0),
(2, 2, NULL, 'email2@m.ru', 'ua', 0),
(3, 3, NULL, 'email3@m.ru', 'ru', 0),
(4, 4, NULL, 'email4@m.ru', 'no', 0),
(5, 5, NULL, 'email5@m.ru', 'ru', 0),
(6, 6, NULL, 'email6@m.ru', 'lv', 0),
(7, 7, NULL, 'email7@m.ru', 'lv', 0),
(8, 8, NULL, 'email8@m.ru', 'ge', 1);

-- --------------------------------------------------------

--
-- Table structure for table `officeworkareas`
--

CREATE TABLE IF NOT EXISTS `officeworkareas` (
  `office` int(11) NOT NULL,
  `workArea` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `officeworkareas`
--

INSERT INTO `officeworkareas` (`office`, `workArea`) VALUES
(1, 'dc'),
(2, 'os'),
(3, 'tk'),
(4, 'ps'),
(5, 'tk'),
(7, 'ps'),
(8, 'os'),
(2, 'ps'),
(5, 'dc');

-- --------------------------------------------------------

--
-- Table structure for table `sourcemessage`
--

CREATE TABLE IF NOT EXISTS `sourcemessage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(32) DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `sourcemessage`
--

INSERT INTO `sourcemessage` (`id`, `category`, `message`) VALUES
(0, 'phrase', 'CV and Attachments'),
(1, 'word', 'All'),
(2, 'workArea', 'dc'),
(3, 'workArea', 'os'),
(4, 'workArea', 'tk'),
(5, 'workArea', 'ps'),
(6, 'region', 'ee'),
(7, 'region', 'eu'),
(8, 'country', 'ru'),
(9, 'country', 'en'),
(10, 'country', 'ua'),
(11, 'word', 'Sector'),
(12, 'word', 'Region'),
(13, 'word', 'Country'),
(14, 'word', 'Company'),
(15, 'phrase', 'Previous Step'),
(16, 'phrase', 'Next Step'),
(17, 'phrase', 'Selecting crewing agencies'),
(18, 'pagePart', 'This is companies select page. Please, select companies that you are interested. You may use filters in left part of page or set checkboxes manually.'),
(19, 'phrase', 'Crewing Agencies'),
(20, 'word', 'Step'),
(21, 'phrase', 'CV and Attachments'),
(22, 'phrase', 'Design and Content'),
(23, 'phrase', 'Crewing Companies'),
(24, 'phrase', 'Order Payment'),
(25, 'phrase', 'Email Campaign'),
(26, 'word', 'Template'),
(27, 'phrase', 'Color Scheme'),
(28, 'phrase', 'Email Subject'),
(29, 'phrase', 'Email Content'),
(30, 'phrase', 'How It Works'),
(31, 'country', 'no'),
(32, 'country', 'ge'),
(33, 'country', 'lv'),
(34, 'region', 'ba');

-- --------------------------------------------------------

--
-- Table structure for table `useremailcontents`
--

CREATE TABLE IF NOT EXISTS `useremailcontents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `body` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKUserEmailContentsUserIndex` (`user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `useremailcontents`
--

INSERT INTO `useremailcontents` (`id`, `user`, `subject`, `body`) VALUES
(3, 11, 'Subject', '<p>Proin pulvinar a nisi id ultrices. Ut aliquet, magna eget tristique faucibus, neque justo bibendum ipsum, vel bibendum elit ligula non orci. Sed faucibus dolor a erat sollicitudin, euismod gravida diam adipiscing. Morbi non risus in mauris vulputate sodales id id arcu. Donec id molestie urna. Nam in tristique odio, a egestas massa. Mauris ac lorem ipsum. Proin posuere sapien eu dui volutpat, in tempus purus consequat. Duis posuere risus sit amet purus aliquam sagittis. Aliquam mattis faucibus mauris ac rhoncus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p><p>Vestibulum quis congue enim. Duis venenatis augue a magna mollis, ac venenatis nulla vestibulum. Etiam venenatis nisi molestie quam viverra facilisis eu sit amet nulla. Maecenas leo nunc, pulvinar non mollis vel, bibendum sit amet nulla. Vivamus consectetur volutpat est, sed convallis quam sodales et. Quisque dictum a purus et congue. Ut sem felis, egestas a ullamcorper eu, molestie ut nulla. Phasellus malesuada condimentum nibh, vitae ullamcorper ipsum hendrerit ut. Sed fringilla nec massa eu pharetra. Duis egestas ipsum eu odio dignissim venenatis. Mauris eleifend lacus libero, sit amet dapibus erat mattis eget. Duis condimentum ullamcorper tellus bibendum cursus. Vestibulum at varius ipsum. Curabitur eu dapibus magna, non rutrum purus. Pellentesque condimentum faucibus lacinia. Donec imperdiet metus sit amet ornare mollis.</p>'),
(4, 12, 'Subjecteee', '<p>Proin pulvinar a nisi id ultrices. Ut aliquet, magna eget tristique faucibus, neque justo bibendum ipsum, vel bibendum elit ligula non orci. Sed faucibus dolor a erat sollicitudin, euismod gravida diam adipiscing. Morbi non risus in mauris vulputate sodales id id arcu. Donec id molestie urna. Nam in tristique odio, a egestas massa. Mauris ac lorem ipsum. Proin posuere sapien eu dui volutpat, in tempus purus consequat. Duis posuere risus sit amet purus aliquam sagittis. Aliquam mattis faucibus mauris ac rhoncus</p>'),
(5, 13, 'вввыпыв', '<p><<<<<<< HEAD</p><h2>Ivanov Ivan</h2><p><a>email@mail.ru</a></p><p>+38044XXXXXXX</p><p><br> ======= >>>>>>> 89e406c6f8159269bc13f08110b82f108cfad72b</p><p>Proin pulvinar a nisi id ultrices. Ut aliquet, magna eget tristique faucibus, neque justo bibendum ipsum, vel bibendum elit ligula non orci. Sed faucibus dolor a erat sollicitudin, euismod gravida diam adipiscing. Morbi non risus in mauris vulputate sodales id id arcu. Donec id molestie urna. Nam in tristique odio, a egestas massa. Mauris ac lorem ipsum. Proin posuere sapien eu dui volutpat, in tempus purus consequat. Duis posuere risus sit amet purus aliquam sagittis. Aliquam mattis faucibus mauris ac rhoncus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p><p>Vestibulum quis congue enim. Duis venenatis augue a magna mollis, ac venenatis nulla vestibulum. Etiam venenatis nisi molestie quam viverra facilisis eu sit amet nulla. Maecenas leo nunc, pulvinar non mollis vel, bibendum sit amet nulla. Vivamus consectetur volutpat est, sed convallis quam sodales et. Quisque dictum a purus et congue. Ut sem felis, egestas a ullamcorper eu, molestie ut nulla. Phasellus malesuada condimentum nibh, vitae ullamcorper ipsum hendrerit ut. Sed fringilla nec massa eu pharetra. Duis egestas ipsum eu odio dignissim venenatis. Mauris eleifend lacus libero, sit amet dapibus erat mattis eget. Duis condimentum ullamcorper tellus bibendum cursus. Vestibulum at varius ipsum. Curabitur eu dapibus magna, non rutrum purus. Pellentesque condimentum faucibus lacinia. Donec imperdiet metus sit amet ornare mollis.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `userfiles`
--

CREATE TABLE IF NOT EXISTS `userfiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `category` varchar(100) NOT NULL,
  `pseudonym` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKUserFilesUserIndex` (`user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `userfiles`
--

INSERT INTO `userfiles` (`id`, `user`, `name`, `category`, `pseudonym`) VALUES
(29, 11, '2.jpg', 'CVFile', 'Резюмеsdf'),
(30, 11, 'bootstrap-3.1.0-dist.zip', 'attachment1', 'Приложение 1dsfs'),
(31, 11, 'CRS.zip', 'attachment2', 'Приложение 2sdf'),
(32, 12, '2.jpg', 'CVFile', 'Re'),
(33, 12, 'ft-calendar.zip', 'attachment1', 'Rt'),
(34, 12, 'jquery-validation-1.11.1.zip', 'attachment2', 'ert'),
(35, 13, 'Справка.docx', 'CVFile', 'Резюме');

-- --------------------------------------------------------

--
-- Table structure for table `userorderedcompanies`
--

CREATE TABLE IF NOT EXISTS `userorderedcompanies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `companies` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKUserOrderedCompaniesUser_idx` (`user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `userorderedcompanies`
--

INSERT INTO `userorderedcompanies` (`id`, `user`, `companies`) VALUES
(1, 11, '1,2,4,5,7,8'),
(2, 12, '5,6,7,8'),
(3, 13, '1,2,3,4,5,6,7,8');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identificator` varchar(40) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`,`identificator`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `identificator`, `email`) VALUES
(11, '2d34r4jrbnv4kbubuplv8qoa41', 'sa@m.o'),
(12, 'g6kc945ulnmm058dpgd0otv7q7', 'g@d.y'),
(13, 'hfep5jope5vr0o8qea8nal49h5', 'aa@aa.aa');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `FK_Message_SourceMessage` FOREIGN KEY (`id`) REFERENCES `sourcemessage` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `offices`
--
ALTER TABLE `offices`
  ADD CONSTRAINT `FKCompanyGeographicAreasCompany` FOREIGN KEY (`company`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `useremailcontents`
--
ALTER TABLE `useremailcontents`
  ADD CONSTRAINT `FKUserEmailContentsUser` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `userfiles`
--
ALTER TABLE `userfiles`
  ADD CONSTRAINT `FKUserFilesUser` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `userorderedcompanies`
--
ALTER TABLE `userorderedcompanies`
  ADD CONSTRAINT `FKUserOrderedCompaniesUser` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
