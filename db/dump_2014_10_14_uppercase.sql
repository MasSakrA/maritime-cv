-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.20 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table maritime-cv.colorschemes
DROP TABLE IF EXISTS `colorschemes`;
CREATE TABLE IF NOT EXISTS `colorschemes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `scheme` text NOT NULL,
  `additionalCSS` text NOT NULL,
  `order` int(11) NOT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table maritime-cv.colorschemes: ~4 rows (approximately)
DELETE FROM `colorschemes`;
/*!40000 ALTER TABLE `colorschemes` DISABLE KEYS */;
INSERT INTO `colorschemes` (`id`, `name`, `scheme`, `additionalCSS`, `order`, `inactive`) VALUES
	(1, 'mz', '.cs-mz.email-content{font-family:Tahoma,Arail,sans-serif;color:#295e60;background:#effdff;background:-moz-linear-gradient(top,#effdff 0,#fcfeff 100%);background:-webkit-gradient(linear,left top,left bottom,color-stop(0%,#effdff),color-stop(100%,#fcfeff));background:-webkit-linear-gradient(top,#effdff 0,#fcfeff 100%);background:-o-linear-gradient(top,#effdff 0,#fcfeff 100%);background:-ms-linear-gradient(top,#effdff 0,#fcfeff 100%);background:linear-gradient(to bottom,#effdff 0,#fcfeff 100%);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#effdff\', endColorstr=\'#fcfeff\', GradientType=0)}\r\n.cs-mz.email-content a{color:#0083ae}\r\n.cs-mz.email-content h1{font-size:26px;font-family:Georgia,serif;color:#0d1519;text-transform:none}\r\n.cs-mz.email-content h2{font-size:20px;font-family:Georgia,serif;color:#0d1519;letter-spacing:normal;text-transform:none}\r\n.cs-mz.email-content h3{font-size:17px;font-family:Georgia,serif;color:#0d1519}\r\n.cs-mz.email-content h4{font-size:18px;font-family:\'Times New Roman\',serif;font-style:italic;letter-spacing:normal;color:#0d1519;text-transform:none}\r\n.cs-mz.email-content strong{color:#1c9490}', '.cs-mz.template-color .one{background: #effdff}\r\n.cs-mz.template-color .two{background: #0083ae}\r\n.cs-mz.template-color .three{background: #1c9490}\r\n.cs-mz.template-color .four{background: #295e60}', 2, 0),
	(2, 'exp', '.cs-exp.email-content{font-family:\'Segoe UI\',Tahoma,Arail,sans-serif;color:#666;background: #f5f5f5;\r\nbackground: -moz-linear-gradient(top,  #f5f5f5 0%, #ffffff 100%);\r\nbackground: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f5f5f5), color-stop(100%,#ffffff));\r\nbackground: -webkit-linear-gradient(top,  #f5f5f5 0%,#ffffff 100%);\r\nbackground: -o-linear-gradient(top,  #f5f5f5 0%,#ffffff 100%);\r\nbackground: -ms-linear-gradient(top,  #f5f5f5 0%,#ffffff 100%);\r\nbackground: linear-gradient(to bottom,  #f5f5f5 0%,#ffffff 100%);\r\nfilter: progid:DXImageTransform.Microsoft.gradient( startColorstr=\'#f5f5f5\', endColorstr=\'#ffffff\',GradientType=0);}\r\n.cs-exp.email-content a{color:#F56B28}\r\n.cs-exp.email-content h1{font-size:26px;font-family:\'Segoe UI\',sens-serif;color:#0d1519;text-transform:none}\r\n.cs-exp.email-content h2{font-size:20px;font-family:\'Segoe UI\',sens-serif;color:#0d1519;letter-spacing:normal;text-transform:none}\r\n.cs-exp.email-content h3{font-size:17px;font-family:\'Segoe UI\',sens-serif;color:#0d1519}\r\n.cs-exp.email-content h4{font-size:18px;font-family:\'Segoe UI\',sens-serif;font-style:italic;letter-spacing:normal;color:#0d1519;text-transform:none}\r\n.cs-exp.email-content strong{color:#0093be}', '.cs-exp.template-color .one { background: #DDD }\r\n.cs-exp.template-color .two { background: #F56B28 }\r\n.cs-exp.template-color .three { background: #0093be }\r\n.cs-exp.template-color .four { background: #666 }', 3, 0),
	(3, 'eco', '.cs-eco.email-content{font-family:Arail,sans-serif;color:#44504C;background: #fcfcfc;\r\nbackground: -moz-linear-gradient(top,  #fcfcfc 0%, #f5f9ef 100%);\r\nbackground: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#fcfcfc), color-stop(100%,#f5f9ef));\r\nbackground: -webkit-linear-gradient(top,  #fcfcfc 0%,#f5f9ef 100%);\r\nbackground: -o-linear-gradient(top,  #fcfcfc 0%,#f5f9ef 100%);\r\nbackground: -ms-linear-gradient(top,  #fcfcfc 0%,#f5f9ef 100%);\r\nbackground: linear-gradient(to bottom,  #fcfcfc 0%,#f5f9ef 100%);\r\nfilter: progid:DXImageTransform.Microsoft.gradient( startColorstr=\'#fcfcfc\', endColorstr=\'#f5f9ef\',GradientType=0 );}\r\n.cs-eco.email-content a{color:#35A887}\r\n.cs-eco.email-content h1{font-size:26px;font-family:\'Times New Roman\',Georgia,serif;color:#46892F;text-transform:none}\r\n.cs-eco.email-content h2{font-size:20px;font-family:\'Times New Roman\',Georgia,serif;color:#0d1519;letter-spacing:normal;text-transform:none}\r\n.cs-eco.email-content h3{font-size:17px;font-family:Georgia,serif;color:#0d1519}\r\n.cs-eco.email-content h4{font-size:18px;font-family:\'Times New Roman\',serif;font-style:italic;letter-spacing:normal;color:#0d1519;text-transform:none}\r\n.cs-eco.email-content strong{color:#46892F}', '.cs-eco.template-color .one{background: #EEE}\r\n.cs-eco.template-color .two{background: #35A887}\r\n.cs-eco.template-color .three{background: #46892F}\r\n.cs-eco.template-color .four{background: #44504C}', 4, 0),
	(4, 'default', '', '.cs-default.template-color .one{background: #effdff}\r\n.cs-default.template-color .two{background: #0083ae}\r\n.cs-default.template-color .three{background: #1c9490}\r\n.cs-default.template-color .four{background: #657073}', 1, 0);
/*!40000 ALTER TABLE `colorschemes` ENABLE KEYS */;


-- Dumping structure for table maritime-cv.companies
DROP TABLE IF EXISTS `companies`;
CREATE TABLE IF NOT EXISTS `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Dumping data for table maritime-cv.companies: ~9 rows (approximately)
DELETE FROM `companies`;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
INSERT INTO `companies` (`id`, `name`) VALUES
	(1, 'A-Crew Management International'),
	(2, 'A1 Lighghouse of the Black Sea'),
	(3, 'Adriatico Mariupol'),
	(4, 'AG Maritime company'),
	(5, 'Baltic Sea Management'),
	(6, 'Baltik Maritime Consulting OU'),
	(7, 'Bibby Ship Management'),
	(8, 'Caravella Odessa'),
	(9, 'New Orlean Companies');
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;


-- Dumping structure for table maritime-cv.countries
DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `region` char(2) NOT NULL,
  `country` char(2) NOT NULL,
  `id` int(2) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

-- Dumping data for table maritime-cv.countries: ~49 rows (approximately)
DELETE FROM `countries`;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` (`region`, `country`, `id`) VALUES
	('bs', 'lv', 1),
	('bs', 'lt', 2),
	('bs', 'ee', 3),
	('eu', 'at', 4),
	('eu', 'be', 5),
	('eu', 'bg', 6),
	('eu', 'hu', 7),
	('eu', 'uk', 8),
	('eu', 'gr', 9),
	('eu', 'de', 10),
	('eu', 'dk', 11),
	('eu', 'it', 12),
	('eu', 'ie', 13),
	('eu', 'es', 14),
	('me', 'cy', 15),
	('eu', 'lu', 16),
	('eu', 'lt', 18),
	('eu', 'mt', 19),
	('eu', 'nl', 20),
	('eu', 'pt', 21),
	('eu', 'pl', 22),
	('eu', 'ro', 23),
	('eu', 'si', 24),
	('eu', 'sk', 25),
	('eu', 'fr', 26),
	('eu', 'fi', 27),
	('eu', 'hr', 28),
	('eu', 'cz', 29),
	('eu', 'se', 30),
	('ru', 'ru', 32),
	('ua', 'ua', 33),
	('ne', 'md', 34),
	('ne', 'by', 35),
	('eu', 'bg', 36),
	('eu', 'cz', 37),
	('eu', 'hu', 38),
	('eu', 'pl', 39),
	('eu', 'ro', 40),
	('eu', 'sk', 41),
	('eu', 'lt', 42),
	('eu', 'ee', 44),
	('eu', 'hr', 45),
	('me', 'ge', 46),
	('as', 'sg', 47),
	('as', 'ph', 48),
	('as', 'in', 49),
	('as', 'id', 50),
	('eu', 'ch', 51),
	('eu', 'en', 52);
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;


-- Dumping structure for table maritime-cv.emailqueueparams
DROP TABLE IF EXISTS `emailqueueparams`;
CREATE TABLE IF NOT EXISTS `emailqueueparams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emailsQueueId` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_emailQueueParams_email_idx` (`emailsQueueId`),
  CONSTRAINT `fk_emailQueueParams_email` FOREIGN KEY (`emailsQueueId`) REFERENCES `emaiqueue` (`queueId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=cp1250;

-- Dumping data for table maritime-cv.emailqueueparams: ~30 rows (approximately)
DELETE FROM `emailqueueparams`;
/*!40000 ALTER TABLE `emailqueueparams` DISABLE KEYS */;
INSERT INTO `emailqueueparams` (`id`, `emailsQueueId`, `name`, `value`) VALUES
	(69, 1, 'accountLink', 'http://maritime-cv.local/account/login?user=cgarrfccbhuapr9irg0an0ft24'),
	(70, 1, 'orderNumber', '4761410525111'),
	(71, 1, 'orderAmount', '4.00'),
	(72, 2, 'accountLink', 'http://maritime-cv.local/account/login?user=85shoudpq90toeua25mfah2is4'),
	(73, 2, 'orderNumber', '5591411384450'),
	(74, 2, 'orderAmount', '8.20'),
	(75, 3, 'accountLink', 'http://maritime-cv.local/account/login?user=85shoudpq90toeua25mfah2is4'),
	(76, 3, 'orderNumber', '5591411384450'),
	(77, 3, 'orderAmount', '8.20'),
	(78, 4, 'accountLink', 'http://maritime-cv.local/account/login?user=vj5g248nr92ujeved7tvtfifj3'),
	(79, 4, 'orderNumber', '7021411385020'),
	(80, 4, 'orderAmount', '8.20'),
	(81, 5, 'accountLink', 'http://maritime-cv.local/account/login?user=vj5g248nr92ujeved7tvtfifj3'),
	(82, 5, 'orderNumber', '7021411385020'),
	(83, 5, 'orderAmount', '8.20'),
	(84, 6, 'accountLink', 'http://maritime-cv.local/account/login?user=vj5g248nr92ujeved7tvtfifj3'),
	(85, 6, 'orderNumber', '7021411385020'),
	(86, 6, 'orderAmount', '8.20'),
	(87, 7, 'accountLink', 'http://maritime-cv.local/account/login?user=vj5g248nr92ujeved7tvtfifj3'),
	(88, 7, 'orderNumber', '7021411385020'),
	(89, 7, 'orderAmount', '8.20'),
	(90, 8, 'accountLink', 'http://maritime-cv.local/account/login?user=07umnjmtbstkpt9u1nbvug0012'),
	(91, 8, 'orderNumber', '2301411386745'),
	(92, 8, 'orderAmount', '8.20'),
	(93, 9, 'accountLink', 'http://maritime-cv.local/account/login?user=bgg6f7kkkdj60m37ee11nld6b1'),
	(94, 9, 'orderNumber', '7491411386924'),
	(95, 9, 'orderAmount', '10.00'),
	(96, 10, 'accountLink', 'http://maritime-cv.local/account/login?user=g1h0os8pcv6f7tq1sv85nntv13'),
	(97, 10, 'orderNumber', '1951411746891'),
	(98, 10, 'orderAmount', '10.00');
/*!40000 ALTER TABLE `emailqueueparams` ENABLE KEYS */;


-- Dumping structure for table maritime-cv.emailtemplates
DROP TABLE IF EXISTS `emailtemplates`;
CREATE TABLE IF NOT EXISTS `emailtemplates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `type` varchar(2) NOT NULL,
  `order` int(11) NOT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table maritime-cv.emailtemplates: ~5 rows (approximately)
DELETE FROM `emailtemplates`;
/*!40000 ALTER TABLE `emailtemplates` DISABLE KEYS */;
INSERT INTO `emailtemplates` (`id`, `name`, `type`, `order`, `inactive`) VALUES
	(1, 'cv-default', 'cv', 1, 0),
	(2, 'cv-exp', 'cv', 2, 0),
	(3, 'new-order-notify', 'nf', 0, 0),
	(4, 'company-notify', 'nf', 1, 0),
	(5, 'completed-order-notify', 'nf', 0, 0);
/*!40000 ALTER TABLE `emailtemplates` ENABLE KEYS */;


-- Dumping structure for table maritime-cv.emaiQueue
DROP TABLE IF EXISTS `emaiQueue`;
CREATE TABLE IF NOT EXISTS `emaiQueue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `queueId` int(11) NOT NULL DEFAULT '0',
  `emailTemplate` int(11) DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  `datetimeIn` datetime NOT NULL,
  `datetimeOut` datetime DEFAULT NULL,
  `inQueue` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_emailQueue_emailTemplate_idx` (`emailTemplate`),
  KEY `fk_emailQueue_queueId_idx` (`queueId`),
  CONSTRAINT `fk_emailQueue_emailTemplate` FOREIGN KEY (`emailTemplate`) REFERENCES `emailtemplates` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=cp1250;

-- Dumping data for table maritime-cv.emaiQueue: ~10 rows (approximately)
DELETE FROM `emaiQueue`;
/*!40000 ALTER TABLE `emaiQueue` DISABLE KEYS */;
INSERT INTO `emaiQueue` (`id`, `queueId`, `emailTemplate`, `address`, `datetimeIn`, `datetimeOut`, `inQueue`) VALUES
	(91, 1, 3, 'sassweee@m.o', '2014-09-12 16:39:12', NULL, 0),
	(92, 2, 3, 'sassweee@m.o', '2014-09-22 15:14:10', '2014-09-22 15:14:18', 0),
	(93, 3, 3, 'sassweee@m.o', '2014-09-22 15:23:07', '2014-09-22 15:23:11', 0),
	(94, 4, 3, 'sassweee@m.o', '2014-09-22 15:23:41', NULL, 1),
	(95, 5, 3, 'sassweee@m.o', '2014-09-22 15:37:09', NULL, 1),
	(96, 6, 3, 'sassweee@m.o', '2014-09-22 15:38:58', NULL, 1),
	(97, 7, 3, 'sassweee@m.o', '2014-09-22 15:51:44', NULL, 1),
	(98, 8, 3, 'sassweee@m.o', '2014-09-22 15:52:26', NULL, 1),
	(99, 9, 3, 'sa@m.o', '2014-09-22 15:55:25', NULL, 1),
	(100, 10, 3, 'sassweee@m.o', '2014-09-26 19:54:52', NULL, 1);
/*!40000 ALTER TABLE `emaiQueue` ENABLE KEYS */;


-- Dumping structure for table maritime-cv.Message
DROP TABLE IF EXISTS `Message`;
CREATE TABLE IF NOT EXISTS `Message` (
  `id` int(11) NOT NULL DEFAULT '0',
  `language` varchar(16) NOT NULL DEFAULT '',
  `translation` text,
  PRIMARY KEY (`id`,`language`),
  CONSTRAINT `FK_Message_SourceMessage` FOREIGN KEY (`id`) REFERENCES `sourcemessage` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table maritime-cv.Message: ~328 rows (approximately)
DELETE FROM `Message`;
/*!40000 ALTER TABLE `Message` DISABLE KEYS */;
INSERT INTO `Message` (`id`, `language`, `translation`) VALUES
	(1, 'en', ''),
	(1, 'ru', 'Все'),
	(2, 'en', 'Dry Cargo'),
	(2, 'ru', 'Сухогрузы'),
	(3, 'en', 'Offshore'),
	(3, 'ru', 'Оффшор'),
	(4, 'en', 'Tankers'),
	(4, 'ru', 'Танкера'),
	(5, 'en', 'Passengers'),
	(5, 'ru', 'Пассажирский флот'),
	(7, 'en', 'European Union'),
	(7, 'ru', 'Европейский Союз'),
	(8, 'en', 'Russia'),
	(8, 'ru', 'Россия'),
	(9, 'en', 'England'),
	(9, 'ru', 'Англия'),
	(10, 'en', 'Ukraine'),
	(10, 'ru', 'Украина'),
	(11, 'en', 'Sector|Sectors'),
	(11, 'ru', 'Сектор|Сектора|Секторов|Сектора'),
	(12, 'en', 'Region|Regions'),
	(12, 'ru', 'Регион|Региона|Регионов|Регионы'),
	(13, 'en', 'Country|Countries'),
	(13, 'ru', 'Страна|Страны|Стран|Страны'),
	(14, 'en', 'Company|Companies'),
	(14, 'ru', 'Компания|Компании|Компаний|Компании'),
	(15, 'ru', 'Предыдущий шаг'),
	(16, 'ru', 'Следующий шаг'),
	(17, 'ru', 'Выбор крюинговых компаний'),
	(18, 'en', ''),
	(18, 'ru', '<p>Это страница выбора компаний. Пожалуйста, выберите компаний, которые вас интересуют. Вы можете использовать фильтры в левой части страницы или отметить их вручную.1</p>'),
	(19, 'ru', 'Агентства'),
	(20, 'en', 'Step|Steps'),
	(20, 'ru', 'Шаг|Шага|Шагов|Шаги'),
	(21, 'en', 'CV and Attachments'),
	(21, 'ru', 'Анкета и Документы'),
	(22, 'en', 'Design and Content'),
	(22, 'ru', 'Оформление и Тексты'),
	(23, 'en', 'Crewing Companies'),
	(23, 'ru', 'Выбор компаний'),
	(24, 'en', 'Order Payment'),
	(24, 'ru', 'Оплата услуги'),
	(25, 'en', 'Email Campaign'),
	(25, 'ru', 'Рассылка писем'),
	(26, 'en', 'Template|Templates'),
	(26, 'ru', 'Шаблон|Шаблона|Шаблонов|Шаблоны'),
	(27, 'en', 'Design Scheme'),
	(27, 'ru', 'Оформление'),
	(28, 'en', 'Email Subject'),
	(28, 'ru', 'Тема письма'),
	(29, 'en', 'Email Content'),
	(29, 'ru', 'Текст письма'),
	(30, 'en', 'How Our System Works'),
	(30, 'ru', 'Как работает сервис Maritime CV?'),
	(31, 'en', 'Norway'),
	(31, 'ru', 'Норвегия'),
	(32, 'en', 'Georgia'),
	(32, 'ru', 'Грузия'),
	(33, 'en', 'Latvia'),
	(33, 'ru', 'Латвия'),
	(34, 'en', 'Baltic Countries'),
	(34, 'ru', 'Прибалтика'),
	(35, 'en', 'Marine'),
	(35, 'ru', 'Море'),
	(36, 'en', 'Default Text'),
	(36, 'ru', 'Стандартный текст'),
	(37, 'en', '<h1>HEADER 1</h1>\r\n<h2>HEADER&nbsp;<em>2</em></h2>\r\n<h3 style="text-align: right;"><em>Header 3</em></h3>\r\n<h4 style="text-align: center;"><em>Header 4</em></h4>\r\n<h5 style="text-align: right;"><em>Header 5</em></h5>\r\n<h6>Header 65</h6>\r\n<p>Donec q\'uis pe\' &lt;div id= llentesque m<a href="ddd">i, ac consequat nibh. Ut lacinia est ac augue tincidunt ultricies. Proin ac erat ac diam dapibus gra</a>vida. Proin sodales tellus vitae nisi volutpat fermentum. Phasellus facilisis quam vel faucibus placerat. Proin egestas nibh lectus,&nbsp;aliquam dapibus dolor&nbsp;ultricies vitae. Cras nec varius lectus.</p>\r\n<p>Suspendisse malesuada ut purus vel feugiat. Aenean faucibus nulla nec ligula tempus, eget varius nulla dapibus. Nam ultrices sem elit, ac iaculis nulla adipiscing eget. Vestibulum non nibh congue, elementum turpis et, feugiat massa. Fusce magna lectus, eleifend ac ante sit amet, feugiat euismod felis. Ut sit amet eros quis quam placerat porta. Morbi a nisi arcu.</p>\r\n<p><a href="http://aaa.aa">aaa@aa.aa</a>&lt;div id="&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<h1>HEADER 1</h1>\r\n<h2>HEADER&nbsp;<em>2</em></h2>\r\n<h3 style="text-align: right;"><em>Header 3</em></h3>\r\n<h4 style="text-align: center;"><em>Header 4</em></h4>\r\n<h5 style="text-align: right;"><em>Header 5</em></h5>\r\n<h6>Header 65</h6>\r\n<p>Donec q\'uis pe\' &lt;div id= llentesque m<a href="ddd">i, ac consequat nibh. Ut lacinia est ac augue tincidunt ultricies. Proin ac erat ac diam dapibus gra</a>vida. Proin sodales tellus vitae nisi volutpat fermentum. Phasellus facilisis quam vel faucibus placerat. Proin egestas nibh lectus,&nbsp;aliquam dapibus dolor&nbsp;ultricies vitae. Cras nec varius lectus.</p>\r\n<p>Suspendisse malesuada ut purus vel feugiat. Aenean faucibus nulla nec ligula tempus, eget varius nulla dapibus. Nam ultrices sem elit, ac iaculis nulla adipiscing eget. Vestibulum non nibh congue, elementum turpis et, feugiat massa. Fusce magna lectus, eleifend ac ante sit amet, feugiat euismod felis. Ut sit amet eros quis quam placerat porta. Morbi a nisi arcu.</p>\r\n<p><a href="http://aaa.aa">aaa@aa.aa</a>&lt;div id="&nbsp;</p>'),
	(37, 'ru', '<p><strong>Риск</strong>&nbsp;&mdash; сочетание&nbsp;<a title="Вероятность" href="https://ru.wikipedia.org/wiki/%D0%92%D0%B5%D1%80%D0%BE%D1%8F%D1%82%D0%BD%D0%BE%D1%81%D1%82%D1%8C">вероятности</a>&nbsp;и последствий наступления неблагоприятных событий. Знание вероятности неблагоприятного события позволяет определить вероятность благоприятных событий по формуле&nbsp;<img class="mwe-math-fallback-png-inline tex" src="https://upload.wikimedia.org/math/d/e/8/de834eac4081059d3d8cf7d1aa6d8f39.png" alt="P_{+}=1-P_{-}" />. Также риском часто называют непосредственно предполагаемое событие, способное принести кому-либо&nbsp;<a title="Ущерб" href="https://ru.wikipedia.org/wiki/%D0%A3%D1%89%D0%B5%D1%80%D0%B1">ущерб</a>&nbsp;или&nbsp;<a title="Убыток" href="https://ru.wikipedia.org/wiki/%D0%A3%D0%B1%D1%8B%D1%82%D0%BE%D0%BA">убыток</a>r</p>'),
	(38, 'en', 'Good day sirs!\r\n\r\nI\'m a well-experienced seaman who wants to work with your company. You offer the greatest conditions.\r\n\r\nRegards,\r\nseaman.'),
	(38, 'ru', 'Добрый день!\r\nЯ моряк, прослужил на флоте более 20 лет.\r\nХочу работать именно в вашей компании, т.к. вы можете предложить мне лучшие условия работы и оплату труда.\r\n\r\nС уважением,\r\nморяк.'),
	(39, 'en', 'Experience'),
	(39, 'ru', 'Для опытных моряков'),
	(40, 'en', 'Default'),
	(40, 'ru', 'Стандарт'),
	(41, 'en', 'Ecology'),
	(41, 'ru', 'Трава'),
	(42, 'en', 'Exposition'),
	(42, 'ru', 'Экспозиция'),
	(43, 'en', 'Click to upload'),
	(43, 'ru', 'Прикрепить'),
	(44, 'en', 'Your CV'),
	(44, 'ru', 'Анкету'),
	(45, 'en', 'File 1'),
	(45, 'ru', 'Вложение 1'),
	(46, 'en', 'File 2'),
	(46, 'ru', 'Вложение 2'),
	(47, 'en', 'Your Email'),
	(47, 'ru', 'Ваш Email'),
	(48, 'en', 'Continue'),
	(48, 'ru', 'Продолжить'),
	(49, 'en', 'Allowed file types'),
	(49, 'ru', 'Допустимые типы файлов'),
	(50, 'en', 'Attach your CV'),
	(50, 'ru', 'Прикрепите анкету моряка'),
	(51, 'en', '<p class="lead">Follow These Steps</p>\r\n<ol>\r\n<li>Upload your CV</li>\r\n<li>Upload attachments</li>\r\n<li>Enter your email</li>\r\n<li>Click the button</li>\r\n</ol>\r\n<p class="text-justify">Praesent ut dignissim orci, a congue diam. Maecenas sit amet est tellus. Morbi vitae malesuada purus. Duis consectetur urna congue elit scelerisque, vel tincidunt leo commodo. Aliquam cursus sit amet sem et pharetra.</p>'),
	(51, 'ru', '<p class="lead">Форма загрузки ваших документов</p>\r\n<p>Воспользуйтесь формой для загрузки анкеты моряка и других необходимых документов.</p>\r\n<p>&nbsp;</p>\r\n<ol>\r\n<li>Загрузите анкету моряка</li>\r\n<li>Загрузите другие документы (вложения)</li>\r\n<li>Введите свой email</li>\r\n<li>Нажмите на кнопку "Продолжить"</li>\r\n</ol><hr />\r\n<p>Допустимые типы файлов анкеты моряка и вложений:<br />doc, docx, xls, xlsx, rtf, pdf, jpg, zip, rar.<br /> Максимальный размер анкеты составляет 1 МБ.<br /> Максимальный размер каждого из вложений составляет 500 Кб.</p>'),
	(52, 'en', 'Send your CV right now'),
	(52, 'ru', 'Начните рассылку своей анкеты прямо сейчас'),
	(53, 'en', 'Why Maritime CV?'),
	(53, 'ru', 'Почему стоит воспользоваться нашим сервисом?'),
	(54, 'en', '<p class="lead">Benefits of Maritime CV</p>'),
	(54, 'ru', '<p class="lead">Преимущества сервиса Maritime CV</p>\r\n<ul>\r\n<li>Только проверенные email-адреса компаний</li>\r\n<li>Рассылка по региону</li>\r\n<li>Рассылка по сектору судоходства</li>\r\n<li>Редактирование списка компаний-получателей</li>\r\n<li>Множество вариантов оформления письма</li>\r\n<li>Отчет о выполнении услуги1</li>\r\n</ul>'),
	(55, 'en', '<p class="lead">We accept different payment systems</p>'),
	(55, 'ru', '			<div class="padding" id="payment-systems">\r\n				<p class="lead">Мы работаем со многими платёжными системами</p>\r\n				\r\n				<div>\r\n					<div class="col-md-3">\r\n						<div class="payment-system">\r\n							<img src="lib/skin/img/wallet-one.png" title="Wallet One" alt="Wallet One" />\r\n							<span><div class="dt"><div class="dr"><div class="dc">Wallet<br />One</div></div></div></span>\r\n							<div class="clearfix"></div>\r\n						</div>\r\n					</div>\r\n					\r\n					<div class="col-md-3">\r\n						<div class="payment-system">\r\n							<img src="lib/skin/img/megaphone.png" title="Мегафон" alt="Мегафон" />\r\n							<span><div class="dt"><div class="dr"><div class="dc">Мегафон</div></div></div></span>\r\n							<div class="clearfix"></div>\r\n						</div>\r\n					</div>\r\n					\r\n					<div class="col-md-3">\r\n						<div class="payment-system">\r\n							<img src="lib/skin/img/telemoney.png" title="TeleMoney" alt="TeleMoney" />\r\n							<span><div class="dt"><div class="dr"><div class="dc">TeleMoney</div></div></div></span>\r\n							<div class="clearfix"></div>\r\n						</div>\r\n					</div>\r\n					\r\n					<div class="col-md-3">\r\n						<div class="payment-system">\r\n							<img src="lib/skin/img/terminals.png" title="Терминалы России" alt="Терминалы России" />\r\n							<span><div class="dt"><div class="dr"><div class="dc">Терминалы<br />России</div></div></div></span>\r\n							<div class="clearfix"></div>\r\n						</div>\r\n					</div>\r\n				</div>\r\n				<div>\r\n					<div class="col-md-3">\r\n						<div class="payment-system">\r\n							<img src="lib/skin/img/perfect-money.png" title="Perfect Money" alt="Perfect Money" />\r\n							<span><div class="dt"><div class="dr"><div class="dc">Perfect<br />Money</div></div></div></span>\r\n							<div class="clearfix"></div>\r\n						</div>\r\n					</div>\r\n					\r\n					<div class="col-md-3">\r\n						<div class="payment-system">\r\n							<img src="lib/skin/img/paxum.png" title="Paxum" alt="Paxum" />\r\n							<span><div class="dt"><div class="dr"><div class="dc">Paxum</div></div></div></span>\r\n							<div class="clearfix"></div>\r\n						</div>\r\n					</div>\r\n					\r\n					<div class="col-md-3">\r\n						<div class="payment-system">\r\n							<img src="lib/skin/img/mts.png" title="МТС" alt="МТС" />\r\n							<span><div class="dt"><div class="dr"><div class="dc">МТС</div></div></div></span>\r\n							<div class="clearfix"></div>\r\n						</div>\r\n					</div>\r\n					\r\n					<div class="col-md-3">\r\n						<div class="payment-system">\r\n							<img src="lib/skin/img/terminals.png" title="Терминалы Украины" alt="Терминалы Украины" />\r\n							<span><div class="dt"><div class="dr"><div class="dc">Терминалы<br />Украины</div></div></div></span>\r\n							<div class="clearfix"></div>\r\n						</div>\r\n					</div>\r\n				</div> \r\n				\r\n			</div>'),
	(56, 'en', 'Contact Us'),
	(56, 'ru', 'Обратная связь'),
	(57, 'en', 'Enter the Symbols'),
	(57, 'ru', 'Антиспам'),
	(58, 'en', 'Phone Number (optional)'),
	(58, 'ru', 'Номер телефона (опционально)'),
	(59, 'en', 'Email'),
	(59, 'ru', 'Email'),
	(60, 'en', 'First Name / Last Name'),
	(60, 'ru', 'Ваше имя'),
	(61, 'en', 'Message|Messages'),
	(61, 'ru', 'Сообщение|Сообщения|Сообщений|Сообщения'),
	(62, 'en', 'Send Message'),
	(62, 'ru', 'Отправить сообщение'),
	(63, 'en', 'Click to Refresh'),
	(63, 'ru', 'Нажмите для обновления'),
	(64, 'en', 'More Information'),
	(64, 'ru', 'Подробнее'),
	(65, 'en', '<p>Instead of many other services we provide you with ... </p>'),
	(65, 'ru', '<p class="text-justify">В отличие от других подобных сервисов помимо анкеты моряка вы можете прикреплять к письму любые документы, а общий размер вложений может составлять до 2 МБ. Наш сервис подскажет и поможет вам подготовить индивидуально оформленное письмо с логично составленным текстом и презентабельным внешним видом. Расширенная система фильтров по регионам мира и секторам судоходства с лёгкостью позволит вам выбрать лишь нужных получателей из нашей огромной базы крюинговых компаний. Мы работаем со множеством платёжных систем, поэтому вы без труда сможете оплатить наши услуги удобным для вас способом. Также вы имеете возможность следить за статусом выполнения услуги, а в конце получить подробный отчёт о её выполнении.</p>'),
	(66, 'en', '<p class="lead">If you would like </p>'),
	(66, 'ru', '<p class="lead">Если вы хотите быстро встать на учёт во множестве различных крюинговых компаний, разослать свою анкету работодателям нужного вам сектора судоходства и региона или даже точечно разослать свои документы в нужные агентства, то этот сервис именно для вас!</p>'),
	(67, 'en', 'Hello! <br /> <br /> Been successfully implemented an order to mailing your resume to our database companies. <br /> Your order number: {orderNumber} <br /> Order amount: {orderAmount} <br /> Further details you can view in your <a href="{accountLink}">personal account</a> <br /> <br /> Regards, Maritime'),
	(67, 'ru', 'Здравствуйте!<br /><br />Был успешно осуществлён заказ рассылки Вашего резюме по нашей базе компаний.<br />Номер вашего заказа: {orderNumber}<br />Сумма заказа: {orderAmount}<br />Более подробные детали вы можете просмотреть в вашем <a href="{accountLink}">личном кабинете</a><br /><br />С уважением, Maritime'),
	(68, 'en', 'Greece'),
	(68, 'ru', 'Греция'),
	(69, 'en', 'Bulgaria'),
	(69, 'ru', 'Болгария'),
	(70, 'en', 'Switzerland'),
	(70, 'ru', 'Швейцария'),
	(71, 'en', 'Cyprus'),
	(71, 'ru', 'Кипр'),
	(72, 'en', 'Germany'),
	(72, 'ru', 'Германия'),
	(73, 'en', 'Estonia'),
	(73, 'ru', 'Эстония'),
	(74, 'en', 'Spain'),
	(74, 'ru', 'Испания'),
	(75, 'en', 'Croatia'),
	(75, 'ru', 'Хорватия'),
	(76, 'en', 'Indonesia'),
	(76, 'ru', 'Индонезия'),
	(77, 'en', 'India'),
	(77, 'ru', 'Индия'),
	(78, 'en', 'Lithuania'),
	(78, 'ru', 'Литва'),
	(79, 'en', 'Myanmar'),
	(79, 'ru', 'Мьянма'),
	(80, 'en', 'Netherlands'),
	(80, 'ru', 'Нидерланды'),
	(81, 'en', 'Philippines'),
	(81, 'ru', 'Филиппины'),
	(82, 'en', 'Poland'),
	(82, 'ru', 'Польша'),
	(83, 'en', 'Romania'),
	(83, 'ru', 'Румыния'),
	(84, 'en', 'Singapore'),
	(84, 'ru', 'Сингапур'),
	(85, 'en', 'United Kingdom'),
	(85, 'ru', 'Великобритания'),
	(86, 'en', 'USA'),
	(86, 'ru', 'США'),
	(88, 'en', 'Asia'),
	(88, 'ru', 'Азия'),
	(89, 'ru', '		<div class="row-fluid-5">\r\n			<div class="span5">\r\n				<p class="lead">Вас отделает всего 5 шагов до получения достойной работы!</p>\r\n			</div>\r\n		</div>\r\n		\r\n		<div class="row-fluid-5">\r\n            <div class="span1">\r\n				<div class=\'square-box\'>\r\n					\r\n					<div class=\'square-content\'>\r\n					<div class="dt">\r\n					<div class="dc">\r\n								\r\n						<div class="glyph">\r\n							<span class="glyph-item mega" aria-hidden="true" data-icon="&#xe034;" data-js-prompt="&#xe034;"></span>\r\n						</div>\r\n		\r\n					</div>\r\n					</div>\r\n					</div>\r\n					\r\n				</div>\r\n				\r\n				<p class="lead">Шаг 1</p>\r\n				<p>Загружаете анкету и документы</p>\r\n				\r\n			</div>\r\n			\r\n            <div class="span1">\r\n				<div class=\'square-box\'>\r\n					\r\n					<div class=\'square-content\'>\r\n					<div class="dt">\r\n					<div class="dc">\r\n								\r\n						<div class="glyph">\r\n							<span class="glyph-item mega" aria-hidden="true" data-icon="&#xe05f;" data-js-prompt="&#xe05f;"></span>\r\n						</div>\r\n		\r\n					</div>\r\n					</div>\r\n					</div>\r\n					\r\n				</div>\r\n				\r\n				<p class="lead">Шаг 2</p>\r\n				<p>Оформляете вид письма и сопроводительный текст</p>\r\n				\r\n			</div>\r\n			\r\n            <div class="span1">\r\n				<div class=\'square-box\'>\r\n					\r\n					<div class=\'square-content\'>\r\n					<div class="dt">\r\n					<div class="dc">\r\n								\r\n						<div class="glyph">\r\n							<span class="glyph-item mega" aria-hidden="true" data-icon="&#xe003;" data-js-prompt="&#xe003;"></span>\r\n						</div>\r\n		\r\n					</div>\r\n					</div>\r\n					</div>\r\n					\r\n				</div>\r\n\r\n				<p class="lead">Шаг 3</p>\r\n				<p>Выбираете <nobr>компании-получателей</nobr></p>\r\n		\r\n			</div>\r\n			\r\n            <div class="span1">\r\n				<div class=\'square-box\'>\r\n					\r\n					<div class=\'square-content\'>\r\n					<div class="dt">\r\n					<div class="dc">\r\n								\r\n						<div class="glyph">\r\n							<span class="glyph-item mega" aria-hidden="true" data-icon="&#xe02a;" data-js-prompt="&#xe02a;"></span>\r\n						</div>\r\n		\r\n					</div>\r\n					</div>\r\n					</div>\r\n					\r\n				</div>\r\n				\r\n				<p class="lead">Шаг 4</p>\r\n				<p>Оплачиваете услугу удобным для вас способом</p>\r\n				\r\n			</div>\r\n			\r\n            <div class="span1">\r\n				<div class=\'square-box\'>\r\n					\r\n					<div class=\'square-content\'>\r\n					<div class="dt">\r\n					<div class="dc">\r\n								\r\n						<div class="glyph">\r\n							<span class="glyph-item mega" aria-hidden="true" data-icon="&#xe01f;" data-js-prompt="&#xe01f;"></span>\r\n						</div>\r\n		\r\n					</div>\r\n					</div>\r\n					</div>\r\n					\r\n				</div>\r\n				\r\n				<p class="lead">Шаг 5</p>\r\n				<p>Через некоторое время ваша анкета разослана!</p>\r\n				\r\n			</div>			\r\n		</div>'),
	(90, 'en', 'CV File Name'),
	(90, 'ru', 'Имя файла анкеты'),
	(91, 'en', 'CV'),
	(91, 'ru', 'Анкета|Анкеты|Анкет|Анкеты'),
	(92, 'en', 'Attachment|Attachments'),
	(92, 'ru', 'Вложение|Вложения|Вожений|Вложения'),
	(93, 'en', 'Attachment {number} Name'),
	(93, 'ru', 'Имя вложения {number}'),
	(94, 'en', 'Reset'),
	(94, 'ru', 'Начать сначала'),
	(95, 'ru', 'Подтвердить и перейти к оплате'),
	(96, 'en', 'Russian Federation'),
	(96, 'ru', 'Российская Федерация'),
	(97, 'en', 'Ukraine'),
	(97, 'ru', 'Украина'),
	(98, 'en', 'Middle East'),
	(98, 'ru', 'Ближний Восток'),
	(100, 'en', 'Order|Orders'),
	(100, 'ru', 'Заказ|Заказа|Заказов|Заказы'),
	(101, 'en', 'User|Users'),
	(101, 'ru', 'Пользователь|Пользователя|Пользователей|Пользователи'),
	(102, 'en', 'Parameter|Parameters'),
	(102, 'ru', 'Параметр|Параметра|Параметров|Параметры'),
	(103, 'en', 'Translation|Translations'),
	(103, 'ru', 'Перевод|Перевода|Переводов|Переводы'),
	(104, 'en', 'Category|Categories'),
	(104, 'ru', 'Категория|Категории|Категорий|Категории'),
	(105, 'en', 'Word|Words'),
	(105, 'ru', 'Слово|Слова|Слов|Слова'),
	(106, 'en', 'Phrase|Phrases'),
	(106, 'ru', 'Фраза|Фразы|Фраз|Фразы'),
	(107, 'en', 'Name|Names'),
	(107, 'ru', 'Имя|Имени|Имён|Имена'),
	(108, 'en', 'Value|Values'),
	(108, 'ru', 'Значение|Значения|Значений|Значения'),
	(109, 'en', 'Language|Languages'),
	(109, 'ru', 'Язык|Языка|Языков|Языки'),
	(110, 'en', 'English'),
	(110, 'ru', 'Английский'),
	(111, 'en', 'Russian'),
	(111, 'ru', 'Русский'),
	(112, 'en', 'Parameter category'),
	(112, 'ru', 'Категория параметра'),
	(113, 'en', 'Administrator'),
	(113, 'ru', 'Администратор'),
	(114, 'en', 'Administrator panel'),
	(114, 'ru', 'Панель администратора'),
	(115, 'en', 'E-mail'),
	(115, 'ru', 'Электронная почта'),
	(116, 'en', 'Interkassa'),
	(116, 'ru', 'Интеркасса'),
	(117, 'en', 'Price'),
	(117, 'ru', 'Цена'),
	(118, 'en', 'E-mail queue'),
	(118, 'ru', 'Очередь писем'),
	(119, 'en', 'Site'),
	(119, 'ru', 'Сайт'),
	(120, 'en', 'User'),
	(120, 'ru', 'Пользователь'),
	(121, 'en', 'E-mail'),
	(121, 'ru', 'Электронная почта'),
	(122, 'en', 'Login'),
	(122, 'ru', 'Логин'),
	(123, 'en', 'Password'),
	(123, 'ru', 'Пароль'),
	(124, 'en', 'Rows per page'),
	(124, 'ru', 'Записей на странице'),
	(125, 'en', 'Charset'),
	(125, 'ru', 'Кодировка'),
	(126, 'en', 'Minimum'),
	(126, 'ru', 'Минимум'),
	(127, 'en', 'Passenger'),
	(127, 'ru', 'Пассажирский'),
	(128, 'en', 'Offshore'),
	(128, 'ru', 'Оффшор'),
	(129, 'en', 'Tanker'),
	(129, 'ru', 'Танкер'),
	(130, 'en', 'Dry Cargo'),
	(130, 'ru', 'Сухогруз'),
	(131, 'en', 'Sendings per hour (common)'),
	(131, 'ru', 'Отправок в час (обычных)'),
	(132, 'en', 'Sendings Per Hour (important)'),
	(132, 'ru', 'Отправок в час (важных)'),
	(133, 'en', 'Contact Email Subject'),
	(133, 'ru', 'Тема контактного письма'),
	(134, 'en', 'Account Timelife'),
	(134, 'ru', 'Время жизни пользователя'),
	(135, 'en', 'Host'),
	(135, 'ru', 'Адрес сервера'),
	(136, 'en', 'Port'),
	(136, 'ru', 'Порт'),
	(137, 'en', 'User'),
	(137, 'ru', 'Пользователь'),
	(138, 'en', 'Shop ID'),
	(138, 'ru', 'ID Магазина'),
	(139, 'en', 'Sign Key'),
	(139, 'ru', 'Ключ (электронная подпись)'),
	(140, 'en', 'SMTP Secure'),
	(140, 'ru', 'Тип безопасности SMTP'),
	(141, 'en', 'Use SMTP'),
	(141, 'ru', 'Использовать SMTP'),
	(142, 'en', 'SMTP Authentication type'),
	(142, 'ru', 'Тип авторизации SMTP'),
	(143, 'en', 'Your order is completed'),
	(143, 'ru', 'Ваш заказ выполнен'),
	(144, 'en', 'Order details'),
	(144, 'ru', 'Детали заказа'),
	(145, 'en', 'Template name'),
	(145, 'ru', 'Имя шаблона'),
	(146, 'en', 'Order completed'),
	(146, 'ru', 'Заказ выполнен'),
	(147, 'en', 'New order'),
	(147, 'ru', 'Новый заказ'),
	(148, 'en', 'To the companies list'),
	(148, 'ru', 'К списку компаний'),
	(149, 'en', 'Companies in base'),
	(149, 'ru', 'Всего компаний в базе'),
	(150, 'en', 'Offices'),
	(150, 'ru', 'Офисы'),
	(151, 'en', 'Countries'),
	(151, 'ru', 'Страны'),
	(152, 'en', 'TODO write license text here from admin panel (licensePage/Lisense)'),
	(152, 'ru', 'TODO написать лицензионный текст в адинке (licensePage/Lisense)'),
	(153, 'en', 'Agree'),
	(153, 'ru', 'Согласен'),
	(154, 'en', 'Design and text of the letter'),
	(154, 'ru', 'Оформление и текст письма'),
	(155, 'en', 'Confirm data'),
	(155, 'ru', 'Подтвердите данные'),
	(156, 'en', 'Payment services'),
	(156, 'ru', 'Оплата услуги'),
	(157, 'en', 'Order details'),
	(157, 'ru', 'Детали заказа'),
	(158, 'en', 'Account details'),
	(158, 'ru', 'Детали счёта'),
	(159, 'en', 'Notification companies'),
	(159, 'ru', 'Ход оповещения компаний'),
	(160, 'en', 'Number of order'),
	(160, 'ru', 'Номер заказа'),
	(161, 'en', 'Selected companies'),
	(161, 'ru', 'Выбранно компаний'),
	(162, 'en', 'Cost'),
	(162, 'ru', 'Стоимость'),
	(163, 'en', 'Paid'),
	(163, 'ru', 'Оплачено'),
	(164, 'en', 'Go to payment'),
	(164, 'ru', 'Перейти к оплате'),
	(165, 'en', 'View status'),
	(165, 'ru', 'Просмотреть статус'),
	(166, 'en', 'Total notification companies'),
	(166, 'ru', 'Итого оповещенно компаний'),
	(167, 'en', 'COMPLETED'),
	(167, 'ru', 'ЗАВЕРШЕН'),
	(168, 'en', 'NOT COMPLETED'),
	(168, 'ru', 'НЕ ЗАВЕРШЕН'),
	(169, 'en', 'Unnotificated companies left'),
	(169, 'ru', 'Осталось неоповещённых компаний'),
	(170, 'en', 'Order status'),
	(170, 'ru', 'Статус заказа'),
	(171, 'en', 'Payment'),
	(171, 'ru', 'Оплата'),
	(172, 'en', 'Payment status'),
	(172, 'ru', 'Статус оплаты'),
	(173, 'en', 'Report'),
	(173, 'ru', 'Отчёт'),
	(174, 'en', 'on the implementation of services'),
	(174, 'ru', 'о выполнении услуги'),
	(175, 'en', 'List of Companies'),
	(175, 'ru', 'Список компаний'),
	(176, 'en', 'Contact'),
	(176, 'ru', 'Контакты'),
	(177, 'en', 'All Rights Reserved'),
	(177, 'ru', 'Все права защищены'),
	(178, 'en', 'Personal account'),
	(178, 'ru', 'Личный кабинет'),
	(179, 'en', 'Ошибка оплаты'),
	(179, 'ru', 'The order pay is fail'),
	(180, 'en', 'Sorry! But has been some error when you tried to pay. Please, try again.'),
	(180, 'ru', 'Извините! Но произошла ошибка, когда вы пытались заплатить. Пожалуйста, попробуйте еще раз.'),
	(181, 'en', 'Paying of the order is pending'),
	(181, 'ru', 'Оплата заказа находится на рассмотрении'),
	(182, 'en', 'Thank you! Your payment is pending now.'),
	(182, 'ru', 'Спасибо! Ваш платеж сейчас находится на рассмотрении.'),
	(183, 'en', 'The order is successfully paid up'),
	(183, 'ru', 'Заказ успешно оплачен'),
	(184, 'en', 'Thank you!<br />Your order is paid up, and will be started at nearest time.'),
	(184, 'ru', 'Спасибо! <br /> Ваш заказ оплачен, и будет запущен в ближайшее время.');
/*!40000 ALTER TABLE `Message` ENABLE KEYS */;


-- Dumping structure for table maritime-cv.officeregions
DROP TABLE IF EXISTS `officeregions`;
CREATE TABLE IF NOT EXISTS `officeregions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `office` int(11) NOT NULL,
  `region` varchar(2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_officeregions_office_idx` (`office`),
  CONSTRAINT `fk_officeregions_office` FOREIGN KEY (`office`) REFERENCES `offices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- Dumping data for table maritime-cv.officeregions: ~7 rows (approximately)
DELETE FROM `officeregions`;
/*!40000 ALTER TABLE `officeregions` DISABLE KEYS */;
INSERT INTO `officeregions` (`id`, `office`, `region`) VALUES
	(31, 1, 'ee'),
	(32, 2, 'eu'),
	(33, 2, 'ee'),
	(34, 3, 'eu'),
	(35, 3, 'ee'),
	(36, 4, 'eu'),
	(37, 5, 'ee');
/*!40000 ALTER TABLE `officeregions` ENABLE KEYS */;


-- Dumping structure for table maritime-cv.offices
DROP TABLE IF EXISTS `offices`;
CREATE TABLE IF NOT EXISTS `offices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company` int(11) NOT NULL,
  `name` varchar(255) DEFAULT '',
  `email` varchar(255) NOT NULL,
  `country` varchar(2) DEFAULT '',
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FKCompanyGeographicAreasCompanyIndex` (`company`),
  CONSTRAINT `FKCompanyGeographicAreasCompany` FOREIGN KEY (`company`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Dumping data for table maritime-cv.offices: ~9 rows (approximately)
DELETE FROM `offices`;
/*!40000 ALTER TABLE `offices` DISABLE KEYS */;
INSERT INTO `offices` (`id`, `company`, `name`, `email`, `country`, `inactive`) VALUES
	(1, 1, '', 'email1@m.ru', 'en', 0),
	(2, 2, '', 'email2@m.ru', 'en', 0),
	(3, 3, '', 'email6@m.ru', '', 0),
	(4, 4, '', 'email5@m.ru', '', 0),
	(5, 5, '', 'email7@m.ru', '', 0),
	(6, 6, '', 'email3@m.ru', 'en', 0),
	(7, 7, '', 'email8@m.ru', NULL, 0),
	(8, 8, '', 'email4@m.ru', NULL, 0),
	(9, 9, '', 'j@d.ii', NULL, 0);
/*!40000 ALTER TABLE `offices` ENABLE KEYS */;


-- Dumping structure for table maritime-cv.officeworkareas
DROP TABLE IF EXISTS `officeworkareas`;
CREATE TABLE IF NOT EXISTS `officeworkareas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `office` int(11) NOT NULL,
  `workArea` varchar(2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_officeworkareas_office_idx` (`office`),
  CONSTRAINT `fk_officeworkareas_office` FOREIGN KEY (`office`) REFERENCES `offices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=238 DEFAULT CHARSET=utf8;

-- Dumping data for table maritime-cv.officeworkareas: ~12 rows (approximately)
DELETE FROM `officeworkareas`;
/*!40000 ALTER TABLE `officeworkareas` DISABLE KEYS */;
INSERT INTO `officeworkareas` (`id`, `office`, `workArea`) VALUES
	(226, 1, 'dc'),
	(227, 2, 'os'),
	(228, 2, 'ps'),
	(229, 3, 'dc'),
	(230, 4, 'tk'),
	(231, 4, 'dc'),
	(232, 5, 'ps'),
	(233, 6, 'os'),
	(234, 7, 'os'),
	(235, 8, 'ps'),
	(236, 8, 'os'),
	(237, 9, 'dc');
/*!40000 ALTER TABLE `officeworkareas` ENABLE KEYS */;


-- Dumping structure for table maritime-cv.orders
DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `number` varchar(100) NOT NULL,
  `creationDate` date NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `currency` char(3) DEFAULT 'USD',
  `paid` int(1) NOT NULL DEFAULT '0',
  `executed` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_orders_user_idx` (`user`),
  CONSTRAINT `fk_orders_user` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

-- Dumping data for table maritime-cv.orders: ~19 rows (approximately)
DELETE FROM `orders`;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` (`id`, `user`, `number`, `creationDate`, `amount`, `currency`, `paid`, `executed`) VALUES
	(31, 29, '1821408293452', '2014-08-17', 4.00, 'UAH', 0, 0),
	(32, 30, '7181408293487', '2014-08-17', 4.00, 'UAH', 1, 0),
	(33, 31, '6511408295310', '2014-08-17', 4.00, 'UAH', 1, 0),
	(34, 32, '3171408295963', '2014-08-17', 4.00, 'UAH', 1, 0),
	(35, 33, '1661409660542', '2014-09-02', 4.00, 'UAH', 0, 0),
	(36, 34, '5851409660882', '2014-09-02', 4.00, 'UAH', 0, 1),
	(37, 35, '5671409755286', '2014-09-03', 4.00, 'UAH', 0, 0),
	(38, 36, '4261410258809', '2014-09-09', 4.00, 'UAH', 0, 0),
	(39, 37, '4801410524816', '2014-09-12', 4.00, 'UAH', 0, 0),
	(40, 38, '4511410524934', '2014-09-12', 4.00, 'UAH', 0, 0),
	(41, 39, '2891410525035', '2014-09-12', 4.00, 'UAH', 0, 0),
	(42, 40, '4761410525111', '2014-09-12', 4.00, 'UAH', 0, 0),
	(43, 41, '5591411384450', '2014-09-22', 8.20, 'UAH', 0, 0),
	(44, 42, '7021411385020', '2014-09-22', 8.20, 'USD', 0, 0),
	(45, 43, '2301411386745', '2014-09-22', 8.20, 'USD', 0, 0),
	(46, 44, '7491411386924', '2014-09-22', 10.00, 'USD', 0, 0),
	(47, 45, '1951411746891', '2014-09-26', 10.00, 'USD', 0, 0),
	(48, 46, '8221411747598', '2014-09-26', 10.00, 'USD', 0, 0),
	(49, 47, '2431411747673', '2014-09-26', 10.00, 'USD', 0, 0);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;


-- Dumping structure for table maritime-cv.orderStatus
DROP TABLE IF EXISTS `orderStatus`;
CREATE TABLE IF NOT EXISTS `orderStatus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order` int(11) NOT NULL,
  `company` int(11) NOT NULL,
  `datetime` datetime DEFAULT NULL,
  `success` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_orderStatus_order_idx` (`order`),
  KEY `fk_orderStatus_company_idx` (`company`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- Dumping data for table maritime-cv.orderStatus: ~12 rows (approximately)
DELETE FROM `orderStatus`;
/*!40000 ALTER TABLE `orderStatus` DISABLE KEYS */;
INSERT INTO `orderStatus` (`id`, `order`, `company`, `datetime`, `success`) VALUES
	(6, 1, 1, '2014-07-29 00:00:00', 1),
	(7, 1, 2, '2014-07-29 00:00:00', 1),
	(8, 2, 8, '2014-07-29 00:00:00', 1),
	(9, 1, 1, '2014-07-30 00:00:00', 1),
	(10, 1, 2, '2014-07-31 00:00:00', 1),
	(11, 34, 1, '2014-08-17 18:34:24', 1),
	(12, 34, 2, '2014-08-17 18:56:12', 1),
	(13, 34, 5, '2014-08-17 19:34:00', 1),
	(14, 34, 7, '2014-08-17 19:34:00', 1),
	(15, 34, 6, '2014-08-17 19:34:00', 1),
	(16, 34, 2, '2014-08-17 19:34:00', 1),
	(17, 34, 5, '2014-08-17 19:34:00', 1);
/*!40000 ALTER TABLE `orderStatus` ENABLE KEYS */;


-- Dumping structure for table maritime-cv.parameters
DROP TABLE IF EXISTS `parameters`;
CREATE TABLE IF NOT EXISTS `parameters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(50) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- Dumping data for table maritime-cv.parameters: ~23 rows (approximately)
DELETE FROM `parameters`;
/*!40000 ALTER TABLE `parameters` DISABLE KEYS */;
INSERT INTO `parameters` (`id`, `category`, `name`, `value`) VALUES
	(1, 'admin', 'email', 'ex@mp.le'),
	(2, 'email', 'smtp', '1'),
	(3, 'email', 'smtpAuth', '1'),
	(4, 'email', 'smtpSecure', 'ssl'),
	(5, 'email', 'host', 'smtp.gmail.com'),
	(6, 'email', 'port', '465'),
	(7, 'email', 'user', 'laspiairlines@gmail.com'),
	(8, 'email', 'password', 'S1332jJK'),
	(9, 'email', 'charset', 'UTF-8'),
	(10, 'interkassa', 'shopId', '538ec854bf4efc6719507933'),
	(11, 'interkassa', 'signKey', 'IcqdLHLZtg8rR8Gf'),
	(12, 'queue', 'sendingsPerHour', '5'),
	(13, 'queue', 'importantSendingsPerHour', '2'),
	(14, 'admin', 'login', 'admin2'),
	(15, 'admin', 'password', '123'),
	(16, 'user', 'accountTimelife', '30'),
	(18, 'adminPanel', 'rowsPerPage', '50'),
	(19, 'site', 'contactEmailSubject', 'Contact Email'),
	(20, 'price', 'dc', '5.50'),
	(21, 'price', 'os', '2.7'),
	(22, 'price', 'ps', '3.15'),
	(23, 'price', 'tk', '4'),
	(24, 'price', 'minimum', '10');
/*!40000 ALTER TABLE `parameters` ENABLE KEYS */;


-- Dumping structure for table maritime-cv.payments
DROP TABLE IF EXISTS `payments`;
CREATE TABLE IF NOT EXISTS `payments` (
  `id` int(11) NOT NULL,
  `order` int(10) NOT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `ikTransaction` varchar(100) DEFAULT NULL,
  `method` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_payment_order_idx` (`order`),
  CONSTRAINT `fk_payment_order` FOREIGN KEY (`order`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table maritime-cv.payments: ~0 rows (approximately)
DELETE FROM `payments`;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;


-- Dumping structure for table maritime-cv.SourceMessage
DROP TABLE IF EXISTS `SourceMessage`;
CREATE TABLE IF NOT EXISTS `SourceMessage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(32) DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=185 DEFAULT CHARSET=utf8;

-- Dumping data for table maritime-cv.SourceMessage: ~170 rows (approximately)
DELETE FROM `SourceMessage`;
/*!40000 ALTER TABLE `SourceMessage` DISABLE KEYS */;
INSERT INTO `SourceMessage` (`id`, `category`, `message`) VALUES
	(0, 'phrase', 'CV and Attachments'),
	(1, 'word', 'All'),
	(2, 'workArea', 'dc'),
	(3, 'workArea', 'os'),
	(4, 'workArea', 'tk'),
	(5, 'workArea', 'ps'),
	(7, 'region', 'eu'),
	(8, 'country', 'ru'),
	(9, 'country', 'en'),
	(10, 'country', 'ua'),
	(11, 'word', 'Sector'),
	(12, 'word', 'Region'),
	(13, 'word', 'Country'),
	(14, 'word', 'Company'),
	(15, 'phrase', 'Previous Step'),
	(16, 'phrase', 'Next Step'),
	(17, 'phrase', 'Selecting crewing agencies'),
	(18, 'pagePart', 'This is companies select page. Please, select companies that you are interested. You may use filters in left part of page or set checkboxes manually.'),
	(19, 'phrase', 'Crewing Agencies'),
	(20, 'word', 'Step'),
	(21, 'phrase', 'CV and Attachments'),
	(22, 'phrase', 'Design and Content'),
	(23, 'phrase', 'Crewing Companies'),
	(24, 'phrase', 'Order Payment'),
	(25, 'phrase', 'Email Campaign'),
	(26, 'word', 'Template'),
	(27, 'phrase', 'Color Scheme'),
	(28, 'phrase', 'Email Subject'),
	(29, 'phrase', 'Email Content'),
	(30, 'phrase', 'How It Works'),
	(31, 'country', 'no'),
	(32, 'country', 'ge'),
	(33, 'country', 'lv'),
	(34, 'region', 'bs'),
	(35, 'colorScheme', 'mz'),
	(36, 'emailTemplateName', 'cv-default'),
	(37, 'emailTemplateContent', 'cv-default'),
	(38, 'emailTemplateContent', 'cv-exp'),
	(39, 'emailTemplateName', 'cv-exp'),
	(40, 'colorScheme', 'default'),
	(41, 'colorScheme', 'eco'),
	(42, 'colorScheme', 'exp'),
	(43, 'phrase', 'Click to upload'),
	(44, 'phrase', 'Your CV'),
	(45, 'phrase', 'File 1'),
	(46, 'phrase', 'File 2'),
	(47, 'phrase', 'Your Email'),
	(48, 'phrase', 'Continue'),
	(49, 'phrase', 'Allowed file types'),
	(50, 'phrase', 'Attach your CV'),
	(51, 'filesPage', 'File Upload Explanation'),
	(52, 'phrase', 'Top Section Header'),
	(53, 'phrase', 'Middle Section Header'),
	(54, 'homePage', 'Middle Section Left'),
	(55, 'homePage', 'Middle Section Right'),
	(56, 'phrase', 'Contact Us'),
	(57, 'phrase', 'Enter the Symbols'),
	(58, 'phrase', 'Phone Number'),
	(59, 'word', 'Email'),
	(60, 'phrase', 'Name and Surname'),
	(61, 'word', 'Message'),
	(62, 'phrase', 'Send Message'),
	(63, 'phrase', 'Click to Refresh'),
	(64, 'phrase', 'More Info'),
	(65, 'homePage', 'Middle Section'),
	(66, 'homePage', 'Top Section'),
	(67, 'emailTemplateContent', 'new-order-notify'),
	(68, 'country', 'gr'),
	(69, 'country', 'bg'),
	(70, 'country', 'ch'),
	(71, 'country', 'cy'),
	(72, 'country', 'de'),
	(73, 'country', 'ee'),
	(74, 'country', 'es'),
	(75, 'country', 'hr'),
	(76, 'country', 'id'),
	(77, 'country', 'in'),
	(78, 'country', 'lt'),
	(79, 'country', 'mm'),
	(80, 'country', 'nl'),
	(81, 'country', 'ph'),
	(82, 'country', 'pl'),
	(83, 'country', 'ro'),
	(84, 'country', 'sg'),
	(85, 'country', 'uk'),
	(86, 'country', 'us'),
	(88, 'region', 'as'),
	(89, 'homePage', 'Bottom Section'),
	(90, 'phrase', 'CV File Name'),
	(91, 'word', 'CV'),
	(92, 'word', 'Attachment'),
	(93, 'phrase', 'Attachment {number} Name'),
	(94, 'phrase', 'Reset'),
	(95, 'phrase', 'Submit'),
	(96, 'region', 'ru'),
	(97, 'region', 'ua'),
	(98, 'region', 'me'),
	(99, 'country', 'ge'),
	(100, 'word', 'Order'),
	(101, 'word', 'User'),
	(102, 'word', 'Parameter'),
	(103, 'word', 'Translation'),
	(104, 'word', 'Category'),
	(105, 'word', 'Word'),
	(106, 'Word', 'Phrase'),
	(107, 'word', 'Name'),
	(108, 'word', 'Value'),
	(109, 'word', 'Language'),
	(110, 'language', 'en'),
	(111, 'language', 'ru'),
	(112, 'word', 'Parametercategory'),
	(113, 'Parametercategory', 'admin'),
	(114, 'Parametercategory', 'adminPanel'),
	(115, 'Parametercategory', 'email'),
	(116, 'Parametercategory', 'interkassa'),
	(117, 'Parametercategory', 'price'),
	(118, 'Parametercategory', 'queue'),
	(119, 'Parametercategory', 'site'),
	(120, 'parameterCategory', 'user'),
	(121, 'Parameter', 'email'),
	(122, 'Parameter', 'login'),
	(123, 'Parameter', 'password'),
	(124, 'Parameter', 'rowsPerPage'),
	(125, 'Parameter', 'charset'),
	(126, 'Parameter', 'minimum'),
	(127, 'Parameter', 'ps'),
	(128, 'Parameter', 'os'),
	(129, 'Parameter', 'tk'),
	(130, 'Parameter', 'dc'),
	(131, 'Parameter', 'sendingsPerHour'),
	(132, 'Parameter', 'importantSendingsPerHour'),
	(133, 'Parameter', 'contactEmailSubject'),
	(134, 'Parameter', 'accountTimelife'),
	(135, 'Parameter', 'host'),
	(136, 'Parameter', 'port'),
	(137, 'Parameter', 'user'),
	(138, 'Parameter', 'shopId'),
	(139, 'Parameter', 'signKey'),
	(140, 'Parameter', 'smtpSecure'),
	(141, 'Parameter', 'smtp'),
	(142, 'Parameter', 'smtpAuth'),
	(143, 'emailTemplateSubject', 'completed-order-notify'),
	(144, 'emailTemplateSubject', 'new-order-notify'),
	(145, 'emailTemplateType', 'emailTemplateName'),
	(146, 'emailTemplateName', 'completed-order-notify'),
	(147, 'emailTemplateName', 'new-order-notify'),
	(148, 'phrase', 'to company list'),
	(149, 'phrase', 'companies in base'),
	(150, 'word', 'offices'),
	(151, 'word', 'countries'),
	(152, 'licensePage', 'Lisense'),
	(153, 'word', 'agree'),
	(154, 'phrase', 'Design and text of the letter'),
	(155, 'phrase', 'confirm data'),
	(156, 'phrase', 'payment services'),
	(157, 'phrase', 'order details'),
	(158, 'phrase', 'account details'),
	(159, 'phrase', 'notification companies'),
	(160, 'phrase', 'number of order'),
	(161, 'phrase', 'selected companies'),
	(162, 'word', 'cost'),
	(163, 'word', 'paid'),
	(164, 'phrase', 'go to payment'),
	(165, 'phrase', 'view status'),
	(166, 'phrase', 'total notification companies'),
	(167, 'word', 'completed'),
	(168, 'phrase', 'not completed'),
	(169, 'phrase', 'unnotificated companies left'),
	(170, 'phrase', 'order status'),
	(171, 'word', 'payment'),
	(172, 'phrase', 'payment status'),
	(173, 'word', 'report'),
	(174, 'phrase', 'on the implementation of services'),
	(175, 'phrase', 'list of companies'),
	(176, 'word', 'contact'),
	(177, 'phrase', 'all rights reserved'),
	(178, 'phrase', 'personal account'),
	(179, 'phrase', 'the order pay is fail'),
	(180, 'phrase', 'pay error'),
	(181, 'phrase', 'paying is pending'),
	(182, 'phrase', 'thank payment is pending'),
	(183, 'phrase', 'successfully paid up'),
	(184, 'phrase', 'thank you paid up');
/*!40000 ALTER TABLE `SourceMessage` ENABLE KEYS */;


-- Dumping structure for table maritime-cv.userEmailContents
DROP TABLE IF EXISTS `userEmailContents`;
CREATE TABLE IF NOT EXISTS `userEmailContents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `colorScheme` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKUserEmailContentsUserIndex` (`user`),
  CONSTRAINT `fk_useremailcontents_user` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

-- Dumping data for table maritime-cv.userEmailContents: ~19 rows (approximately)
DELETE FROM `userEmailContents`;
/*!40000 ALTER TABLE `userEmailContents` DISABLE KEYS */;
INSERT INTO `userEmailContents` (`id`, `user`, `subject`, `body`, `colorScheme`) VALUES
	(32, 29, 'Subject', '\r\n				Добрый день!\r\nЯ моряк, прослужил на флоте более 20 лет.\r\nХочу работать именно в вашей компании, т.к. вы можете предложить мне лучшие условия работы и оплату труда.\r\n\r\nС уважением,\r\nморяк.\r\n			', 'mz'),
	(33, 30, 'try', '\r\n				Добрый день!\r\nЯ моряк, прослужил на флоте более 20 лет.\r\nХочу работать именно в вашей компании, т.к. вы можете предложить мне лучшие условия работы и оплату труда.\r\n\r\nС уважением,\r\nморяк.\r\n			', 'mz'),
	(34, 31, 'Subject', '\r\n				Добрый день!\r\nЯ моряк, прослужил на флот', 'mz'),
	(35, 32, 'Subject', '\r\n				Добрый день!\r\nЯ моряк, прослужил на флоте более 20 лет.\r\nХочу работать именно в вашей компании, т.к. вы можете предложить мне лучшие условия работы и оплату труда.\r\n\r\nС уважением,\r\nморяк.\r\n			', 'mz'),
	(36, 33, 'Sedan', '<p>\r\n				Добрый день!\r\nЯ хочу отправить вам своё резюме!\r\n\r\nС уважением,\r\nморякssdddd</p>', 'exp'),
	(37, 34, 'Sedanert', '\r\n				Добрый день!\r\nЯ моряк, прослужил на флоте более 20 лет.\r\nХочу работать именно в вашей компании, т.к. вы можете предложить мне лучшие условия работы и оплату труда.\r\n\r\nС уважением,\r\nморяк.\r\n			', 'mz'),
	(38, 35, 'asasasa', '\r\n				Добрый день!\r\nЯ моряк, прослужил на флоте более 20 лет.\r\nХочу работать именно в вашей компании, т.к. вы можете предложить мне лучшие условия работы и оплату труда.\r\n\r\nС уважением,\r\nморяк.\r\n			', 'mz'),
	(39, 36, 'Sedanert', '\r\n				Добрый день!\r\nЯ моряк, прослужил на флоте более 20 лет.\r\nХочу работать именно в вашей компании, т.к. вы можете предложить мне лучшие условия работы и оплату труда.\r\n\r\nС уважением,\r\nморяк.\r\n			', ''),
	(40, 37, 'try', '\r\n				Добрый день!\r\nЯ моряк, прослужил на флоте более 20 лет.\r\nХочу работать именно в вашей компании, т.к. вы можете предложить мне лучшие условия работы и оплату труда.\r\n\r\nС уважением,\r\nморяк.\r\n			', ''),
	(41, 38, 'Sedanert', '\r\n				Добрый день!\r\nЯ моряк, прослужил на флоте более 20 лет.\r\nХочу работать именно в вашей компании, т.к. вы можете предложить мне лучшие условия работы и оплату труда.\r\n\r\nС уважением,\r\nморяк.\r\n			', 'mz'),
	(42, 39, 'Sedan', '\r\n				Добрый день!\r\nЯ моряк, прослужил на флоте более 20 лет.\r\nХочу работать именно в вашей компании, т.к. вы можете предложить мне лучшие условия работы и оплату труда.\r\n\r\nС уважением,\r\nморяк.\r\n			', 'mz'),
	(43, 40, 'Sedan', '\r\n				Добрый день!\r\nЯ моряк, прослужил на флоте более 20 лет.\r\nХочу работать именно в вашей компании, т.к. вы можете предложить мне лучшие условия работы и оплату труда.\r\n\r\nС уважением,\r\nморяк.\r\n			', 'mz'),
	(44, 41, 'Subject', '\r\n				Добрый день!\r\nЯ моряк, прослужил на флоте более 20 лет.\r\nХочу работать именно в вашей компании, т.к. вы можете предложить мне лучшие условия работы и оплату труда.\r\n\r\nС уважением,\r\nморяк.\r\n			', 'mz'),
	(45, 42, 'try', '\r\n				Добрый день!\r\nЯ моряк, прослужил на флоте более 20 лет.\r\nХочу работать именно в вашей компании, т.к. вы можете предложить мне лучшие условия работы и оплату труда.\r\n\r\nС уважением,\r\nморяк.\r\n			', 'mz'),
	(46, 43, 'try', '<p>\r\n				Добрый день!\r\nЯ моряк, прослужил на флоте более 20 лет.\r\nХочу работать именно в вашей компании, т.к. вы можете предложить мне лучшие условия работы и оплату труда.\r\n\r\nС уважением,\r\nморяк.\r\n			</p>', 'mz'),
	(47, 44, 'Sedanert', '\r\n				Добрый день!\r\nЯ моряк, прослужил на флоте более 20 лет.\r\nХочу работать именно в вашей компании, т.к. вы можете предложить мне лучшие условия работы и оплату труда.\r\n\r\nС уважением,\r\nморяк.\r\n			', 'exp'),
	(48, 45, 'sdf', '<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;d</p>', ''),
	(49, 46, 'fgh', '<p>dfhf</p>', ''),
	(50, 47, 'fgh', '<p><strong>Риск</strong>&nbsp;&mdash; сочетание&nbsp;<a title="Вероятность" href="https://ru.wikipedia.org/wiki/%D0%92%D0%B5%D1%80%D0%BE%D1%8F%D1%82%D0%BD%D0%BE%D1%81%D1%82%D1%8C">вероятности</a>&nbsp;и последствий наступления неблагоприятных событий. Знание вероятности неблагоприятного события позволяет определить вероятность благоприятных событий по формуле&nbsp;<img class="mwe-math-fallback-png-inline tex" src="https://upload.wikimedia.org/math/d/e/8/de834eac4081059d3d8cf7d1aa6d8f39.png" alt="P_{+}=1-P_{-}" />. Также риском часто называют непосредственно предполагаемое событие, способное принести кому-либо&nbsp;<a title="Ущерб" href="https://ru.wikipedia.org/wiki/%D0%A3%D1%89%D0%B5%D1%80%D0%B1">ущерб</a>&nbsp;или&nbsp;<a title="Убыток" href="https://ru.wikipedia.org/wiki/%D0%A3%D0%B1%D1%8B%D1%82%D0%BE%D0%BA">убыток</a>r</p>', '');
/*!40000 ALTER TABLE `userEmailContents` ENABLE KEYS */;


-- Dumping structure for table maritime-cv.userFiles
DROP TABLE IF EXISTS `userFiles`;
CREATE TABLE IF NOT EXISTS `userFiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `category` varchar(100) NOT NULL,
  `pseudonym` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKUserFilesUserIndex` (`user`),
  CONSTRAINT `fk_userfiles_user` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

-- Dumping data for table maritime-cv.userFiles: ~19 rows (approximately)
DELETE FROM `userFiles`;
/*!40000 ALTER TABLE `userFiles` DISABLE KEYS */;
INSERT INTO `userFiles` (`id`, `user`, `name`, `category`, `pseudonym`) VALUES
	(33, 29, 'DemnArctis_250x250.jpg', 'CVFile', 'Резюме'),
	(34, 30, 'DemnArctis_250x250.jpg', 'CVFile', 'Резюме'),
	(35, 31, 'DemnArctis_250x250.jpg', 'CVFile', 'Резюме'),
	(36, 32, 'DemnArctis_250x250.jpg', 'CVFile', 'Резюме'),
	(37, 33, 'DemnArctis_250x250.jpg', 'CVFile', 'Резюме'),
	(38, 34, 'DemnArctis_250x250.jpg', 'CVFile', 'Резюме'),
	(39, 35, 'DemnArctis_250x250.jpg', 'CVFile', 'Резюме'),
	(40, 36, 'IOACOrderBUG.jpg', 'CVFile', 'Резюме'),
	(41, 37, 'IOACOrderBUG.jpg', 'CVFile', 'Резюме'),
	(42, 38, 'IOACOrderBUG.jpg', 'CVFile', 'Резюме'),
	(43, 39, 'IOACOrderBUG.jpg', 'CVFile', 'Резюме'),
	(44, 40, 'IOACOrderBUG.jpg', 'CVFile', 'Резюме'),
	(45, 41, '2.jpg', 'CVFile', 'Резюме'),
	(46, 42, '2.jpg', 'CVFile', 'Резюме'),
	(47, 43, '2.jpg', 'CVFile', 'Резюме'),
	(48, 44, '2.jpg', 'CVFile', 'Резюме'),
	(49, 45, '2.jpg', 'CVFile', 'Резюме'),
	(50, 46, '2.jpg', 'CVFile', 'Резюме'),
	(51, 47, '2.jpg', 'CVFile', 'Резюме');
/*!40000 ALTER TABLE `userFiles` ENABLE KEYS */;


-- Dumping structure for table maritime-cv.userOrderedCompanies
DROP TABLE IF EXISTS `userOrderedCompanies`;
CREATE TABLE IF NOT EXISTS `userOrderedCompanies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `companies` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKUserOrderedCompaniesUser_idx` (`user`),
  CONSTRAINT `fk_userorderedcompanies_user` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

-- Dumping data for table maritime-cv.userOrderedCompanies: ~19 rows (approximately)
DELETE FROM `userOrderedCompanies`;
/*!40000 ALTER TABLE `userOrderedCompanies` DISABLE KEYS */;
INSERT INTO `userOrderedCompanies` (`id`, `user`, `companies`) VALUES
	(32, 29, '1,2,6,5,7,4'),
	(33, 30, '1,2,6,5,7,3,4'),
	(34, 31, '1,2,6,7,3,4'),
	(35, 32, '1,2,6,5,7,3,4'),
	(36, 33, '1,2,6,5,7,3,4'),
	(37, 34, '1,2,7,3,4'),
	(38, 35, '1'),
	(39, 36, '7,3,4'),
	(40, 37, '1,5,7,3,8,4,9,10'),
	(41, 38, '1,2,6,8,4,9,10'),
	(42, 39, '1,2,6,5,7,3,8,4,9,10'),
	(43, 40, '1,2,6,5,7,3,8,4,10'),
	(44, 41, '6,9'),
	(45, 42, '6,9'),
	(46, 43, '6,9'),
	(47, 44, '6,9'),
	(48, 45, '3'),
	(49, 46, '1'),
	(50, 47, '1');
/*!40000 ALTER TABLE `userOrderedCompanies` ENABLE KEYS */;


-- Dumping structure for table maritime-cv.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identificator` varchar(40) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

-- Dumping data for table maritime-cv.users: ~19 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `identificator`, `email`) VALUES
	(29, '0742icifcdj1m5pe3r99d2nev2', 'sassweee@m.oaaaa'),
	(30, '36rh3pim97ee2o00fb8nm4upl7', 'sa@m.o'),
	(31, 'l5ubaaot6uqvb74fj0rtco76u3', 'sassweee@m.o'),
	(32, '3glalo0uc7nsm92iubep9in400', 'sassweee@m.owwq'),
	(33, 'on06ed3gb8kon2kv8f335f93b4', 'kul_13@mail.ru'),
	(34, 'irh5gjag5umao9hgeh93gq1k24', 'kul_13@mail.ru'),
	(35, '38022kk9tue4dinlkcllp2plm6', 'kul_13@mail.ru'),
	(36, 'av79t1asilq4ddn19au5lsh8p6', 'sassweee@m.o'),
	(37, 'apq3ekmv91vah3pk7bjr41rfs2', 'sassweee@m.o'),
	(38, 'b08bu1nf47t5rs1aqiu1v2vr33', 'sassweee@m.o'),
	(39, 'p0p5hgt31cuqqvstlp7uf13l96', 'sassweee@m.o'),
	(40, 'cgarrfccbhuapr9irg0an0ft24', 'sassweee@m.o'),
	(41, '85shoudpq90toeua25mfah2is4', 'sassweee@m.o'),
	(42, 'vj5g248nr92ujeved7tvtfifj3', 'sassweee@m.o'),
	(43, '07umnjmtbstkpt9u1nbvug0012', 'sassweee@m.o'),
	(44, 'bgg6f7kkkdj60m37ee11nld6b1', 'sa@m.o'),
	(45, 'g1h0os8pcv6f7tq1sv85nntv13', 'sassweee@m.o'),
	(46, 'esh2804s4p6dbmth026lpg31c3', 'd@d.l'),
	(47, 's0n83o1bpo53c42fetbc5486d3', 'd@d.l');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
